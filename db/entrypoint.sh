#!/bin/bash
set -e

parse_database_url() {
    python3 <<EOF
import os
from urllib.parse import urlparse, parse_qs

url = os.environ['DATABASE_URL']
parsed = urlparse(url)

print(f"export PGHOST={parsed.hostname}")
print(f"export PGPORT={parsed.port or 5432}")
print(f"export PGUSER={parsed.username}")
print(f"export PGPASSWORD={parsed.password}")
print(f"export PGDATABASE={parsed.path.lstrip('/')}")

query = parse_qs(parsed.query)
if (sslmode := query.get('sslmode')):
    print(f"export PGSSLMODE={sslmode[0]}")
EOF
}

source <(parse_database_url)

RAW_DUMP_FILE="/tmp/${PGDATABASE}_$(date --iso-8601=minutes).sql"
echo "Dumping postgres://$PGUSER@$PGHOST:$PGPORT/$PGDATABASE to $RAW_DUMP_FILE"

until pg_isready; do
    echo "Postgres isn't ready yet..."
    sleep 1
done

echo "Postgres ready!"

echo "Sleeping for 15s to ensure Postgres is ready"
sleep 15s

command time -v pg_dump --verbose --file "$RAW_DUMP_FILE"
echo "Dump complete."

GZIP_DUMP_FILE="$RAW_DUMP_FILE.gz"
command time -v gzip "$RAW_DUMP_FILE"

BASE_PATH="$B2_BUCKET/dumps/$PGDATABASE"
echo "Uploading $GZIP_DUMP_FILE to b2://$BASE_PATH/$(basename $GZIP_DUMP_FILE)"
rclone copy --verbose "$GZIP_DUMP_FILE" "$B2_CONFIG:$BASE_PATH"

if [ -n "$BACKUP_RETENTION_DAYS" ]; then
    echo "Deleting backups older then $BACKUP_RETENTION_DAYS days"
    rclone delete --verbose --min-age "${BACKUP_RETENTION_DAYS}d" "$B2_CONFIG:$BASE_PATH"
fi
