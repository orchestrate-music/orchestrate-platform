using System.Text;
using System.Text.Json.Serialization.Metadata;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.IdentityModel.Tokens;
using OpenTelemetry.Metrics;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Access;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.DemoSeed;
using OrchestrateApi.Email;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Feature;
using OrchestrateApi.Feature.Assets;
using OrchestrateApi.Feature.Concerts;
using OrchestrateApi.Feature.MemberBilling;
using OrchestrateApi.Feature.Scores;
using OrchestrateApi.Feature.Settings;
using OrchestrateApi.Feature.MailingLists;
using OrchestrateApi.PaymentProcessor;
using OrchestrateApi.Platform;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace OrchestrateApi
{
    public class Startup(IConfiguration configuration)
    {
        public AppConfiguration Configuration { get; } = new AppConfiguration(configuration);

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<OrchestrateContext>(contextOpt =>
            {
                contextOpt.UseNpgsql(Configuration.ConnectionString, npgsqlOpt =>
                {
                    npgsqlOpt.EnableRetryOnFailure(Configuration.DbMaxTransientFailureRetryCount);
                });
                // To avoid errors being logged every time a transient error is encountered (that will be retried)
                // suppress these events
                // Should be able to be removed when https://github.com/dotnet/efcore/issues/15269 implemented
                // For now we'll log in the interceptors when we're over the retry count for a particular
                // connection or command.
                contextOpt.ConfigureWarnings(b => b.Log(
                    (RelationalEventId.ConnectionError, LogLevel.Information),
                    (RelationalEventId.CommandError, LogLevel.Information)));
                contextOpt.ReplaceService<IQueryTranslationPreprocessorFactory, CustomQueryTranslationPreprocessorFactory>();
            });
            services.AddSingleton<IInterceptor, OrchestrateDbConnectionInterceptor>();
            services.AddSingleton<IInterceptor, OrchestrateDbCommandInterceptor>();

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                // See https://fly.io/docs/reference/runtime-environment/#fly-client-ip
                options.ForwardedForHeaderName = "Fly-Client-IP";
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor;

                // Loopback only by default. Since we run behind fly firewall, this should be safe
                options.KnownNetworks.Add(new IPNetwork(System.Net.IPAddress.Any, 0));
                options.KnownNetworks.Add(new IPNetwork(System.Net.IPAddress.IPv6Any, 0));
            });

            services.AddIdentity<OrchestrateUser, OrchestrateRole>(options =>
                {
                    // TODO setup default users and configure this to realistic values
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 1;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;

                    options.User.RequireUniqueEmail = true;
                })
                .AddEntityFrameworkStores<OrchestrateContext>()
                .AddUserStore<OrchestrateUserStore>()
                .AddDefaultTokenProviders();
            services.AddDataProtection()
                .PersistKeysToDbContext<OrchestrateContext>();
            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = Configuration.TokenLifetime;
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false; // TODO true if not dev
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Configuration.FrontEndUri.AbsoluteUri,
                    ValidIssuer = Configuration.JwtIssuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.JwtSecret))
                };
            });
            services.AddAuthorization();

            services.AddCors(opt =>
            {
                opt.AddDefaultPolicy(policy =>
                {
                    // Must remove trailing '/' that Uri adds
                    var uri = Configuration.FrontEndUri.AbsoluteUri;
                    var origin = uri.Remove(uri.Length - 1);

                    policy.WithOrigins(origin)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonDocumentConverter());
                    options.JsonSerializerOptions.Converters.Add(new OptionalJsonConverter());
                    options.JsonSerializerOptions.TypeInfoResolver = new DefaultJsonTypeInfoResolver
                    {
                        Modifiers = { OptionalJsonConverter.IgnoreUnsetOptionalValues },
                    };
                });

            services.AddHttpClient();

            services.AddOpenTelemetry()
                .WithMetrics(builder => builder
                    .AddRuntimeInstrumentation()
                    .AddAspNetCoreInstrumentation()
                    .AddHttpClientInstrumentation()
                    .AddPrometheusExporter());

            services.AddHttpContextAccessor();
            services.AddSingleton<IAuthorizationPolicyProvider, CustomAuthorizationPolicyProvider>();
            services.AddSingleton<IAuthorizationHandler, StandardTenantPermissionAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, HasTenantPermissionAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, IsGlobalAdminAuthorizationHandler>();
            services.AddScoped<IAuthorizationHandler, IsApiUserAuthorizationHandler>();

            services.AddScoped<StorageService>();
            services.AddScoped<IStorageProvider, DatabaseStorageProvider>();
            services.AddScoped<IBlobStorageStrategy, AssetAttachmentStorageStrategy>();
            services.AddScoped<IBlobStorageStrategy, ScoreAttachmentStorageStrategy>();
            services.AddScoped<IBlobStorageStrategy, ConcertAttachmentStorageStrategy>();
            services.AddScoped<IBlobStorageStrategy, ConcertCoverImageStorageStrategy>();
            services.AddScoped<IPeriodicJob, OrphanedBlobCleanupPeriodicJob>();

            services.AddScoped<SingleEnsembleSeed>();

            // TODO Add attribute to automatically add services
            services.AddTransient<OrchestratePersistenceManager>();
            services.AddSingleton<IEntityAccessorRegistrar, EntityAccessorRegistrar>();
            services.AddTransient<IEntityHookFactoryRegistrar, EntityHookFactoryRegistrar>();
            services.AddScoped<ITenantService, HttpContextTenantService>();
            services.AddScoped<IEntityHookFactory<Tenant>, TenantHookFactory>();
            services.AddScoped<IEntityHookFactory<Member>, MemberHookFactory>();
            services.AddScoped<IEntityHookFactory<MemberDetails>, MemberDetailHookFactory>();
            services.AddScoped<IEntityHookFactory<EnsembleMembership>, EnsembleMembershipHookFactory>();

            services.AddTransient<IAppConfiguration, AppConfiguration>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddTransient<IEmailProvider, PostmarkEmailProvider>();
            services.AddTransient<IPostmarkApiService, PostmarkApiService>();
            services.AddTransient<PostmarkService>();
            services.AddTransient<IEmailEventHandler, SpamComplainEmailEventHander>();
            services.AddTransient<IEmailEventHandler, MailingListUnsubscribeEmailEventHandler>();
            services.AddTransient<UserService>();

            services.AddScoped<IPaymentProcessor, StripePaymentProcessor>();
            services.AddScoped<StripeEventHandler>();
            services.AddScoped<PaymentProcessorService>();
            services.AddScoped<SubscriptionSettingsService>();
            services.AddScoped<IPeriodicJob, TrialEndCheckPeriodicJob>();

            services.AddTransient<MemberBillingPeriodService>();
            services.AddTransient<IInvoiceRenderer, SharpPdfInvoiceRenderer>();
            services.AddTransient<IMemberInvoiceRenderingService, MemberInvoiceRenderingService>();
            services.AddTransient<IMemberInvoiceSender, MemberInvoiceSender>();
            services.AddTransient<IEmailEventHandler, MemberInvoiceEmailEventHandler>();
            services.AddTransient<MemberInvoiceFactory>();
            services.AddScoped<IPeriodicJob, BillingPeriodTransitionPeriodicJob>();

            services.AddSingleton<IBackgroundTaskQueue, BackgroundTaskQueue>();
            services.AddHostedService<BackgroundTaskHostedService>();
            services.AddHostedService<PeriodicJobHostedService>();
            services.AddTransient<TenantContextIterator>();

            services.AddScoped<FrontEnd>();
            services.AddScoped<LandingService>();

            services.AddScoped<IIncomingEmailHandler, MailingListIncomingEmailHandler>();
            services.AddScoped<MailingListService>();
            services.AddScoped<RecipientListService>();
            services.AddScoped<IPeriodicJob, PostmarkSuppressionSyncPeriodicJob>();

            // Search the assembly for all Automapper.Profile implementations (in each controller)
            services.AddAutoMapper(cfg =>
            {
                cfg.AddMaps([typeof(Startup)]);

                cfg.CreateMap<Optional<int>, int>().ConvertUsing(new OptionalMapper<int>());
                cfg.CreateMap<Optional<string>, string>().ConvertUsing(new OptionalMapper<string>());
            });
            services.AddValidatorsFromAssemblyContaining<Startup>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env, ILogger<Startup> logger)
        {
            logger.LogInformation("Starting in {Environment} environment", env.EnvironmentName);

            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseOpenTelemetryPrometheusScrapingEndpoint(
                context => context.Connection.LocalPort == 9091);
        }
    }
}
