using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Concerts
{
    [StandardTenantPermissionAuthorize(FeaturePermission.ConcertRead, FeaturePermission.ConcertWrite)]
    [ApiController]
    [Route("concert")]
    public class ConcertController : ControllerBase
    {
        public ConcertController(OrchestrateContext dbContext)
        {
            DbContext = dbContext;
        }

        private OrchestrateContext DbContext { get; }

        [HttpGet("{id}/stats")]
        public async Task<ActionResult<ConcertStats>> GetConcertStatsAsync(int id, CancellationToken cancellationToken)
        {
            if (!await DbContext.Concert.AnyAsync(e => e.Id == id, cancellationToken))
            {
                return NotFound();
            }

            return await DbContext.Concert
                .Where(e => e.Id == id)
                .Select(e => new ConcertStats
                {
                    TotalMemberCount = e.Performance
                        .SelectMany(p => p.PerformanceMembers.Select(pm => pm.MemberId))
                        .Distinct()
                        .Count(),
                    TotalScoreCount = e.Performance
                        .SelectMany(p => p.PerformanceScores.Select(ps => ps.ScoreId))
                        .Count(),
                })
                .FirstAsync(cancellationToken);
        }
    }

    public class ConcertStats
    {
        public int TotalMemberCount { get; set; }

        public int TotalScoreCount { get; set; }
    }
}
