using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.CustomSave;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.MemberBilling;

public class SendInvoiceCustomSave : SingleEntityCustomSave<MemberInvoice>
{
    private readonly IMemberInvoiceSender memberInvoiceSender;

    public SendInvoiceCustomSave(IMemberInvoiceSender memberInvoiceSender)
    {
        this.memberInvoiceSender = memberInvoiceSender;
    }
    public override FeaturePermission RequiredPermission { get; } = FeaturePermission.MemberBillingWrite;

    internal override bool EntityIsPreSaveValid(MemberInvoice? entity) => true;

    internal override async Task ModifyEntity(OrchestrateContext context, MemberInvoice entity, CancellationToken cancellationToken)
    {
        var tenant = await context.Tenant.SingleAsync(cancellationToken);
        var billingConfig = await context.MemberBillingConfig.SingleAsync(cancellationToken);
        var primedMemberInvoice = await context.MemberInvoice
            .Include(e => e.MemberBillingPeriod)
            .Include(e => e.Member!.Details)
            .Include(e => e.LineItems)
            .SingleAsync(e => e.Id == entity.Id, cancellationToken);

        // Modifies send status which will be returned to client
        await this.memberInvoiceSender.SendAsync(tenant, billingConfig, new[] { primedMemberInvoice });
    }

    internal override Task<IEnumerable<object>> SaveLogicAsync(OrchestrateContext context, MemberInvoice entity, CancellationToken cancellationToken)
    {
        return Task.FromResult(Enumerable.Empty<object>());
    }
}
