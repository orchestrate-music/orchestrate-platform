using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MemberBilling.State
{
    public class DraftState : BillingPeriodState
    {
        public DraftState(BillingPeriodStateContext stateContext, ILogger<DraftState> logger)
            : base(stateContext, logger)
        {
        }

        public override MemberBillingPeriodState State => MemberBillingPeriodState.Draft;

        public override async Task<StateTransitionResult> SendInvoices()
        {
            Logger.LogInformation("Sending invoices to members");

            if (BillingConfig.EarlyBirdDiscountDollars > 0 && BillingPeriod.EarlyBirdDue == null)
            {
                // TODO #73 will hopefully make this a bit clearer
                var localDate = DateTime.UtcNow.InTimeZone(Tenant.TimeZone).Date;
                var localEarlyBirdDue = localDate
                    .AddDays(BillingConfig.EarlyBirdDiscountDaysValid)
                    .AddHours(23)
                    .AddMinutes(59)
                    .AddSeconds(59);
                BillingPeriod.EarlyBirdDue = localEarlyBirdDue.ToUtcFromTimeZone(Tenant.TimeZone);
                await DbContext.SaveChangesAsync();
            }

            var invoicesToSend = await DbContext.MemberInvoice
                .Where(e => e.MemberBillingPeriodId == BillingPeriod.Id && e.Sent == null)
                .Include(e => e.Member!.Details)
                .Include(e => e.LineItems)
                .ToListAsync();
            await MemberInvoiceSender.SendAsync(Tenant, BillingConfig, invoicesToSend);
            Logger.LogInformation("Sent {Count} invoices to members", invoicesToSend.Count);

            BillingPeriod.Sent = DateTime.UtcNow;
            StateContext.SetState(MemberBillingPeriodState.Open);
            await DbContext.SaveChangesAsync();
            return StateTransitionResult.Successful;
        }
    }
}
