using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;

namespace OrchestrateApi.Feature.MemberBilling.State
{
    public abstract class BillingPeriodState : IBillingPeriodState, IDisposable
    {
        private readonly IDisposable? loggerScope;

        protected BillingPeriodState(BillingPeriodStateContext stateContext, ILogger logger)
        {
            StateContext = stateContext;
            Logger = logger;
            this.loggerScope = Logger.BeginScope(BillingPeriod.Name);
        }

        public abstract MemberBillingPeriodState State { get; }

        protected BillingPeriodStateContext StateContext { get; }
        protected ILogger Logger { get; }
        protected OrchestrateContext DbContext => StateContext.DbContext;
        protected IAppConfiguration AppConfig => StateContext.AppConfig;
        protected IEmailService EmailService => StateContext.EmailService;
        protected IMemberInvoiceSender MemberInvoiceSender => StateContext.MemberInvoiceSender;

        protected Tenant Tenant => StateContext.Tenant;
        protected MemberBillingConfig BillingConfig => StateContext.BillingConfig;
        protected MemberBillingPeriod BillingPeriod => StateContext.BillingPeriod;

        public virtual Task GenerateInvoices(string? generatingUserEmail = null) => Task.CompletedTask;
        public virtual Task<StateTransitionResult> SendInvoices() => Task.FromResult(StateTransitionResult.NotAllowed);
        public virtual Task PerformTemporalTasks() => Task.CompletedTask;
        public virtual Task<StateTransitionResult> CloseBillingPeriod() => Task.FromResult(StateTransitionResult.NotAllowed);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.loggerScope?.Dispose();
            }
        }
    }
}
