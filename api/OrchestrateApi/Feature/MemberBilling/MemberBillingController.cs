using System.Linq.Expressions;
using Breeze.Persistence;
using LinqKit;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.MemberBilling
{
    [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingRead)]
    [ApiController]
    [Route("member-billing")]
    public class MemberBillingController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly OrchestratePersistenceManager persistenceManager;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly MemberBillingPeriodService billingPeriodService;
        private readonly IMemberInvoiceSender memberInvoiceSender;
        private readonly IMemberInvoiceRenderingService memberInvoiceRenderingService;
        private readonly MemberInvoiceFactory memberInvoiceFactory;

        public MemberBillingController(
            OrchestrateContext context,
            OrchestratePersistenceManager persistenceManager,
            UserManager<OrchestrateUser> userManager,
            MemberBillingPeriodService billingPeriodService,
            IMemberInvoiceSender memberInvoiceSender,
            IMemberInvoiceRenderingService memberInvoiceRenderingService,
            MemberInvoiceFactory memberInvoiceFactory)
        {
            // TODO Move breeze endpoints into here?
            this.context = context;
            this.persistenceManager = persistenceManager;
            this.userManager = userManager;
            this.billingPeriodService = billingPeriodService;
            this.memberInvoiceSender = memberInvoiceSender;
            this.memberInvoiceRenderingService = memberInvoiceRenderingService;
            this.memberInvoiceFactory = memberInvoiceFactory;
        }

        [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingWrite)]
        [HttpPost("generate-next-billing-period")]
        public async Task<ActionResult<object>> GenerateNextBillingPeriod()
        {
            if (this.context.MemberBillingPeriod.Any(e => e.State == MemberBillingPeriodState.Created || e.State == MemberBillingPeriodState.Draft))
            {
                return BadRequest();
            }

            // TODO Continue from inturrupted created one here
            var createdBillingPeriod = await this.billingPeriodService.TryCreateNextBillingPeriodAsync();
            if (createdBillingPeriod == null)
            {
                return StatusCode(500);
            }

            var tenant = await this.context.Tenant.SingleAsync();
            var billingConfig = await this.context.MemberBillingConfig.SingleAsync();
            var billingPeriodStateContext = this.billingPeriodService.BuildStateContext(tenant, billingConfig, createdBillingPeriod);
            var user = await this.userManager.GetUserAsync(User);
            await billingPeriodStateContext.GenerateInvoices(user?.Email);

            return Ok();
        }

        [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingWrite)]
        [HttpPost("auto-advance-billing-periods")]
        public async Task<ActionResult> AutoAdvanceBillingPeriods(CancellationToken cancellationToken)
        {
            await this.billingPeriodService.AutoAdvanceBillingPeriodsAsync(cancellationToken);
            return Ok();
        }

        [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingWrite)]
        [HttpPost("billing-period/{billingPeriodId}/generate-invoice-for-member/{memberId}")]
        public async Task<ActionResult<int>> GenerateInvoice(int billingPeriodId, int memberId)
        {
            var billingConfig = await this.context.MemberBillingConfig.FirstAsync();
            var billingPeriod = await this.context.MemberBillingPeriod.FirstOrDefaultAsync(e => e.Id == billingPeriodId);
            if (billingPeriod == null)
            {
                return NotFound();
            }

            var member = await this.context.Member
                .Include(e => e.Details)
                .FirstOrDefaultAsync(e => e.Id == memberId);
            if (member == null)
            {
                return NotFound();
            }

            var invoice = await this.memberInvoiceFactory.BuildAddAndSave(billingConfig, billingPeriod, member);
            return Ok(invoice.Id);
        }

        [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingWrite)]
        [HttpPost("billing-period/{billingPeriodId}/send")]
        public async Task<ActionResult> SendBillingPeriod(int billingPeriodId)
        {
            var billingPeriod = await this.context.MemberBillingPeriod.FirstOrDefaultAsync(e => e.Id == billingPeriodId);
            if (billingPeriod == null)
            {
                return NotFound();
            }

            var tenant = await this.context.Tenant.SingleAsync();
            var billingConfig = await this.context.MemberBillingConfig.SingleAsync();
            var billingPeriodStateContext = this.billingPeriodService.BuildStateContext(tenant, billingConfig, billingPeriod);

            var sendResult = await billingPeriodStateContext.SendInvoices();
            return sendResult switch
            {
                StateTransitionResult.Successful => Ok(),
                _ => BadRequest(),
            };
        }

        [HasTenantPermissionAuthorize(FeaturePermission.MemberBillingWrite)]
        [HttpPost("billing-period/{billingPeriodId}/close")]
        public async Task<ActionResult> CloseBillingPeriod(int billingPeriodId)
        {
            var billingPeriod = await this.context.MemberBillingPeriod.FirstOrDefaultAsync(e => e.Id == billingPeriodId);
            if (billingPeriod == null)
            {
                return NotFound();
            }

            var tenant = await this.context.Tenant.SingleAsync();
            var billingConfig = await this.context.MemberBillingConfig.SingleAsync();
            var billingPeriodStateContext = this.billingPeriodService.BuildStateContext(tenant, billingConfig, billingPeriod);

            var closeResult = await billingPeriodStateContext.CloseBillingPeriod();
            return closeResult switch
            {
                StateTransitionResult.Successful => Ok(),
                _ => BadRequest(),
            };
        }

        [HttpGet("billing-period/{billingPeriodId}/stats")]
        public async Task<ActionResult> GetBillingPeriodStats(int billingPeriodId)
        {
            var stats = await this.context.MemberBillingPeriod
                .Where(e => e.Id == billingPeriodId)
                .Select(ToBillingPeriodStats())
                .FirstOrDefaultAsync();
            if (stats == null)
            {
                return NotFound();
            }

            return Ok(stats);
        }

        [HttpPost("send-invoice")]
        [BreezeJsonSerialization]
        public async Task<ActionResult<SaveResult>> SendInvoice(JObject saveBundle)
        {
            var sendCustomSave = new SendInvoiceCustomSave(this.memberInvoiceSender);
            return await persistenceManager.CustomSaveAsync(saveBundle, sendCustomSave);
        }

        [HttpGet("invoice/{memberInvoiceId}/pdf")]
        public async Task<IActionResult> GetInvoicePdf(int memberInvoiceId)
        {
            var primedMemberInvoice = await this.context.MemberInvoice
                .AsNoTracking()
                .Include(e => e.MemberBillingPeriod)
                .Include(e => e.Member)
                .Include(e => e.LineItems)
                .Where(e => e.Id == memberInvoiceId)
                .SingleOrDefaultAsync();

            if (primedMemberInvoice == null)
            {
                return NotFound();
            }

            var tenant = await this.context.Tenant.SingleAsync();
            var billingConfig = await this.context.MemberBillingConfig.SingleAsync();
            var pdf = this.memberInvoiceRenderingService.RenderAsPdf(tenant, billingConfig, primedMemberInvoice);
            return File(pdf, "application/pdf", $"{primedMemberInvoice.Reference}.pdf");
        }

        [HttpGet("history-stats")]
        public async Task<ActionResult> GetBillingHistoryStats()
        {
            var stats = await this.context.MemberBillingPeriod
                .Where(e => e.State != MemberBillingPeriodState.Created && e.State != MemberBillingPeriodState.Draft)
                .OrderBy(e => e.Due)
                .Select(ToBillingPeriodStats())
                .ToListAsync();
            return Ok(stats);
        }

        private static Expression<Func<MemberBillingPeriod, BillingPeriodStats>> ToBillingPeriodStats()
        {
            // At some point probably want to cache these stats on the BillingPeriod, but we'll
            // worry about that when we've got some performance issues!
            Expression<Func<MemberBillingPeriod, BillingPeriodStats>> expr = e => new BillingPeriodStats
            {
                Name = e.Name,
                PaidEarly = MemberBillingExpressions.PaidEarlyCountExpr.Invoke(e),
                PaidOnTime = MemberBillingExpressions.PaidOnTimeCountExpr.Invoke(e),
                PaidLate = MemberBillingExpressions.PaidLateCountExpr.Invoke(e),
                Unpaid = MemberBillingExpressions.UnpaidCountExpr.Invoke(e),
                NumConcession = e.Invoices.Count(i => i.IsConcession),
                IncomeDollars = MemberBillingExpressions.BillingPeriodDollarsReceivedExpr.Invoke(e),
                TotalValueDollars = e.Invoices.Sum(i => i.LineItems.Sum(l => l.AmountCents)) / 100M,
            };

            return expr.Expand();
        }

        private class BillingPeriodStats
        {
            public required string Name { get; set; }

            public int PaidEarly { get; set; }

            public int PaidOnTime { get; set; }

            public int PaidLate { get; set; }

            public int Unpaid { get; set; }

            public int NumConcession { get; set; }

            public decimal IncomeDollars { get; set; }

            public decimal TotalValueDollars { get; set; }
        }
    }
}
