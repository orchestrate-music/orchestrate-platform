using System.Linq.Expressions;
using System.Text.Json.Serialization;
using Breeze.Persistence;
using LinqKit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature.Ensembles
{
    [StandardTenantPermissionAuthorize(FeaturePermission.EnsembleRead, FeaturePermission.EnsembleWrite)]
    [ApiController]
    [Route("ensemble")]
    public class EnsembleController : ControllerBase
    {
        private readonly OrchestrateContext context;
        private readonly OrchestratePersistenceManager persistenceManager;

        public EnsembleController(OrchestrateContext context, OrchestratePersistenceManager persistenceManager)
        {
            this.context = context;
            this.persistenceManager = persistenceManager;
        }

        private static Expression<Func<EnsembleMembership, bool>> MembershipIsActiveNowExpr
            => TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>();

        [HttpGet("stats")]
        public async Task<IEnumerable<EnsembleStats>> GetEnsembleStatsAsync([FromQuery] EnsembleStatus status, CancellationToken cancellationToken)
        {
            var membershipIsActiveNowExpr = MembershipIsActiveNowExpr;
            var rawStats = await this.context.Ensemble
                .AsExpandable()
                .Where(e => e.Status == status)
                .Select(e => new
                {
                    e.Id,
                    e.Name,
                    CurrentMemberCount = e.EnsembleMembership.Count(membershipIsActiveNowExpr.Compile()),
                    PerformanceCount = e.Performance.Count,
                    Membership = e.EnsembleMembership
                        .Where(membershipIsActiveNowExpr.Compile())
                        .Select(m => m.Type == MembershipType.Special ? FeeClass.Special : m.Member!.Details!.FeeClass)
                })
                .OrderByDescending(e => e.CurrentMemberCount)
                .ToListAsync(cancellationToken);

            return rawStats.Select(e => new EnsembleStats
            {
                Id = e.Id,
                Name = e.Name,
                CurrentMemberCount = e.CurrentMemberCount,
                PerformanceCount = e.PerformanceCount,
                Membership = e.Membership
                    // Can't do this bit in the DB query
                    // See https://docs.microsoft.com/en-us/ef/core/what-is-new/ef-core-5.0/breaking-changes#some-queries-with-correlated-collection-that-also-use-distinct-or-groupby-are-no-longer-supported
                    .GroupBy(m => m)
                    .ToDictionary(g => g.Key.ToString(), g => g.Count()),
            });
        }

        [HttpGet("{id}/summary")]
        public async Task<ActionResult<EnsembleSummary>> GetEnsembleSummaryAsync(int id, CancellationToken cancellationToken)
        {
            if (!await this.context.Ensemble.AnyAsync(e => e.Id == id, cancellationToken))
            {
                return NotFound();
            }

            var membershipIsActiveNowExpr = MembershipIsActiveNowExpr;
            var summary = await this.context.Ensemble
                .AsExpandable()
                .Where(e => e.Id == id)
                .Select(e => new EnsembleSummary
                {
                    CurrentMemberCount = e.EnsembleMembership.Count(membershipIsActiveNowExpr.Compile()),
                    PastMemberCount = e.EnsembleMembership
                        .Where(m => m.DateLeft != null)
                        .Select(m => m.MemberId)
                        .Distinct()
                        .Count(),
                    PerformanceCount = e.Performance.Count,
                    FirstConcertDate = e.Performance
                        .Select(p => (DateTime?)p.Concert!.Date)
                        .OrderBy(d => d)
                        .FirstOrDefault(),
                    LastConcertDate = e.Performance
                        .Select(p => (DateTime?)p.Concert!.Date)
                        .OrderByDescending(d => d)
                        .FirstOrDefault(),
                })
                .FirstAsync(cancellationToken);

            summary.Membership = await this.context.EnsembleMembership
                .Where(e => e.EnsembleId == id)
                .Where(membershipIsActiveNowExpr.Expand())
                .GroupBy(
                    e => e.Type == MembershipType.Special ? FeeClass.Special : e.Member!.Details!.FeeClass,
                    (k, g) => new EnsembleMembershipSummary { FeeClass = k, Count = g.Count() })
                .ToListAsync(cancellationToken);
            // Order after the query so we order by enum int value, not string representation
            summary.Membership = summary.Membership.OrderBy(m => m.FeeClass);

            summary.YearlyStats = await GetYearlyEnsembleStats(id, cancellationToken);

            return summary;
        }

        private async Task<IEnumerable<YearEnsembleStats>> GetYearlyEnsembleStats(int id, CancellationToken cancellationToken)
        {
            var yearlyConcerts = await this.context.Performance
                .Where(e => e.EnsembleId == id)
                .Select(e => e.Concert!.Date.Year)
                .ToListAsync(cancellationToken);
            var yearlyData = yearlyConcerts
                .GroupBy(y => y)
                .ToDictionary(g => g.Key, g => new YearEnsembleStats
                {
                    Year = g.Key,
                    PerformanceCount = g.Count(),
                });

            var allEnsembleMembership = await this.context.EnsembleMembership
                .Where(m => m.EnsembleId == id)
                .ToListAsync(cancellationToken);

            var earliestMemberYear = allEnsembleMembership
                .OrderBy(m => m.DateJoined)
                .Select(m => (int?)m.DateJoined.Year)
                .FirstOrDefault() ?? DateTime.UtcNow.Year;
            var latestMemberYear = allEnsembleMembership
                .OrderBy(m => m.DateLeft.HasValue) // So nulls (no left date) comes first
                .ThenByDescending(m => m.DateLeft)
                .Select(m => m.DateLeft?.Year)
                .FirstOrDefault() ?? DateTime.UtcNow.Year;
            for (var year = earliestMemberYear; year <= latestMemberYear; year++)
            {
                if (!yearlyData.TryGetValue(year, out var data))
                {
                    yearlyData[year] = new YearEnsembleStats { Year = year };
                    data = yearlyData[year];
                }

                var startOfYear = new DateTime(year, 1, 1);
                var startOfNextYear = new DateTime(year + 1, 1, 1);
                var calculator = new MembershipCalculator(startOfYear, startOfNextYear);
                data.MemberJoinedCount = calculator.JoinedCount(allEnsembleMembership);
                data.MemberStayedCount = calculator.StayedCount(allEnsembleMembership);
                data.MemberLeftCount = calculator.LeftCount(allEnsembleMembership);
                data.MemberJoinedAndLeftCount = calculator.JoinedAndLeftCount(allEnsembleMembership);
            }

            return yearlyData.Values.OrderBy(e => e.Year);
        }

        [HttpGet("{id}/member-performance-counts")]
        public async Task<IDictionary<int, int>> GetMemberPerformanceCountsAsync(int id, CancellationToken cancellationToken)
        {
            return await this.context.PerformanceMember
                .Where(m => m.Performance!.EnsembleId == id)
                .Select(m => m.MemberId)
                .GroupBy(m => m, (k, g) => new { MemberId = k, PerformanceCount = g.Count() })
                .ToDictionaryAsync(e => e.MemberId, e => e.PerformanceCount, cancellationToken);
        }
    }

    public class EnsembleStats
    {
        public required int Id { get; set; }

        public required string Name { get; set; }

        public required int CurrentMemberCount { get; set; }

        public required int PerformanceCount { get; set; }

        public required IDictionary<string, int> Membership { get; set; }
    }

    public class EnsembleSummary
    {
        public required int CurrentMemberCount { get; set; }

        public required int PastMemberCount { get; set; }

        public required int PerformanceCount { get; set; }

        public required DateTime? FirstConcertDate { get; set; }

        public required DateTime? LastConcertDate { get; set; }

        public IEnumerable<EnsembleMembershipSummary> Membership { get; set; } = Enumerable.Empty<EnsembleMembershipSummary>();

        public IEnumerable<YearEnsembleStats> YearlyStats { get; set; } = Enumerable.Empty<YearEnsembleStats>();
    }

    public class EnsembleMembershipSummary
    {
        [JsonConverter(typeof(JsonStringEnumConverter))]
        public FeeClass FeeClass { get; set; }

        public int Count { get; set; }
    }

    public class YearEnsembleStats
    {
        public int Year { get; set; }

        public int PerformanceCount { get; set; }

        public int MemberJoinedCount { get; set; }

        public int MemberStayedCount { get; set; }

        public int MemberLeftCount { get; set; }

        public int MemberJoinedAndLeftCount { get; set; }
    }
}
