using System.Linq.Expressions;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MailingLists;

// A recipient that may or may not have an email and may or may not have an active source
public record class MailingListRecipient(string Name, string? Email, bool IsActive);

public record class MailingListDeliverableRecipient(string Name, string Email);

public class RecipientListService(OrchestrateContext dbContext)
{
    public async Task<IList<MailingListDeliverableRecipient>> GetDeliverableRecipientListAsync(MailingList mailingList)
    {
        var ensembleIds = await LoadNavIds(mailingList, e => e.Ensembles, e => e.Id);
        var ensembleRecipients = await GetEnsembleRecipients(ensembleIds);

        var memberIds = await LoadNavIds(mailingList, e => e.Members, e => e.Id);
        var memberRecipients = await GetMemberRecipients(memberIds);

        var contactIds = await LoadNavIds(mailingList, e => e.Contacts, e => e.Id);
        var contactRecipients = await GetContactRecipients(contactIds);

        var suppressions = await dbContext.Entry(mailingList)
            .Collection(e => e.Suppressions)
            .Query()
            .Select(e => e.Email)
            .ToListAsync();

        return ensembleRecipients
            .Concat(memberRecipients)
            .Concat(contactRecipients)
            .Where(r => r.IsActive && !string.IsNullOrWhiteSpace(r.Email))
            .Select(r => new MailingListDeliverableRecipient(r.Name, r.Email!))
            .Where(r => !suppressions.Any(s => s.Equals(r.Email, StringComparison.OrdinalIgnoreCase)))
            .DistinctBy(r => r.Email.ToUpperInvariant())
            .ToList();
    }

    private async Task<IList<int>> LoadNavIds<T>(
        MailingList mailingList,
        Expression<Func<MailingList, IEnumerable<T>>> getNav,
        Expression<Func<T, int>> getId)
        where T : class
    {
        return await dbContext.Entry(mailingList)
            .Collection(getNav)
            .Query()
            .Select(getId)
            .ToListAsync();
    }

    public Expression<Func<MailingList, bool>> RecipientIsActiveInListExpr(int memberId)
    {
        var memberActiveExpr = TemporalLifetimeExtensions.IsActiveNowExpr<Member>();
        var membershipActiveExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>();
        Expression<Func<MailingList, Member, bool>> isSuppressed = (ml, m)
            => ml.Suppressions.Any(s => EF.Functions.ILike(s.Email, m.Details!.Email!));

        return ml => ml.Members
                .Where(memberActiveExpr.Compile())
                .Any(m => m.Id == memberId && !isSuppressed.Invoke(ml, m))
            || ml.Ensembles
                .Where(e => e.Status == EnsembleStatus.Active)
                .SelectMany(e => e.EnsembleMembership)
                .Where(membershipActiveExpr.Compile())
                .Any(m => m.MemberId == memberId && !isSuppressed.Invoke(ml, m.Member!));
    }

    public Expression<Func<MailingList, int>> GetRecipientCountExpr()
    {
        var membershipIsActiveNowExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>();
        // Don't union - it removes the duplicate NULLs!
        return e => e.Ensembles
            .SelectMany(e => e.EnsembleMembership)
            .Where(membershipIsActiveNowExpr.Compile())
            .Select(e => e.Member!.Details!.Email)
            .Concat(e.Members.Select(e => e.Details!.Email))
            .Concat(e.Contacts.Select(e => e.Email))
            .Select(e => string.IsNullOrWhiteSpace(e) ? dbContext.GenUuid().ToString() : e)
            .Select(e => e.ToUpperInvariant())
            .Distinct()
            .Count();
    }

    public async Task<IList<MailingListRecipient>> GetEnsembleRecipients(IList<int> ensembleIds)
    {
        var membershipIsActiveNowExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>();
        return await dbContext.Ensemble
            .AsExpandable()
            .Where(e => ensembleIds.Contains(e.Id))
            .SelectMany(e => e!.EnsembleMembership
                .Where(membershipIsActiveNowExpr.Compile())
                .Select(m => new MailingListRecipient(
                    $"{m.Member!.FirstName} {m.Member.LastName}",
                    m.Member.Details!.Email,
                    e.Status == EnsembleStatus.Active)))
            .ToListAsync();
    }

    public async Task<IList<MailingListRecipient>> GetMemberRecipients(IList<int> memberIds)
    {
        var isActiveMemberExpr = TemporalLifetimeExtensions.IsActiveNowExpr<Member>();
        return await dbContext.Member
            .AsExpandable()
            .Where(e => memberIds.Contains(e.Id))
            .Select(e => new MailingListRecipient(
                $"{e.FirstName} {e.LastName}",
                e.Details!.Email,
                isActiveMemberExpr.Invoke(e)))
            .ToListAsync();

    }

    public async Task<IList<MailingListRecipient>> GetContactRecipients(IList<int> contactIds)
    {
        return await dbContext.Contact
            .Where(e => contactIds.Contains(e.Id))
            .Select(e => new MailingListRecipient(e.Name, e.Email, !e.ArchivedOn.HasValue))
            .ToListAsync();
    }

}
