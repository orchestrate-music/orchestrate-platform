using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Feature.MailingLists;

public class MailingListDto
{
    public int Id { get; set; }
    public required string Name { get; set; }
    public required string Slug { get; set; }
    public required AllowedSenders AllowedSenders { get; set; }
    public required ReplyStrategy ReplyTo { get; set; }
    public string? Footer { get; set; }
}

public class MailingListDtoAutoMapperProfile : AutoMapper.Profile
{
    public MailingListDtoAutoMapperProfile()
    {
        CreateMap<MailingList, MailingListDto>();
    }
}
