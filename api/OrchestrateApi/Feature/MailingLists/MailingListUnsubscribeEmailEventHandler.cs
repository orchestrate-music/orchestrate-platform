using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using static OrchestrateApi.Feature.MailingLists.MailingListIncomingEmailHandler;

namespace OrchestrateApi.Feature.MailingLists;

public class MailingListUnsubscribeEmailEventHandler(
        ITenantService tenantService,
        OrchestrateContext dbContext,
        ILogger<MailingListUnsubscribeEmailEventHandler> logger)
        : IEmailEventHandler
{
    public async Task<int> HandleAsync(IList<EmailEvent> events)
    {
        var subscriptionChangeEvents = events
            .OfType<SubscriptionChangeEmailEvent>()
            .Where(e => e.IsType(MailingListEmailType))
            .ToList();

        var numProcessed = 0;
        foreach (var @event in subscriptionChangeEvents)
        {
            if (!TryGetTenantIdAndMailingListId(@event, out var tenantId, out var mailingListId))
            {
                continue;
            }

            using var tenantIdOverride = tenantService.ForceOverrideTenantId(tenantId);

            if (!await dbContext.MailingList.AnyAsync(e => e.Id == mailingListId))
            {
                logger.LogError("Did not find MailingList {MailingListId}", mailingListId);
                continue;
            }

            if (@event.IsSubscribed)
            {
                var existingSuppressions = await dbContext.MailingList
                    .Where(e => e.Id == mailingListId)
                    .SelectMany(e => e.Suppressions)
                    .Where(e => EF.Functions.ILike(e.Email, @event.Recipient))
                    .ToListAsync();
                dbContext.RemoveRange(existingSuppressions);
                await dbContext.SaveChangesAsync();
            }
            else
            {
                dbContext.Add(new MailingListSuppression
                {
                    MailingListId = mailingListId,
                    Email = @event.Recipient,
                    AddedAt = @event.DateTime,
                    Reason = @event.Details,
                    IsRemovable = @event.IsChangable,
                });
                await dbContext.SaveChangesAsync();
            }

            numProcessed++;
        }

        return numProcessed;
    }

    private bool TryGetTenantIdAndMailingListId(
        SubscriptionChangeEmailEvent @event,
        out Guid tenantId,
        out int mailingListId)
    {
        tenantId = Guid.Empty;
        mailingListId = 0;

        if (!@event.Metadata.TryGetValue(EmailService.TenantIdKey, out var tenantIdRaw))
        {
            logger.LogError("Did not find TenantId on event");
            return false;
        }

        if (!Guid.TryParse(tenantIdRaw, out tenantId))
        {
            logger.LogError("TenantId is not a Guid: {TenantId}", tenantIdRaw);
            return false;
        }

        if (!@event.Metadata.TryGetValue(MailingListIdMetadataKey, out var mailingListIdRaw))
        {
            logger.LogError("Did not find MailingListId on event");
            return false;
        }

        if (!int.TryParse(mailingListIdRaw, out mailingListId))
        {
            logger.LogError("MailingListId is not an int: {MailingListId}", mailingListIdRaw);
            return false;
        }

        return true;
    }
}
