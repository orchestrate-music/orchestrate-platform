using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Ensembles;
using OrchestrateApi.Feature.MailingLists;
using OrchestrateApi.Feature.MemberBilling;
using OrchestrateApi.Security;

namespace OrchestrateApi.Feature;

public class LandingService(
    OrchestrateContext dbContext,
    MailingListService mailingListService,
    RecipientListService recipientListService,
    AutoMapper.IMapper mapper)
{
    internal const int MailingListUsageDaysCutOff = 7;

    public async Task<PersonalDataDto> BuildPersonalDataDtoAsync(int memberId)
    {
        var tenant = await dbContext.GetTenantAsync()
            ?? throw new InvalidOperationException("Tenant must be set");

        return new PersonalDataDto
        {
            MemberId = memberId,
            Details = await GetPersonalDetailsAsync(memberId),
            CurrentMembership = await GetPersonalMembershipAsync(memberId),
            TotalPerformanceCount = await dbContext.PerformanceMember
                .CountAsync(e => e.MemberId == memberId),
            PerformanceHistory = await GetPerformanceHistoryAsync(memberId),
            LastInvoice = await dbContext.MemberInvoice
                .Where(e => e.MemberId == memberId)
                .Where(e => e.Paid.HasValue)
                .OrderByDescending(e => e.Paid)
                .ProjectTo<InvoiceDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(),
            UnpaidInvoices = await dbContext.MemberInvoice
                .Where(e => e.MemberId == memberId)
                .Where(e => e.MemberBillingPeriod!.State == MemberBillingPeriodState.Open)
                .Where(e => !e.Paid.HasValue)
                .OrderBy(e => e.MemberBillingPeriod!.Due)
                .ProjectTo<InvoiceDto>(mapper.ConfigurationProvider)
                .ToListAsync(),
            LoanedAssets = await dbContext.AssetLoan
                .Where(e => e.MemberId == memberId)
                .Where(e => !e.DateReturned.HasValue)
                .OrderBy(e => e.DateBorrowed)
                .ProjectTo<LoanedAssetDto>(mapper.ConfigurationProvider)
                .ToListAsync(),
            MailingLists = await dbContext.MailingList
                .AsExpandable()
                .Where(recipientListService.RecipientIsActiveInListExpr(memberId))
                .ProjectTo<MailingListMembershipDto>(mapper.ConfigurationProvider, new
                {
                    mailingListAddressExpr = mailingListService.MailingListEmailExpr(tenant),
                })
                .ToListAsync(),
        };
    }

    private async Task<PersonalDetailsDto> GetPersonalDetailsAsync(int memberId)
    {
        return await dbContext.Member
            .Where(e => e.Id == memberId)
            .ProjectTo<PersonalDetailsDto>(mapper.ConfigurationProvider)
            .SingleAsync();
    }

    private async Task<IEnumerable<MembershipDto>> GetPersonalMembershipAsync(int memberId)
    {
        return await dbContext.EnsembleMembership
            .Where(e => e.MemberId == memberId)
            .WhereIsActiveNow()
            .ProjectTo<MembershipDto>(mapper.ConfigurationProvider)
            .ToListAsync();
    }

    private async Task<IEnumerable<PerformanceHistoryDto>> GetPerformanceHistoryAsync(int memberId)
    {
        // Can't use AutoMapper due to GroupBy()
        return await dbContext.PerformanceMember
            .Where(e => e.MemberId == memberId)
            .GroupBy(e => new
            {
                e.Performance!.Concert!.Date.Year,
                EnsembleName = e.Performance!.Ensemble!.Name,
            })
            .Select(g => new PerformanceHistoryDto
            {
                Year = g.Key.Year,
                EnsembleName = g.Key.EnsembleName,
                PerformanceCount = g.Count()
            })
            .ToListAsync();
    }

    public async Task<TenantDataDto> BuildTenantDataDtoAsync(ICollection<FeaturePermission> permissions)
    {
        if (!permissions.Contains(FeaturePermission.PlatformAccess))
        {
            throw new InvalidOperationException($"User must have {FeaturePermission.PlatformAccess}");
        }

        return new TenantDataDto
        {
            Details = await GetTenantDetailsAsync(),
            Assets = permissions.Contains(FeaturePermission.AssetRead)
                ? await GetAssetSummaryAsync()
                : null,
            MusicLibrary = permissions.Contains(FeaturePermission.ScoreRead)
                ? await GetMusicLibrarySummaryAsync()
                : null,
            Contacts = permissions.Contains(FeaturePermission.ContactsRead)
                ? await GetContactsSummaryAsync()
                : null,
            EnsembleSummaries = permissions.Contains(FeaturePermission.EnsembleRead)
                ? await GetEnsembleSummariesAsync()
                : Enumerable.Empty<EnsembleSummaryDto>(),
            MembershipSnapshot = permissions.Contains(FeaturePermission.MemberRead)
                ? await GetMembershipSnapshotAsync()
                : null,
            LastConcert = permissions.Contains(FeaturePermission.ConcertRead)
                ? await dbContext.Concert
                    .OrderByDescending(e => e.Date)
                    .ProjectTo<ConcertDto>(mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync()
                : null,
            LatestBillingPeriods = permissions.Contains(FeaturePermission.MemberBillingRead)
                ? await dbContext.MemberBillingPeriod
                    .Where(e => e.State == MemberBillingPeriodState.Open)
                    .ProjectTo<BillingPeriodDto>(mapper.ConfigurationProvider)
                    .Union(dbContext.MemberBillingPeriod
                        .Where(e => e.State == MemberBillingPeriodState.Closed)
                        .OrderByDescending(e => e.Due)
                        .ProjectTo<BillingPeriodDto>(mapper.ConfigurationProvider)
                        .Take(1))
                    .OrderByDescending(e => e.Due)
                    .ToListAsync()
                : Enumerable.Empty<BillingPeriodDto>(),
            MailingListUsage = permissions.Contains(FeaturePermission.MailingListRead)
                ? await dbContext.MailingListMessageMetadata
                    .Where(e => e.Sent > DateTime.UtcNow.AddDays(-MailingListUsageDaysCutOff))
                    .GroupBy(e => new { e.MailingList!.Id, e.MailingList.Name })
                    .Select(e => new MailingListUsage
                    {
                        Id = e.Key.Id,
                        Name = e.Key.Name,
                        UsageCount = e.Count(),
                    })
                    .ToListAsync()
                : Enumerable.Empty<MailingListUsage>(),
        };
    }

    private async Task<TenantDetailsDto> GetTenantDetailsAsync()
    {
        return await dbContext.Tenant
            .ProjectTo<TenantDetailsDto>(mapper.ConfigurationProvider)
            .SingleAsync();
    }

    private async Task<AssetSummaryDto> GetAssetSummaryAsync()
    {
        return new AssetSummaryDto
        {
            Count = await dbContext.Asset
                .WhereIsActiveNow()
                .CountAsync(),
            OnLoanCount = await dbContext.AssetLoan
                .WhereIsActiveNow()
                .CountAsync(),
        };
    }

    private async Task<MusicLibrarySummaryDto> GetMusicLibrarySummaryAsync()
    {
        return new MusicLibrarySummaryDto
        {
            Count = await dbContext.Score.CountAsync(e => e.InLibrary),
            NeverPlayedCount = await dbContext.Score
                .Except(dbContext.PerformanceScore.Select(e => e.Score))
                .CountAsync(),
            PerformedInLastYearCount = await dbContext.Concert
                .Where(e => e.Date >= DateTime.UtcNow.AddYears(-1))
                .SelectMany(e => e.Performance)
                .SelectMany(e => e.PerformanceScores)
                .Select(e => e.ScoreId)
                .Distinct()
                .CountAsync(),
        };
    }

    private async Task<ContactsSummaryDto> GetContactsSummaryAsync()
    {
        return new ContactsSummaryDto
        {
            CurrentCount = await dbContext.Contact.CountAsync(e => !e.ArchivedOn.HasValue),
        };
    }

    private async Task<IEnumerable<EnsembleSummaryDto>> GetEnsembleSummariesAsync()
    {
        return await dbContext.Ensemble
            .Where(e => e.Status == EnsembleStatus.Active)
            .ProjectTo<EnsembleSummaryDto>(mapper.ConfigurationProvider, new
            {
                isActiveNowExpr = TemporalLifetimeExtensions.IsActiveNowExpr<EnsembleMembership>(),
            })
            .OrderByDescending(e => e.MemberCount)
            .ThenByDescending(e => e.ConcertCount)
            .Take(3)
            .ToListAsync();
    }

    private async Task<MembershipSnapshotDto> GetMembershipSnapshotAsync()
    {
        var yearAgo = DateTime.UtcNow.AddYears(-1);
        var applicableMemberDates = await dbContext.Member
            .Where(e => e.DateLeft == null || e.DateLeft.Value >= yearAgo)
            .Select(e => new Membership(e.DateJoined, e.DateLeft))
            .ToListAsync();

        var calculator = new MembershipCalculator(yearAgo, DateTime.UtcNow);
        return new MembershipSnapshotDto
        {
            JoinedCount = calculator.JoinedCount(applicableMemberDates),
            StayedCount = calculator.StayedCount(applicableMemberDates),
            LeftCount = calculator.LeftCount(applicableMemberDates),
            JoinedAndLeftCount = calculator.JoinedAndLeftCount(applicableMemberDates),
        };
    }

    private record Membership(DateTime DateJoined, DateTime? DateLeft) : IMembership;
}

public class LandingSummaryAutoMapperProfile : AutoMapper.Profile
{
    public LandingSummaryAutoMapperProfile()
    {
        CreateMap<Member, PersonalDetailsDto>()
            .IncludeMembers(e => e.Details)
            .ForMember(e => e.Name, src => src.MapFrom(e => $"{e.FirstName} {e.LastName}"))
            .ForMember(e => e.Instruments, src => src.MapFrom(e => e.MemberInstrument.Select(i => i.InstrumentName)));
        // Add explicit map here So IncludeMembers above works, and MemberList.None so we don't
        // validate any of these mappings.
        // See https://docs.automapper.org/en/stable/Flattening.html#includemembers
        CreateMap<MemberDetails, PersonalDetailsDto>(AutoMapper.MemberList.None);

        CreateMap<EnsembleMembership, MembershipDto>();
        CreateMap<AssetLoan, LoanedAssetDto>()
            .ForMember(e => e.SerialNo, src => src.MapFrom(e => e.Asset!.SerialNo));
        CreateMap<MemberInvoice, InvoiceDto>()
            .ForMember(e => e.Total, src => src.MapFrom(MemberBillingExpressions.InvoiceTotalExpr))
            .ForMember(e => e.BillingPeriodName, src => src.MapFrom(e => e.MemberBillingPeriod!.Name))
            .ForMember(e => e.BillingPeriodState, src => src.MapFrom(e => e.MemberBillingPeriod!.State))
            .ForMember(e => e.EarlyBirdDue, src => src.MapFrom(e => e.MemberBillingPeriod!.EarlyBirdDue))
            .ForMember(e => e.Due, src => src.MapFrom(e => e.MemberBillingPeriod!.Due));

        Expression<Func<MailingList, string>> mailingListAddressExpr = null!;
        CreateMap<MailingList, MailingListMembershipDto>()
            .ForMember(e => e.Address, src => src.MapFrom(e => mailingListAddressExpr.Invoke(e)));

        CreateMap<Tenant, TenantDetailsDto>();

        CreateMap<Concert, ConcertDto>()
            .ForMember(e => e.Performances, src => src.MapFrom(e => e.Performance));
        CreateMap<Performance, PerformanceDto>()
            .ForMember(e => e.MemberCount, src => src.MapFrom(e => e.PerformanceMembers.Count))
            .ForMember(e => e.PieceCount, src => src.MapFrom(e => e.PerformanceScores.Count));

        CreateMap<MemberBillingPeriod, BillingPeriodDto>()
            .ForMember(e => e.InvoiceCount, src => src.MapFrom(e => e.Invoices.Count))
            .ForMember(e => e.DollarsReceived, src => src.MapFrom(MemberBillingExpressions.BillingPeriodDollarsReceivedExpr))
            .ForMember(e => e.PaidEarly, src => src.MapFrom(MemberBillingExpressions.PaidEarlyCountExpr))
            .ForMember(e => e.PaidOnTime, src => src.MapFrom(MemberBillingExpressions.PaidOnTimeCountExpr))
            .ForMember(e => e.PaidLate, src => src.MapFrom(MemberBillingExpressions.PaidLateCountExpr))
            .ForMember(e => e.Unpaid, src => src.MapFrom(MemberBillingExpressions.UnpaidCountExpr));

        Expression<Func<EnsembleMembership, bool>> isActiveNowExpr = null!;
        CreateMap<Ensemble, EnsembleSummaryDto>()
            .ForMember(e => e.FirstConcertDate, src => src.MapFrom(e => e.Performance
                .Select(p => (DateTime?)p.Concert!.Date)
                .OrderBy(e => e)
                .FirstOrDefault()))
            .ForMember(e => e.ConcertCount, src => src.MapFrom(e => e.Performance.Count))
            .ForMember(e => e.MemberCount, src => src.MapFrom(e => e.EnsembleMembership
                .AsQueryable()
                .Count(isActiveNowExpr.Expand())));
    }
}
