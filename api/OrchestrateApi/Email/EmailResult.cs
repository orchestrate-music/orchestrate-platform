namespace OrchestrateApi.Email
{
    public enum EmailResultStatus
    {
        Success,
        Error,
    }

    public class EmailResult
    {
        public required EmailMessage Email { get; set; }

        public required EmailResultStatus Status { get; set; }

        public required string StatusMessage { get; set; }

        public object? OriginalResult { get; set; }
    }
}
