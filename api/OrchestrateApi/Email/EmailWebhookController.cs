using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.Common;
using OrchestrateApi.Email.Postmark;
using OrchestrateApi.Security;
using PostmarkDotNet.Webhooks;

namespace OrchestrateApi.Email
{
    [ApiController]
    [Route("EmailWebhook")]
    public class EmailWebhookController : ControllerBase
    {
        private readonly IBackgroundTaskQueue backgroundTaskQueue;
        private readonly ILogger logger;

        public EmailWebhookController(
            IBackgroundTaskQueue backgroundTaskQueue,
            ILogger<EmailWebhookController> logger)
        {
            this.backgroundTaskQueue = backgroundTaskQueue;
            this.logger = logger;
        }

        [IsApiUserAuthorize]
        [HttpPost("Postmark/Event")]
        public async Task<IActionResult> Postmark(PostmarkEvent @event)
        {
            var normalisedEvent = @event.ToEmailEvent();
            await QueueEmailEventsForProcessing(new[] { normalisedEvent });
            return Ok();
        }

        [IsApiUserAuthorize]
        [HttpPost("Postmark/InboundMessage")]
        public async Task<IActionResult> PostmarkInboundMessage(PostmarkInboundWebhookMessage email)
        {
            var incomingEmail = email.ToIncomingEmail();
            await this.backgroundTaskQueue.QueueAsync(new IncomingEmailBackgroundWorkItem(incomingEmail));
            return Ok();
        }

        private ValueTask QueueEmailEventsForProcessing(IList<EmailEvent> events)
        {
            return this.backgroundTaskQueue.QueueAsync(new EmailEventBackgroundWorkItem(events));
        }

        private class EmailEventBackgroundWorkItem : IBackgroundWorkItem
        {
            private readonly IList<EmailEvent> events;

            public EmailEventBackgroundWorkItem(IList<EmailEvent> events)
            {
                this.events = events ?? throw new ArgumentNullException(nameof(events));
            }

            public async Task DoWork(IServiceProvider services, CancellationToken cancellationToken)
            {
                var emailService = services.GetRequiredService<IEmailService>();
                await emailService.ProcessEventsAsync(this.events);
            }
        }

        private class IncomingEmailBackgroundWorkItem : IBackgroundWorkItem
        {
            private readonly IncomingEmail email;

            public IncomingEmailBackgroundWorkItem(IncomingEmail email)
            {
                ArgumentNullException.ThrowIfNull(email);
                this.email = email;
            }

            [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1031")]
            public async Task DoWork(IServiceProvider services, CancellationToken cancellationToken)
            {
                var incomingMailHandlers = services.GetServices<IIncomingEmailHandler>();
                var logger = services.GetRequiredService<ILogger<IncomingEmailBackgroundWorkItem>>();
                foreach (var handler in incomingMailHandlers)
                {
                    try
                    {
                        await handler.HandleAsync(email);
                        logger.LogInformation(
                            "Incoming email from {From} handled successfully by {Handler}",
                            this.email.From.Email,
                            handler.GetType().Name);
                    }
                    catch (Exception e)
                    {
                        logger.LogError(
                            e,
                            "Error when processing incoming email from {From} in {Handler}",
                            this.email.From.Email,
                            handler.GetType());
                    }
                }

            }
        }
    }
}
