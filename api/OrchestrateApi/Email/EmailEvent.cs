namespace OrchestrateApi.Email
{
    public enum EmailEventStatus
    {
        Delivered,
        Opened,
        Failed,
    }

    public abstract class EmailEvent
    {
        public string Recipient { get; set; } = string.Empty;

        public DateTime DateTime { get; set; }

        public abstract EmailEventStatus? Status { get; }

        public abstract string Summary { get; }

        public string Details { get; set; } = string.Empty;

        public IDictionary<string, string?> Metadata { get; init; } = new Dictionary<string, string?>();

        public object? OriginalEvent { get; set; }

        public bool IsFromEnvironment(string environmentName)
        {
            return Metadata.TryGetValue(EmailService.EmailEnvironmentMetadataKey, out var emailEnvironmentName)
                && emailEnvironmentName == environmentName;
        }

        /// <summary>Returns true if this event is from an email that matches <see cref="EmailMessage.Type" /></summary>
        public bool IsType(string type)
        {
            return Metadata.TryGetValue(EmailService.EmailTypeKey, out var emailType)
                && emailType == type;
        }

        public bool IsKnownEvent { get; protected set; } = true;
    }

    public class DeliveredEmailEvent : EmailEvent
    {
        public override EmailEventStatus? Status => EmailEventStatus.Delivered;
        public override string Summary => $"Email delivered successfully";
    }

    public class OpenedEmailEvent : EmailEvent
    {
        public override EmailEventStatus? Status => EmailEventStatus.Opened;
        public override string Summary => $"Email opened by recipient";
    }

    public class UndeliverableEmailEvent : EmailEvent
    {
        public override EmailEventStatus? Status => EmailEventStatus.Failed;
        public override string Summary => "Email was unable to be delivered";
    }

    public class SpamComplaintEmailEvent : EmailEvent
    {
        public override EmailEventStatus? Status => EmailEventStatus.Failed;
        public override string Summary => "The user recieved this email but marked it as spam.";
    }

    public class SubscriptionChangeEmailEvent : EmailEvent
    {
        public override EmailEventStatus? Status => EmailEventStatus.Failed;
        public override string Summary => "Suppression status has changed";
        public required bool IsSubscribed { get; init; }
        public required bool IsChangable { get; init; }
    }

    public class UnknownEmailEvent : EmailEvent
    {
        public UnknownEmailEvent()
        {
            IsKnownEvent = false;
        }

        public override EmailEventStatus? Status => EmailEventStatus.Failed;
        public override string Summary => "Unknown email event";
    }
}
