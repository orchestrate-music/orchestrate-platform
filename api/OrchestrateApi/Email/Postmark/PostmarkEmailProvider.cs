using System.Text;
using PostmarkDotNet;

namespace OrchestrateApi.Email.Postmark;

public class PostmarkEmailProvider(
    IPostmarkApiService postmarkApiService,
    ILogger<PostmarkEmailProvider> logger)
    : IEmailProvider
{
    // Postmark has limits of 5MB, 10MB and 50MB respectively for sending,
    // We're giving ourselves a bit of a buffer here for other parts of the email etc
    // https://postmarkapp.com/support/article/1056-what-are-the-attachment-and-email-size-limits
    public const int MaxBodySizeBytes = 4 * 1024 * 1024;
    public const int MaxMessageSizeBytes = 8 * 1024 * 1024;
    public const int MaxBatchSizeBytes = 45 * 1024 * 1024;

    internal const string DefaultPostmarkStream = "outbound";
    internal const string PostmarkUnsubscribePlaceholder = "{{{ pm:unsubscribe }}}";

    // See https://postmarkapp.com/developer/user-guide/send-email-with-api/batch-emails
    private const int MaxNumBatchMessages = 500;

    public string Name => "Postmark";

    public async Task<IList<EmailResult>> SendAsync(IList<EmailMessage> emails)
    {
        var results = new List<EmailResult> { Capacity = emails.Count };
        foreach (var tenantGroup in emails.GroupBy(e => e.SendContext?.Tenant))
        {
            var client = await postmarkApiService.GetOrCreateClientAsync(tenantGroup.Key);

            foreach (var emailBatch in BatchEmails(tenantGroup, results))
            {
                var chunkResults = await SendBatchAsync(client, emailBatch.ToList());
                results.AddRange(chunkResults);
            }
        }

        return results;
    }

    internal static IEnumerable<IList<EmailMessage>> BatchEmails(IEnumerable<EmailMessage> emails, IList<EmailResult> results)
    {
        var batchSize = 0;
        var batch = new List<EmailMessage>();
        foreach (var email in emails)
        {
            var textSize = Encoding.UTF8.GetByteCount(email.TextContent ?? string.Empty);
            var htmlSize = Encoding.UTF8.GetByteCount(email.HtmlContent ?? string.Empty);
            var attachmentSize = email.Attachments.Sum(a => Encoding.UTF8.GetByteCount(a.Base64Content));
            var totalLowerBoundSize = textSize + htmlSize + attachmentSize;
            if (textSize > MaxBodySizeBytes || htmlSize > MaxBodySizeBytes || totalLowerBoundSize > MaxMessageSizeBytes)
            {
                results.Add(new EmailResult
                {
                    Status = EmailResultStatus.Error,
                    StatusMessage = "Email is too big to send",
                    Email = email,
                });
                continue;
            }

            if (batch.Count == MaxNumBatchMessages || batchSize + totalLowerBoundSize > MaxBatchSizeBytes)
            {
                yield return batch;
                batch = [];
                batchSize = 0;
            }

            batchSize += totalLowerBoundSize;
            batch.Add(email);
        }

        if (batch.Count > 0)
        {
            yield return batch;
        }
    }

    private async Task<IList<EmailResult>> SendBatchAsync(IPostmarkAdapter client, List<EmailMessage> emails)
    {
        var messages = new List<PostmarkMessage> { Capacity = emails.Count };
        foreach (var email in emails)
        {
            messages.Add(await ToPostmarkMessageAsync(client, email));
        }

        var responses = await client.SendMessagesAsync(messages.ToArray());

        var responseCount = responses.Count();
        if (responseCount != emails.Count)
        {
            logger.LogError("Expected response count ({ResponseCount}) to equal email count ({EmailCount})", responseCount, emails.Count);
            return EmailHelpers.FailAllEmails(emails);
        }

        return Enumerable.Zip(emails, responses)
            .Select(tuple =>
            {
                var (email, response) = tuple;
                return ToEmailResult(email, response);
            })
            .ToList();
    }

    internal async Task<PostmarkMessage> ToPostmarkMessageAsync(IPostmarkAdapter client, EmailMessage email)
    {
        return new PostmarkMessage
        {
            From = ToRecipientStructure(email.From),
            To = ToRecipientStructure(email.To),
            ReplyTo = ToRecipientStructure(email.ReplyTo),
            Subject = email.Subject,
            TextBody = EmailHelpers.ReplacePlainTextUnsubscribeLink(
                email.TextContent, PostmarkUnsubscribePlaceholder),
            HtmlBody = EmailHelpers.ReplaceHtmlUnsubscribeLink(
                email.HtmlContent, EmailHelpers.BuildUnsubscribeHtml(PostmarkUnsubscribePlaceholder)),
            Metadata = email.Metadata,
            Attachments = email.Attachments
                .Select(e => new PostmarkMessageAttachment
                {
                    Name = e.Name,
                    Content = e.Base64Content,
                    ContentType = e.MimeType,
                })
                .ToList(),
            MessageStream = email.SendContext?.MailingList is { } mailingList
                ? await postmarkApiService.GetOrCreateMailingListStreamIdAsync(client, mailingList)
                : DefaultPostmarkStream,
            Headers = new(email.Headers),
        };
    }

    private static string? ToRecipientStructure(EmailMessageAddress? address)
    {
        if (address == null)
        {
            return null;
        }
        else if (!string.IsNullOrWhiteSpace(address.Name))
        {
            return $"{address.Name} <{address.Address}>";
        }
        else
        {
            return address.Address;
        }
    }

    internal static EmailResult ToEmailResult(EmailMessage email, PostmarkResponse response)
    {
        var status = response.ErrorCode == 0
            ? EmailResultStatus.Success
            : EmailResultStatus.Error;
        var statusMessage = status == EmailResultStatus.Success
            ? "Delivery pending"
            : $"{response.ErrorCode}: {response.Message ?? "Unknown error"}";
        return new EmailResult
        {
            Email = email,
            Status = status,
            StatusMessage = statusMessage,
            OriginalResult = response,
        };
    }
}
