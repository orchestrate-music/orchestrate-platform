using System.Text.Json.Serialization;

namespace OrchestrateApi.Email.Postmark;


// We only include the fields we need here, to see the full specification
// go to https://postmarkapp.com/developer/webhooks/webhooks-overview
[JsonConverter(typeof(PostmarkEventJsonConverter))]
public abstract class PostmarkEvent
{
    public string RecordType { get; set; } = string.Empty;

    [JsonPropertyName("MessageID")]
    public Guid MessageId { get; set; }

    public IDictionary<string, string?> Metadata { get; set; } = new Dictionary<string, string?>();

    public abstract EmailEvent ToEmailEvent();

    protected static string GenerateEmailEventDetails(string? description, string? details)
    {
        string emailEventDetails = "";

        if (!string.IsNullOrWhiteSpace(description))
        {
            emailEventDetails += description;
        }

        if (!string.IsNullOrWhiteSpace(details))
        {
            emailEventDetails += $" - {details}";
        }

        if (string.IsNullOrWhiteSpace(emailEventDetails))
        {
            emailEventDetails = "Unknown error";
        }

        return emailEventDetails;
    }
}

public class PostmarkDeliveryEvent : PostmarkEvent
{
    public required string Recipient { get; set; }

    public DateTime DeliveredAt { get; set; }

    public override EmailEvent ToEmailEvent()
    {
        return new DeliveredEmailEvent
        {
            Recipient = Recipient,
            DateTime = DeliveredAt,
            OriginalEvent = this,
            Metadata = Metadata,
        };
    }
}

public class PostmarkOpenEvent : PostmarkEvent
{
    public required string Recipient { get; set; }

    public DateTime ReceivedAt { get; set; }

    public override EmailEvent ToEmailEvent()
    {
        return new OpenedEmailEvent
        {
            Recipient = Recipient,
            DateTime = ReceivedAt,
            OriginalEvent = this,
            Metadata = Metadata,
        };
    }
}

public class PostmarkBounceEvent : PostmarkEvent
{
    // See https://postmarkapp.com/developer/api/bounce-api#bounce-types
    public required string Type { get; set; }

    public required string Name { get; set; }

    public required string Description { get; set; }

    public required string Details { get; set; }

    public required string Email { get; set; }

    public DateTime BouncedAt { get; set; }

    public override EmailEvent ToEmailEvent()
    {
        return new UndeliverableEmailEvent
        {
            Recipient = Email,
            DateTime = BouncedAt,
            OriginalEvent = this,
            Metadata = Metadata,
            Details = GenerateEmailEventDetails(Description, Details),
        };
    }
}

public class PostmarkSpamComplaintEvent : PostmarkEvent
{
    public required string Type { get; set; }

    public required string Name { get; set; }

    public required string Email { get; set; }

    public required DateTime BouncedAt { get; set; }

    public required string Description { get; set; }

    public required string Details { get; set; }

    public override EmailEvent ToEmailEvent()
    {
        return new UndeliverableEmailEvent
        {
            Recipient = Email,
            DateTime = BouncedAt,
            OriginalEvent = this,
            Metadata = Metadata,
            Details = GenerateEmailEventDetails(Description, Details),
        };
    }
}

public class PostmarkSubscriptionChangeEvent : PostmarkEvent
{
    public required string Recipient { get; set; }

    public required DateTime ChangedAt { get; set; }

    public required bool SuppressSending { get; set; }

    public required string? SuppressionReason { get; set; }

    public override EmailEvent ToEmailEvent() => new SubscriptionChangeEmailEvent
    {
        Recipient = Recipient,
        DateTime = ChangedAt,
        OriginalEvent = this,
        IsSubscribed = !SuppressSending,
        IsChangable = PostmarkService.SuppressionReasonIsRemovable(SuppressionReason),
        Details = SuppressionReason is { } reason
            ? PostmarkService.SummariseSuppressionReason(reason)
            : string.Empty,
        Metadata = Metadata,
    };
}

public class PostmarkUnknownEvent : PostmarkEvent
{
    public override EmailEvent ToEmailEvent()
    {
        return new UnknownEmailEvent
        {
            Recipient = string.Empty,
            DateTime = DateTime.UtcNow,
            OriginalEvent = this,
            Metadata = Metadata,
        };
    }
}
