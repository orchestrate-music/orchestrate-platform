using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using Postmark.Model.Suppressions;
using PostmarkDotNet.Exceptions;
using PostmarkDotNet.Model.Webhooks;

namespace OrchestrateApi.Email.Postmark;

public class PostmarkService(
    OrchestrateContext dbContext,
    IPostmarkApiService postmarkApiService,
    ILogger<PostmarkService> logger)
{
    public const string HardBounceSuppressionReason = "HardBounce";
    public const string SpamComplaintSuppressionReason = "SpamComplaint";
    public const string ManualSuppressionReason = "ManualSuppression";

    public async Task DeleteMailingListStreamAsync(MailingList mailingList)
    {
        if (postmarkApiService.GetTenantClient(await dbContext.GetTenantAsync()) is not { } client
            || postmarkApiService.GetMailingListStreamId(mailingList) is not { } streamId)
        {
            return;
        }

        try
        {
            await client.ArchiveMessageStream(streamId);
        }
        catch (PostmarkValidationException e)
        {
            logger.LogError(e, "Failed to archive message stream");
        }

        mailingList.PostmarkStreamId = null;
        await dbContext.SaveChangesAsync();
    }

    public async Task RemoveSuppressions(MailingList mailingList, IList<string> emails)
    {
        if (emails.Count == 0
            || postmarkApiService.GetTenantClient(await dbContext.GetTenantAsync()) is not { } client
            || postmarkApiService.GetMailingListStreamId(mailingList) is not { } streamId)
        {
            return;
        }

        try
        {
            var result = await client.DeleteSuppressions(
                emails.Select(e => new PostmarkSuppressionChangeRequest { EmailAddress = e }),
                streamId);
            var failedDeletions = result.Suppressions
                .Where(e => e.Status == PostmarkReactivationRequestStatus.Failed)
                .ToList();
            if (failedDeletions.Count > 0)
            {
                logger.LogError(
                    "Failed to delete Postmark suppressions: {EmailAndReasons}",
                    string.Join(", ", failedDeletions.Select(f => $"{f.EmailAddress}: {f.Message}")));
            }
        }
        catch (PostmarkValidationException e)
        {
            logger.LogError(e, "Failed to delete suppressions");
        }
    }

    public async Task UpdateWebhookApiUserPasswords(string newPassword)
    {
        foreach (var client in await postmarkApiService.GetAllClients())
        {
            var response = await client.GetWebhookConfigurationsAsync();
            foreach (var webhook in response.Webhooks)
            {
                if (!webhook.ID.HasValue || webhook.HttpAuth?.Username != UserService.ApiUsername)
                {
                    continue;
                }

                await client.EditWebhookConfigurationAsync(webhook.ID.Value, webhook.Url, new HttpAuth
                {
                    Username = UserService.ApiUsername,
                    Password = newPassword,
                });
            }
        }
    }

    public async Task AddAllMissingSuppressions(CancellationToken cancellationToken = default)
    {
        var tenant = await dbContext.GetTenantAsync(cancellationToken);
        if (postmarkApiService.GetTenantClient(tenant) is not { } client)
        {
            return;
        }

        var mailingLists = await dbContext.MailingList
            .Include(e => e.Suppressions)
            .Where(e => e.PostmarkStreamId != null)
            .ToListAsync(cancellationToken);
        foreach (var mailingList in mailingLists)
        {
            var suppressions = await GetSuppressions(client, mailingList);
            var missingSuppressions = suppressions.Where(s => !mailingList.Suppressions
                .Any(e => e.Email.Equals(s.EmailAddress, StringComparison.OrdinalIgnoreCase)))
                .ToList();
            foreach (var s in missingSuppressions)
            {
                mailingList.Suppressions.Add(new MailingListSuppression
                {
                    Email = s.EmailAddress,
                    Reason = SummariseSuppressionReason(s.SuppressionReason),
                    AddedAt = s.CreatedAt.ToUniversalTime(),
                    IsRemovable = SuppressionReasonIsRemovable(s.SuppressionReason),
                });
            }
        }

        await dbContext.SaveChangesAsync(cancellationToken);
    }

    private async Task<IList<PostmarkSuppression>> GetSuppressions(IPostmarkAdapter client, MailingList mailingList)
    {
        var streamId = postmarkApiService.GetMailingListStreamId(mailingList);
        if (streamId is null)
        {
            return [];
        }

        try
        {
            var response = await client.ListSuppressions(new PostmarkSuppressionQuery(), streamId);
            return response.Suppressions.ToList();
        }
        catch (PostmarkValidationException e)
        {
            logger.LogError(e, "Failed to list suppressions");
            return [];
        }
    }

    public static bool SuppressionReasonIsRemovable(string? suppressionReason) => suppressionReason != SpamComplaintSuppressionReason;

    public static string SummariseSuppressionReason(string suppressionReason) => suppressionReason switch
    {
        HardBounceSuppressionReason => "An email to this recipient failed to be delivered",
        SpamComplaintSuppressionReason => "The recipient marked an email from this mailing list as spam",
        _ => "The recipient unsubscribed from this mailing list",
    };
}
