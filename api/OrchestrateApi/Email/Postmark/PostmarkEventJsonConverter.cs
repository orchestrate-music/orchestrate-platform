using OrchestrateApi.Common;

namespace OrchestrateApi.Email.Postmark;

public class PostmarkEventJsonConverter : PolymorphicJsonConverter<PostmarkEvent>
{
    protected override string DiscriminatorProperty => "RecordType";

    protected override Type SelectTargetType(string? discriminator)
    {
        return discriminator switch
        {
            "Delivery" => typeof(PostmarkDeliveryEvent),
            "Open" => typeof(PostmarkOpenEvent),
            "Bounce" => typeof(PostmarkBounceEvent),
            "SpamComplaint" => typeof(PostmarkSpamComplaintEvent),
            "SubscriptionChange" => typeof(PostmarkSubscriptionChangeEvent),
            _ => typeof(PostmarkUnknownEvent),
        };
    }
}
