namespace OrchestrateApi.Email
{
    public interface IEmailEventHandler
    {
        /// <summary>
        /// Handles the given events, returning the number which were successfully processed
        /// </summary>
        Task<int> HandleAsync(IList<EmailEvent> events);
    }
}
