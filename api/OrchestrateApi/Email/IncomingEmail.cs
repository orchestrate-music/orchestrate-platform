namespace OrchestrateApi.Email;

public class IncomingEmail
{
    public required IncomingEmailRecipient From { get; init; }

    public IList<IncomingEmailRecipient> To { get; init; } = new List<IncomingEmailRecipient>();
    public IList<IncomingEmailRecipient> Cc { get; init; } = new List<IncomingEmailRecipient>();
    public IList<IncomingEmailRecipient> Bcc { get; init; } = new List<IncomingEmailRecipient>();

    public string Subject { get; set; } = string.Empty;

    public string TextContent { get; set; } = string.Empty;

    public string HtmlContent { get; set; } = string.Empty;

    public IList<EmailAttachment> Attachments { get; init; } = new List<EmailAttachment>();

    public IList<IncomingEmailHeader> Headers { get; init; } = new List<IncomingEmailHeader>();

    public IncomingEmailSpamInfo? SpamInfo { get; set; }

    public IncomingEmailSpfInfo? SpfInfo { get; set; }
}

public class IncomingEmailRecipient
{
    public string Email { get; set; } = string.Empty;

    public string Name { get; set; } = string.Empty;

    public override string ToString()
    {
        var name = string.IsNullOrWhiteSpace(Name)
            ? string.Empty
            : $"{Name} ";
        return $"{name}<{Email}>";
    }
}

public record class IncomingEmailHeader(string Name, string Value);

public class IncomingEmailSpamInfo
{
    public bool IsSpam { get; set; }

    public double? Score { get; set; }
}

public record class IncomingEmailSpfInfo(IncomingEmailSpfStatus Status, string Message);

public enum IncomingEmailSpfStatus
{
    Unknown,
    Neutral,
    Pass,
    SoftFail,
    Fail,
}
