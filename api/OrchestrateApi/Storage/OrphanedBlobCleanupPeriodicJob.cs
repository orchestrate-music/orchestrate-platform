using OrchestrateApi.Common;

namespace OrchestrateApi.Storage;

public class OrphanedBlobCleanupPeriodicJob(
    IAppConfiguration appConfig,
    TenantContextIterator tenantContextIterator,
    StorageService storageService) : IPeriodicJob
{
    public string Name => "OrphanedBlobCleanup";
    public TimeSpan RunPeriod { get; } = appConfig.OrphanedBlobCleanupInterval;

    public async Task Run(CancellationToken cancellationToken)
    {
        await tenantContextIterator.ForEachTenant(
            nameof(OrphanedBlobCleanupPeriodicJob),
            async (tenant) =>
            {
                await storageService.CleanupOrphanedBlobsAsync(cancellationToken);
            },
            cancellationToken);
    }
}
