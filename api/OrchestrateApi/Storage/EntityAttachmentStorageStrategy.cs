using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Storage;

public enum EntityAttachmentSensitivity
{
    /// <summary>Allowed to be read by users with read permissions</summary>
    Secured,
    ///<summary>Allowed to be read by users with write permissions</summary>
    Sensitive,
}

public abstract class EntityAttachmentStorageStrategy : LinkedBlobStorageStrategy<EntityAttachmentAccessData, EntityAttachmentLinkData>
{
    public abstract FeaturePermission SecuredReadPermission { get; }

    public abstract FeaturePermission WriteAndSensitiveReadPermission { get; }

    public override bool TryParseAccessDataTyped(
        JsonDocument rawData,
        [NotNullWhen(true)] out EntityAttachmentAccessData? accessData)
    {
        return StorageStrategyUtils.TryParseRequiredJsonData(rawData, out accessData);
    }

    public override bool UserCanRead(ClaimsPrincipal user, Guid tenantId, EntityAttachmentAccessData? accessData)
    {
        if (accessData is null)
        {
            return false;
        }

        var userPermissions = user.PermissionsInTenant(tenantId);
        var requiredPermission = accessData.Sensitivity switch
        {
            EntityAttachmentSensitivity.Secured => SecuredReadPermission,
            EntityAttachmentSensitivity.Sensitive => WriteAndSensitiveReadPermission,
            _ => FeaturePermission.None,
        };
        return userPermissions.Contains(requiredPermission);
    }

    public override bool UserCanWrite(ClaimsPrincipal user, Guid tenantId, EntityAttachmentAccessData? accessData)
    {
        return user.HasPermissionInTenant(tenantId, WriteAndSensitiveReadPermission);
    }

    public override Expression<Func<OrchestrateBlob, bool>> ReadableBlobsExpr(ClaimsPrincipal user, Guid tenantId)
    {
        var userPermissions = user.PermissionsInTenant(tenantId);
        // TODO Replace with LinqKit
        // TODO Duplicate of UserCanRead logic, can we remove if we implement #143?
        return e => userPermissions.Contains(
            e.StorageStrategy.AccessData.RootElement.GetProperty(nameof(EntityAttachmentAccessData.Sensitivity)).GetString()
                == nameof(EntityAttachmentSensitivity.Secured) ? SecuredReadPermission :
            e.StorageStrategy.AccessData.RootElement.GetProperty(nameof(EntityAttachmentAccessData.Sensitivity)).GetString()
                == nameof(EntityAttachmentSensitivity.Sensitive) ? WriteAndSensitiveReadPermission :
            FeaturePermission.None);
    }

    public override bool TryParseLinkDataTyped(JsonDocument rawData, [NotNullWhen(true)] out EntityAttachmentLinkData? linkData)
    {
        return StorageStrategyUtils.TryParseRequiredJsonData(rawData, out linkData);
    }
}

public class EntityAttachmentAccessData
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public required EntityAttachmentSensitivity Sensitivity { get; init; }
}

public class EntityAttachmentLinkData
{
    public required int EntityId { get; init; }
}
