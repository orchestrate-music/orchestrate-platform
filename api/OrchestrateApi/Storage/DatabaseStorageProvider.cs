using System.Data;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

public class DatabaseStorageProvider : IStorageProvider
{
    private readonly OrchestrateContext dbContext;

    public DatabaseStorageProvider(OrchestrateContext dbContext)
    {
        this.dbContext = dbContext;
    }

    private NpgsqlConnection Connection => (NpgsqlConnection)this.dbContext.Database.GetDbConnection();

    public async Task<IStreamableDisposible?> ReadBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken)
    {
        using var command = new NpgsqlCommand(@"SELECT bytes FROM blobs WHERE id = @id", Connection);
        command.Parameters.Add(new NpgsqlParameter
        {
            DbType = DbType.Guid,
            ParameterName = "@id",
            Value = blob.Id,
        });

        if (Connection.State != ConnectionState.Open)
        {
            await Connection.OpenAsync(cancellationToken);
        }

        // https://stackoverflow.com/a/58613644/17403538
        var reader = await command.ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken);
        if (await reader.ReadAsync(cancellationToken))
        {
            var stream = await reader.GetStreamAsync(0, cancellationToken);
            return new DbBlobStreamableDisposible(stream, reader);
        }
        else
        {
            return null;
        }
    }

    public async Task<long> WriteBlobAsync(OrchestrateBlob blob, Stream bytes, CancellationToken cancellationToken)
    {
        using var command = new NpgsqlCommand(@"UPDATE blobs SET bytes = @bytes WHERE id = @id", Connection);
        command.Parameters.Add(new NpgsqlParameter
        {
            DbType = DbType.Guid,
            ParameterName = "@id",
            Value = blob.Id,
        });

        command.Parameters.Add(new NpgsqlParameter
        {
            DbType = DbType.Binary,
            ParameterName = "@bytes",
            Value = bytes,
            Size = -1,
        });

        if (Connection.State != ConnectionState.Open)
        {
            await Connection.OpenAsync(cancellationToken);
        }

        var modifiedRows = await command.ExecuteNonQueryAsync(cancellationToken);
        return modifiedRows == 1
            ? bytes.Length
            : -1;
    }

    public Task<bool> DeleteBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken)
    {
        // Data will be deleted with the row
        return Task.FromResult(true);
    }

    private sealed class DbBlobStreamableDisposible : IStreamableDisposible
    {
        private readonly NpgsqlDataReader disposable;

        public DbBlobStreamableDisposible(Stream stream, NpgsqlDataReader dataReader)
        {
            ArgumentNullException.ThrowIfNull(stream);
            Stream = stream;
            this.disposable = dataReader;
        }

        public Stream Stream { get; set; }

        public void Dispose() => this.disposable?.Dispose();
    }
}
