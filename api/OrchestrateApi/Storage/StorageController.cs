using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Storage;

[ApiController]
[Route("blob")]
public class StorageController : ControllerBase
{
    public StorageController(
        OrchestrateContext dbContext,
        ITenantService tenantService,
        StorageService storageService,
        ILogger<StorageController> logger)
    {
        DbContext = dbContext;
        TenantService = tenantService;
        StorageService = storageService;
        Logger = logger;
    }

    private OrchestrateContext DbContext { get; }
    private ITenantService TenantService { get; }
    private StorageService StorageService { get; }
    private ILogger Logger { get; }

    // If this endpoint is hanging on the client side for a large file upload it is because
    // the HTTP server is closing the connection before the upload has finished.
    // This occurs on HTTP1.1 because there is no way to respond to the client until it has
    // finished transmitting its request. A fix for this is to add the `Expect: 100-Continue`
    // header, but we don't have control over this in the browser. HTTP2 doesn't have this
    // issue because it allows for streams to be inturrupted by the server. Locally we can't
    // use HTTP2 without a dev cert (perhaps we use this in future), but since we run on Fly.io
    // requests will be normalised to HTTP1.1 (to our server) anyway. We could just use Fly TLS
    // termination which would forward HTTP2 as clear text to our server, but then our server
    // has no way of knowing if the request is HTTP1 or 2 (since that negotiation occurs during
    // TLS negotiation). We could ONLY support HTTP2/clear-text however this then breaks support
    // for HTTP1 clients so we don't really want to do this. Since we check the size client side
    // anyway only malicous actors will hit this logic so don't bother handling any further.
    // See https://stackoverflow.com/a/13424453/17403538 and
    // https://community.fly.io/t/tls-proxy-protocol-how-to-set-http-2-alpn/4495
    [HttpPost]
    public async Task<ActionResult<IEnumerable<OrchestrateBlobMetadata>>> UploadBlobAsync(
        [FromForm] UploadBlobDto uploadDto,
        CancellationToken cancellationToken)
    {
        if (!TryGetUploadDtoStrategyAndData(uploadDto, out var storageStrategy, out var accessData, out var linkData))
        {
            return BadRequest("Invalid storage strategy, access data or link data");
        }
        if (storageStrategy.IsLinked() && linkData == null)
        {
            Logger.LogError("StorageStrategy ({StorageStrategy}) is linked but linkData is null. UploadDto: {UploadDto}", storageStrategy.Name, uploadDto);
            return StatusCode(500);
        }

        if (TenantService.TenantId is not Guid tenantId || !storageStrategy.UserCanWrite(User, tenantId, accessData))
        {
            return Forbid();
        }

        var blobs = new List<OrchestrateBlob>();
        var storedStorageStrategy = new StorageStrategyDto(storageStrategy.Name, accessData);

        // We could do this as a raw stream which doesn't buffer the uploaded file (which ASP.NET Core
        // does to disk if the file is >64K, see
        // https://docs.microsoft.com/en-us/aspnet/core/mvc/models/file-uploads?view=aspnetcore-6.0#file-upload-scenarios)
        // However, buffering the file gives us some benefits such as being able to stream the file to the DB instead
        // of pulling it into memory (see https://github.com/npgsql/npgsql/issues/4466, as we need the length to do
        // this which we don't have when streaming). Another benefit is that it is easier to check the file signature
        // to validate the file type off more than the extension or in future perhaps do virus scanning or that sort
        // of thing.
        // A sample implementation of streaming was previously implemented in a503a53e9f4ef6ad76725489fce162a773197563
        foreach (var file in Request.Form.Files)
        {
            var uploadedBlob = new UploadedFormFile(file);
            if (await storageStrategy.TryValidate(uploadedBlob, cancellationToken) is { } validationResult
                && !validationResult.IsValid)
            {
                return BadRequest($"Uploaded file {file.FileName} is not valid: {validationResult.Message}");
            }

            var blob = await StorageService.CreateBlobAsync(uploadedBlob, storedStorageStrategy, cancellationToken);
            blobs.Add(blob);

            if (storageStrategy.TryCreateLinkingEntity(blob.Id, linkData) is { } linkingEntity)
            {
                DbContext.Add(linkingEntity);
            }
        }

        await DbContext.SaveChangesAsync(cancellationToken);

        var metadata = blobs.Select(e => OrchestrateBlobMetadata.FromBlob(e)).ToList();
        return metadata;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<OrchestrateBlobMetadata>>> ListBlobsAsync(
        [FromQuery] ListBlobsDto listBlobsDto, CancellationToken cancellationToken)
    {
        var storageStrategy = StorageService.GetStorageStrategy(listBlobsDto.StorageStrategyName);
        var linkData = !string.IsNullOrWhiteSpace(listBlobsDto.LinkData) ? JsonDocument.Parse(listBlobsDto.LinkData) : null;
        if (storageStrategy == null || !storageStrategy.TryParseOptionalLinkData(linkData, out var parsedLinkData))
        {
            return BadRequest();
        }

        var blobIds = storageStrategy.LinkedBlobIds(DbContext, parsedLinkData)
            ?? DbContext.Blobs.Where(e => e.StorageStrategy.Name == storageStrategy.Name).Select(e => e.Id);
        var blobs = await StorageService.GetReadableBlobsAsync(User, blobIds.AsEnumerable(), cancellationToken);
        return blobs.Select(e => OrchestrateBlobMetadata.FromBlob(e)).ToList();
    }

    [HttpGet("{blobId}/file")]
    public async Task<IActionResult> GetBlobContentAsync(Guid blobId, CancellationToken cancellationToken)
    {
        if ((await StorageService.GetReadableBlobAsync(User, blobId, cancellationToken)) is not OrchestrateBlob blob)
        {
            return NotFound();
        }

        var blobStream = await StorageService.ReadBlobAsync(blob, cancellationToken);
        if (blobStream == null)
        {
            return NotFound();
        }

        return new DisposingFileStreamResult(blob, blobStream);
    }

    [HttpPut("{blobId}/access-data")]
    public async Task<IActionResult> UpdateAccessDataAsync(
        Guid blobId, [FromBody] JsonDocument rawAccessData, CancellationToken cancellationToken)
    {
        var blob = await StorageService.GetWritableBlobAsync(User, blobId, cancellationToken);
        if (blob == null)
        {
            return NotFound();
        }

        if (StorageService.GetStorageStrategy(blob.StorageStrategy.Name) is not { } provider
            || !provider.TryParseAccessData(rawAccessData, out var accessData))
        {
            return BadRequest("Invalid storage strategy or access data");
        }

        if (!TenantService.TenantId.HasValue
            || !provider.UserCanWrite(User, TenantService.TenantId.Value, accessData))
        {
            return Forbid();
        }

        var dto = new StorageStrategyDto(provider.Name, accessData);
        await StorageService.UpdateBlobStorageStrategyAsync(blob, dto, cancellationToken);

        return Ok();
    }

    [HttpDelete("{blobId}")]
    public async Task<IActionResult> DeleteBlobAsync(Guid blobId, CancellationToken cancellationToken)
    {
        if ((await StorageService.GetWritableBlobAsync(User, blobId, cancellationToken)) is not OrchestrateBlob blob)
        {
            return NotFound();
        }

        await StorageService.DeleteBlobAsync(blob, cancellationToken);
        return Ok();
    }

    private bool TryGetUploadDtoStrategyAndData(
        UploadBlobDto uploadDto,
        [NotNullWhen(true)] out IBlobStorageStrategy? storageStrategy,
        out object? accessData,
        out object? linkData)
    {
        storageStrategy = StorageService.GetStorageStrategy(uploadDto.StorageStrategyName);
        accessData = null;
        linkData = null;

        if (storageStrategy == null)
        {
            Logger.LogError("Unknown StorageStrategy ({StorageStrategy})", uploadDto.StorageStrategyName);
            return false;
        }

        using var rawAccessData = JsonDocument.Parse(uploadDto.AccessData);
        if (!storageStrategy.TryParseAccessData(rawAccessData, out accessData))
        {
            Logger.LogError("Invalid AccessData: {AccessData}", uploadDto.AccessData);
            return false;
        }

        using var rawLinkData = uploadDto.LinkData != null ? JsonDocument.Parse(uploadDto.LinkData) : null;
        if (!storageStrategy.TryParseRequiredLinkData(rawLinkData, out linkData))
        {
            Logger.LogError("Invalid LinkData: {LinkData}", uploadDto.LinkData);
            return false;
        }

        return true;
    }

    private class DisposingFileStreamResult : FileStreamResult
    {
        private readonly IStreamableDisposible streamableDisposible;

        public DisposingFileStreamResult(OrchestrateBlob blob, IStreamableDisposible streamableDisposible)
            : base(streamableDisposible.Stream, blob.MimeType)
        {
            ArgumentNullException.ThrowIfNull(streamableDisposible);
            this.streamableDisposible = streamableDisposible;
            FileDownloadName = blob.Name;
            LastModified = blob.Modified;
        }

        public override void ExecuteResult(ActionContext context)
        {
            base.ExecuteResult(context);
            streamableDisposible.Dispose();
        }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            await base.ExecuteResultAsync(context);
            streamableDisposible.Dispose();
        }
    }
}

// Because these are populated in [FromQuery] and [FromForm] we can't use JsonDocument
// for the data properties.
public class UploadBlobDto
{
    public string StorageStrategyName { get; set; } = null!;

    public string AccessData { get; set; } = null!;

    public string? LinkData { get; set; }
}

public class ListBlobsDto
{
    public string StorageStrategyName { get; set; } = null!;

    public string? LinkData { get; set; }
}
