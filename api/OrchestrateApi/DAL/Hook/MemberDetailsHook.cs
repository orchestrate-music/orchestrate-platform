using Breeze.Persistence;
using Breeze.Persistence.EFCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.DAL.Hook
{
    public class MemberDetailHookFactory : EntityHookFactory<MemberDetails>
    {
        private readonly UserService userService;

        public MemberDetailHookFactory(UserService userService)
        {
            this.userService = userService;
        }

        public override IEntityHook<MemberDetails> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
        {
            return new MemberDetailHook
            {
                PersistenceManager = persistenceManager,
                SaveMap = saveMap,
                UserService = this.userService,
            };
        }
    }

    public class MemberDetailHook : EntityHook<MemberDetails>
    {
        public required UserService UserService { get; init; }

        public override IEnumerable<EntityError> Validate(IEntityInfo<MemberDetails> entityInfo) => Enumerable.Empty<EntityError>();

        public override async Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<MemberDetails>> entityInfos, CancellationToken cancellationToken)
        {
            var errors = new List<EntityError>();

            var entityInfosToCheck = entityInfos
                .Where(e => e.EntityState == EntityState.Added || (e.EntityState == EntityState.Modified && e.PropertyWasModified(e => e.Email)));
            foreach (var entityInfo in entityInfosToCheck)
            {
                // Ideally would be a check against a NormalisedEmail like we have for users, but this will work too
                var memberWithEmailAlreadyExists = await Context.Member.AnyAsync(
                    e => EF.Functions.ILike(e.Details!.Email!, entityInfo.Entity.Email!),
                    cancellationToken);
                if (memberWithEmailAlreadyExists)
                {
                    errors.Add(GenerateEmailError(
                        entityInfo,
                        "There is already a member with this email address"));
                }
            }

            return errors;
        }

        public async override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<MemberDetails>> entityInfos, CancellationToken cancellationToken)
        {
            var errors = new List<EntityError>();

            var entityInfosToProcess = entityInfos
                .WithState(EntityState.Modified)
                .Where(e => e.Entity.UserId != null && e.PropertyWasModified(p => p.Email));
            foreach (var entityInfo in entityInfosToProcess)
            {
                var user = await Context.Users.SingleAsync(e => e.Id == entityInfo.Entity.UserId, cancellationToken);
                if (user.Email != entityInfo.Entity.Email)
                {
                    var result = await UserService.UpdateUserEmail(user, entityInfo.Entity.Email);
                    if (!result.Succeeded)
                    {
                        var errorText = string.Join(", ", result.Errors.Select(GenerateErrorText));
                        errors.Add(GenerateEmailError(entityInfo, errorText));
                    }
                }
            }

            return errors;
        }

        private static EFEntityError GenerateEmailError(IEntityInfo<MemberDetails> entityInfo, string errorMessage)
        {
            return new EFEntityError(
                entityInfo.Raw,
                "ConflictingEmail",
                errorMessage,
                nameof(MemberDetails.Email));
        }

        private static string GenerateErrorText(IdentityError error)
        {
            // error.Code from https://github.com/dotnet/aspnetcore/blob/main/src/Identity/Extensions.Core/src/IdentityErrorDescriber.cs
            return error.Code switch
            {
                nameof(IdentityErrorDescriber.DuplicateEmail) => "There is already an existing login with this email",
                _ => error.Description,
            };
        }
    }
}
