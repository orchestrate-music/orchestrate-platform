using Breeze.Persistence;
using Breeze.Persistence.EFCore;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using EntityState = Breeze.Persistence.EntityState;

namespace OrchestrateApi.DAL.Hook
{
    public class MemberHookFactory : EntityHookFactory<Member>
    {
        private readonly UserService userService;

        public MemberHookFactory(UserService userService)
        {
            this.userService = userService;
        }

        public override IEntityHook<Member> BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
        {
            return new MemberHook
            {
                PersistenceManager = persistenceManager,
                SaveMap = saveMap,
                UserService = this.userService,
            };
        }
    }

    public class MemberHook : EntityHook<Member>
    {
        public required UserService UserService { get; init; }

        public override IEnumerable<EntityError> Validate(IEntityInfo<Member> entityInfo)
        {
            var errors = new List<EntityError>();

            if (entityInfo.PropertyWasModified(m => m.DateLeft)
                && SaveMap.EntitiesOfType<EnsembleMembership>().Any(m => m.MemberId == entityInfo.Entity.Id))
            {
                errors.Add(new EFEntityError(
                    entityInfo.Raw,
                    "InvalidSaveData",
                    "Date Left cannot be altered when also modifying ensemble membership",
                    nameof(Member.DateLeft)));
            }

            return errors;
        }

        public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<Member>> entityInfos, CancellationToken cancellationToken)
            => Task.FromResult(Enumerable.Empty<EntityError>());

        public async override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<Member>> entityInfos, CancellationToken cancellationToken)
        {
            foreach (var entityInfo in entityInfos.Where(e => e.EntityState == EntityState.Modified))
            {
                if (entityInfo.TryGetChangedValue(m => m.DateLeft, out var dateLeft))
                {
                    var matchingMemberships = await Context.EnsembleMembership
                        .Where(i => i.MemberId == entityInfo.Entity.Id)
                        .Where(i => i.DateLeft == null || i.DateLeft == dateLeft)
                        .ToListAsync(cancellationToken);
                    foreach (var membership in matchingMemberships)
                    {
                        membership.DateLeft = dateLeft;
                        ReturnToClient(membership, EntityState.Modified);
                    }
                }
            }

            return Enumerable.Empty<EntityError>();
        }
    }
}
