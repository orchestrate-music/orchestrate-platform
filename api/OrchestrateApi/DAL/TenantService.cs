namespace OrchestrateApi.DAL
{
    public interface ITenantService
    {
        Guid? TenantId { get; }

        IDisposable ForceOverrideTenantId(Guid? tenantId);
    }

    public class TenantServiceBase : ITenantService
    {
        public Guid? TenantId { get; protected set; }

        public IDisposable ForceOverrideTenantId(Guid? tenantId) => new TenantOverride(this, tenantId);

        private sealed class TenantOverride : IDisposable
        {
            public TenantOverride(TenantServiceBase tenantService, Guid? newTenantId)
            {
                TenantService = tenantService;
                OldTenantId = TenantService.TenantId;
                TenantService.TenantId = newTenantId;
            }

            private TenantServiceBase TenantService { get; }
            private Guid? OldTenantId { get; }

            public void Dispose()
            {
                TenantService.TenantId = OldTenantId;
            }
        }
    }

    public class StaticTenantService : TenantServiceBase
    {
        public StaticTenantService(Guid tenantId)
        {
            TenantId = tenantId;
        }
    }

    public class HttpContextTenantService : TenantServiceBase
    {
        public HttpContextTenantService(IHttpContextAccessor contextAccessor)
        {
            TenantId = contextAccessor.HttpContext?.GetTenantId();
        }
    }

    public static class TenantedHttpContextExtensions
    {
        internal const string TenantIdHeader = "X-Tenant-Id";

        public static Guid? GetTenantId(this HttpContext context)
        {
            return TryGetTenantId(context, out var tenantId)
                ? tenantId
                : null;
        }

        public static bool TryGetTenantId(this HttpContext context, out Guid tenantId)
        {
            if (context?.Request?.Headers is IHeaderDictionary headers
                && headers.TryGetValue(TenantIdHeader, out var tenantHeaders)
                && Guid.TryParse(tenantHeaders[0], out tenantId))
            {
                return true;
            }
            else
            {
                tenantId = Guid.Empty;
                return false;
            }

        }
    }
}
