using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;
using OrchestrateApi.DAL.Access;

namespace OrchestrateApi.DAL
{
    public class CustomQueryTranslationPreprocessorFactory : IQueryTranslationPreprocessorFactory
    {
        public CustomQueryTranslationPreprocessorFactory(
            QueryTranslationPreprocessorDependencies dependencies,
            RelationalQueryTranslationPreprocessorDependencies relationalDependencies)
        {
            Dependencies = dependencies;
            RelationalDependencies = relationalDependencies;
        }
        protected QueryTranslationPreprocessorDependencies Dependencies { get; }
        protected RelationalQueryTranslationPreprocessorDependencies RelationalDependencies { get; }
        public QueryTranslationPreprocessor Create(QueryCompilationContext queryCompilationContext)
        {
            return new CustomQueryTranslationPreprocessor(
                Dependencies,
                RelationalDependencies,
                queryCompilationContext);
        }
    }

    public class CustomQueryTranslationPreprocessor : RelationalQueryTranslationPreprocessor
    {
        public CustomQueryTranslationPreprocessor(
            QueryTranslationPreprocessorDependencies dependencies,
            RelationalQueryTranslationPreprocessorDependencies relationalDependencies,
            QueryCompilationContext queryCompilationContext)
            : base(dependencies, relationalDependencies, queryCompilationContext)
        {
        }

        public override Expression Process(Expression query)
        {
            query = base.Process(query);

            // TODO This can only handle secured query expressions that reference the entities direct
            // properties. It would be good to update this to properly filter against even references
            // to other tables (but only for security queries, any other joins should be filtered)
            if (SecuredQueryData.TryParseFromQueryTags(QueryCompilationContext.Tags, out var data))
            {
                var visitor = new SecuredQueryExpressionVisitor(EntityAccessorRegistrar.Instance, data);
                query = visitor.Visit(query);
            }

            return query;
        }
    }
}
