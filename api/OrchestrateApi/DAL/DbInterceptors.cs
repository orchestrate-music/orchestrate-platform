using System.Collections.Concurrent;
using System.Data.Common;
using Microsoft.EntityFrameworkCore.Diagnostics;
using OrchestrateApi.Common;

namespace OrchestrateApi.DAL;

public abstract class OrchestrateDbInterceptor : IDisposable
{
    private record Entry(int Count, DateTime LastSeen);

    private readonly IAppConfiguration config;
    private readonly ILogger logger;

    private readonly ConcurrentDictionary<Guid, Entry> entries = new();
    private readonly Timer ageOutTimer;
    private bool disposedValue;

    protected OrchestrateDbInterceptor(IAppConfiguration config, ILogger<OrchestrateDbInterceptor> logger)
    {
        this.config = config;
        this.logger = logger;

        this.ageOutTimer = new Timer(AgeOutEntries, null, AgeOutPeriod, AgeOutPeriod);
    }

    protected abstract string CounterKeyName { get; }
    private TimeSpan AgeOutPeriod => this.config.DbTransientFailureTrackingAgeOutPeriod;
    private int RetryCount => this.config.DbMaxTransientFailureRetryCount;

    protected void LogFailure(Guid key, Exception e)
    {
        var failureCount = GetAndIncrement(key);
        var level = failureCount > RetryCount
            ? LogLevel.Error
            : LogLevel.Information;
        logger.Log(
            level,
            e,
            "{KeyName} {Key} has failed {FailureCount} times (maximum allowed: {RetryCount})",
            CounterKeyName,
            key,
            failureCount,
            config.DbMaxTransientFailureRetryCount);
    }

    private int GetAndIncrement(Guid key)
    {
        var (count, _) = this.entries.AddOrUpdate(
            key,
            new Entry(1, DateTime.UtcNow),
            (_, entry) => new Entry(entry.Count + 1, DateTime.UtcNow));
        return count;
    }

    private void AgeOutEntries(object? state)
    {
        var agedOutKvps = this.entries
            .Select(kvp => Tuple.Create(kvp.Key, DateTime.UtcNow - kvp.Value.LastSeen))
            .Where((tuple) => tuple.Item2 > AgeOutPeriod);
        foreach (var (key, lastSeenAgo) in agedOutKvps)
        {
            this.entries.Remove(key, out _);
            logger.LogInformation(
                "Aged out {KeyName} {Key}. Was last seen {LastSeenMinAgo} minutes ago",
                CounterKeyName,
                key,
                lastSeenAgo.TotalMinutes);
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                this.ageOutTimer.Dispose();
            }

            disposedValue = true;
        }
    }
}

public sealed class OrchestrateDbConnectionInterceptor(
    IAppConfiguration config,
    ILogger<OrchestrateDbConnectionInterceptor> logger)
    : OrchestrateDbInterceptor(config, logger), IDbConnectionInterceptor
{
    protected override string CounterKeyName => "Connection";

    public Task ConnectionFailedAsync(DbConnection connection, ConnectionErrorEventData eventData, CancellationToken cancellationToken)
    {
        LogFailure(eventData.ConnectionId, eventData.Exception);
        return Task.CompletedTask;
    }

    public void ConnectionFailed(DbConnection connection, ConnectionErrorEventData eventData)
    {
        LogFailure(eventData.ConnectionId, eventData.Exception);
    }
}

public sealed class OrchestrateDbCommandInterceptor(
    IAppConfiguration config,
    ILogger<OrchestrateDbCommandInterceptor> logger)
    : OrchestrateDbInterceptor(config, logger), IDbCommandInterceptor
{
    protected override string CounterKeyName => "Command";

    public Task CommandFailedAsync(DbCommand command, CommandErrorEventData eventData, CancellationToken cancellationToken)
    {
        LogFailure(eventData.CommandId, eventData.Exception);
        return Task.CompletedTask;
    }

    public void CommandFailed(DbCommand command, CommandErrorEventData eventData)
    {
        LogFailure(eventData.CommandId, eventData.Exception);
    }
}
