using System.Collections.Concurrent;

namespace OrchestrateApi.DAL.Access
{
    public interface IEntityAccessorRegistrar
    {
        IEntityAccessor GetEntityAccessor(Type entityType);
    }

    public class EntityAccessorRegistrar : IEntityAccessorRegistrar
    {
        // Bit of a horrible hack to expose it statically like this but we need to be able to use it
        // in the CustomQueryTranslationPreprocessor which doesn't have access to the same
        // ServiceCollection as normal services
        public static IEntityAccessorRegistrar Instance { get; } = new EntityAccessorRegistrar();

        private static ConcurrentDictionary<Type, IEntityAccessor> EntityAccessors { get; } = new(
            Array.Empty<IEntityAccessor>()
                .Concat(TenantEntityAccess.Accessors)
                .Concat(MemberBillingEntityAccess.Accessors)
                .Concat(MemberEntityAccess.Accessors)
                .Concat(EnsembleEntityAccess.Accessors)
                .Concat(RoleEntityAccess.Accessors)
                .ToDictionary(a => a.Type, a => a));

        public IEntityAccessor GetEntityAccessor(Type entityType)
        {
            return EntityAccessors.GetOrAdd(entityType, t =>
            {
                var builderType = typeof(EntityAccessorBuilder<>).MakeGenericType(t);
                var builder = (IDefaultEntityAccessorBuilder)Activator.CreateInstance(builderType)!;
                return builder.BuildDefault();
            });
        }
    }
}
