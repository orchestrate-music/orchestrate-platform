using System.Linq.Expressions;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public interface IDefaultEntityAccessorBuilder
    {
        IEntityAccessor BuildDefault();
    }

    public class EntityAccessorBuilder<T> : IDefaultEntityAccessorBuilder
    {
        private readonly BuiltEntityAccessor entityAccessor = new();

        public IEntityAccessor BuildDefault()
        {
            return CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformAccess))
                .CanReadAllRows()
                // The only place we can't really use a specific permission as this could
                // be used across any entity types. Not too worried about this as the
                // intention is to remove this access mechanism in favour of controller
                // based security eventually.
                .CanWriteToTableWhen(queryData => queryData.HasAtLeastOneRole(StandardRole.StandardEditors))
                .Build();
        }

        public EntityAccessorBuilder<T> CanReadTableWhen(Func<SecuredQueryData, bool> when)
        {
            this.entityAccessor.CanReadTableFunc = when;
            return this;
        }

        public EntityAccessorBuilder<T> CanReadAllRows()
        {
            return CanReadRowWhen(_ => null);
        }

        public EntityAccessorBuilder<T> CanReadRowWhen(Func<SecuredQueryData, Expression<Func<T, bool>>?> buildWhen)
        {
            this.entityAccessor.CanReadRowFunc = buildWhen;
            return this;
        }

        public EntityAccessorBuilder<T> CanWriteToTableWhen(Func<SecuredQueryData, bool> when)
        {
            this.entityAccessor.CanWriteToTableFunc = when;
            return this;
        }

        public EntityAccessor<T> Build()
        {
            return this.entityAccessor;
        }

        private class BuiltEntityAccessor : EntityAccessor<T>
        {
            public Func<SecuredQueryData, bool> CanReadTableFunc { get; set; } = (_) => false;
            public Func<SecuredQueryData, Expression<Func<T, bool>>?> CanReadRowFunc { get; set; } = (_) => (_) => false;
            public Func<SecuredQueryData, bool> CanWriteToTableFunc { get; set; } = (_) => false;

            public override bool CanReadTable(SecuredQueryData queryData) => CanReadTableFunc(queryData);

            public override Expression<Func<T, bool>>? CanReadRow(SecuredQueryData queryData) => CanReadRowFunc(queryData);

            public override bool CanWriteToTable(SecuredQueryData queryData) => CanWriteToTableFunc(queryData);
        }
    }
}
