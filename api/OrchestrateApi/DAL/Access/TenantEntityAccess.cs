using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.DAL.Access
{
    public static class TenantEntityAccess
    {
        public static IEnumerable<IEntityAccessor> Accessors { get; } = new List<IEntityAccessor>
        {
            new EntityAccessorBuilder<Tenant>()
                .CanReadTableWhen(queryData => queryData.HasPermission(FeaturePermission.PlatformAccess))
                .CanReadAllRows()
                .CanWriteToTableWhen(queryData => queryData.HasPermission(FeaturePermission.SettingsWrite))
                .Build(),
        };
    }
}
