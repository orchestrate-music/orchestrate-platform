using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using Breeze.Persistence;

namespace OrchestrateApi.DAL
{
    public interface IEntityInfo<T>
    {
        EntityInfo Raw { get; }
        T Entity { get; }
        EntityState EntityState { get; set; }
        Dictionary<string, object?> OriginalValuesMap { get; }
        Dictionary<string, object?> UnmappedValuesMap { get; }
    }

    public class EntityInfo<T> : IEntityInfo<T>
    {
        public EntityInfo(EntityInfo entityInfo)
        {
            if (entityInfo.Entity is not T)
            {
                throw new ArgumentException($"Expected EntityInfo of type {typeof(T).Name}");
            }

            Raw = entityInfo;
        }

        public EntityInfo Raw { get; }

        public T Entity => (T)Raw.Entity;

        public EntityState EntityState
        {
            get => Raw.EntityState;
            set => Raw.EntityState = value;
        }

        public Dictionary<string, object?> OriginalValuesMap
        {
            get => Raw.OriginalValuesMap;
            set => Raw.OriginalValuesMap = value;
        }

        public Dictionary<string, object?> UnmappedValuesMap
        {
            get => Raw.UnmappedValuesMap;
            set => Raw.OriginalValuesMap = value;
        }
    }

    public static class TypedEntityInfoExtensions
    {
        public static bool PropertyWasModified<T, TVal>(this IEntityInfo<T> entityInfo, Expression<Func<T, TVal>> propertyExpression)
        {
            if (propertyExpression.Body is not MemberExpression me || me.Member.DeclaringType != typeof(T))
            {
                throw new ArgumentException($"You must specify a property of {typeof(T).Name} in the expression");
            }

            return entityInfo.OriginalValuesMap.ContainsKey(me.Member.Name);
        }

        public static bool TryGetChangedValue<T, TVal>(this IEntityInfo<T> entityInfo, Expression<Func<T, TVal>> propertyExpression, [NotNullWhen(true)] out TVal? value)
        {
            if (!entityInfo.PropertyWasModified(propertyExpression))
            {
                value = default;
                return false;
            }

            // TODO Cache?
            value = propertyExpression.Compile()(entityInfo.Entity);
            return value is not null;
        }

        public static IEnumerable<IEntityInfo<T>> WithState<T>(this IEnumerable<IEntityInfo<T>> entityInfos, EntityState entityState)
        {
            return entityInfos.Where(e => e.EntityState == entityState);
        }
    }
}
