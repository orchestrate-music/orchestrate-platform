using System.Linq.Expressions;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Query;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.DAL
{
    public partial class OrchestrateContext
        : IdentityDbContext<
            OrchestrateUser,
            OrchestrateRole,
            Guid,
            IdentityUserClaim<Guid>,
            OrchestrateUserRole,
            IdentityUserLogin<Guid>,
            IdentityRoleClaim<Guid>,
            IdentityUserToken<Guid>>,
        IDataProtectionKeyContext
    {
        private readonly ITenantService tenantService;
        private readonly ILogger<OrchestrateContext> logger;
        private readonly IEnumerable<IInterceptor> interceptors;

        public OrchestrateContext(
            DbContextOptions<OrchestrateContext> options,
            ITenantService tenantService,
            ILogger<OrchestrateContext> logger,
            IEnumerable<IInterceptor> interceptors)
            : base(options)
        {
            this.tenantService = tenantService;
            this.logger = logger;
            this.interceptors = interceptors;

            ChangeTracker.StateChanged += SetEntityTenantId;
            ChangeTracker.Tracked += SetEntityTenantId;
        }

        public virtual DbSet<DataProtectionKey> DataProtectionKeys { get; set; } = null!;
        public virtual DbSet<PeriodicJobRecord> PeriodicJobRecord { get; set; } = null!;

        public virtual DbSet<PlanMetadata> PlanMetadata { get; set; } = null!;
        public virtual DbSet<PaidPlanMetadata> PaidPlanMetadata { get; set; } = null!;

        public virtual DbSet<Tenant> Tenant { get; set; } = null!;
        public virtual DbSet<OrchestrateBlob> Blobs { get; set; } = null!;

        public virtual DbSet<Asset> Asset { get; set; } = null!;
        public virtual DbSet<AssetLoan> AssetLoan { get; set; } = null!;
        public virtual DbSet<Concert> Concert { get; set; } = null!;
        public virtual DbSet<Ensemble> Ensemble { get; set; } = null!;
        public virtual DbSet<EnsembleMembership> EnsembleMembership { get; set; } = null!;
        public virtual DbSet<EnsembleRole> EnsembleRole { get; set; } = null!;
        public virtual DbSet<Member> Member { get; set; } = null!;
        public virtual DbSet<MemberInstrument> MemberInstrument { get; set; } = null!;
        public virtual DbSet<Performance> Performance { get; set; } = null!;
        public virtual DbSet<PerformanceMember> PerformanceMember { get; set; } = null!;
        public virtual DbSet<PerformanceScore> PerformanceScore { get; set; } = null!;
        public virtual DbSet<Score> Score { get; set; } = null!;
        public virtual DbSet<Contact> Contact { get; set; } = null!;

        public virtual DbSet<MemberBillingConfig> MemberBillingConfig { get; set; } = null!;
        public virtual DbSet<MemberBillingPeriodConfig> MemberBillingPeriodConfig { get; set; } = null!;
        public virtual DbSet<MemberBillingPeriod> MemberBillingPeriod { get; set; } = null!;
        public virtual DbSet<MemberInvoice> MemberInvoice { get; set; } = null!;
        public virtual DbSet<MemberInvoiceLineItem> MemberInvoiceLineItem { get; set; } = null!;

        public virtual DbSet<MailingList> MailingList { get; set; } = null!;
        public virtual DbSet<MailingListMessageMetadata> MailingListMessageMetadata { get; set; } = null!;

        public ValueTask<Tenant?> GetTenantAsync(CancellationToken cancellationToken = default)
        {
            return this.tenantService.TenantId.HasValue
                ? Tenant.FindAsync(new object[] { this.tenantService.TenantId.Value }, cancellationToken)
                : ValueTask.FromResult<Tenant?>(null);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // Reference & Platform data
            builder.Entity<PlanMetadata>(entity =>
            {
                entity.HasKey(i => i.Type);
                entity.Property(i => i.Type).HasConversion<string>();
            });

            builder.Entity<PeriodicJobRecord>(entity =>
            {
                entity.OwnsOne(i => i.InProgress);
                entity.OwnsOne(i => i.Completed, completed =>
                {
                    completed.Property(i => i.Result).HasConversion<string>();
                });
            });

            // Tenancy
            builder.Entity<Tenant>(entity =>
            {
                entity.HasIndex(i => i.Name).IsUnique();
                entity.HasIndex(i => i.Abbreviation).IsUnique();

                entity.HasQueryFilter(e => e.Id == this.tenantService.TenantId);

                entity.OwnsOne(i => i.Plan, plan =>
                {
                    plan.Property(e => e.Type).HasConversion<string>();
                });
            });

            var tenantedEntities = builder.Model.GetEntityTypes()
                .Where(e => e.ClrType != typeof(Tenant) && e.ClrType.IsAssignableTo(typeof(ITenanted)));
            Expression<Func<ITenanted, bool>> genericFilter = e => EF.Property<Guid>(e, ITenanted.Column) == this.tenantService.TenantId;
            foreach (var entity in tenantedEntities)
            {
                builder.Entity(entity.ClrType)
                    .Property<Guid>(ITenanted.Column)
                    .IsRequired()
                    .HasColumnOrder(0);
                builder.Entity(entity.ClrType)
                    .HasOne(typeof(Tenant))
                    .WithMany()
                    .HasForeignKey(ITenanted.Column)
                    .OnDelete(DeleteBehavior.Cascade);

                // Adapted from https://docs.microsoft.com/en-us/answers/questions/658905/apply-common-configuration-to-all-entity-types-ef.html
                var clrParameter = Expression.Parameter(entity.ClrType);
                var body = ReplacingExpressionVisitor.Replace(genericFilter.Parameters.First(), clrParameter, genericFilter.Body);
                builder.Entity(entity.ClrType).HasQueryFilter(Expression.Lambda(body, clrParameter));
            }

            // Identity, has to be after Tenancy due to UserRole dependency
            builder.Entity<OrchestrateUserRole>(entity =>
            {
                entity.HasKey(ITenanted.Column, nameof(OrchestrateUserRole.UserId), nameof(OrchestrateUserRole.RoleId));
            });

            // Plain entities
            builder.Entity<OrchestrateBlob>(entity =>
            {
                entity.OwnsOne(e => e.StorageStrategy);
                entity.HasOne<OrchestrateBlobBackingStorage>()
                    .WithOne()
                    .HasForeignKey<OrchestrateBlobBackingStorage>(e => e.Id);
            });
            builder.Entity<OrchestrateBlobBackingStorage>(entity =>
            {
                // Do table splitting so we don't fetch the bytes for normal queries
                // https://docs.microsoft.com/en-us/ef/core/modeling/table-splitting
                entity.ToTable(builder.Entity<OrchestrateBlob>().Metadata.GetTableName());
            });

            builder.Entity<AssetAttachment>(entity =>
            {
                entity.HasIndex(e => new { e.AssetId, e.BlobId })
                    .IsUnique();
                entity.HasOne<OrchestrateBlob>()
                    .WithMany()
                    .HasForeignKey(e => e.BlobId);
            });

            builder.Entity<Ensemble>(entity =>
            {
                entity.HasTenantedIndex(i => i.Name)
                    .IsUnique();
                entity.Property(i => i.Status)
                    .HasDefaultValue(EnsembleStatus.Active)
                    .HasConversion<string>();
            });
            builder.Entity<EnsembleMembership>(entity =>
            {
                entity.Property(i => i.Type)
                    .HasDefaultValue(MembershipType.Regular)
                    .HasConversion<string>();
            });

            builder.Entity<Member>(entity =>
            {
                entity.HasOne(m => m.Details)
                    .WithOne(d => d.Member)
                    .IsRequired();
            });
            // We could implement this as an OwnsOne() relationship in Member,
            // but that basically stops us doing independent security (using breeze)
            // on this entity so do it as a completely seperate entity.
            // We can refactor when we move away from Breeze
            builder.Entity<MemberDetails>(entity =>
            {
                entity.HasTenantedIndex(i => i.Email)
                    .IsUnique();
                entity.Property(i => i.FeeClass)
                    .HasDefaultValue(FeeClass.Full)
                    .HasConversion<string>();
            });

            builder.Entity<PerformanceMember>()
                .HasIndex(e => new { e.MemberId, e.PerformanceId })
                .IsUnique();

            builder.Entity<PerformanceScore>()
                .HasIndex(e => new { e.PerformanceId, e.ScoreId })
                .IsUnique();
            builder.Entity<ScoreAttachment>(entity =>
            {
                entity.HasIndex(e => new { e.ScoreId, e.BlobId })
                    .IsUnique();
                entity.HasOne<OrchestrateBlob>()
                    .WithMany()
                    .HasForeignKey(e => e.BlobId);
            });

            builder.Entity<Concert>(entity =>
            {
                entity.HasOne<OrchestrateBlob>()
                    .WithMany()
                    .HasForeignKey(e => e.CoverPhotoBlobId);
            });
            builder.Entity<ConcertAttachment>(entity =>
            {
                entity.HasIndex(e => new { e.ConcertId, e.BlobId })
                    .IsUnique();
                entity.HasOne<OrchestrateBlob>()
                    .WithMany()
                    .HasForeignKey(e => e.BlobId);
            });

            builder.Entity<MemberBillingPeriod>(entity =>
            {
                entity.Property(i => i.State)
                    .HasDefaultValue(MemberBillingPeriodState.Created)
                    .HasConversion<string>();
            });
            builder.Entity<MemberInvoice>(entity =>
            {
                entity.Property(i => i.SendStatus)
                    .HasConversion<string>();
            });

            builder.Entity<MailingList>(entity =>
            {
                entity.Property(e => e.AllowedSenders).HasConversion<string>();
                entity.Property(e => e.ReplyTo).HasConversion<string>();

                entity.HasTenantedIndex(e => e.Name).IsUnique();
                entity.HasTenantedIndex(e => e.Slug).IsUnique();

                entity.HasMany(e => e.Ensembles).WithMany().UsingEntity<MailingListEnsemble>();
                entity.HasMany(e => e.Members).WithMany().UsingEntity<MailingListMember>();
                entity.HasMany(e => e.Contacts).WithMany().UsingEntity<MailingListContact>();
                entity.HasMany(e => e.Messages).WithOne(e => e.MailingList);
            });

            OnModelCreatingPartial(builder);

            // Use SnakeCase for database naming. Adapted from:
            // https://andrewlock.net/customising-asp-net-core-identity-ef-core-naming-conventions-for-postgresql/#replacing-the-default-conventions-with-snake-case
            // Is an alternative to https://github.com/efcore/EFCore.NamingConventions which doesn't seem to
            // handle EF Identity Core and also is a dependency I'd rather not rely on
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                // Replace ALL table names first, to make sure all references in the properties
                // are correct (otherwise we hit strange errors due to the alphanumeric ordering
                // of our class names!!)
                entity.SetTableName(entity.GetTableName().ToSnakeCase());
            }

            foreach (var entity in builder.Model.GetEntityTypes())
            {
                // Replace column names
                var identifier = StoreObjectIdentifier.Create(entity, StoreObjectType.Table);
                if (identifier.HasValue)
                {
                    foreach (var property in entity.GetProperties())
                    {
                        property.SetColumnName(property.GetColumnName(identifier.Value).ToSnakeCase());
                    }
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetDatabaseName(index.GetDatabaseName().ToSnakeCase());
                }
            }

            builder.HasDbFunction(typeof(OrchestrateContext).GetMethod(nameof(GenUuid))!)
                .HasName("gen_random_uuid");
        }

        private void SetEntityTenantId(object? sender, EntityEntryEventArgs e)
        {
            if (e.Entry.Entity is ITenanted && e.Entry.State == EntityState.Added)
            {
                e.Entry.Property(ITenanted.Column).CurrentValue = this.tenantService.TenantId;
            }
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public Guid GenUuid() => throw new NotImplementedException();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.AddInterceptors(this.interceptors);
            optionsBuilder.LogTo(
                filter: (@event, level) => @event.Id == CoreEventId.ExecutionStrategyRetrying,
                logger: (eventData) =>
                {
                    if (eventData is not ExecutionStrategyEventData retryEventData)
                    {
                        logger.Log(eventData.LogLevel, "Unexpected event data type {EventType}", eventData.GetType().Name);
                        return;
                    }

                    var exceptions = retryEventData.ExceptionsEncountered;
                    logger.Log(
                        eventData.LogLevel,
                        "Retry #{RetryNo} with delay {Delay} due to {ExceptionName}: {Message}",
                        exceptions.Count,
                        retryEventData.Delay,
                        exceptions[^1].GetType().Name,
                        exceptions[^1].Message);
                });
        }
    }
}
