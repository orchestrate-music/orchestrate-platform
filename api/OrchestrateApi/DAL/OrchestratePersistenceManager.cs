using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Security.Claims;
using Breeze.Persistence;
using Breeze.Persistence.EFCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Access;
using OrchestrateApi.DAL.CustomSave;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;
using EntityState = Breeze.Persistence.EntityState;
using SaveMap = System.Collections.Generic.Dictionary<System.Type, System.Collections.Generic.List<Breeze.Persistence.EntityInfo>>;

namespace OrchestrateApi.DAL
{
    public class OrchestratePersistenceManager : EFPersistenceManager<OrchestrateContext>
    {
        private bool inSafeSave;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IEntityAccessorRegistrar entityAccessorRegistrar;
        private readonly IEntityHookFactoryRegistrar hookFactoryRegistrar;
        private readonly ITenantService tenantService;

        private Dictionary<Type, IEntityHook> currentSaveHooks = new();

        public OrchestratePersistenceManager(
            OrchestrateContext context,
            IHttpContextAccessor contextAccessor,
            IEntityAccessorRegistrar entityAccessorRegistrar,
            IEntityHookFactoryRegistrar hookFactoryRegistrar,
            ITenantService tenantService)
            : base(context)
        {
            this.contextAccessor = contextAccessor;
            this.entityAccessorRegistrar = entityAccessorRegistrar;
            this.hookFactoryRegistrar = hookFactoryRegistrar;
            this.tenantService = tenantService;
        }

        private ClaimsPrincipal? User => this.contextAccessor?.HttpContext?.User;
        private Guid? TenantId => this.tenantService.TenantId;

        public IQueryable<T> AuthorisedEntitySet<T>() where T : class
        {
            var queryData = SecuredQueryData.ForUserInTenant(User, TenantId);

            if (!this.entityAccessorRegistrar.GetEntityAccessor(typeof(T)).CanReadTable(queryData))
            {
                // Ensure the EF Async queryable extensions can be used with the empty array
                return Array.Empty<T>().AsAsyncQueryable();
            }

            var entities = Context.Set<T>().AsNoTracking().TagWith(queryData.ToQueryTag());
            return entities;
        }

        public async Task<SaveResult> AuthorisedSaveChangesAsync(JObject saveBundle, TransactionSettings? transactionSettings = null, CancellationToken cancellationToken = default)
        {
            try
            {
                this.inSafeSave = true;

                // Have to do this since we enabled EnableRetryOnFailure()
                // See https://learn.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/implement-resilient-entity-framework-core-sql-connections
                var strategy = Context.Database.CreateExecutionStrategy();
                return await strategy.ExecuteAsync(async (cancellationToken) =>
                {
                    await using var transaction = await Context.Database.BeginTransactionAsync(cancellationToken);

                    BeforeSaveEntitiesDelegate = BeforeSave;
                    BeforeSaveEntitiesAsyncDelegate = BeforeSaveAsync;
                    AfterSaveEntitiesAsyncDelegate = (saveMap, _, cancellationToken) => AfterSaveAsync(saveMap, cancellationToken);
                    var result = await SaveChangesAsync(saveBundle, transactionSettings, cancellationToken);

                    // Ensure if any hooks have made changes, we persist them here
                    await Context.SaveChangesAsync(cancellationToken);

                    await transaction.CommitAsync(cancellationToken);

                    return result;
                }, cancellationToken);
            }
            finally
            {
                this.inSafeSave = false;
            }
        }

        internal async Task<SaveMap> StubbedSaveAsync(SaveMap saveMap)
        {
            try
            {
                this.inSafeSave = true;
                saveMap = BeforeSave(saveMap);
                await BeforeSaveAsync(saveMap, default);
                await AfterSaveAsync(saveMap, default);
                return saveMap;
            }
            finally
            {
                this.inSafeSave = false;
            }
        }

        public async Task<SaveResult> CustomSaveAsync(JObject saveBundle, ICustomSave customSave, TransactionSettings? transactionSettings = null, CancellationToken cancellationToken = default)
        {
            try
            {
                this.inSafeSave = true;

                if (!TenantId.HasValue || User is null || !User.HasPermissionInTenant(TenantId.Value, customSave.RequiredPermission))
                {
                    throw new UnauthorisedSaveException();
                }

                // Have to do this since we enabled EnableRetryOnFailure()
                // See https://learn.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/implement-resilient-entity-framework-core-sql-connections
                var strategy = Context.Database.CreateExecutionStrategy();
                return await strategy.ExecuteAsync(async (cancellationToken) =>
                {
                    using var transaction = await Context.Database.BeginTransactionAsync(cancellationToken);
                    BeforeSaveEntitiesDelegate = customSave.BeforeSave;
                    AfterSaveEntitiesAsyncDelegate = (saveMap, keys, cancellationToken) =>
                    {
                        return customSave.AfterSaveAsync(this, saveMap, cancellationToken);
                    };
                    var result = await SaveChangesAsync(saveBundle, transactionSettings, cancellationToken);

                    // Ensure if any changes are persisted
                    await Context.SaveChangesAsync(cancellationToken);
                    await transaction.CommitAsync(cancellationToken);

                    return result;
                }, cancellationToken);
            }
            finally
            {
                this.inSafeSave = false;
            }
        }

        private SaveMap BeforeSave(SaveMap saveMap)
        {
            if (!this.inSafeSave)
            {
                throw new InvalidOperationException($"Call save via {nameof(AuthorisedSaveChangesAsync)}");
            }

            var queryData = SecuredQueryData.ForUserInTenant(User, TenantId);
            var unauthorisedTypes = saveMap.Keys
                .Select(type => this.entityAccessorRegistrar.GetEntityAccessor(type))
                .Where(accessor => !accessor.CanWriteToTable(queryData));
            if (unauthorisedTypes.Any())
            {
                throw new UnauthorisedSaveException();
            }

            this.currentSaveHooks = new();
            foreach (var kvp in saveMap)
            {
                if (TryBuildEntityHook(kvp.Key, saveMap, out var hook))
                {
                    this.currentSaveHooks[kvp.Key] = hook;
                }
            }

            var errors = new List<EntityError>();
            var entitiesToValidate = saveMap.SelectMany(kvp => kvp.Value)
                .Where(e => e.EntityState == EntityState.Added || e.EntityState == EntityState.Modified)
                .ToList();
            foreach (var entityInfo in entitiesToValidate)
            {
                var vc = new ValidationContext(entityInfo.Entity);
                var results = new List<ValidationResult>();
                if (!Validator.TryValidateObject(entityInfo.Entity, vc, results, true))
                {
                    foreach (var r in results)
                    {
                        foreach (var m in r.MemberNames)
                        {
                            errors.Add(new EFEntityError(entityInfo, "InvalidValue", r.ErrorMessage, m));
                        }
                    }
                }

                if (this.currentSaveHooks.TryGetValue(entityInfo.Entity.GetType(), out var hook))
                {
                    foreach (var error in hook.Validate(entityInfo))
                    {
                        errors.Add(error);
                    }
                }
            }

            ThrowIfAnyErrors(errors);

            return saveMap;
        }

        private async Task<SaveMap> BeforeSaveAsync(SaveMap saveMap, CancellationToken cancellationToken)
        {
            // Horrible hacks to prevent sensitive information being overwritten
            // Needs to be kept in sync with OrchestrateBreezeController.cs
            // TODO All of this will go when Breeze is removed
            foreach (var entityInfo in saveMap.Values.SelectMany(e => e))
            {
                if (entityInfo.Entity is Tenant tenant)
                {
                    entityInfo.OriginalValuesMap.Remove(nameof(Tenant.PostmarkServerToken));
                    tenant.PostmarkServerToken = await Context.Tenant
                        .IgnoreQueryFilters()
                        .Where(e => e.Id == tenant.Id)
                        .Select(e => e.PostmarkServerToken)
                        .SingleAsync(cancellationToken);
                }
            }

            var errors = new List<EntityError>();

            // Take a shallow copy to prevent CollectionModifiedExceptions and to remove the possibility
            // that one hook adds an entity to the saveMap that already has other types being saved, which
            // then gets picked up by a subsequent hook.
            foreach (var kvp in saveMap.ShallowCopy())
            {
                if (!this.currentSaveHooks.TryGetValue(kvp.Key, out var hook))
                {
                    continue;
                }

                errors.AddRange(await hook.BeforeSaveAsync(kvp.Value, cancellationToken));
            }

            ThrowIfAnyErrors(errors);

            return saveMap;
        }

        private async Task AfterSaveAsync(SaveMap saveMap, CancellationToken cancellationToken)
        {
            var errors = new List<EntityError>();

            // Take a shallow copy to prevent CollectionModifiedExceptions and to remove the possibility
            // that one hook adds an entity to the saveMap that already has other types being saved, which
            // then gets picked up by a subsequent hook.
            foreach (var kvp in saveMap.ShallowCopy())
            {
                if (!this.currentSaveHooks.TryGetValue(kvp.Key, out var hook))
                {
                    continue;
                }

                errors.AddRange(await hook.AfterSaveAsync(kvp.Value, cancellationToken));
            }

            ThrowIfAnyErrors(errors);
        }

        private static void ThrowIfAnyErrors(IEnumerable<EntityError> errors)
        {
            if (errors.Any())
            {
                throw new EntityErrorsException("Invalid data", errors)
                {
                    StatusCode = HttpStatusCode.BadRequest,
                };
            }
        }

        protected override string BuildJsonMetadata()
        {
            var metadata = MetadataBuilder.BuildFrom(DbContext);

            metadata.PopulateCustomValidators();

            var jss = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore,
            };
            jss.Converters.Add(new StringEnumConverter());

            var json = JsonConvert.SerializeObject(metadata, jss);

            var altMetadata = BuildAltJsonMetadata();
            if (altMetadata != null)
            {
                json = string.Concat("{ \"altMetadata\": ", altMetadata, ",", json.AsSpan()[1..]);
            }
            return json;
        }

        private bool TryBuildEntityHook(Type entityType, SaveMap saveMap, [NotNullWhen(true)] out IEntityHook? hook)
        {
            var hookFactory = this.hookFactoryRegistrar.GetHookFactory(entityType);
            if (hookFactory == null)
            {
                hook = null;
                return false;
            }

            hook = hookFactory.BuildEntityHook(this, saveMap);
            return true;
        }
    }
}
