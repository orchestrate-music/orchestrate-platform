namespace OrchestrateApi.DAL.Model;

public enum AllowedSenders
{
    /// <summary>
    /// Only members with the MailingListGlobalSend permission can send to this list
    /// </summary>
    GlobalSendOnly,
    /// <summary>Only people who are on this list can send to the list</summary>
    RecipientsOnly,
    /// <summary>Anyone on the internet can send to this address</summary>
    Anyone,
}

public enum ReplyStrategy
{
    /// <summary>Replies go to everyone in the list</summary>
    MailingList,
    /// <summary>Replies go to the person who sent the email</summary>
    Sender,
}

public class MailingList : ITenanted
{
    public int Id { get; set; }

    public required string Name { get; set; }

    public required string Slug { get; set; }

    public AllowedSenders AllowedSenders { get; set; } = AllowedSenders.RecipientsOnly;

    public ReplyStrategy ReplyTo { get; set; } = ReplyStrategy.MailingList;

    public string? Footer { get; set; }

    public string? PostmarkStreamId { get; set; }

    public ICollection<Ensemble> Ensembles { get; init; } = new HashSet<Ensemble>();
    public ICollection<Member> Members { get; init; } = new HashSet<Member>();
    public ICollection<Contact> Contacts { get; init; } = new HashSet<Contact>();

    public ICollection<MailingListSuppression> Suppressions { get; init; } = new HashSet<MailingListSuppression>();
    public ICollection<MailingListMessageMetadata> Messages { get; init; } = new HashSet<MailingListMessageMetadata>();
}

// Explicit join tables to force column names and orders in EFCore.

public class MailingListEnsemble
{
    public int MailingListId { get; set; }
    public int EnsembleId { get; set; }
}

public class MailingListMember
{
    public int MailingListId { get; set; }
    public int MemberId { get; set; }
}

public class MailingListContact
{
    public int MailingListId { get; set; }
    public int ContactId { get; set; }
}
