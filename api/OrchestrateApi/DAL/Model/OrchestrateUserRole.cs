using Microsoft.AspNetCore.Identity;

namespace OrchestrateApi.DAL.Model
{
    public class OrchestrateUserRole : IdentityUserRole<Guid>, ITenanted
    {
    }
}
