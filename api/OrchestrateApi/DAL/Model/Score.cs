using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class Score : ITenanted
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; } = null!;

        public string? Composer { get; set; }

        public string? Arranger { get; set; }

        public string? Genre { get; set; }

        public string? Grade { get; set; }

        public int? Duration { get; set; }

        public DateTime? DatePurchased { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        public decimal? ValuePaid { get; set; }

        public string? Location { get; set; }

        public string? Notes { get; set; }

        public bool IsOwned { get; set; }

        public bool InLibrary { get; set; }

        public virtual ICollection<PerformanceScore> PerformanceScore { get; set; } = new HashSet<PerformanceScore>();

        public virtual ICollection<ScoreAttachment> Attachments { get; set; } = new HashSet<ScoreAttachment>();
    }

    public class ScoreAttachment : ITenanted
    {
        public int Id { get; set; }

        public int ScoreId { get; set; }

        public Guid BlobId { get; set; }

        [ForeignKey(nameof(ScoreId))]
        public virtual Score? Score { get; set; }
    }
}
