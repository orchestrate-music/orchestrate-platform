using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class PerformanceMember : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int MemberId { get; set; }

        public int PerformanceId { get; set; }

        [ForeignKey(nameof(MemberId))]
        public Member? Member { get; set; }

        [ForeignKey(nameof(PerformanceId))]
        public Performance? Performance { get; set; }
    }
}
