using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model;

public class Contact : ITenanted
{
    [Key]
    public int Id { get; set; }

    public required string Name { get; set; }

    [EmailAddress]
    public string? Email { get; set; }

    [Phone]
    public string? PhoneNo { get; set; }

    public string? Affiliation { get; set; }

    public string? Notes { get; set; }

    public DateTime? ArchivedOn { get; set; }
}
