using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchestrateApi.DAL.Model
{
    public class EnsembleRole : ITenanted
    {
        [Key]
        public int Id { get; set; }

        public int EnsembleId { get; set; }

        public Guid RoleId { get; set; }

        [ForeignKey(nameof(EnsembleId))]
        public Ensemble? Ensemble { get; set; }

        [ForeignKey(nameof(RoleId))]
        public OrchestrateRole? Role { get; set; }
    }
}
