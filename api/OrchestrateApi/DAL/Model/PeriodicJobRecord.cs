using System.ComponentModel.DataAnnotations;

namespace OrchestrateApi.DAL.Model;

public class PeriodicJobRecord
{
    [Key]
    public required string Name { get; set; }

    public InProgressPeriodicJobData? InProgress { get; set; }

    public CompletedPeriodicJobData? Completed { get; set; }

    [ConcurrencyCheck]
    public required Guid ConcurrencyToken { get; set; }
}

public class InProgressPeriodicJobData
{
    public required DateTime StartTime { get; set; }

    public required string InstanceName { get; set; }
}

public enum CompletedPeriodicJobResult
{
    Success,
    Error,
}

public class CompletedPeriodicJobData
{
    public required CompletedPeriodicJobResult Result { get; set; }

    public required DateTime Time { get; set; }

    public required string InstanceName { get; set; }
}
