using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace OrchestrateApi.DAL.Model
{
    public class Tenant
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Abbreviation { get; set; } = string.Empty;

        [Required]
        public string TimeZone { get; set; } = string.Empty;

        [Url]
        public string? Website { get; set; }

        [EmailAddress]
        public string ContactAddress { get; set; } = string.Empty;

        [SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public byte[]? Logo { get; set; }

        public DateTime DateRegistered { get; set; } = DateTime.UtcNow;

        // TODO #73 LocalDate
        public DateTime? DateFounded { get; set; }

        public TenantPlan Plan { get; set; } = new TenantPlan
        {
            Type = PlanType.Free
        };

        public string? PostmarkServerToken { get; set; }
    }

    public class TenantPlan
    {
        public PlanType Type { get; set; }

        public bool IsNotForProfit { get; set; }

        public DateTime? PeriodEnd { get; set; }

        public string? StripeCustomerId { get; set; }
    }
}
