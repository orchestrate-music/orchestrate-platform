﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OrchestrateApi.Migrations
{
    public partial class DotNet6Tweaks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("SET TimeZone='UTC';");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_purchased",
                table: "score",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "sent",
                table: "member_invoice",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "send_status_updated",
                table: "member_invoice",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "paid",
                table: "member_invoice",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "sent",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "notification_contact_notified",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "generated",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "early_bird_due",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "due",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "closed",
                table: "member_billing_period",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_left",
                table: "member",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "member",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_left",
                table: "ensemble_membership",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "ensemble_membership",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "concert",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_returned",
                table: "asset_loan",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_borrowed",
                table: "asset_loan",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_discarded",
                table: "asset",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_acquired",
                table: "asset",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp without time zone",
                oldNullable: true);

            RemoveForeignKeys(migrationBuilder, "asp_net_users", "user_id",
                "asp_net_user_tokens", "asp_net_user_roles", "asp_net_user_logins", "asp_net_user_claims", "member_details");
            RemoveForeignKeys(migrationBuilder, "asp_net_roles",
                "role_id", "asp_net_user_roles", "asp_net_role_claims", "ensemble_role");

            AlterColumnToUuid(migrationBuilder, "member_details", "user_id", nullable: true);
            AlterColumnToUuid(migrationBuilder, "ensemble_role", "role_id");

            AlterColumnToUuid(migrationBuilder, "asp_net_users", "id");
            AlterColumnToUuid(migrationBuilder, "asp_net_user_tokens", "user_id");
            AlterColumnToUuid(migrationBuilder, "asp_net_user_roles", "role_id");
            AlterColumnToUuid(migrationBuilder, "asp_net_user_roles", "user_id");
            AlterColumnToUuid(migrationBuilder, "asp_net_user_logins", "user_id");
            AlterColumnToUuid(migrationBuilder, "asp_net_user_claims", "user_id");
            AlterColumnToUuid(migrationBuilder, "asp_net_roles", "id");
            AlterColumnToUuid(migrationBuilder, "asp_net_role_claims", "role_id");

            AddForeignKeys(migrationBuilder, "asp_net_users", "user_id",
                "asp_net_user_tokens", "asp_net_user_roles", "asp_net_user_logins", "asp_net_user_claims", "member_details");
            AddForeignKeys(migrationBuilder, "asp_net_roles", "role_id",
                "asp_net_user_roles", "asp_net_role_claims", "ensemble_role");

            migrationBuilder.CreateTable(
                name: "data_protection_keys",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    friendly_name = table.Column<string>(type: "text", nullable: true),
                    xml = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_data_protection_keys", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("SET TimeZone='UTC';");

            migrationBuilder.DropTable(
                name: "data_protection_keys");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_purchased",
                table: "score",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "sent",
                table: "member_invoice",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "send_status_updated",
                table: "member_invoice",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "paid",
                table: "member_invoice",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "sent",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "notification_contact_notified",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "generated",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "early_bird_due",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "due",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "closed",
                table: "member_billing_period",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_left",
                table: "member",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "member",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_left",
                table: "ensemble_membership",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_joined",
                table: "ensemble_membership",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date",
                table: "concert",
                type: "timestamp without time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_returned",
                table: "asset_loan",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_borrowed",
                table: "asset_loan",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_discarded",
                table: "asset",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "date_acquired",
                table: "asset",
                type: "timestamp without time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            RemoveForeignKeys(migrationBuilder, "asp_net_users", "user_id",
                "asp_net_user_tokens", "asp_net_user_roles", "asp_net_user_logins", "asp_net_user_claims", "member_details");
            RemoveForeignKeys(migrationBuilder, "asp_net_roles",
                "role_id", "asp_net_user_roles", "asp_net_role_claims", "ensemble_role");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "member_details",
                type: "text",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "role_id",
                table: "ensemble_role",
                type: "text",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "asp_net_users",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "asp_net_user_tokens",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "role_id",
                table: "asp_net_user_roles",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "asp_net_user_roles",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "asp_net_user_logins",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "user_id",
                table: "asp_net_user_claims",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "id",
                table: "asp_net_roles",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<string>(
                name: "role_id",
                table: "asp_net_role_claims",
                type: "text",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            AddForeignKeys(migrationBuilder, "asp_net_users", "user_id",
                "asp_net_user_tokens", "asp_net_user_roles", "asp_net_user_logins", "asp_net_user_claims", "member_details");
            AddForeignKeys(migrationBuilder, "asp_net_roles", "role_id",
                "asp_net_user_roles", "asp_net_role_claims", "ensemble_role");
        }

        private void RemoveForeignKeys(MigrationBuilder migrationBuilder, string targetTable, string fkColumn, params string[] tables)
        {
            foreach (var table in tables)
            {
                migrationBuilder.DropForeignKey(
                    name: $"fk_{table}_{targetTable}_{fkColumn}",
                    table: table);
            }
        }

        private void AddForeignKeys(MigrationBuilder migrationBuilder, string targetTable, string fkColumn, params string[] tables)
        {
            foreach (var table in tables)
            {
                migrationBuilder.AddForeignKey(
                    name: $"fk_{table}_{targetTable}_{fkColumn}",
                    table: table,
                    column: fkColumn,
                    principalTable: targetTable,
                    principalColumn: "id",
                    onDelete: ReferentialAction.Cascade);
            }
        }

        private void AlterColumnToUuid(MigrationBuilder migrationBuilder, string table, string column, bool nullable = false)
        {
            migrationBuilder.Sql($"ALTER TABLE {table} ALTER COLUMN {column} TYPE uuid USING {column}::uuid");

            if (!nullable)
            {
                migrationBuilder.Sql($"ALTER TABLE {table} ALTER COLUMN {column} SET NOT NULL");
            }
        }
    }
}
