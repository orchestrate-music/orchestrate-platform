﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrchestrateApi.Migrations
{
    public partial class MemberBillingTweaks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Failed' WHERE send_status = 'Error';");
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Pending' WHERE send_status = 'Success' AND send_status_message like '%pending%';");
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Opened' WHERE send_status = 'Success' AND send_status_message like '%opened%';");
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Delivered' WHERE send_status = 'Success';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Error' WHERE send_status = 'Failed';");
            migrationBuilder.Sql(@"UPDATE member_invoice SET send_status = 'Success' WHERE send_status <> 'Error';");
        }
    }
}
