﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrchestrateApi.Migrations
{
    /// <inheritdoc />
    public partial class TenantPlan : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "contact_address",
                table: "tenant",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "plan_is_not_for_profit",
                table: "tenant",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "plan_period_end",
                table: "tenant",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "plan_stripe_customer_id",
                table: "tenant",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "plan_type",
                table: "tenant",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "plan_metadata",
                columns: table => new
                {
                    type = table.Column<string>(type: "text", nullable: false),
                    member_limit = table.Column<int>(name: "member_limit", type: "integer", nullable: true),
                    discriminator = table.Column<string>(type: "text", nullable: false),
                    monthly_fee_cents = table.Column<int>(name: "monthly_fee_cents", type: "integer", nullable: true),
                    stripe_product_id = table.Column<string>(name: "stripe_product_id", type: "text", nullable: true),
                    stripe_price_id = table.Column<string>(name: "stripe_price_id", type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_plan_metadata", x => x.type);
                });

            migrationBuilder.Sql("""
                UPDATE tenant
                SET plan_period_end = date_registered + INTERVAL '30 days'
                WHERE date_registered <> '-infinity'
            """);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "plan_metadata");

            migrationBuilder.DropColumn(
                name: "contact_address",
                table: "tenant");

            migrationBuilder.DropColumn(
                name: "plan_is_not_for_profit",
                table: "tenant");

            migrationBuilder.DropColumn(
                name: "plan_period_end",
                table: "tenant");

            migrationBuilder.DropColumn(
                name: "plan_stripe_customer_id",
                table: "tenant");

            migrationBuilder.DropColumn(
                name: "plan_type",
                table: "tenant");
        }
    }
}
