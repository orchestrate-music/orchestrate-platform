using OrchestrateApi.Common;
using Stripe;

namespace OrchestrateApi.PaymentProcessor;

// From https://stripe.com/docs/api/invoices/object#invoice_object-status
public static class StripeInvoiceStatus
{
    public const string Paid = "paid";
}

// From https://stripe.com/docs/api/checkout/sessions/create#create_checkout_session-mode
public static class StripeCheckoutMode
{
    public const string Subscription = "subscription";
}

// From https://stripe.com/docs/api/payment_methods/object#payment_method_object-type
public static class StripePaymentMethodType
{
    public const string AuBecsDebit = "au_becs_debit";
    public const string CreditCard = "card";
}

public class StripePaymentProcessor : IPaymentProcessor
{
    private readonly IAppConfiguration appConfiguration;
    private readonly ILogger logger;
    private readonly StripeClient stripeClient;

    public StripePaymentProcessor(
        IAppConfiguration configuration,
        HttpClient httpClient,
        ILogger<StripePaymentProcessor> logger)
    {
        this.appConfiguration = configuration;
        this.logger = logger;
        var stripeHttpClient = new SystemNetHttpClient(httpClient);
        this.stripeClient = new StripeClient(
            apiKey: configuration.StripeSecretKey,
            httpClient: stripeHttpClient);
    }

    private CustomerService CustomerService => this.stripeClient.V1.Customers;
    private SubscriptionService SubscriptionService => this.stripeClient.V1.Subscriptions;
    private InvoiceService InvoiceService => this.stripeClient.V1.Invoices;
    private PaymentMethodService PaymentMethodService => this.stripeClient.V1.PaymentMethods;
    private Stripe.BillingPortal.SessionService BillingPortalSessionService => this.stripeClient.V1.BillingPortal.Sessions;
    private Stripe.Checkout.SessionService CheckoutSessionService => this.stripeClient.V1.Checkout.Sessions;
    private PromotionCodeService PromotionCodeService => this.stripeClient.V1.PromotionCodes;

    public async Task<Customer?> GetCustomerAsync(string customerId)
    {
        return await CustomerService.GetAsync(customerId);
    }

    public async Task<Customer?> GetCustomerWithSubscriptionsAsync(string customerId)
    {
        return await CustomerService.GetAsync(customerId, new CustomerGetOptions
        {
            Expand = new() { "subscriptions" },
        });
    }

    public async Task<Customer> CreateCustomerAsync(string name, string email)
    {
        return await CustomerService.CreateAsync(new CustomerCreateOptions
        {
            Name = name,
            Email = email,
        });
    }

    public async Task ApplyNotForProfitDiscountAsync(Customer customer)
    {
        var couponId = customer.Discount?.Coupon?.Id;
        if (couponId == this.appConfiguration.StripeNotForProfitCouponId)
        {
            this.logger.LogInformation("Tenant {CustomerId} already has NFP discount", customer.Id);
            return;
        }

        if (couponId == null)
        {
            await CustomerService.UpdateAsync(customer.Id, new CustomerUpdateOptions
            {
                Coupon = this.appConfiguration.StripeNotForProfitCouponId,
            });
        }
        else
        {
            this.logger.LogWarning(
                "Tenant {CustomerId} has coupon, but it isn't the Nfp discount. Has {Has}, expected {Expected}. Not Overriding",
                customer.Id,
                couponId,
                this.appConfiguration.StripeNotForProfitCouponId);
        }
    }

    public async Task<Subscription?> GetSubscriptionAsync(string subscriptionId)
    {
        return await SubscriptionService.GetAsync(subscriptionId);
    }

    public async Task<IList<Invoice>> GetMostRecentPaidInvoicesAsync(string customerId, int last = 12)
    {
        var stripeList = await InvoiceService.ListAsync(new InvoiceListOptions
        {
            Customer = customerId,
            Status = StripeInvoiceStatus.Paid,
            Limit = last,
        });
        return stripeList.Data;
    }

    public async Task<Invoice?> GetUpcomingInvoiceAsync(string customerId, string subscriptionId)
    {
        return await InvoiceService.UpcomingAsync(new UpcomingInvoiceOptions
        {
            Customer = customerId,
            Subscription = subscriptionId,
        });
    }

    public async Task<PaymentMethod?> GetPaymentMethodAsync(string paymentMethodId)
    {
        return await PaymentMethodService.GetAsync(paymentMethodId);
    }

    public async Task<Stripe.BillingPortal.Session> CreateBillingPortalSessionAsync(string customerId, Uri returnUrl)
    {
        return await BillingPortalSessionService.CreateAsync(new Stripe.BillingPortal.SessionCreateOptions
        {
            Customer = customerId,
            ReturnUrl = returnUrl.AbsoluteUri,
        });
    }

    public async Task<Result<Stripe.Checkout.Session>> CreateCheckoutSessionAsync(CheckoutSessionOptions options)
    {
        var sessionOptions = new Stripe.Checkout.SessionCreateOptions
        {
            Customer = options.CustomerId,
            SuccessUrl = options.ReturnUrl.AbsoluteUri,
            CancelUrl = options.ReturnUrl.AbsoluteUri,
            Mode = StripeCheckoutMode.Subscription,
            LineItems = new List<Stripe.Checkout.SessionLineItemOptions>
            {
                new Stripe.Checkout.SessionLineItemOptions
                {
                    Price = options.Plan.StripePriceId,
                    Quantity = 1,
                },
            },
        };

        if (!string.IsNullOrWhiteSpace(options.PromoCode))
        {
            var matchingCodes = await PromotionCodeService.ListAsync(new PromotionCodeListOptions
            {
                Code = options.PromoCode,
                Limit = 1,
            });
            if (matchingCodes.SingleOrDefault() is not { } promoCode)
            {
                return new UserErrorResult("Unable to find that promotion code");
            }
            else if (!promoCode.Active || (promoCode.ExpiresAt.HasValue && promoCode.ExpiresAt.Value < DateTime.UtcNow))
            {
                return new UserErrorResult("Promotion code is no longer valid");
            }

            sessionOptions.Discounts = new List<Stripe.Checkout.SessionDiscountOptions>
            {
                new Stripe.Checkout.SessionDiscountOptions
                {
                    PromotionCode = promoCode.Id,
                },
            };
        }

        try
        {
            return await CheckoutSessionService.CreateAsync(sessionOptions);
        }
        catch (StripeException e)
        {
            return new ServerErrorResult($"Failed to checkout: {e.Message}");
        }
    }
}
