using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Security
{
    public class RoleData
    {
        public string Name { get; init; } = null!;

        public IEnumerable<FeaturePermission> Permissions { get; init; } = null!;
    }

    public static class StandardRole
    {
        public const string Administrator = "Administrator";
        public const string BillingAdministrator = "BillingAdministrator";
        public const string CommitteeMember = "CommitteeMember";
        public const string Editor = "Editor";
        public const string Viewer = "Viewer";

        /// <summary>
        /// Roles which have general edit permissions to operational data.
        /// </summary>
        public static readonly string[] StandardEditors = new[]
        {
            Editor,
            CommitteeMember,
            Administrator
        };

        public static RoleData AdministratorData { get; } = new RoleData
        {
            Name = Administrator,
            Permissions = Enum.GetValues<FeaturePermission>().Except(new[] { FeaturePermission.None }),
        };

        // Keep these permissions in sync with role.ts
        public static RoleData BillingAdministratorData { get; } = new RoleData
        {
            Name = BillingAdministrator,
            Permissions = new[]
            {
                FeaturePermission.PlatformAccess,
                FeaturePermission.MemberRead,
                FeaturePermission.MemberBillingRead,
                FeaturePermission.MemberBillingWrite,
                FeaturePermission.EnsembleRead,
                FeaturePermission.EnsembleWrite,
            },
        };

        public static RoleData CommitteeMemberData { get; } = new RoleData
        {
            Name = CommitteeMember,
            Permissions = new[]
            {
                FeaturePermission.PlatformAccess,
                FeaturePermission.AssetRead,
                FeaturePermission.AssetWrite,

                FeaturePermission.ScoreRead,
                FeaturePermission.ScoreWrite,

                FeaturePermission.MemberRead,
                FeaturePermission.MemberWrite,
                FeaturePermission.MemberDetailRead,
                FeaturePermission.MemberDetailWrite,

                FeaturePermission.MemberBillingRead,

                FeaturePermission.EnsembleRead,
                FeaturePermission.EnsembleWrite,

                FeaturePermission.ConcertRead,
                FeaturePermission.ConcertWrite,

                FeaturePermission.ContactsRead,
                FeaturePermission.ContactsWrite,

                FeaturePermission.MailingListRead,
                FeaturePermission.MailingListWrite,
                FeaturePermission.MailingListGlobalSend,
            }
        };

        public static RoleData EditorData { get; } = new RoleData
        {
            Name = Editor,
            Permissions = new[]
            {
                FeaturePermission.PlatformAccess,
                FeaturePermission.AssetRead,
                FeaturePermission.AssetWrite,

                FeaturePermission.ScoreRead,
                FeaturePermission.ScoreWrite,

                FeaturePermission.MemberRead,
                FeaturePermission.MemberWrite,
                FeaturePermission.MemberDetailRead,
                FeaturePermission.MemberDetailWrite,

                FeaturePermission.EnsembleRead,
                FeaturePermission.EnsembleWrite,

                FeaturePermission.ConcertRead,
                FeaturePermission.ConcertWrite,

                FeaturePermission.ContactsRead,
                FeaturePermission.ContactsWrite,
            },
        };
        public static RoleData ViewerData { get; } = new RoleData
        {
            Name = Viewer,
            Permissions = new[]
            {
                FeaturePermission.PlatformAccess,
                FeaturePermission.AssetRead,

                FeaturePermission.ScoreRead,

                FeaturePermission.MemberRead,

                FeaturePermission.EnsembleRead,

                FeaturePermission.ConcertRead,
            },
        };

        public static IEnumerable<RoleData> AllRoles { get; } = new[]
        {
            AdministratorData,
            BillingAdministratorData,
            CommitteeMemberData,
            EditorData,
            ViewerData,
        };

        public static IEnumerable<string> AllRoleNames { get; } = AllRoles.Select(r => r.Name!).ToList();

        public static IEnumerable<FeaturePermission> GetRolePermissions(string role)
        {
            var roleData = AllRoles.SingleOrDefault(r => r.Name.Equals(role, StringComparison.OrdinalIgnoreCase));
            return roleData?.Permissions ?? Enumerable.Empty<FeaturePermission>();
        }
    }
}
