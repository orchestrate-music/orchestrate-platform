using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Security
{
    [Authorize]
    [ApiController]
    [Route("tenant")]
    public class TenantController : ControllerBase
    {
        private readonly OrchestrateContext context;

        public TenantController(OrchestrateContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<TenantDto>> GetAllTenantsAsync()
        {
            return await UserAccessibleTenants()
                .SelectTenantDto()
                .ToListAsync();
        }

        [HttpGet("{abbreviation}")]
        public async Task<TenantDto?> GetTenantByAbbreviationAsync(string abbreviation)
        {
            return await UserAccessibleTenants()
                .Where(t => t.Abbreviation == abbreviation)
                .SelectTenantDto()
                .SingleOrDefaultAsync();
        }

        private IQueryable<Tenant> UserAccessibleTenants()
        {
            var accessibleTenants = User.AccessibleTenants();
            return this.context.Tenant
                .IgnoreQueryFilters()
                .Where(t => User.IsGlobalAdmin() || accessibleTenants.Contains(t.Id));
        }
    }
}
