using System.Diagnostics.CodeAnalysis;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Common;

namespace OrchestrateApi.Security;

public sealed class IsApiUserAuthorizeAttribute : AuthorizeAttribute
{
    public IsApiUserAuthorizeAttribute()
    {
        Policy = IsApiUserRequirement.PolicyName;
    }
}

public class IsApiUserRequirement : IAuthorizationRequirement
{
    public const string PolicyName = "IsApiUserAuthorize";
}

public class IsApiUserAuthorizationHandler : AuthorizationHandler<IsApiUserRequirement>
{
    private readonly UserService userService;
    private readonly IHttpContextAccessor httpContextAccessor;

    public IsApiUserAuthorizationHandler(UserService userService, IHttpContextAccessor httpContextAccessor)
    {
        this.userService = userService;
        this.httpContextAccessor = httpContextAccessor;
    }

    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, IsApiUserRequirement requirement)
    {
        if (await CheckApiUserCredentialsAsync())
        {
            context.Succeed(requirement);
        }
    }

    private async Task<bool> CheckApiUserCredentialsAsync()
    {
        return TryGetBasicAuthUsernameAndPassword(out var username, out var password)
            && await this.userService.CheckApiUserCredentialsAsync(username, password);
    }

    // Adapted from https://stackoverflow.com/a/62845041
    private bool TryGetBasicAuthUsernameAndPassword(
        [NotNullWhen(true)] out string? username,
        [NotNullWhen(true)] out string? password)
    {
        username = null;
        password = null;
        if (httpContextAccessor.HttpContext?.Request is not { } request)
        {
            return false;
        }

        if (!request.Headers.TryGetValue("Authorization", out var authHeaders))
        {
            return false;
        }

        var basicAuthHeader = authHeaders
            .WhereNotNull()
            .FirstOrDefault(h => h.StartsWith("Basic ", StringComparison.InvariantCulture));
        if (basicAuthHeader == null)
        {
            return false;
        }

        var encodedCredentials = basicAuthHeader["Basic ".Length..].Trim();
        var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(encodedCredentials));

        var splitCredentials = credentials.Split(":");
        username = splitCredentials[0];
        password = splitCredentials[1];
        return true;
    }
}
