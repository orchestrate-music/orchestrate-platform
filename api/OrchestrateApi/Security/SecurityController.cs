using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Security
{
    [ApiController]
    [Route("security")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1034")]
    public class SecurityController : ControllerBase
    {
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly UserService userService;
        private readonly OrchestrateContext context;

        public SecurityController(
            UserManager<OrchestrateUser> userManager,
            UserService userService,
            OrchestrateContext context)
        {
            this.userManager = userManager;
            this.userService = userService;
            this.context = context;
        }

        [HttpPost("login")]
        public async Task<ActionResult<LoginResponse>> Login(LoginModel model)
        {
            var username = model.UserName.Trim();
            if (string.IsNullOrWhiteSpace(username)
                || await userManager.FindByNameAsync(username) is not OrchestrateUser user
                || !await userManager.CheckPasswordAsync(user, model.Password))
            {
                return Unauthorized();
            }

            var token = await this.userService.GenerateBearerTokenAsync(user);
            return new LoginResponse
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = token.ValidTo,
                User = new UserDto
                {
                    Id = user.Id,
                    Username = user.UserName!,
                    DisplayName = token.Claims.DisplayName(),
                    IsGlobalAdmin = user.IsGlobalAdmin,
                    TenantRoles = await BuildTenantRolesDtos(token.Claims),
                },
            };
        }

        [HttpPost("logout")]
        public void Logout()
        {
            return;
        }

        [HttpGet("user")]
        public async Task<ActionResult<UserDto>> GetLoggedInUser()
        {
            if (User == null || !User.Claims.Any())
            {
                return Unauthorized();
            }

            return new UserDto
            {
                Id = User.UserId(),
                Username = User.Username(),
                DisplayName = User.DisplayName(),
                IsGlobalAdmin = User.IsGlobalAdmin(),
                TenantRoles = await BuildTenantRolesDtos(User.Claims),
            };
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1841:Prefer ContainsKey", Justification = "EF Can't translate expression")]
        private async Task<IEnumerable<TenantRolesDto>> BuildTenantRolesDtos(IEnumerable<Claim> claims)
        {
            var tenantRoles = claims.GetTenantRoles()
                .GroupBy(tr => tr.TenantId)
                .ToDictionary(g => g.Key, g => g.Select(r => new RoleDto { Name = r.Role }));
            var accessibleTenants = await this.context.Tenant
                .IgnoreQueryFilters()
                .Where(t => tenantRoles.Keys.Contains(t.Id))
                .SelectTenantDto()
                .ToListAsync();
            return accessibleTenants.Select(t => new TenantRolesDto
            {
                TenantId = t.TenantId,
                Name = t.Name,
                Abbreviation = t.Abbreviation,
                Logo = t.Logo,
                Roles = tenantRoles[t.TenantId],
            });
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            var email = model.Email.Trim();
            if (string.IsNullOrWhiteSpace(email)
                || await this.userManager.FindByEmailAsync(email) is not OrchestrateUser user)
            {
                return BadRequest();
            }

            var result = await this.userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (!result.Succeeded)
            {
                var messages = result.Errors.Select(error => $"{error.Code} - {error.Description}");
                return BadRequest(string.Join(", ", messages));
            }

            if (!await this.userManager.IsEmailConfirmedAsync(user))
            {
                var token = await this.userManager.GenerateEmailConfirmationTokenAsync(user);
                await this.userManager.ConfirmEmailAsync(user, token);
            }

            return Ok();
        }

        [HttpPost("request-password-reset")]
        public async Task RequestResetPassword(RequestResetPasswordModel model)
        {
            await this.userService.RequestResetPassword(model.Email, model.Redirect);
        }

        public class LoginModel
        {
            public required string UserName { get; set; }

            public required string Password { get; set; }
        }

        public class LoginResponse
        {
            public required string Token { get; set; }

            public required DateTime Expiration { get; set; }

            public required UserDto User { get; set; }
        }

        public class ResetPasswordModel
        {
            public required string Email { get; set; }

            public required string Token { get; set; }

            public required string Password { get; set; }
        }

        public class RequestResetPasswordModel
        {
            public required string Email { get; set; }

            public string? Redirect { get; set; }
        }
    }
}
