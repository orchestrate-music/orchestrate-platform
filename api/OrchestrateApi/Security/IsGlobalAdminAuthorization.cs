using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace OrchestrateApi.Security
{
    public sealed class IsGlobalAdminAuthorizeAttribute : AuthorizeAttribute
    {
        public IsGlobalAdminAuthorizeAttribute()
        {
            Policy = IsGlobalAdminRequirement.PolicyName;
        }
    }

    public class IsGlobalAdminRequirement : IAuthorizationRequirement
    {
        public const string PolicyName = "IsGlobalAdminAuthorize";
    }

    public class IsGlobalAdminAuthorizationHandler : AuthorizationHandler<IsGlobalAdminRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsGlobalAdminRequirement requirement)
        {
            if (context.User.IsGlobalAdmin())
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
