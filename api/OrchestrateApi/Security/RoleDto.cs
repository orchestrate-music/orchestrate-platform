namespace OrchestrateApi.Security
{
    public class RoleDto
    {
        public Guid Id { get; set; }

        public required string Name { get; set; }
    }
}
