using System.Diagnostics.CodeAnalysis;

namespace OrchestrateApi.Common
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Filters a list, and if there are any elements discarded calls the passed in action
        /// </summary>
        public static IList<T> WhereWithDiscardCountAction<T>(this IList<T> collection, Func<T, bool> predicate, Action<int> doWithDiscardCount)
        {
            var matched = collection.Where(predicate).ToList();
            var numDiscarded = collection.Count - matched.Count;
            if (numDiscarded > 0)
            {
                doWithDiscardCount(numDiscarded);
            }

            return matched;
        }

        [SuppressMessage("", "CA1055")]
        public static string ToDataUrl(this byte[] file)
        {
            var fileType = FileTypeDetector.All.ParseFileTypeOrDefault(file);
            return $"data:{fileType.MimeType};base64,{Convert.ToBase64String(file)}";
        }

        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> source) => source.Where(e => e is not null)!;
    }

}
