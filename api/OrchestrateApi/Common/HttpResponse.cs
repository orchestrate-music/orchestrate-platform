namespace OrchestrateApi.Common;

public interface IErrorResponse
{
    string Type { get; }

    string Message { get; }
}

public class BasicErrorResponse : IErrorResponse
{
    public BasicErrorResponse(IErrorResult errorResult)
    {
        Message = errorResult.Message;
        Errors = errorResult.Errors;
    }

    public BasicErrorResponse(string message, params ResultError[] errors)
    {
        Message = message;
        Errors = errors;
    }

    public string Type => "ErrorResponse";

    public string Message { get; }

    public IReadOnlyCollection<ResultError> Errors { get; }
}
