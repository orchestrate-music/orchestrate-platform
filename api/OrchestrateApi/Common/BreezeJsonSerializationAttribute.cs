using System.Buffers;
using Breeze.AspNetCore;
using Breeze.Core;
using Breeze.Persistence;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Common
{
    /// <summary>Must be placed BEFORE BreezeQueryFilter</summary>
    public sealed class BreezeJsonSerializationAttribute : NewtonsoftJsonBinderAttribute, IExceptionFilter
    {
        private readonly GlobalExceptionFilter breezeExceptionFilter = new();

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            AddNewtonsoftOutputFormatter(context, context.Result);
        }

        public void OnException(ExceptionContext context)
        {
            if (context.Exception is UnauthorisedSaveException)
            {
                context.Result = new ForbidResult();
                return;
            }

            if (context.Exception is not EntityErrorsException entityErrorsException
                || (int)entityErrorsException.StatusCode >= 500)
            {
                var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<BreezeJsonSerializationAttribute>>();
                logger.LogError(context.Exception, "Error when executing {Endpoint}", context.ActionDescriptor.DisplayName);
            }

            // This is likely a bug, so only return after logging the error for investigation
            if (context.Exception is InvalidSaveDataException)
            {
                context.Result = new BadRequestResult();
                return;
            }

            this.breezeExceptionFilter.OnException(context);
            AddNewtonsoftOutputFormatter(context, context.Result);
        }

        private static void AddNewtonsoftOutputFormatter(ActionContext context, IActionResult? result)
        {
            if (result is ObjectResult objectResult)
            {
                var formatter = new NewtonsoftJsonOutputFormatter(
                    JsonSerializationFns.UpdateWithDefaults(new JsonSerializerSettings()),
                    context.HttpContext.RequestServices.GetRequiredService<ArrayPool<char>>(),
                    context.HttpContext.RequestServices.GetRequiredService<IOptions<MvcOptions>>().Value,
                    null);

                objectResult.Formatters.Add(formatter);
            }
        }
    }
}
