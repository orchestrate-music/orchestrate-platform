using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Common;

public interface IPeriodicJob
{
    public string Name { get; }
    public TimeSpan RunPeriod { get; }
    public Task Run(CancellationToken cancellationToken);
}

public sealed class PeriodicJobHostedService : IHostedService, IDisposable
{
    private readonly IAppConfiguration appConfiguration;
    private readonly IHostEnvironment environment;
    private Timer? timer;

    public PeriodicJobHostedService(
        IHostEnvironment environment,
        IServiceProvider services,
        IAppConfiguration appConfiguration,
        ILogger<PeriodicJobHostedService> logger)
    {
        this.appConfiguration = appConfiguration;
        this.environment = environment;
        Services = services;
        Logger = logger;
    }

    private TimeSpan CheckInterval => this.appConfiguration.PeriodicJobCheckInterval;
    private IServiceProvider Services { get; }
    private ILogger Logger { get; }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        if (environment.IsDevelopment())
        {
            Logger.LogInformation("Not starting timer in {Environment} environment", environment.EnvironmentName);
            return Task.CompletedTask;
        }

        if (!this.appConfiguration.PeriodicJobCheckEnabled)
        {
            Logger.LogInformation(
                "Not starting timer as {ConfigKey} is {ConfigValue}",
                nameof(this.appConfiguration.PeriodicJobCheckEnabled),
                this.appConfiguration.PeriodicJobCheckEnabled);
            return Task.CompletedTask;
        }

        Logger.LogInformation("Beginning timer, running every {Interval}", CheckInterval);

        // Don't start immediately
        this.timer = new Timer(TryRunPeriodicJobs, cancellationToken, CheckInterval, Timeout.InfiniteTimeSpan);

        return Task.CompletedTask;
    }

    [SuppressMessage("", "CA1031")]
    private async void TryRunPeriodicJobs(object? state)
    {
        if (state is not CancellationToken cancellationToken)
        {
            Logger.LogWarning("Not running periodic job checks - cancellation token is not defined");
            return;
        }

        try
        {
            // Disable while we're running
            this.timer?.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

            using var scope = Services.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<OrchestrateContext>();

            foreach (var periodicJob in scope.ServiceProvider.GetServices<IPeriodicJob>())
            {
                using var logScope = Logger.BeginScope(periodicJob.Name);
                var runner = new PeriodicJobRunner(dbContext, periodicJob, Logger)
                {
                    MaxExpectedRunTime = this.appConfiguration.PeriodicJobMaxExpectedTime,
                };
                await runner.TryRun(cancellationToken);
            }
        }
        catch (Exception e)
        {
            Logger.LogError(e, "Unhandled exception while running periodic jobs");
        }
        finally
        {
            // Make sure we enable again no matter what! But once, until the next check
            this.timer?.Change(CheckInterval, Timeout.InfiniteTimeSpan);
        }
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        Logger.LogInformation($"Stopping timer");

        this.timer?.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);

        return Task.CompletedTask;
    }

    public void Dispose()
    {
        timer?.Dispose();
    }
}

public class PeriodicJobRunner
{
    private readonly OrchestrateContext dbContext;
    private readonly IPeriodicJob periodicJob;

    public PeriodicJobRunner(
        OrchestrateContext dbContext,
        IPeriodicJob job,
        ILogger logger)
    {
        this.dbContext = dbContext;
        this.periodicJob = job;
        Logger = logger;
    }

    public required TimeSpan MaxExpectedRunTime { get; init; }
    private string Name => this.periodicJob.Name;
    private TimeSpan RunPeriod => this.periodicJob.RunPeriod;
    private ILogger Logger { get; }

    [SuppressMessage("", "CA1031")]
    public async Task TryRun(CancellationToken cancellationToken)
    {
        var jobRecord = await GetOrCreateJobRecord(dbContext, cancellationToken);
        if (!ShouldRunJob(jobRecord))
        {
            return;
        }

        try
        {
            jobRecord.InProgress = new InProgressPeriodicJobData
            {
                StartTime = DateTime.UtcNow,
                InstanceName = Environment.MachineName,
            };
            jobRecord.ConcurrencyToken = Guid.NewGuid();
            await dbContext.SaveChangesAsync(cancellationToken);
        }
        catch (DbUpdateConcurrencyException)
        {
            await dbContext.Entry(jobRecord).ReloadAsync(cancellationToken);
            Logger.LogInformation(
                "Another instance has already picked up job ({InstanceName})",
                jobRecord?.InProgress?.InstanceName ?? jobRecord?.Completed?.InstanceName);
            return;
        }

        CompletedPeriodicJobResult result;
        try
        {
            await periodicJob.Run(cancellationToken);
            result = CompletedPeriodicJobResult.Success;
        }
        catch (Exception e)
        {
            Logger.LogError(e, "Unhandled exception while running job");
            result = CompletedPeriodicJobResult.Error;
        }

        jobRecord.InProgress = null;
        jobRecord.Completed = new CompletedPeriodicJobData
        {
            Result = result,
            Time = DateTime.UtcNow,
            InstanceName = Environment.MachineName,
        };
        jobRecord.ConcurrencyToken = Guid.NewGuid();

        try
        {
            await dbContext.SaveChangesAsync(cancellationToken);
        }
        catch (DbUpdateConcurrencyException e)
        {
            Logger.LogError(
                e,
                "Unexpected conflicting periodic job update after job is completed. "
                    + "Assuming another instance is in bad state and has started job execution, "
                    + "not attempting to persist completion result: {CompletedResult}",
                JsonSerializer.Serialize(jobRecord.Completed));
        }
    }

    private async Task<PeriodicJobRecord> GetOrCreateJobRecord(OrchestrateContext dbContext, CancellationToken cancellationToken)
    {
        var jobRecord = await dbContext.PeriodicJobRecord.SingleOrDefaultAsync(e => e.Name == Name, cancellationToken);
        if (jobRecord != null)
        {
            return jobRecord;
        }

        try
        {
            jobRecord = new PeriodicJobRecord
            {
                Name = Name,
                ConcurrencyToken = Guid.NewGuid(),
            };
            dbContext.Add(jobRecord);
            await dbContext.SaveChangesAsync(cancellationToken);
            return jobRecord;
        }
        catch (DbUpdateException)
        {
            Logger.LogInformation("Another instance must have already created DB instance of job, "
                + "loading it now");
            // Prevent the currently tracked version from polluting the newly fetched one
            dbContext.Remove(jobRecord!);
            jobRecord = await dbContext.PeriodicJobRecord.SingleOrDefaultAsync(e => e.Name == Name, cancellationToken);

            if (jobRecord == null)
            {
                Logger.LogError("Unable to find job when expected! Aborting.");
                throw new InvalidOperationException("Unable to find periodic job in DB");
            }

            return jobRecord;
        }
    }

    private bool ShouldRunJob(PeriodicJobRecord jobRecord)
    {
        if (jobRecord.Completed != null && jobRecord.Completed.Time + RunPeriod > DateTime.UtcNow)
        {
            Logger.LogInformation(
                "Not picking up job. Last finished at {FinishTime} and period is {RunPeriod}",
                jobRecord.Completed.Time,
                RunPeriod);
            return false;
        }

        if (jobRecord.InProgress != null)
        {
            Logger.LogInformation("Not picking up job, {InstanceName} is currently running it", jobRecord.InProgress.InstanceName);

            var runningTime = DateTime.UtcNow - jobRecord.InProgress.StartTime;
            if (runningTime > MaxExpectedRunTime)
            {
                Logger.LogError(
                    "{InstanceName} has been running job for {RunningTime}, has it hung?",
                    jobRecord.InProgress.InstanceName,
                    runningTime);
            }

            return false;
        }

        Logger.LogInformation("Picking up job");
        return true;
    }
}
