using System.Collections;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;

namespace OrchestrateApi.Common
{
    // Shamelessly stolen from https://stackoverflow.com/a/33307293/17403538
    // with some adaptions for EFCore 5+
    // Avoids the need for including the System.Linq.Async package
    // Has the limitation that it doesn't work with EntityFrameworkCore async query methods
    public static class AsyncQuerableExtensions
    {
        public static IQueryable<T> AsAsyncQueryable<T>(this IEnumerable<T> source)
        {
            return new AsyncQueryableWrapper<T>(source);
        }

        public static IQueryable<T> AsAsyncQueryable<T>(this IQueryable<T> source)
        {
            return new AsyncQueryableWrapper<T>(source);
        }

        private class AsyncQueryableWrapper<T> : IAsyncEnumerable<T>, IQueryable<T>
        {
            private readonly IQueryable<T> _source;

            public AsyncQueryableWrapper(IQueryable<T> source)
            {
                _source = source;
            }

            public AsyncQueryableWrapper(IEnumerable<T> source)
            {
                _source = source.AsQueryable();
            }

            public AsyncEnumerator<T> GetAsyncEnumerator()
            {
                return new AsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
            }

            IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
            {
                return GetAsyncEnumerator();
            }

            public IEnumerator<T> GetEnumerator()
            {
                return _source.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            public Expression Expression => _source.Expression;
            public Type ElementType => _source.ElementType;
            public IQueryProvider Provider => new AsyncQueryProvider<T>(_source.Provider);
        }

        private class AsyncEnumerable<T> : EnumerableQuery<T>, IAsyncEnumerable<T>, IQueryable<T>
        {
            public AsyncEnumerable(IEnumerable<T> enumerable)
                : base(enumerable)
            { }

            public AsyncEnumerable(Expression expression)
                : base(expression)
            { }

            public AsyncEnumerator<T> GetAsyncEnumerator()
            {
                return new AsyncEnumerator<T>(this.AsEnumerable().GetEnumerator());
            }

            IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
            {
                return GetAsyncEnumerator();
            }

            IQueryProvider IQueryable.Provider => new AsyncQueryProvider<T>(this);
        }

        private class AsyncQueryProvider<TEntity> : IAsyncQueryProvider
        {
            private readonly IQueryProvider _inner;

            internal AsyncQueryProvider(IQueryProvider inner)
            {
                _inner = inner;
            }

            public IQueryable CreateQuery(Expression expression)
            {
                var t = expression.Type;
                if (!t.IsGenericType)
                {
                    return new AsyncEnumerable<TEntity>(expression);
                }

                var genericParams = t.GetGenericArguments();
                var genericParam = genericParams[0];
                var enumerableType = typeof(AsyncEnumerable<>).MakeGenericType(genericParam);

                return (IQueryable)Activator.CreateInstance(enumerableType, expression)!;
            }

            public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
            {
                return new AsyncEnumerable<TElement>(expression);
            }

            public object? Execute(Expression expression)
            {
                return _inner.Execute(expression);
            }

            public TResult Execute<TResult>(Expression expression)
            {
                return _inner.Execute<TResult>(expression);
            }

            public TResult ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
            {
                return Execute<TResult>(expression);
            }
        }

        private class AsyncEnumerator<T> : IAsyncEnumerator<T>
        {
            private readonly IEnumerator<T> _inner;

            public AsyncEnumerator(IEnumerator<T> inner)
            {
                _inner = inner;
            }

            public void Dispose()
            {
                _inner.Dispose();
            }

            public ValueTask<bool> MoveNextAsync() => ValueTask.FromResult(_inner.MoveNext());

            public ValueTask DisposeAsync() => ValueTask.CompletedTask;

            public T Current => _inner.Current;

            T IAsyncEnumerator<T>.Current => Current;
        }
    }
}
