namespace OrchestrateApi.Common;

// This can likely be applied inline when access to `field` in property accessors
// becomes available: https://github.com/dotnet/csharplang/issues/140
public interface ISanitisable
{
    void Sanitise();
}

public static class ISanitisableExtensions
{
    public static T SanitiseAndReturn<T>(this T n) where T : ISanitisable
    {
        n.Sanitise();
        return n;
    }
}

public static class Sanitiser
{
    public static string StringToEmpty(string? v)
    {
        return v is string s ? s.Trim() : string.Empty;
    }

    public static Optional<string> StringToEmpty(Optional<string> optional)
    {
        return optional.HasValue ? StringToEmpty(optional.Value) : optional;
    }

    public static string? StringToNull(string? v)
    {
        if (v == null) { return v; }

        v = v.Trim();
        return string.IsNullOrWhiteSpace(v) ? null : v;
    }

    public static Optional<string?> StringToNull(Optional<string?> optional)
    {
        return optional.HasValue ? StringToNull(optional.Value) : optional;
    }
}
