using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.DemoSeed;
using OrchestrateApi.Security;

namespace OrchestrateApi.Platform;

[ApiController]
[Route("first-run")]
public class FirstRunController(
    OrchestrateContext dbContext,
    ITenantService tenantService,
    SingleEnsembleSeed singleSeed,
    UserManager<OrchestrateUser> userManager,
    RoleManager<OrchestrateRole> roleManager)
    : ControllerBase
{
    private static bool IsInitialised;

    [HttpPost]
    public async Task<IActionResult> InitGlobalAdmin(InitGlobalAdminModel model)
    {
#if !DEBUG
        if (IsInitialised)
        {
            return NotFound();
        }
#endif

        IsInitialised = await userManager.Users.AnyAsync();
        if (IsInitialised)
        {
            return NotFound();
        }

        foreach (var roleName in StandardRole.AllRoleNames)
        {
            if (await roleManager.RoleExistsAsync(roleName))
            {
                continue;
            }

            var roleResult = await roleManager.CreateAsync(new OrchestrateRole
            {
                Name = roleName,
                ConcurrencyStamp = Guid.NewGuid().ToString(),
            });
            if (!roleResult.Succeeded)
            {
                return BadRequest(new
                {
                    Errors = roleResult.Errors.Select(e => e.Description),
                });
            }
        }

        var user = new OrchestrateUser
        {
            UserName = model.Email,
            Email = model.Email,
            SecurityStamp = Guid.NewGuid().ToString(),
            IsGlobalAdmin = true,
        };
        var userResult = await userManager.CreateAsync(user, model.Password);
        if (!userResult.Succeeded)
        {
            return BadRequest(new
            {
                Errors = userResult.Errors.Select(e => e.Description),
            });
        }

        var tenant = await singleSeed.SeedAsync();
        using var tenantOverride = tenantService.ForceOverrideTenantId(tenant.Id);
        var activeMember = await dbContext.Member
            .Include(e => e.Details)
            .WhereIsActiveNow()
            .FirstAsync();
        activeMember.Details!.UserId = user.Id;
        activeMember.Details!.Email = user.Email;
        await userManager.AddToRoleAsync(user, StandardRole.Administrator);
        await dbContext.SaveChangesAsync();

        IsInitialised = true;

        return Ok();
    }
}

public class InitGlobalAdminModel
{
    public required string Email { get; set; }

    public required string Password { get; set; }
}
