using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class FileTypeDetectorTests
    {
        [Fact]
        public void ReturnsFirstMatchingFileType()
        {
            var detector = new FileTypeDetector
            {
                DefaultFileType = new StaticFileType("application/default"),
                AvailableFileTypes =
                [
                    new NeverFileType(),
                    new StaticFileType("application/a"),
                    new StaticFileType("application/b"),
                ],
            };

            Assert.True(detector.TryParseFileType(Array.Empty<byte>(), out var fileType));
            Assert.Equal("application/a", fileType!.MimeType);
            Assert.Equal("application/a", detector.ParseFileTypeOrDefault(Array.Empty<byte>()).MimeType);
        }

        [Fact]
        public void HandlesNoMatchingFileType()
        {
            var detector = new FileTypeDetector
            {
                DefaultFileType = new StaticFileType("application/default"),
                AvailableFileTypes = [new NeverFileType()],
            };

            Assert.False(detector.TryParseFileType(Array.Empty<byte>(), out var fileType));
            Assert.Null(fileType);
            Assert.Equal("application/default", detector.ParseFileTypeOrDefault(Array.Empty<byte>()).MimeType);
        }

        [Fact]
        public void CorrectlyDetectsFromRegisteredTypes()
        {
            foreach (var type in FileTypeDetector.All.AvailableFileTypes)
            {
                if (type is SentinelFileType sentinalType)
                {
                    Assert.Equal(type.MimeType, FileTypeDetector.All.ParseFileTypeOrDefault(sentinalType.Sentinel).MimeType);
                }
                else if (type is StaticFileType staticType)
                {
                    Assert.Equal(type.MimeType, FileTypeDetector.All.ParseFileTypeOrDefault(Array.Empty<byte>()).MimeType);
                }
                else
                {
                    throw new XunitException("Unknown IFileType");
                }
            }
        }

        private sealed class NeverFileType : IFileType
        {
            public string MimeType => "application/never";

            public bool Test(IEnumerable<byte>? file) => false;

            public Task<bool> TestAsync(Stream stream, CancellationToken cancellationToken) => Task.FromResult(false);
        }
    }

    public class SentinalFileTypeTests
    {
        [Theory]
        [InlineData(false, new byte[] { })]
        [InlineData(false, new byte[] { 0x12, 0x34 })]
        [InlineData(false, new byte[] { 0xCA, 0xFE, 0xB0 })]
        [InlineData(false, new byte[] { 0xCA, 0xFE, 0xB0, 0x0C })]
        [InlineData(true, new byte[] { 0xCA, 0xFE, 0xB0, 0x0B })]
        [InlineData(true, new byte[] { 0xCA, 0xFE, 0xB0, 0x0B, 0xAB })]
        public async Task SentinalFileTypeOnlyMatchesForExactHeader(bool matches, byte[] file)
        {
            var fileType = new SentinelFileType("application/sentinel", [0xCA, 0xFE, 0xB0, 0x0B]);
            Assert.Equal(matches, fileType.Test(file));
            Assert.Equal("application/sentinel", fileType.MimeType);

            if (file != null)
            {
                using var stream = new MemoryStream(file);
                Assert.Equal(matches, await fileType.TestAsync(stream, default));
            }
        }

        [Fact]
        public void ThrowsForNullSentinel()
        {
            Assert.Throws<ArgumentNullException>(() => new SentinelFileType("application/throws", null!));
        }
    }

    public class StaticFileTypeTests
    {
        [Theory]
        [InlineData(new byte[] { })]
        [InlineData(new byte[] { 0x12, 0x34 })]
        [InlineData(new byte[] { 0xCA, 0xFE, 0xB0, 0x0B })]
        public void StaticFileTypeAlwaysMatches(byte[] file)
        {
            var fileType = new StaticFileType("application/static");
            Assert.True(fileType.Test(file));
            Assert.Equal("application/static", fileType.MimeType);
        }
    }
}
