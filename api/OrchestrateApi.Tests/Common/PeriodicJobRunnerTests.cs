using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.Common;

public sealed class PeriodicJobHostedServiceTests : IDisposable
{
    private readonly ILogger logger;

    public PeriodicJobHostedServiceTests()
    {
        this.logger = TestHelper.BuildMockLogger();
    }

    private static OrchestrateContext BuildContext() => SharedDatabaseFixture.BuildContext();

    public void Dispose()
    {
        using var dbContext = BuildContext();
        dbContext.PeriodicJobRecord.ExecuteDelete();
    }

    [Theory]
    [InlineData(null, true)]
    [InlineData(-10, false)]
    [InlineData(-60, true)]
    public async Task JobIsOnlyPickedUpIfItIsDue(int? completedSecondsAgo, bool isRun)
    {
        using (var dbContext = BuildContext())
        {
            var entry = dbContext.Add(new PeriodicJobRecord
            {
                Name = "Test",
                ConcurrencyToken = Guid.NewGuid(),
            });

            if (completedSecondsAgo.HasValue)
            {
                entry.Entity.Completed = new CompletedPeriodicJobData
                {
                    Result = CompletedPeriodicJobResult.Success,
                    Time = DateTime.UtcNow.AddSeconds(completedSecondsAgo.Value),
                    InstanceName = "mock",
                };
            }
            await dbContext.SaveChangesAsync();
        }

        var ranJob = false;
        await RunPeriodicJob(new TestPeriodicJob
        {
            RunPeriod = TimeSpan.FromSeconds(20),
            DoJobRunTime = TimeSpan.Zero,
            OnRunningCallback = () => ranJob = true,
        });
        Assert.Equal(isRun, ranJob);

        using var context = BuildContext();
        var jobRecord = await context.PeriodicJobRecord.FindAsync("Test");
        Assert.NotNull(jobRecord);
        Assert.NotNull(jobRecord.Completed);

        var expectedRunTime = isRun
            ? DateTime.UtcNow
            : DateTime.UtcNow.AddSeconds(completedSecondsAgo!.Value);
        Assert.Equal(expectedRunTime, jobRecord.Completed.Time, TimeSpan.FromSeconds(1));
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public async Task JobIsOnlyPickedUpIfNoOtherInstanceIsRunningIt(bool isRunning)
    {
        using var dbContext = BuildContext();
        var entry = dbContext.Add(new PeriodicJobRecord
        {
            Name = "Test",
            ConcurrencyToken = Guid.NewGuid(),
        });

        if (isRunning)
        {
            entry.Entity.InProgress = new InProgressPeriodicJobData
            {
                StartTime = DateTime.UtcNow,
                InstanceName = "mock",
            };
        }
        await dbContext.SaveChangesAsync();

        var ranJob = false;
        await RunPeriodicJob(new TestPeriodicJob
        {
            RunPeriod = TimeSpan.FromSeconds(20),
            DoJobRunTime = TimeSpan.Zero,
            OnRunningCallback = () => ranJob = true,
        });
        Assert.Equal(isRunning, !ranJob);
    }

    [Fact]
    public async Task InProgressDataIsFilledOutDuringExecution()
    {
        using var cancellationTokenSource = new CancellationTokenSource();
        var taskCompletionSource = new TaskCompletionSource();
        var runTask = RunPeriodicJob(new TestPeriodicJob
        {
            DoJobRunTime = TimeSpan.FromSeconds(10),
            OnRunningCallback = () => taskCompletionSource.SetResult(),
        }, cancellationTokenSource.Token);

        await taskCompletionSource.Task;
        using var dbContext = BuildContext();
        var jobRecord = await dbContext.PeriodicJobRecord.FindAsync("Test");
        Assert.NotNull(jobRecord);
        Assert.NotNull(jobRecord.InProgress);
        Assert.Equal(DateTime.UtcNow, jobRecord.InProgress.StartTime, TimeSpan.FromSeconds(1));

        await cancellationTokenSource.CancelAsync();
    }

    [Fact]
    public async Task OnlyOneOfManyConcurrentRunnersRunJob()
    {
        int callCount = 0;

        await Task.WhenAll(Enumerable.Range(0, 3).Select(_ => Task.Run(async () =>
        {
            await RunPeriodicJob(new TestPeriodicJob
            {
                DoJobRunTime = TimeSpan.FromMilliseconds(100),
                OnRunningCallback = () => Interlocked.Increment(ref callCount),
            });
        })));

        Assert.Equal(1, callCount);
        using var dbContext = BuildContext();
        var jobRecord = await dbContext.PeriodicJobRecord.FindAsync("Test");
        Assert.NotNull(jobRecord);
        Assert.Null(jobRecord.InProgress);
        Assert.NotNull(jobRecord.Completed);
    }

    [Fact]
    public async Task ConcurrencyTokenUpdateWhileRunningJobWillLogAndAbort()
    {
        var taskCompletionSource = new TaskCompletionSource();
        var runTask = RunPeriodicJob(new TestPeriodicJob
        {
            DoJobRunTime = TimeSpan.FromMilliseconds(100),
            OnRunningCallback = () => taskCompletionSource.SetResult(),
        });

        await taskCompletionSource.Task;
        using var dbContext = BuildContext();
        var jobRecord = await dbContext.PeriodicJobRecord.FindAsync("Test");
        Assert.NotNull(jobRecord);
        jobRecord.ConcurrencyToken = Guid.NewGuid();
        await dbContext.SaveChangesAsync();

        await runTask;

        await dbContext.Entry(jobRecord).ReloadAsync();
        Assert.Null(jobRecord.Completed);
    }

    private async Task RunPeriodicJob(IPeriodicJob job, CancellationToken cancellationToken = default)
    {
        using var dbContext = BuildContext();
        var jobRunner = new PeriodicJobRunner(dbContext, job, this.logger)
        {
            MaxExpectedRunTime = TimeSpan.FromSeconds(10),
        };
        await jobRunner.TryRun(cancellationToken);
    }

    private sealed class TestPeriodicJob : IPeriodicJob
    {
        public string Name => "Test";
        public TimeSpan RunPeriod { get; init; } = TimeSpan.FromMinutes(5);
        public required TimeSpan DoJobRunTime { get; init; }
        public Action OnRunningCallback { get; init; } = () => { };

        public Task Run(CancellationToken cancellationToken)
        {
            OnRunningCallback();
            return Task.Delay(DoJobRunTime, cancellationToken);
        }
    }
}
