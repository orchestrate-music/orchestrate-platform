using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common
{
    public class AsyncQueryableTests
    {
        [Fact]
        public void LinqWorksAsExpected()
        {
            var data = new[] { 1, 2, 3 }.AsAsyncQueryable();
            Assert.Equal(2, data.Count(d => d % 2 == 1));
        }

        [Fact]
        public async Task EfAsyncQueryMethodsAreBroken()
        {
            var data = new[] { 1, 2, 3 }.AsAsyncQueryable();
            await Assert.ThrowsAsync<ArgumentException>(async () => await data.CountAsync(d => d % 2 == 1));
        }
    }
}
