using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

public class TemporalLifetimeTests
{
    public static IEnumerable<object?[]> OpenIsActiveTestCases()
    {
        yield return TestCase(null, -1, false);
        yield return TestCase(null, 0, true);
        yield return TestCase(null, 1, true);
        yield return TestCase(null, null, true);
        yield return TestCase(-2, -2, false);
        yield return TestCase(-2, -1, false);
        yield return TestCase(-2, 0, true);
        yield return TestCase(-2, 1, true);
        yield return TestCase(-2, null, true);
        yield return TestCase(0, 0, true);
        yield return TestCase(0, 1, true);
        yield return TestCase(0, null, true);
        yield return TestCase(1, 2, false);
        yield return TestCase(1, null, false);
    }

    public static IEnumerable<object[]> ClosedIsActiveTestCases()
    {
        return OpenIsActiveTestCases()
            .Where(t => t.All(p => p != null))
            .Select(t => new object[] { (DateTime)t[0]!, (DateTime)t[1]!, t[2]!, t[3]! });
    }

    private static object?[] TestCase(int? start, int? finish, bool isActive)
    {
        // We pass now as a parameter so we don't get "failures" when running tests with
        // Right Click -> Run Tests in context
        // See https://stackoverflow.com/questions/53071355
        var now = DateTime.UtcNow;
        DateTime? GenerateDateTime(int? offset) => offset.HasValue ? now.AddDays(offset.Value) : null;
        return new object?[] { GenerateDateTime(start), GenerateDateTime(finish), now, isActive };
    }

    [Theory]
    [MemberData(nameof(OpenIsActiveTestCases))]
    public void NullableStartFinishIsHandled(DateTime? start, DateTime? finish, DateTime now, bool isActive)
    {
        var openLifetime = new OpenTemporalLifetime
        {
            Start = start,
            Finish = finish,
        };
        Assert.Equal(isActive, openLifetime.IsActiveAt(now));

        var openQueryable = new[] { openLifetime }.AsQueryable();
        Assert.Equal(isActive, openQueryable.WhereIsActiveAt(now).Any());

        var isActiveExpr = TemporalLifetimeExtensions.IsActiveAtExpr<OpenTemporalLifetime>(now);
        Assert.Equal(isActive, openQueryable.Where(isActiveExpr).Any());
    }

    [Theory]
    [MemberData(nameof(ClosedIsActiveTestCases))]
    public void NonNullableStartFinishIsHandled(DateTime start, DateTime finish, DateTime now, bool isActive)
    {
        var closedLifetime = new ClosedTemporalLifetime
        {
            Start = start,
            Finish = finish,
        };
        Assert.Equal(isActive, closedLifetime.IsActiveAt(now));

        var closedQueryable = new[] { closedLifetime }.AsQueryable();
        Assert.Equal(isActive, closedQueryable.WhereIsActiveAt(now).Any());

        var isActiveExpr = TemporalLifetimeExtensions.IsActiveAtExpr<ClosedTemporalLifetime>(now);
        Assert.Equal(isActive, closedQueryable.Where(isActiveExpr).Any());
    }

    [Fact]
    public void WillThrowIfLifetimeAttributesAreMissing()
    {
        var noStartLifetime = new NoStartAttribute();
        Assert.Throws<InvalidOperationException>(() => noStartLifetime.IsActiveAt(DateTime.UtcNow));
        Assert.Throws<InvalidOperationException>(() => new[] { noStartLifetime }.AsQueryable().WhereIsActiveAt(DateTime.UtcNow));

        var noFinishLifetime = new NoFinishAttribute();
        Assert.Throws<InvalidOperationException>(() => noFinishLifetime.IsActiveAt(DateTime.UtcNow));
        Assert.Throws<InvalidOperationException>(() => new[] { noFinishLifetime }.AsQueryable().WhereIsActiveAt(DateTime.UtcNow));
    }

    [Fact]
    public void WillThrowIfLifetimeAttributesArentOnDateTimeTypesAreMissing()
    {
        var noStartLifetime = new InvalidStartAttribute();
        Assert.Throws<InvalidOperationException>(() => noStartLifetime.IsActiveAt(DateTime.UtcNow));
        Assert.Throws<InvalidOperationException>(() => new[] { noStartLifetime }.AsQueryable().WhereIsActiveAt(DateTime.UtcNow));

        var noFinishLifetime = new InvalidFinishAttribute();
        Assert.Throws<InvalidOperationException>(() => noFinishLifetime.IsActiveAt(DateTime.UtcNow));
        Assert.Throws<InvalidOperationException>(() => new[] { noFinishLifetime }.AsQueryable().WhereIsActiveAt(DateTime.UtcNow));
    }

    private sealed class ClosedTemporalLifetime : ITemporalLifetime
    {
        [TemporalLifetimeStart]
        public DateTime Start { get; set; }
        [TemporalLifetimeFinish]
        public DateTime Finish { get; set; }
    }

    private sealed class OpenTemporalLifetime : ITemporalLifetime
    {
        [TemporalLifetimeStart]
        public DateTime? Start { get; set; }
        [TemporalLifetimeFinish]
        public DateTime? Finish { get; set; }
    }

    private sealed class NoStartAttribute : ITemporalLifetime
    {
        public DateTime Start { get; set; }
        [TemporalLifetimeFinish]
        public DateTime Finish { get; set; }
    }

    private sealed class NoFinishAttribute : ITemporalLifetime
    {
        [TemporalLifetimeStart]
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
    }

    private sealed class InvalidStartAttribute : ITemporalLifetime
    {
        [TemporalLifetimeStart]
        public string? Start { get; set; }

        [TemporalLifetimeFinish]
        public DateTime Finish { get; set; }
    }

    private sealed class InvalidFinishAttribute : ITemporalLifetime
    {
        [TemporalLifetimeStart]
        public DateTime Start { get; set; }

        [TemporalLifetimeFinish]
        public string? Finish { get; set; }
    }
}
