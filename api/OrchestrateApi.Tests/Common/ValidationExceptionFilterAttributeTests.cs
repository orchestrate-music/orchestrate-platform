using FluentValidation.Results;
using static OrchestrateApi.Common.ValidationExceptionFilterAttribute;

namespace OrchestrateApi.Tests.Common;

public class ValidationExceptionFilterAttributeTests
{
    [Fact]
    public void ErrorDtoIsGeneratedCorrectly()
    {
        var errorModel = GenerateValidationErrorDto(new List<ValidationFailure>
        {
            new ValidationFailure("Prop", "Message1") {ErrorCode = "A"},
        });

        var errors = Assert.Contains("Prop", errorModel);
        var dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        var dto = Assert.Single(dtos);
        Assert.Equal("A", dto.ErrorCode);
        Assert.Equal("Message1", dto.ErrorMessage);
    }

    [Fact]
    public void MultipleErrorsAreAllGeneratedCorrectly()
    {
        var errorModel = GenerateValidationErrorDto(new List<ValidationFailure>
        {
            new ValidationFailure("Prop", "Message1") {ErrorCode = "A"},
            new ValidationFailure("Prop", "Message2") {ErrorCode = "B"},
        });

        var errors = Assert.Contains("Prop", errorModel);
        var dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        Assert.True(dtos.Count() == 2);
        Assert.Contains(dtos, dto => dto.ErrorCode == "A" && dto.ErrorMessage == "Message1");
        Assert.Contains(dtos, dto => dto.ErrorCode == "B" && dto.ErrorMessage == "Message2");
    }

    [Fact]
    public void NestedPropertiesGetGeneratedCorrectly()
    {
        var errorModel = GenerateValidationErrorDto(new List<ValidationFailure>
        {
            new ValidationFailure("Obj.Prop", "Message1") {ErrorCode = "A"},
        });

        var obj = Assert.Contains("Obj", errorModel);
        var objDict = Assert.IsAssignableFrom<IDictionary<string, object>>(obj);
        var errors = Assert.Contains("Prop", objDict);
        var dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        var dto = Assert.Single(dtos);
        Assert.Equal("A", dto.ErrorCode);
        Assert.Equal("Message1", dto.ErrorMessage);
    }

    [Fact]
    public void MultipleNestedPropertiesGetGeneratedCorrectly()
    {
        var errorModel = GenerateValidationErrorDto(new List<ValidationFailure>
        {
            new ValidationFailure("Obj.Prop", "Message1") {ErrorCode = "A"},
            new ValidationFailure("Obj.Prop2", "Message2") {ErrorCode = "B"},
        });

        var obj = Assert.Contains("Obj", errorModel);
        var objDict = Assert.IsAssignableFrom<IDictionary<string, object>>(obj);

        var errors = Assert.Contains("Prop", objDict);
        var dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        var dto = Assert.Single(dtos);
        Assert.Equal("A", dto.ErrorCode);
        Assert.Equal("Message1", dto.ErrorMessage);

        errors = Assert.Contains("Prop2", objDict);
        dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        dto = Assert.Single(dtos);
        Assert.Equal("B", dto.ErrorCode);
        Assert.Equal("Message2", dto.ErrorMessage);
    }

    [Fact]
    public void NestedAndDuplicatePropertiesGetGeneratedCorrectly()
    {
        var errorModel = GenerateValidationErrorDto(new List<ValidationFailure>
        {
            new ValidationFailure("Obj.Prop", "Message1") {ErrorCode = "A"},
            new ValidationFailure("Obj.Prop", "Message2") {ErrorCode = "B"},
        });

        var obj = Assert.Contains("Obj", errorModel);
        var objDict = Assert.IsAssignableFrom<IDictionary<string, object>>(obj);

        var errors = Assert.Contains("Prop", objDict);
        var dtos = Assert.IsAssignableFrom<IEnumerable<ErrorDto>>(errors);
        Assert.True(dtos.Count() == 2);
        Assert.Contains(dtos, dto => dto.ErrorCode == "A" && dto.ErrorMessage == "Message1");
        Assert.Contains(dtos, dto => dto.ErrorCode == "B" && dto.ErrorMessage == "Message2");
    }
}
