using OrchestrateApi.Common;

namespace OrchestrateApi.Tests.Common;

public class AppConfigurationTests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:URI-like parameters should not be strings", Justification = "That's the API")]
    [Theory]
    [InlineData(
        "postgres://user_name:password@hostname.fqdn:5432/db_name?sslmode=disable",
        "Host=hostname.fqdn; SSL Mode=disable; Trust Server Certificate=true; Port=5432; Database=db_name; Username=user_name; Password=password")]
    [InlineData(
        "postgres://user_name:password@hostname.fqdn:5432/db_name?Sslmode=disable",
        "Host=hostname.fqdn; SSL Mode=disable; Trust Server Certificate=true; Port=5432; Database=db_name; Username=user_name; Password=password")]
    [InlineData(
        "postgres://user_name:password@hostname.fqdn:5432/db_name",
        "Host=hostname.fqdn; SSL Mode=Prefer; Trust Server Certificate=true; Port=5432; Database=db_name; Username=user_name; Password=password")]
    public void UrlToConnectionStringParsesCorrectly(string url, string connectionString)
    {
        Assert.Equal(connectionString, AppConfiguration.ConvertUrlStringToConnectionString(url));
    }
}
