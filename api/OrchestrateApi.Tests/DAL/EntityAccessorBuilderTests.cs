using System.Security.Claims;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Access;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.DAL
{
    public class EntityAccessorBuilderTests
    {
        private readonly EntityAccessorBuilder<TestEntity> builder;
        private readonly ClaimsPrincipal viewer;
        private readonly SecuredQueryData viewerQueryData;
        private readonly ClaimsPrincipal editor;
        private readonly SecuredQueryData editorQueryData;

        public EntityAccessorBuilderTests()
        {
            this.builder = new();
            this.viewer = new(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, "testuser@email.com"),
                new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Viewer").Serialise()),
            }));
            this.viewerQueryData = SecuredQueryData.ForUserInTenant(this.viewer, Guid.Empty);
            this.editor = new(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, "testuser2@email.com"),
                new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, "Editor").Serialise()),
            }));
            this.editorQueryData = SecuredQueryData.ForUserInTenant(this.editor, Guid.Empty);
        }

        [Fact]
        public void CanReadTableIsPassedThroughCorrectly()
        {
            var accessor = builder.CanReadTableWhen(qd => qd.Roles.Contains("Editor")).Build();
            Assert.False(accessor.CanReadTable(this.viewerQueryData));
            Assert.True(accessor.CanReadTable(this.editorQueryData));
        }

        [Fact]
        public void CanReadRowIsPassedThroughCorrectly()
        {
            var accessor = builder.CanReadRowWhen(qd => e => e.Id % 2 == 0).Build();
            var hasAccessToRow = accessor.CanReadRow(this.viewerQueryData)!.Compile();
            Assert.False(hasAccessToRow(new TestEntity { Id = 1 }));
            Assert.True(hasAccessToRow(new TestEntity { Id = 2 }));
        }

        [Fact]
        public void CanReadAllRowsSetsCorrectly()
        {
            var accessor = builder.CanReadAllRows().Build();
            Assert.Null(accessor.CanReadRow(this.viewerQueryData));
        }

        [Fact]
        public void CanWriteToTableIsPassedThroughCorrectly()
        {
            var accessor = builder.CanWriteToTableWhen(queryData => queryData.HasAtLeastOneRole("Editor")).Build();
            Assert.False(accessor.CanWriteToTable(this.viewerQueryData));
            Assert.True(accessor.CanWriteToTable(this.editorQueryData));
        }

        [Fact]
        public void DefaultsAreSetCorrectly()
        {
            var noRoleUser = new ClaimsPrincipal(new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, "testuser3@email.com"),
            }));
            var noRoleQueryData = SecuredQueryData.ForUserInTenant(noRoleUser, Guid.Empty);

            var accessor = builder.BuildDefault();

            Assert.False(accessor.CanReadTable(noRoleQueryData));
            Assert.True(accessor.CanReadTable(this.viewerQueryData));
            Assert.True(accessor.CanReadTable(this.editorQueryData));

            Assert.Null(accessor.CanReadRow(noRoleQueryData));
            Assert.Null(accessor.CanReadRow(this.viewerQueryData));
            Assert.Null(accessor.CanReadRow(this.editorQueryData));

            Assert.False(accessor.CanWriteToTable(noRoleQueryData));
            Assert.True(accessor.CanWriteToTable(this.editorQueryData));
            Assert.True(accessor.CanWriteToTable(this.editorQueryData));
        }

        private sealed class TestEntity
        {
            public int Id { get; set; }

            public int Fk { get; set; }
        }
    }
}
