using System.Linq.Expressions;
using Breeze.Persistence;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Access;
using OrchestrateApi.DAL.Hook;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.DAL
{
    public class OrchestratePersistenceManagerTests : AutoRollbackTestHarness
    {
        private readonly MockEntityHookFactoryRegistrar registrar;
        private readonly OrchestratePersistenceManager manager;

        public OrchestratePersistenceManagerTests()
        {
            this.registrar = new();
            this.manager = new(Context, null!, new AllowAllEntityAccessorRegistrar(), this.registrar, new StaticTenantService(Guid.Empty));
        }

        private IEnumerable<IEntityInfo<MockEntity2>>? LastMockEntity2Infos
            => this.registrar.GetHookFactory<StoreLastMockEntity2sHookFactory>().LastHook?.LastEntityInfos;

        [Fact]
        public async Task AddingToSaveMapDuringAfterSaveAsyncDoesntThrowCollectionModifiedException()
        {
            this.registrar.HookFactories[typeof(MockEntity)] = new AddToSaveMapMockEntityHookFactory();

            var entityInfo = this.manager.CreateEntityInfo(new MockEntity(), EntityState.Modified);
            var saveMap = await this.manager.StubbedSaveAsync(new()
            {
                [typeof(MockEntity)] = new() { entityInfo },
            });

            Assert.Equal(2, saveMap[typeof(MockEntity)].Count);
            Assert.Single(saveMap[typeof(MockEntity2)]);
        }

        [Fact]
        public async Task AddingToSaveMapInPreviousAfterSaveDoesntAddEntityToSubsequentAfterSave()
        {
            this.registrar.HookFactories[typeof(MockEntity)] = new AddToSaveMapMockEntityHookFactory();
            this.registrar.HookFactories[typeof(MockEntity2)] = new StoreLastMockEntity2sHookFactory();

            var entityInfo = this.manager.CreateEntityInfo(new MockEntity(), EntityState.Modified);
            var entityInfo2 = this.manager.CreateEntityInfo(new MockEntity2(), EntityState.Modified);
            var saveMap = await this.manager.StubbedSaveAsync(new()
            {
                [typeof(MockEntity)] = new() { entityInfo },
                [typeof(MockEntity2)] = new() { entityInfo2 },
            });

            Assert.Equal(2, saveMap[typeof(MockEntity2)].Count);
            Assert.NotNull(LastMockEntity2Infos);
            Assert.Single(LastMockEntity2Infos);
            Assert.Same(entityInfo2, LastMockEntity2Infos.First().Raw); ;
        }

        [Theory]
        [InlineData(EntityState.Unchanged, null, true)]
        [InlineData(EntityState.Added, null, false)]
        [InlineData(EntityState.Added, "", false)]
        [InlineData(EntityState.Added, "valid", true)]
        [InlineData(EntityState.Added, "invalidValidate", false)]
        [InlineData(EntityState.Modified, null, false)]
        [InlineData(EntityState.Modified, "", false)]
        [InlineData(EntityState.Modified, "valid", true)]
        [InlineData(EntityState.Modified, "invalidValidate", false)]
        [InlineData(EntityState.Deleted, null, true)]
        [InlineData(EntityState.Detached, null, true)]
        public async Task ValidationOnlyOccursForAddedAndModifiedEntities(EntityState entityState, string? name, bool isValid)
        {
            this.registrar.HookFactories[typeof(MemberInstrument)] = new ValidatingMemberInstrumentHookFactory();

            var entityInfo = this.manager.CreateEntityInfo(new MemberInstrument { InstrumentName = name ?? string.Empty }, entityState);
            var saveMap = new Dictionary<Type, List<EntityInfo>>
            {
                [typeof(MemberInstrument)] = [entityInfo],
            };

            if (isValid)
            {
                await this.manager.StubbedSaveAsync(saveMap);
            }
            else
            {
                var ex = await Assert.ThrowsAsync<EntityErrorsException>(() => this.manager.StubbedSaveAsync(saveMap));
                Assert.Single(ex.EntityErrors);
                Assert.Equal("InvalidValue", ex.EntityErrors[0].ErrorName);
                if (name == "invalid")
                {
                    Assert.Equal("mock_invalid", ex.EntityErrors[0].ErrorMessage);
                }
            }
        }

        [Theory]
        [InlineData("valid", true)]
        [InlineData("invalidBeforeSave", false)]
        public async Task EntityErrorsInBeforeSaveGetThrown(string name, bool isValid)
        {
            this.registrar.HookFactories[typeof(MemberInstrument)] = new ValidatingMemberInstrumentHookFactory();

            var entityInfo = this.manager.CreateEntityInfo(new MemberInstrument { InstrumentName = name }, EntityState.Modified);
            var saveMap = new Dictionary<Type, List<EntityInfo>>
            {
                [typeof(MemberInstrument)] = new() { entityInfo },
            };

            if (isValid)
            {
                await this.manager.StubbedSaveAsync(saveMap);
            }
            else
            {
                var ex = await Assert.ThrowsAsync<EntityErrorsException>(() => this.manager.StubbedSaveAsync(saveMap));
                Assert.Single(ex.EntityErrors);
                Assert.Equal("InvalidValue", ex.EntityErrors[0].ErrorName);
                Assert.Equal("mock_invalid", ex.EntityErrors[0].ErrorMessage);
            }
        }

        [Theory]
        [InlineData("valid", true)]
        [InlineData("invalidAfterSave", false)]
        public async Task EntityErrorsInAfterSaveGetThrown(string name, bool isValid)
        {
            this.registrar.HookFactories[typeof(MemberInstrument)] = new ValidatingMemberInstrumentHookFactory();

            var entityInfo = this.manager.CreateEntityInfo(new MemberInstrument { InstrumentName = name }, EntityState.Modified);
            var saveMap = new Dictionary<Type, List<EntityInfo>>
            {
                [typeof(MemberInstrument)] = new() { entityInfo },
            };

            if (isValid)
            {
                await this.manager.StubbedSaveAsync(saveMap);
            }
            else
            {
                var ex = await Assert.ThrowsAsync<EntityErrorsException>(() => this.manager.StubbedSaveAsync(saveMap));
                Assert.Single(ex.EntityErrors);
                Assert.Equal("InvalidValue", ex.EntityErrors[0].ErrorName);
                Assert.Equal("mock_invalid", ex.EntityErrors[0].ErrorMessage);
            }
        }

        private sealed class MockEntity
        {
        }

        private sealed class MockEntity2
        {
        }

        private sealed class AllowAllEntityAccessorRegistrar : IEntityAccessorRegistrar
        {
            public IEntityAccessor GetEntityAccessor(Type entityType)
            {
                return new AllowAllEntityAccessor();
            }
        }

        private sealed class AllowAllEntityAccessor : IEntityAccessor
        {
            public Type Type => typeof(MockEntity); // Still handles everything

            public Expression? CanReadRow(SecuredQueryData queryData) => null;

            public bool CanReadTable(SecuredQueryData queryData) => true;

            public bool CanWriteToTable(SecuredQueryData queryData) => true;
        }

        private sealed class MockEntityHookFactoryRegistrar : IEntityHookFactoryRegistrar
        {
            public Dictionary<Type, IEntityHookFactory> HookFactories { get; } = new();

            public IEntityHookFactory GetHookFactory(Type entityType) => HookFactories[entityType];

            public T GetHookFactory<T>() => (T)HookFactories.First(kvp => kvp.Value.GetType() == typeof(T)).Value;
        }

        private sealed class AddToSaveMapMockEntityHookFactory : IEntityHookFactory
        {
            public Type EntityType => typeof(MockEntity);

            public IEntityHook BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
            {
                return new AddToSaveMapMockEntityHook { PersistenceManager = persistenceManager, SaveMap = saveMap };
            }
        }

        private sealed class AddToSaveMapMockEntityHook : EntityHook<MockEntity>
        {
            public override IEnumerable<EntityError> Validate(IEntityInfo<MockEntity> entityInfo) => Enumerable.Empty<EntityError>();

            public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<MockEntity>> entityInfos, CancellationToken cancellationToken)
                => Task.FromResult(Enumerable.Empty<EntityError>());

            public override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<MockEntity>> entityInfos, CancellationToken cancellationToken)
            {
                foreach (var _ in entityInfos)
                {
                    SaveMap.Add(PersistenceManager.CreateEntityInfo(new MockEntity(), EntityState.Added));
                    SaveMap.Add(PersistenceManager.CreateEntityInfo(new MockEntity2(), EntityState.Added));
                }
                return Task.FromResult(Enumerable.Empty<EntityError>());
            }
        }

        private sealed class StoreLastMockEntity2sHookFactory : IEntityHookFactory
        {
            public Type EntityType => typeof(MockEntity2);

            public StoreLastMockEntity2sHook? LastHook { get; private set; }

            public IEntityHook BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
            {
                LastHook = new StoreLastMockEntity2sHook { PersistenceManager = persistenceManager, SaveMap = saveMap };
                return LastHook;
            }
        }

        private sealed class StoreLastMockEntity2sHook : EntityHook<MockEntity2>
        {
            public IEnumerable<IEntityInfo<MockEntity2>>? LastEntityInfos { get; private set; }
            public override IEnumerable<EntityError> Validate(IEntityInfo<MockEntity2> entityInfo) => Enumerable.Empty<EntityError>();

            public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<MockEntity2>> entityInfos, CancellationToken cancellationToken)
                => Task.FromResult(Enumerable.Empty<EntityError>());

            public override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<MockEntity2>> entityInfos, CancellationToken cancellationToken)
            {
                LastEntityInfos = entityInfos;
                return Task.FromResult(Enumerable.Empty<EntityError>());
            }
        }

        private sealed class ValidatingMemberInstrumentHookFactory : IEntityHookFactory
        {
            public Type EntityType => typeof(MemberInstrument);

            public IEntityHook BuildEntityHook(OrchestratePersistenceManager persistenceManager, Dictionary<Type, List<EntityInfo>> saveMap)
            {
                return new ValidatingMemberInstrumentHook { PersistenceManager = persistenceManager, SaveMap = saveMap };
            }
        }

        private sealed class ValidatingMemberInstrumentHook : EntityHook<MemberInstrument>
        {
            public override IEnumerable<EntityError> Validate(IEntityInfo<MemberInstrument> entityInfo)
            {
                return entityInfo.Entity.InstrumentName == "invalidValidate"
                    ? new[] { new EntityError { ErrorName = "InvalidValue", ErrorMessage = "mock_invalid" } }
                    : Enumerable.Empty<EntityError>();
            }

            public override Task<IEnumerable<EntityError>> BeforeSaveAsync(IEnumerable<IEntityInfo<MemberInstrument>> entityInfos, CancellationToken cancellationToken)
            {
                var errors = entityInfos.First().Entity.InstrumentName == "invalidBeforeSave"
                    ? new[] { new EntityError { ErrorName = "InvalidValue", ErrorMessage = "mock_invalid" } }
                    : Enumerable.Empty<EntityError>();
                return Task.FromResult(errors);
            }

            public override Task<IEnumerable<EntityError>> AfterSaveAsync(IEnumerable<IEntityInfo<MemberInstrument>> entityInfos, CancellationToken cancellationToken)
            {
                var errors = entityInfos.First().Entity.InstrumentName == "invalidAfterSave"
                    ? new[] { new EntityError { ErrorName = "InvalidValue", ErrorMessage = "mock_invalid" } }
                    : Enumerable.Empty<EntityError>();
                return Task.FromResult(errors);
            }
        }
    }
}
