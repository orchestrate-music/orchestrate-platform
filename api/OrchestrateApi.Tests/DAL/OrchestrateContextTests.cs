using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Npgsql;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;

namespace OrchestrateApi.Tests.DAL
{
    public sealed class OrchestrateContextTests : IDisposable
    {
        private readonly DbConnection connection;
        private readonly DbContextOptions<OrchestrateContext> dbOptions;

        public OrchestrateContextTests()
        {
            // Reuse the connection + transaction between contexts below so we can query between the
            // contexts without needing to commit the changes
            // https://docs.microsoft.com/en-us/ef/core/saving/transactions#cross-context-transaction
            this.connection = new NpgsqlConnection(TestHelper.AppConfig.ConnectionString);
            this.dbOptions = new DbContextOptionsBuilder<OrchestrateContext>()
                .UseNpgsql(this.connection)
                .Options;

            // Even though we don't use the seed, it will drop the database, so we
            // need to wait for it to be ready!
            using var context = new OrchestrateContext(
                this.dbOptions,
                new StaticTenantService(OrchestrateSeed.TenantId),
                TestHelper.BuildMockLogger<OrchestrateContext>(),
                []);
            SharedDatabaseFixture.Seed(context);
        }

        [Fact]
        public void TenantIdAutomaticallyGetsAddedToEntitiesAndCanBeSaved()
        {
            var tenantId = Guid.NewGuid();
            using var context = ContextForTenant(tenantId);
            using var transaction = context.Database.BeginTransaction();

            var assetEntry = context.Add(new Asset
            {
                Description = "Some asset",
            });
            Assert.Equal(tenantId, assetEntry.Property<Guid>(ITenanted.Column).CurrentValue);
        }

        [Fact]
        public void ContextsForDifferentTenantsCantReadEachOthersData()
        {
            var tenantIdA = Guid.NewGuid();
            using var contextA = ContextForTenant(tenantIdA);
            using var transaction = contextA.Database.BeginTransaction();
            contextA.Add(new Tenant
            {
                Id = tenantIdA,
                Name = tenantIdA.ToString(),
                Abbreviation = tenantIdA.ToString()[..12],
                TimeZone = "Australia/Perth",
            });
            contextA.Add(new Asset
            {
                Description = tenantIdA.ToString(),
            });
            contextA.SaveChanges();

            var tenantIdB = Guid.NewGuid();
            using var contextB = ContextForTenant(tenantIdB);
            contextB.Database.UseTransaction(transaction.GetDbTransaction());
            contextB.Add(new Tenant
            {
                Id = tenantIdB,
                Name = tenantIdB.ToString(),
                Abbreviation = tenantIdB.ToString()[..12],
                TimeZone = "Australia/Perth",
            });
            contextB.Add(new Asset
            {
                Description = tenantIdB.ToString(),
            });
            contextB.SaveChanges();

            Assert.Single(contextA.Tenant.Where(e => e.Name == tenantIdA.ToString()).ToList());
            Assert.Single(contextA.Asset.Where(e => e.Description == tenantIdA.ToString()).ToList());
            Assert.Empty(contextA.Tenant.Where(e => e.Name == tenantIdB.ToString()).ToList());
            Assert.Empty(contextA.Asset.Where(e => e.Description == tenantIdB.ToString()).ToList());

            Assert.Empty(contextB.Tenant.Where(e => e.Name == tenantIdA.ToString()).ToList());
            Assert.Empty(contextB.Asset.Where(e => e.Description == tenantIdA.ToString()).ToList());
            Assert.Single(contextB.Tenant.Where(e => e.Name == tenantIdB.ToString()).ToList());
            Assert.Single(contextB.Asset.Where(e => e.Description == tenantIdB.ToString()).ToList());
        }

        [Fact]
        public void DataFromMultipleTenantsCanBeReadIfGlobalQueryFilterDisabled()
        {
            var tenantIdA = Guid.NewGuid();
            using var contextA = ContextForTenant(tenantIdA);
            using var transaction = contextA.Database.BeginTransaction();
            contextA.Add(new Tenant
            {
                Id = tenantIdA,
                Name = tenantIdA.ToString(),
                Abbreviation = tenantIdA.ToString()[..12],
                TimeZone = "Australia/Perth",
            });
            contextA.Add(new Asset
            {
                Description = tenantIdA.ToString(),
            });
            contextA.SaveChanges();

            var tenantIdB = Guid.NewGuid();
            using var contextB = ContextForTenant(tenantIdB);
            contextB.Database.UseTransaction(transaction.GetDbTransaction());
            contextB.Add(new Tenant
            {
                Id = tenantIdB,
                Name = tenantIdB.ToString(),
                Abbreviation = tenantIdB.ToString()[..12],
                TimeZone = "Australia/Perth",
            });
            contextB.Add(new Asset
            {
                Description = tenantIdB.ToString(),
            });
            contextB.SaveChanges();

            // We can't assume that the seed has been completed here
            // Tenant counts could be 2 or 3, so check for that
            // Asset count should be at least 2, so check for that
            using var contextC = ContextForTenant(Guid.NewGuid());
            contextC.Database.UseTransaction(transaction.GetDbTransaction());
            Assert.True(contextC.Tenant.IgnoreQueryFilters().Count() is >= 2 and <= 3);
            Assert.True(contextC.Asset.IgnoreQueryFilters().Count() is >= 2);
            Assert.Single(contextC.Tenant.IgnoreQueryFilters().Where(e => e.Name == tenantIdA.ToString()).ToList());
            Assert.Single(contextC.Asset.IgnoreQueryFilters().Where(e => e.Description == tenantIdA.ToString()).ToList());
            Assert.Single(contextC.Tenant.IgnoreQueryFilters().Where(e => e.Name == tenantIdB.ToString()).ToList());
            Assert.Single(contextC.Asset.IgnoreQueryFilters().Where(e => e.Description == tenantIdB.ToString()).ToList());
        }

        [Fact]
        public async Task ModifyingAnExistingEntityWontChangeTheTenantId()
        {
            var tenantIdA = Guid.NewGuid();
            using var contextA = ContextForTenant(tenantIdA);
            using var transaction = await contextA.Database.BeginTransactionAsync();
            contextA.Add(new Tenant
            {
                Id = tenantIdA,
                Name = tenantIdA.ToString(),
                Abbreviation = tenantIdA.ToString()[..12],
                TimeZone = "Australia/Perth",
            });
            contextA.Add(new Asset
            {
                Description = tenantIdA.ToString(),
            });
            await contextA.SaveChangesAsync();

            using var contextB = ContextForTenant(Guid.NewGuid());
            await contextB.Database.UseTransactionAsync(transaction.GetDbTransaction());
            var assetA = await contextB.Asset.IgnoreQueryFilters().WhereInTenant(tenantIdA).FirstAsync();

            assetA.DateAcquired = DateTime.UtcNow;
            Assert.Equal(tenantIdA, assetA.TenantIdConcrete(contextB));

            contextB.Remove(assetA);
            Assert.Equal(tenantIdA, assetA.TenantIdConcrete(contextB));
        }

        private OrchestrateContext ContextForTenant(Guid tenantId)
        {
            return new OrchestrateContext(
                this.dbOptions,
                new StaticTenantService(tenantId),
                TestHelper.BuildMockLogger<OrchestrateContext>(),
                []);
        }

        public void Dispose()
        {
            this.connection.Dispose();
        }
    }
}
