using System.Security.Claims;
using System.Text.Json;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Storage;

public class EntityAttachmentStorageStrategyTests
{
    public EntityAttachmentStorageStrategyTests()
    {
        StorageStrategy = new MockEntityAttachmentStorageStrategy();
    }

    private EntityAttachmentStorageStrategy StorageStrategy { get; }

    [Theory]
    [InlineData(""" {"Invalid": true} """, false, null)]
    [InlineData(""" {"Sensitivity": "Invalid"} """, false, null)]
    [InlineData(""" {"Sensitivity": "Secured"} """, true, EntityAttachmentSensitivity.Secured)]
    [InlineData(""" {"Sensitivity": "Sensitive"} """, true, EntityAttachmentSensitivity.Sensitive)]
    public void AccessDataGetsParsedCorrectly(string rawData, bool isValid, EntityAttachmentSensitivity? sensitivity)
    {
        Assert.Equal(isValid, StorageStrategy.TryParseAccessData(JsonDocument.Parse(rawData), out var accessData));

        if (isValid)
        {
            var data = Assert.IsType<EntityAttachmentAccessData>(accessData);
            Assert.True(sensitivity.HasValue);
            Assert.Equal(sensitivity!.Value, data.Sensitivity);
        }
    }

    [Theory]
    [InlineData(StandardRole.Viewer, EntityAttachmentSensitivity.Secured, true)]
    [InlineData(StandardRole.Viewer, EntityAttachmentSensitivity.Sensitive, false)]
    [InlineData(StandardRole.Editor, EntityAttachmentSensitivity.Secured, true)]
    [InlineData(StandardRole.Editor, EntityAttachmentSensitivity.Sensitive, true)]
    public void UserCanReadMatchesToRole(string role, EntityAttachmentSensitivity sensitivity, bool canRead)
    {
        var user = new ClaimsPrincipal(new ClaimsIdentity(new[]
        {
            new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, role).Serialise()),
        }));
        Assert.Equal(canRead, StorageStrategy.UserCanRead(
            user, Guid.Empty, new EntityAttachmentAccessData { Sensitivity = sensitivity }));

        // ReadableBlobs should produce exact same result
        var isReadable = StorageStrategy.ReadableBlobsExpr(user, Guid.Empty).Compile();
        var blob = new OrchestrateBlob
        {
            StorageStrategy = new()
            {
                AccessData = JsonSerializer.SerializeToDocument(
                    new EntityAttachmentAccessData { Sensitivity = sensitivity }),
            },
        };
        Assert.Equal(canRead, isReadable(blob));
    }

    [Theory]
    [InlineData(StandardRole.Viewer, EntityAttachmentSensitivity.Secured, false)]
    [InlineData(StandardRole.Viewer, EntityAttachmentSensitivity.Sensitive, false)]
    [InlineData(StandardRole.Editor, EntityAttachmentSensitivity.Secured, true)]
    [InlineData(StandardRole.Editor, EntityAttachmentSensitivity.Sensitive, true)]
    public void UserCanWriteMatchesToRole(string role, EntityAttachmentSensitivity sensitivity, bool canWrite)
    {
        var user = new ClaimsPrincipal(new ClaimsIdentity(new[]
        {
            new Claim(ClaimTypes.Role, new TenantRole(Guid.Empty, role).Serialise()),
        }));

        Assert.Equal(canWrite, StorageStrategy.UserCanWrite(
            user, Guid.Empty, new EntityAttachmentAccessData { Sensitivity = sensitivity }));
    }

    [Theory]
    [InlineData(""" {"Invalid": true} """, false, null)]
    [InlineData(""" {"EntityId": false} """, false, null)]
    [InlineData(""" {"EntityId": "5"} """, true, 5)]
    [InlineData(""" {"EntityId": 5} """, true, 5)]
    public void LinkDataGetsParsedCorrectly(string rawData, bool isValid, int? entityId)
    {
        Assert.Equal(isValid, StorageStrategy.TryParseLinkData(JsonDocument.Parse(rawData), out var linkData));

        if (isValid)
        {
            var data = Assert.IsType<EntityAttachmentLinkData>(linkData);
            Assert.True(entityId.HasValue);
            Assert.Equal(entityId!.Value, data.EntityId);
        }
    }

    private sealed class MockEntityAttachmentStorageStrategy : EntityAttachmentStorageStrategy
    {
        public override FeaturePermission SecuredReadPermission => FeaturePermission.AssetRead;

        public override FeaturePermission WriteAndSensitiveReadPermission => FeaturePermission.AssetWrite;

        public override string Name => "MockAttachment";

        public override object CreateLinkingEntity(Guid blobId, EntityAttachmentLinkData linkData) => new MockLinkingEntity();

        public override IQueryable<Guid> LinkedBlobIds(OrchestrateContext dbContext, EntityAttachmentLinkData linkData)
        {
            throw new NotImplementedException();
        }

        public override IQueryable<Guid> ValidBlobIds(OrchestrateContext dbContext)
        {
            throw new NotImplementedException();
        }
    }

    private sealed class MockLinkingEntity { }
}
