using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests
{
    public sealed class SharedDatabaseFixture
    {
        private static readonly Lock seedLock = new Lock();
        private static bool isSeeded;

        public static OrchestrateContext BuildContext(ITenantService? tenantService = null)
        {
            var dbOptions = new DbContextOptionsBuilder<OrchestrateContext>()
                .UseNpgsql(TestHelper.AppConfig.ConnectionString)
                .Options;
            tenantService ??= new StaticTenantService(OrchestrateSeed.TenantId);
            var dbContext = new OrchestrateContext(
                dbOptions,
                tenantService,
                TestHelper.BuildMockLogger<OrchestrateContext>(),
                []);
            // TODO Use EFCore 9 UseSeeding/UseAsyncSeeding
            Seed(dbContext);
            return dbContext;
        }

        public static void Seed(OrchestrateContext context)
        {
            if (isSeeded)
            {
                return;
            }

            lock (seedLock)
            {
                if (isSeeded)
                {
                    return;
                }

                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                new OrchestrateSeed(context).Seed();

                context.SaveChanges();

                isSeeded = true;
            }
        }
    }
}
