using System.Diagnostics.CodeAnalysis;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests;

[SuppressMessage("", "CA1819")]
public class MockStorageProvider : IStorageProvider
{
    public byte[] ReadData { get; set; } = Array.Empty<byte>();
    public byte[]? WrittenData { get; set; }
    public bool WritesSuccessfully { get; set; } = true;
    public bool DeletesSuccessfully { get; set; } = true;

    public async Task<IStreamableDisposible?> ReadBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken)
    {
        return await Task.FromResult(new MockStreamableDisposable(ReadData));
    }

    public async Task<long> WriteBlobAsync(OrchestrateBlob blob, Stream bytes, CancellationToken cancellationToken)
    {
        if (!WritesSuccessfully)
        {
            return -1;
        }

        using var ms = new MemoryStream();
        await bytes.CopyToAsync(ms, cancellationToken);
        WrittenData = ms.ToArray();
        return ms.Length;
    }

    public Task<bool> DeleteBlobAsync(OrchestrateBlob blob, CancellationToken cancellationToken)
    {
        return Task.FromResult(DeletesSuccessfully);
    }

    private sealed class MockStreamableDisposable : IStreamableDisposible
    {
        public MockStreamableDisposable(byte[] data)
        {
            Stream = new MemoryStream(data);
        }

        public Stream Stream { get; }

        public void Dispose() => Stream.Dispose();
    }
}
