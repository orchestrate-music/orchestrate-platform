using Microsoft.EntityFrameworkCore;
using Npgsql;
using OrchestrateApi.DAL;

namespace OrchestrateApi.Tests
{
    public class SeedTests
    {
        // [Fact]
        [Fact(Skip = "Only to be run manually")]
        public async Task DatabaseWillBeSeeded()
        {
            var connectionString = TestHelper.AppConfig.ConnectionString;
            using var connection = new NpgsqlConnection(connectionString);
            var dbOptions = new DbContextOptionsBuilder<OrchestrateContext>().UseNpgsql(connection).Options;
            using var context = new OrchestrateContext(
                dbOptions,
                new StaticTenantService(OrchestrateSeed.TenantId),
                TestHelper.BuildMockLogger<OrchestrateContext>(),
                []);

            SharedDatabaseFixture.Seed(context);

            // Just some smoke tests to make sure it completed successfully
            Assert.True(await context.Asset.AnyAsync());
            Assert.True(await context.Score.AnyAsync());
            Assert.True(await context.Member.AnyAsync());
            Assert.True(await context.Ensemble.AnyAsync());
            Assert.True(await context.Concert.AnyAsync());
        }
    }
}
