using System.Security.Claims;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public class TenantControllerTests : AutoRollbackTestHarness
    {
        private readonly HttpContext httpContext;
        private readonly TenantController tenantController;

        public TenantControllerTests()
        {
            this.httpContext = new DefaultHttpContext();
            this.tenantController = new(Context)
            {
                ControllerContext = new()
                {
                    HttpContext = this.httpContext,
                }
            };

            Context.Add(new Tenant
            {
                Name = "Test Group",
                Abbreviation = "test",
                TimeZone = "Australia/Perth",
            });
            Context.SaveChanges();
        }

        [Fact]
        public async Task GlobalAdminCanAccessAllTenants()
        {
            this.httpContext.User = SeedUserThatIsGlobalAdmin(true);

            var tenants = await this.tenantController.GetAllTenantsAsync();
            Assert.Equal(2, tenants.Count());
            Assert.Contains(tenants, t => t.Name == "Motley Crue");
            Assert.Contains(tenants, t => t.Name == "Test Group");

            var mc = await this.tenantController.GetTenantByAbbreviationAsync("mc");
            Assert.Equal("Motley Crue", mc?.Name);

            var testGroup = await this.tenantController.GetTenantByAbbreviationAsync("test");
            Assert.Equal("Test Group", testGroup?.Name);
        }

        [Fact]
        public async Task NormalUserCanOnlyAccessAuthorisedTenants()
        {
            this.httpContext.User = SeedUserThatIsGlobalAdmin(false);

            var tenants = await this.tenantController.GetAllTenantsAsync();
            Assert.Single(tenants);
            Assert.Contains(tenants, t => t.Name == "Motley Crue");

            var mc = await this.tenantController.GetTenantByAbbreviationAsync("mc");
            Assert.Equal("Motley Crue", mc?.Name);

            Assert.Null(await this.tenantController.GetTenantByAbbreviationAsync("test"));
        }

        private static ClaimsPrincipal SeedUserThatIsGlobalAdmin(bool isGlobalAdmin)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Role, new TenantRole(OrchestrateSeed.TenantId, StandardRole.Viewer).Serialise()),
            };

            if (isGlobalAdmin)
            {
                claims.Add(new Claim(ClaimsPrincipalExtensions.GlobalAdminClaimName, bool.TrueString));
            }

            return new ClaimsPrincipal(new ClaimsIdentity(claims));
        }
    }
}
