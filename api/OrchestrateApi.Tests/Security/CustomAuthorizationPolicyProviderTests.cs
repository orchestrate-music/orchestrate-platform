using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public class CustomAuthorizationPolicyProviderTests
    {
        private readonly IAuthorizationPolicyProvider provider = new CustomAuthorizationPolicyProvider();

        [Theory]
        [InlineData("Invalid", null)]
        [InlineData(IsGlobalAdminRequirement.PolicyName, typeof(IsGlobalAdminRequirement))]
        public async Task PoliciesAreGeneratedCorrectly(string policyName, Type? requirementType)
        {
            var policy = await this.provider.GetPolicyAsync(policyName);

            if (requirementType == null)
            {
                Assert.Null(policy);
            }
            else
            {
                Assert.NotNull(policy);
                var requirement = Assert.Single(policy.Requirements);
                Assert.IsType(requirementType, requirement);
            }
        }

        [Fact]
        public async Task FallbackPolicyIsNull()
        {
            Assert.Null(await this.provider.GetFallbackPolicyAsync());
        }

        [Fact]
        public async Task DefaultPolicyRequiresAuthenticated()
        {
            var policy = await this.provider.GetDefaultPolicyAsync();
            Assert.Single(policy.Requirements);
            Assert.IsType<DenyAnonymousAuthorizationRequirement>(policy.Requirements[0]);
        }
    }
}
