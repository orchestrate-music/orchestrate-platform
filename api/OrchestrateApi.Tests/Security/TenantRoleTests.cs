using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public class TenantRoleTests
    {
        [Fact]
        public void SerialiseWorkaAsExpected()
        {
            var tenantId = Guid.NewGuid();
            var tenantRole = new TenantRole(tenantId, "Test");
            Assert.Equal($"{tenantId}::Test", tenantRole.Serialise());
        }

        [Fact]
        public void ParseWorkaAsExpected()
        {
            var tenantId = Guid.NewGuid();
            var tenantRole = TenantRole.Parse($"{tenantId}::Test");
            Assert.Equal(tenantId, tenantRole.TenantId);
            Assert.Equal("Test", tenantRole.Role);
        }

        [Theory]
        [InlineData("")]
        [InlineData("abc::Role")]
        [InlineData("123e4567-e89b-12d3-a456-426614174000::")]
        [InlineData("::Role")]
        [InlineData("123e4567-e89b-12d3-a456-426614174000::Role::Something else")]
        public void InvalidFormatsThrows(string tenantRole)
        {
            Assert.Throws<FormatException>(() => TenantRole.Parse(tenantRole));
        }
    }
}
