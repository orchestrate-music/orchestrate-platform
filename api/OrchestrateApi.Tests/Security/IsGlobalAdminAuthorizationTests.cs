using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Security
{
    public class IsGlobalAdminAuthorizeAttributeTests
    {
        [Fact]
        public void PolicyNameIsSetCorrectly()
        {
            var attribute = new IsGlobalAdminAuthorizeAttribute();
            Assert.Equal(IsGlobalAdminRequirement.PolicyName, attribute.Policy);
        }
    }

    public class IsGlobalAdminAuthorizationHandlerTests
    {
        [Theory]
        [InlineData("True", true)] // bool.TrueString
        [InlineData("False", false)]
        [InlineData("", false)]
        public async Task CorrectlyChecksForRoleInTenant(string claimValue, bool succeeds)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsPrincipalExtensions.GlobalAdminClaimName, claimValue),
            };
            var user = new ClaimsPrincipal(new ClaimsIdentity(claims));

            var requirement = new IsGlobalAdminRequirement();
            var handlerContext = new AuthorizationHandlerContext(new[] { requirement }, user, null);
            var handler = new IsGlobalAdminAuthorizationHandler();

            Assert.False(handlerContext.HasSucceeded);
            await handler.HandleAsync(handlerContext);
            Assert.Equal(succeeds, handlerContext.HasSucceeded);
        }
    }
}
