using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Settings;
using OrchestrateApi.PaymentProcessor;

namespace OrchestrateApi.Tests.Feature.Settings;

public class SubscriptionSettingsServiceTests : AutoRollbackTestHarness
{
    private readonly SubscriptionSettingsService service;
    private readonly MockPaymentProcessor paymentProcessor;

    public SubscriptionSettingsServiceTests()
    {
        this.paymentProcessor = new();
        this.service = new(
            Context,
            this.paymentProcessor,
            TestHelper.BuildMockLogger<SubscriptionSettingsService>());
    }

    [Theory]
    [InlineData(PlanType.Demo, null, null)]
    [InlineData(PlanType.Trial, PlanType.Free, null)]
    [InlineData(PlanType.Free, null, 10)]
    [InlineData(PlanType.Medium, null, 30)]
    public async Task DefaultSubscriptionDetailsAreReturned(PlanType currentPlan, PlanType? nextPlan, int? maxMembers)
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.Type = currentPlan;
        await Context.SaveChangesAsync();

        var dto = await this.service.DefaultDetailsAsync(tenant);

        Assert.Equal(currentPlan, dto.CurrentPlan);
        Assert.Equal(nextPlan, dto.NextPlan);
        Assert.False(dto.IsNotForProfit);
        Assert.Equal(DateTime.UtcNow, dto.DateRegistered, TimeSpan.FromMinutes(3));
        Assert.Null(dto.AccessEndsDate);
        Assert.False(dto.CanAccessCustomerPortal);
        Assert.Equal(maxMembers, dto.MaximumMemberCount);
        Assert.Empty(dto.PreviousInvoices);
    }

    [Theory]
    [InlineData(MockPaymentProcessor.MockCustomerId, MockPaymentProcessor.MockCustomerId)]
    [InlineData("cust_unknown", null)]
    public async Task CustomerWillBeFetchedIfFound(string customerId, string? foundCustomerId)
    {
        this.paymentProcessor.Customer = new()
        {
            Id = MockPaymentProcessor.MockCustomerId,
        };

        var tenant = await Context.Tenant.SingleAsync();
        tenant.ContactAddress = "not@used.com";
        tenant.Plan.StripeCustomerId = customerId;

        var customer = await this.service.GetOrCreateCustomerAsync(tenant);
        if (foundCustomerId == null)
        {
            Assert.Null(customer);
        }
        else
        {
            Assert.NotNull(customer);
            Assert.Equal(foundCustomerId, customer.Id);
        }
    }

    [Fact]
    public async Task CustomerWillBeCreatedIfIdIsEmpty()
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.ContactAddress = "email@test.com";
        await Context.SaveChangesAsync();
        Assert.Null(tenant.Plan.StripeCustomerId);

        var customer = await this.service.GetOrCreateCustomerAsync(tenant);

        Assert.NotNull(customer);
        Assert.Equal(tenant.Name, customer.Name);
        Assert.Equal("email@test.com", customer.Email);
        Assert.NotNull(tenant.Plan.StripeCustomerId);
    }

    [Fact]
    public void ContactDetailsDtoIsConstructedCorrectly()
    {
        var customer = new Stripe.Customer
        {
            Name = "Test Customer",
            Email = "test@email.com",
            Address = new Stripe.Address
            {
                Line1 = "1 Address Street",
                City = "City",
                State = "State",
                Country = "Country",
            },
        };

        var dto = this.service.CustomerToContactDetailsDto(customer);

        Assert.Equal("Test Customer", dto.Name);
        Assert.Equal("test@email.com", dto.Email);
        Assert.Equal("1 Address Street\nCity\nState\nCountry", dto.Address);
    }

    [Fact]
    public async Task PaymentMethodDtoIsConstructedCorrectlyWithDirectDebit()
    {
        this.paymentProcessor.PaymentMethod = new Stripe.PaymentMethod
        {
            Type = StripePaymentMethodType.AuBecsDebit,
            AuBecsDebit = new Stripe.PaymentMethodAuBecsDebit
            {
                BsbNumber = "123-456",
                Last4 = "7890",
            },
        };

        var dto = await this.service.GetPaymentMethodDtoAsync(new Stripe.Subscription
        {
            DefaultPaymentMethodId = MockPaymentProcessor.MockPaymentMethodId,
        });

        Assert.NotNull(dto);
        Assert.Equal("Direct Debit", dto.Type);
        Assert.Equal("BSB: ***-456, Account: ****7890", dto.Preview);
    }

    [Fact]
    public async Task PaymentMethodDtoIsConstructedCorrectlyWithCreditCard()
    {
        this.paymentProcessor.PaymentMethod = new Stripe.PaymentMethod
        {
            Type = StripePaymentMethodType.CreditCard,
            Card = new()
            {
                Last4 = "7890",
                ExpMonth = 2,
                ExpYear = 2026,
            },
        };

        var dto = await this.service.GetPaymentMethodDtoAsync(new Stripe.Subscription
        {
            DefaultPaymentMethodId = MockPaymentProcessor.MockPaymentMethodId,
        });

        Assert.NotNull(dto);
        Assert.Equal("Credit Card", dto.Type);
        Assert.Equal("****-****-****-7890, Expires 02/26", dto.Preview);
    }

    [Fact]
    public async Task PaymentMethodDtoIsConstructedCorrectlyForUnknownType()
    {
        this.paymentProcessor.PaymentMethod = new Stripe.PaymentMethod
        {
            Type = "unknown",
        };

        var dto = await this.service.GetPaymentMethodDtoAsync(new Stripe.Subscription
        {
            DefaultPaymentMethodId = MockPaymentProcessor.MockPaymentMethodId,
        });

        Assert.NotNull(dto);
        Assert.Equal("unknown", dto.Type);
        Assert.Equal("Open Customer Portal to see details", dto.Preview);
    }
}
