using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Settings;
using OrchestrateApi.PaymentProcessor;
using Stripe;

namespace OrchestrateApi.Tests.Feature.Settings;

public class SubscriptionSettingsControllerTests : FullIntegrationTestHarness
{
    private readonly SubscriptionSettingsController controller;
    private readonly MockPaymentProcessor paymentProcessor;

    public SubscriptionSettingsControllerTests()
    {
        this.controller = ConstructController<SubscriptionSettingsController>();
        this.paymentProcessor = GetServiceAs<IPaymentProcessor, MockPaymentProcessor>();
    }

    [Theory]
    [InlineData(PlanType.Demo, null, null)]
    [InlineData(PlanType.Trial, PlanType.Free, null)]
    [InlineData(PlanType.Free, null, 10)]
    [InlineData(PlanType.Medium, null, 30)]
    public async Task DefaultSubscriptionDetailsAreReturned(PlanType currentPlan, PlanType? nextPlan, int? maxMembers)
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.Type = currentPlan;
        await Context.SaveChangesAsync();

        var dto = await this.controller.GetSubscriptionDetailsAsync();

        Assert.Equal(currentPlan, dto.CurrentPlan);
        Assert.Equal(nextPlan, dto.NextPlan);
        Assert.False(dto.IsNotForProfit);
        Assert.Equal(DateTime.UtcNow, dto.DateRegistered, TimeSpan.FromMinutes(3));
        Assert.Null(dto.AccessEndsDate);
        Assert.False(dto.CanAccessCustomerPortal);
        Assert.Equal(maxMembers, dto.MaximumMemberCount);
        Assert.Empty(dto.PreviousInvoices);
    }

    [Fact]
    public async Task CustomerWithoutSubscriptionIsPopulatedCorrectly()
    {
        this.paymentProcessor.Customer = new Customer
        {
            Id = MockPaymentProcessor.MockCustomerId,
            Name = "Test Customer",
            Email = "test@email.com",
            Address = new Address
            {
                Line1 = "My House",
                City = "Middle",
                State = "Street",
            },
            Subscriptions = new StripeList<Subscription> { Data = new List<Subscription>() },
        };
        this.paymentProcessor.PaidInvoices = new List<Invoice>
        {
            new Invoice
            {
                Total = 1500,
                HostedInvoiceUrl = "https://google.com",
                Lines = new StripeList<InvoiceLineItem>
                {
                    Data = new List<InvoiceLineItem>
                    {
                        new InvoiceLineItem
                        {
                            Price = new Price{ProductId = $"prod_{PlanType.Medium}"},
                            Period = new InvoiceLineItemPeriod
                            {
                                Start = new DateTime(2023, 1, 1),
                                End = new DateTime(2023, 1, 31),
                            },
                        }
                    }
                }
            }
        };
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.StripeCustomerId = MockPaymentProcessor.MockCustomerId;
        await Context.SaveChangesAsync();

        var dto = await this.controller.GetSubscriptionDetailsAsync();

        Assert.NotNull(dto.ContactDetails);
        Assert.Equal("Test Customer", dto.ContactDetails.Name);
        Assert.Equal("test@email.com", dto.ContactDetails.Email);
        Assert.Equal("My House\nMiddle\nStreet", dto.ContactDetails.Address);

        var invoice = Assert.Single(dto.PreviousInvoices);
        Assert.Equal("Orchestrate - Medium", invoice.Description);
        Assert.Equal(new DateTime(2023, 1, 1), invoice.StartDate);
        Assert.Equal(new DateTime(2023, 1, 31), invoice.EndDate);
        Assert.Equal(15M, invoice.TotalDollars);
        Assert.Equal(new Uri("https://google.com"), invoice.Url);

        Assert.Null(dto.PaymentMethod);
        Assert.Equal(0, dto.MonthlyFeeDollars);
        Assert.Null(dto.UpcomingInvoice);
        Assert.Null(dto.NextPlan);
    }

    [Theory]
    [InlineData(false, false, PlanType.Demo, null)]
    [InlineData(false, true, PlanType.Demo, PlanType.Free)]
    [InlineData(true, false, PlanType.Trial, PlanType.Demo)]
    [InlineData(true, true, PlanType.Trial, PlanType.Free)]
    public async Task CustomerWithSubscriptionIsPopulatedCorrectly(bool isTrialing, bool cancelAtPeriodEnd, PlanType currentPlan, PlanType? nextPlan)
    {
        this.paymentProcessor.Customer = new Customer
        {
            Id = MockPaymentProcessor.MockCustomerId,
            Name = "Test Customer",
            Email = "test@email.com",
            Address = new Address
            {
                Line1 = "My House",
                City = "Middle",
                State = "Street",
            },
            Subscriptions = new StripeList<Subscription>
            {
                Data = new List<Subscription>
                {
                    new Subscription
                    {
                        CustomerId = MockPaymentProcessor.MockCustomerId,
                        Status = isTrialing
                            ? SubscriptionStatuses.Trialing
                            : SubscriptionStatuses.Active,
                        CancelAtPeriodEnd = cancelAtPeriodEnd,
                        DefaultPaymentMethodId = MockPaymentProcessor.MockPaymentMethodId,
                        Items = new StripeList<SubscriptionItem>
                        {
                            Data = new List<SubscriptionItem>
                            {
                                new SubscriptionItem
                                {
                                    Quantity = 1,
                                    Price = new Price{UnitAmount = 3000},
                                }
                            }
                        }
                    }
                }
            },
        };
        this.paymentProcessor.PaymentMethod = new PaymentMethod
        {
            Id = MockPaymentProcessor.MockPaymentMethodId,
            Type = StripePaymentMethodType.AuBecsDebit,
            AuBecsDebit = new PaymentMethodAuBecsDebit
            {
                BsbNumber = "000-000",
                Last4 = "1234",
            }
        };
        this.paymentProcessor.UpcomingInvoice = new Invoice
        {
            Total = 1500,
            PeriodEnd = new DateTime(2023, 2, 1),
            Lines = new StripeList<InvoiceLineItem>
            {
                Data = new List<InvoiceLineItem>
                {
                    new InvoiceLineItem
                    {
                        Price = new Price{ProductId = $"prod_{PlanType.Medium}"},
                        Period = new InvoiceLineItemPeriod
                        {
                            Start = new DateTime(2023, 1, 1),
                            End = new DateTime(2023, 1, 31),
                        },
                    }
                }
            }
        };
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.StripeCustomerId = MockPaymentProcessor.MockCustomerId;
        await Context.SaveChangesAsync();

        var dto = await this.controller.GetSubscriptionDetailsAsync();

        Assert.Equal(currentPlan, dto.CurrentPlan);
        Assert.Equal(nextPlan, dto.NextPlan);

        Assert.NotNull(dto.PaymentMethod);
        Assert.Equal("Direct Debit", dto.PaymentMethod.Type);
        Assert.Equal("BSB: ***-000, Account: ****1234", dto.PaymentMethod.Preview);

        Assert.NotNull(dto.UpcomingInvoice);
        Assert.Equal(new DateTime(2023, 2, 1), dto.UpcomingInvoice.BillingDate);
        Assert.Equal("Orchestrate - Medium", dto.UpcomingInvoice.Description);
        Assert.Equal(new DateTime(2023, 1, 1), dto.UpcomingInvoice.StartDate);
        Assert.Equal(new DateTime(2023, 1, 31), dto.UpcomingInvoice.EndDate);
        Assert.Equal(15M, dto.UpcomingInvoice.TotalDollars);
    }

    [Theory]
    [InlineData(PlanType.Small, 10, 5, 20)]
    [InlineData(PlanType.Medium, 20, 10, 30)]
    [InlineData(PlanType.Large, 30, 15, 40)]
    public async Task PaidPlansAreReturnsAsExpected(PlanType type, int fee, int nfpFee, int maxMembers)
    {
        var paidPlans = await this.controller.GetPaidPlansAsync();
        Assert.Equal(3, paidPlans.Count);

        var plan = Assert.Single(paidPlans, e => e.PlanType == type);
        Assert.Equal(fee, plan.MonthlyFeeDollars);
        Assert.Equal(nfpFee, plan.NfpMonthlyFeeDollars);
        Assert.Equal(maxMembers, plan.MaxMemberCount);
    }

    [Fact]
    public async Task UnpaidPlanReturnsBadRequest()
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.Type = PlanType.Free;
        await Context.SaveChangesAsync();

        var result = await this.controller.GetSubscriptionCheckoutUrlAsync(new StartSubscriptionDto
        {
            PlanType = PlanType.Free,
            ReturnUrl = new Uri("https://google.com"),
        });

        var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        var errorResponse = Assert.IsType<BasicErrorResponse>(objectResult.Value);
        Assert.Contains("unpaid plan", errorResponse.Message);
    }

    [Theory]
    [InlineData(PlanType.Demo)]
    [InlineData(PlanType.Small)]
    [InlineData(PlanType.Medium)]
    [InlineData(PlanType.Large)]
    public async Task CantCheckoutWithDemoOrExistingPaidPlan(PlanType type)
    {
        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.Type = type;
        await Context.SaveChangesAsync();

        var result = await this.controller.GetSubscriptionCheckoutUrlAsync(new StartSubscriptionDto
        {
            PlanType = PlanType.Small,
            ReturnUrl = new Uri("https://google.com"),
        });

        var objectResult = Assert.IsType<BadRequestObjectResult>(result.Result);
        var errorResponse = Assert.IsType<BasicErrorResponse>(objectResult.Value);
        Assert.Contains("Cannot start paid plan", errorResponse.Message);
    }

    [Theory]
    [InlineData(false)]
    [InlineData(true)]
    public async Task NotForProfitIsAppliedAndCheckoutSessionCreated(bool isNotForProfit)
    {
        this.controller.ControllerContext = new ControllerContext
        {
            HttpContext = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>
                {
                    new Claim(ClaimTypes.Name, "test@email.com"),
                })),
            }
        };
        this.paymentProcessor.Customer = new Customer
        {
            Id = MockPaymentProcessor.MockCustomerId,
        };

        var tenant = await Context.Tenant.SingleAsync();
        tenant.Plan.StripeCustomerId = MockPaymentProcessor.MockCustomerId;
        tenant.Plan.Type = PlanType.Free;
        tenant.Plan.IsNotForProfit = isNotForProfit;
        await Context.SaveChangesAsync();

        var result = await this.controller.GetSubscriptionCheckoutUrlAsync(new StartSubscriptionDto
        {
            PlanType = PlanType.Medium,
            ReturnUrl = new Uri("https://my.return/url"),
        });

        Assert.Contains("plan=Medium", result.Value);
        Assert.Contains("returnUrl=https://my.return/url", result.Value);

        if (isNotForProfit)
        {
            Assert.Contains(
                MockPaymentProcessor.MockCustomerId,
                this.paymentProcessor.NotForProfitAppliedCustomerIds);
        }
        else
        {
            Assert.DoesNotContain(
                MockPaymentProcessor.MockCustomerId,
                this.paymentProcessor.NotForProfitAppliedCustomerIds);
        }
    }
}
