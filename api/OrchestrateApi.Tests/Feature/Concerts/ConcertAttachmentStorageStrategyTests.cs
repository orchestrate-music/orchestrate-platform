using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Concerts;
using OrchestrateApi.Storage;

namespace OrchestrateApi.Tests.Feature.Concerts;

public class ConcertAttachmentStorageStrategyTests : AutoRollbackTestHarness
{
    private ConcertAttachmentStorageStrategy StorageStrategy { get; } = new();

    [Fact]
    public void CorrectLinkingEntityIsCreated()
    {
        var linkingEntity = Assert.IsType<ConcertAttachment>(StorageStrategy.CreateLinkingEntity(
            Guid.Empty,
            new EntityAttachmentLinkData { EntityId = 5 }));
        Assert.Equal(Guid.Empty, linkingEntity.BlobId);
        Assert.Equal(5, linkingEntity.ConcertId);
    }

    [Fact]
    public async Task ReturnsCorrectLinkedBlobIds()
    {
        var (concert, blobId) = GenerateConcertAndAttachment(StorageStrategy.Name, "Searched");
        GenerateConcertAndAttachment(StorageStrategy.Name, "NotSearched");
        GenerateConcertAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy
            .LinkedBlobIds(Context, new EntityAttachmentLinkData { EntityId = concert!.Id })
            .ToListAsync();
        Assert.Single(blobIds);
        Assert.Contains(blobIds, e => e == blobId);
    }

    [Fact]
    public async Task ReturnsCorrectValidBlobIds()
    {
        var (_, blobA) = GenerateConcertAndAttachment(StorageStrategy.Name, "A");
        var (_, blobB) = GenerateConcertAndAttachment(StorageStrategy.Name, "B");
        GenerateConcertAndAttachment("Other", null);
        await Context.SaveChangesAsync();

        var blobIds = await StorageStrategy.ValidBlobIds(Context).ToListAsync();
        Assert.Equal(2, blobIds.Count);
        Assert.Contains(blobIds, e => e == blobA);
        Assert.Contains(blobIds, e => e == blobB);
    }

    public (Concert?, Guid) GenerateConcertAndAttachment(string strategy, string? concertName)
    {
        var blob = Context.Add(new OrchestrateBlob
        {
            Name = "test.txt",
            MimeType = "text/plain",
            StorageStrategy = new OrchestrateBlobStorageStrategyData
            {
                Name = strategy,
                AccessData = JsonSerializer.SerializeToDocument(
                    new EntityAttachmentAccessData { Sensitivity = EntityAttachmentSensitivity.Secured }),
            }
        }).Entity;

        Concert? concert = null;
        if (concertName != null)
        {
            concert = Context.Add(new Concert { Occasion = concertName }).Entity;
            Context.Add(new ConcertAttachment { Concert = concert, BlobId = blob.Id });
        }

        return (concert, blob.Id);
    }
}
