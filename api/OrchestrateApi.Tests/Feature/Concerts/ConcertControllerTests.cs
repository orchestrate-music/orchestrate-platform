using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Feature.Concerts;

namespace OrchestrateApi.Tests.Feature.Concerts
{
    public class ConcertControllerTests : AutoRollbackTestHarness
    {
        public ConcertControllerTests()
        {
            ConcertController = new ConcertController(Context);
        }

        private ConcertController ConcertController { get; }

        [Fact]
        public async Task ReturnsNotFoundForNonExistantConcert()
        {
            var response = await ConcertController.GetConcertStatsAsync(-1, default);
            Assert.IsType<NotFoundResult>(response.Result);
        }

        [Fact]
        public async Task ReturnsCorrectStatsForConcert()
        {
            var states = await Context.Concert.FirstAsync(e => e.Occasion == "State Championships");
            var response = await ConcertController.GetConcertStatsAsync(states.Id, default);
            var stats = response.Value;

            Assert.NotNull(stats);
            Assert.Equal(3, stats.TotalMemberCount);
            Assert.Equal(3, stats.TotalScoreCount);
        }
    }
}
