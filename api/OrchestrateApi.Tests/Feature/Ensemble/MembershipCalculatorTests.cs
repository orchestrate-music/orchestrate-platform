using OrchestrateApi.DAL;
using OrchestrateApi.Feature.Ensembles;

namespace OrchestrateApi.Tests.Feature.Ensembles;

public class MembershipCalculatorTests
{
    private readonly DateTime Now = DateTime.UtcNow;
    private readonly MembershipCalculator calculator;
    private readonly IEnumerable<Membership> membership;

    public MembershipCalculatorTests()
    {
        this.calculator = new MembershipCalculator(FromDays(-2), FromDays(2));
        this.membership = new[]
        {
            new Membership(FromDays(-4), FromDays(-3)), // Shouldn't be counted at all
            new Membership(FromDays(-4), FromDays(-1)), // Left
            new Membership(FromDays(-4), FromDays(3)), // Stayed
            new Membership(FromDays(-4), null), // Stayed
            new Membership(FromDays(-1), FromDays(1)), // Joined & Left
            new Membership(FromDays(-1), FromDays(3)), // Joined
            new Membership(FromDays(-1), null), // Joined
            new Membership(FromDays(3), FromDays(4)), // Shouldn't be counted
            new Membership(FromDays(3), null), // Shouldn't be counted either
        };
    }

    [Fact]
    public void JoinedCountIsCalculatedCorrectly()
    {
        Assert.Equal(2, this.calculator.JoinedCount(this.membership));
    }

    [Fact]
    public void StayedCountIsCalculatedCorrectly()
    {
        Assert.Equal(2, this.calculator.StayedCount(this.membership));
    }

    [Fact]
    public void LeftCountIsCalculatedCorrectly()
    {
        Assert.Equal(1, this.calculator.LeftCount(this.membership));
    }

    [Fact]
    public void JoinedAndLeftCountIsCalculatedCorrectly()
    {
        Assert.Equal(1, this.calculator.JoinedAndLeftCount(this.membership));
    }

    private DateTime FromDays(int days) => Now.AddDays(days);

    private sealed record Membership(DateTime DateJoined, DateTime? DateLeft) : IMembership;
}
