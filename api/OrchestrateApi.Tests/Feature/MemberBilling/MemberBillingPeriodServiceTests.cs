using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Common;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberBillingPeriodServiceTests : AutoRollbackTestHarness
    {
        public MemberBillingPeriodServiceTests()
        {
            Context.MemberBillingPeriod.RemoveRange(Context.MemberBillingPeriod.ToList());
            Context.MemberBillingPeriodConfig.RemoveRange(Context.MemberBillingPeriodConfig.ToList());
            Context.SaveChanges();

            BillingPeriodService = new MemberBillingPeriodService(
                new MockServiceProvider(),
                Context,
                TestHelper.BuildMockLogger<MemberBillingPeriodService>());
        }

        private MemberBillingPeriodService BillingPeriodService { get; }

        [Fact]
        public async Task CreatesFutureBillingPeriodBeforeItIsDueToBeAutoGenerated()
        {
            var tenant = await Context.Tenant.SingleAsync();
            tenant.TimeZone = TimeZoneInfo.Utc.Id;
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test current Billing Period Config",
                AutoGenerateMonth = DateTime.UtcNow.Month,
                EndOfPeriodMonth = DateTime.UtcNow.Month,
            });
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test future Billing Period Config",
                AutoGenerateMonth = (DateTime.UtcNow.Month - 1 + 6) % 12 + 1,
                EndOfPeriodMonth = (DateTime.UtcNow.Month - 1 + 6) % 12 + 1,
            });
            await Context.MemberBillingPeriod.AddAsync(new MemberBillingPeriod
            {
                Name = "Test Billing Period",
                State = MemberBillingPeriodState.Closed,
                Due = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 28, 23, 59, 59, DateTimeKind.Utc),
            });
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateNextBillingPeriodAsync();

            Assert.NotNull(billingPeriod);
            Assert.Equal(DateTime.UtcNow.Year + (DateTime.UtcNow.Month - 1 + 6) / 12, billingPeriod.Due.Year);
            Assert.Equal((DateTime.UtcNow.Month - 1 + 6) % 12 + 1, billingPeriod.Due.Month);
        }

        [Theory]
        [InlineData(null)] // Check we create if there aren't any previous billing periods
        [InlineData(1)]
        [InlineData(2)] // Check we don't create last years if there is a missing past billing period
        public async Task CreatesCurrentBillingPeriodIfIfHasntBeenGeneratedYet(int? lastBillingPeriodYearsAgo)
        {
            var tenant = await Context.Tenant.SingleAsync();
            tenant.TimeZone = TimeZoneInfo.Utc.Id;
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                AutoGenerateMonth = DateTime.UtcNow.Month,
                EndOfPeriodMonth = DateTime.UtcNow.Month,
            });
            if (lastBillingPeriodYearsAgo.HasValue)
            {
                await Context.MemberBillingPeriod.AddAsync(new MemberBillingPeriod
                {
                    Name = "Test Billing Period",
                    State = MemberBillingPeriodState.Closed,
                    Due = new DateTime(DateTime.UtcNow.Year - lastBillingPeriodYearsAgo.Value, DateTime.UtcNow.Month, 28, 23, 59, 59, DateTimeKind.Utc),
                });
            }
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateNextBillingPeriodAsync();

            Assert.NotNull(billingPeriod);
            Assert.Equal(DateTime.UtcNow.Year, billingPeriod.Due.Year);
            Assert.Equal(DateTime.UtcNow.Month, billingPeriod.Due.Month);
        }

        [Fact]
        public async Task WontCreateBillingPeriodIfNoConfig()
        {
            var configs = await Context.MemberBillingConfig.ToListAsync();
            Context.MemberBillingConfig.RemoveRange(configs);
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync();
            Assert.Null(billingPeriod);
        }

        [Fact]
        public async Task WontCreateBillingPeriodIfDisabled()
        {
            var config = await Context.MemberBillingConfig.SingleAsync();
            config.IsEnabled = false;
            await Context.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                AutoGenerateMonth = DateTime.UtcNow.Month,
                EndOfPeriodMonth = DateTime.UtcNow.Month,
            });
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync();
            Assert.Null(billingPeriod);
        }

        [Fact]
        public async Task WontCreateBillingPeriodIfNoApplicableBillingPeriodConfig()
        {
            var tenant = await Context.Tenant.SingleAsync();
            var tenantTime = DateTime.UtcNow.InTimeZone(tenant.TimeZone);
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                // Not the current month
                AutoGenerateMonth = tenantTime.Month > 1 ? 1 : 2,
                EndOfPeriodMonth = 6,
            });
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync();
            Assert.Null(billingPeriod);
        }

        [Theory]
        [InlineData(MemberBillingPeriodState.Created)]
        [InlineData(MemberBillingPeriodState.Draft)]
        [InlineData(MemberBillingPeriodState.Open)]
        [InlineData(MemberBillingPeriodState.Closed)]
        public async Task WontCreateIfBillingPeriodHasAlreadyBeenGenerated(MemberBillingPeriodState state)
        {
            var tenant = await Context.Tenant.SingleAsync();
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                AutoGenerateMonth = 1,
                EndOfPeriodMonth = 4,
            });
            await Context.MemberBillingPeriod.AddAsync(new MemberBillingPeriod
            {
                Name = "Test Billing Period",
                State = state,
                Due = new DateTime(2021, 4, 30, 23, 59, 59).ToUtcFromTimeZone(tenant.TimeZone),
            });
            await Context.SaveChangesAsync();

            var candidateDate = new DateTime(2021, 1, 1).ToUtcFromTimeZone(tenant.TimeZone);
            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync(candidateDate);
            Assert.Null(billingPeriod);
        }

        [Theory]
        [InlineData(MemberBillingPeriodState.Created)]
        [InlineData(MemberBillingPeriodState.Draft)]
        [InlineData(MemberBillingPeriodState.Open)]
        [InlineData(MemberBillingPeriodState.Closed)]
        public async Task CreatesIfGeneratedBillingPeriodIsFromAPreviousYear(MemberBillingPeriodState state)
        {
            var tenant = await Context.Tenant.SingleAsync();
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                AutoGenerateMonth = 1,
                EndOfPeriodMonth = 4,
            });
            await Context.MemberBillingPeriod.AddAsync(new MemberBillingPeriod
            {
                Name = "Test Billing Period",
                State = state,
                Due = new DateTime(2020, 4, 30, 23, 59, 59).ToUtcFromTimeZone(tenant.TimeZone),
            });
            await Context.SaveChangesAsync();

            var candidateDate = new DateTime(2021, 1, 1).ToUtcFromTimeZone(tenant.TimeZone);
            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync(candidateDate);
            Assert.NotNull(billingPeriod);
            Assert.Equal(2021, billingPeriod.Due.Year);
            Assert.Equal(4, billingPeriod.Due.Month);
        }

        [Fact]
        public async Task CreatesAddsAndSavesNewBillingPeriodWithCorrectValues()
        {
            var tenant = await Context.Tenant.SingleAsync();
            tenant.TimeZone = "Australia/Perth";
            var tenantTime = DateTime.UtcNow.InTimeZone(tenant.TimeZone);
            await Context.MemberBillingPeriodConfig.AddAsync(new MemberBillingPeriodConfig
            {
                Name = "Test Billing Period Config",
                AutoGenerateMonth = tenantTime.Month,
                EndOfPeriodMonth = 6,
            });
            await Context.SaveChangesAsync();

            var billingPeriod = await BillingPeriodService.TryCreateBillingPeriodAsync();
            Assert.NotNull(billingPeriod);
            Assert.Equal($"{tenantTime.Year} - Test Billing Period Config", billingPeriod.Name);
            Assert.Equal(MemberBillingPeriodState.Created, billingPeriod.State);
            Assert.Null(billingPeriod.Generated);
            Assert.Null(billingPeriod.Sent);
            Assert.Null(billingPeriod.Closed);

            var expectedDue = new DateTime(tenantTime.Year, 6, 30, 15, 59, 59, DateTimeKind.Utc);
            Assert.Equal(expectedDue, billingPeriod.Due);
        }

        private sealed class MockServiceProvider : IServiceProvider
        {
            public object? GetService(Type t) => null;
        }
    }
}
