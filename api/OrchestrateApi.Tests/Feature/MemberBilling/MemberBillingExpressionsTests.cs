using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling;

public class MemberBillingExpressionsTests
{
    private readonly DateTime Now = DateTime.UtcNow;
    private readonly MemberBillingPeriod billingPeriod;

    public MemberBillingExpressionsTests()
    {
        this.billingPeriod = new MemberBillingPeriod
        {
            Name = "test",
            EarlyBirdDue = FromDays(2),
            Due = FromDays(4),
            Invoices =
            {
                new MemberInvoice{ Reference = "a", Paid = null }, // Unpaid shouldn't be counted
                new MemberInvoice{ Reference = "b", Paid = FromDays(1) }, // Paid before EarlyBirdDue
                new MemberInvoice{ Reference = "c", Paid = FromDays(3) }, // Paid after EarlyBirdDue
                new MemberInvoice{ Reference = "d", Paid = FromDays(5) }, // Paid after Due
            }
        };
    }

    [Fact]
    public void DollarsReceivedIsCalculatedCorrectly()
    {
        this.billingPeriod.Invoices = new[]
        {
            InvoiceWithLineItems(null, 10, 20),
            InvoiceWithLineItems(null, 30),
            InvoiceWithLineItems(1, 5, 10),
            InvoiceWithLineItems(3, 5, 15),
            InvoiceWithLineItems(5, 10, 7.5M),
        };

        var func = MemberBillingExpressions.BillingPeriodDollarsReceivedExpr.Compile();
        Assert.Equal(52.5M, func(this.billingPeriod));
    }

    [Fact]
    public void PaidEarlyCountIsCalculatedCorrectly()
    {
        var func = MemberBillingExpressions.PaidEarlyCountExpr.Compile();
        Assert.Equal(1, func(this.billingPeriod));

        billingPeriod.EarlyBirdDue = null;
        Assert.Equal(0, func(this.billingPeriod));
    }

    [Fact]
    public void PaidOnTimeCountIsCalculatedCorrectly()
    {
        var func = MemberBillingExpressions.PaidOnTimeCountExpr.Compile();
        Assert.Equal(1, func(this.billingPeriod));

        billingPeriod.EarlyBirdDue = null;
        Assert.Equal(2, func(this.billingPeriod));
    }

    [Fact]
    public void PaidLateCountIsCalculatedCorrectly()
    {
        var func = MemberBillingExpressions.PaidLateCountExpr.Compile();
        Assert.Equal(1, func(this.billingPeriod));

        billingPeriod.EarlyBirdDue = null;
        billingPeriod.Due = Now;
        Assert.Equal(3, func(this.billingPeriod));
    }

    [Fact]
    public void UnpaidCountIsCalculatedCorrectly()
    {
        var func = MemberBillingExpressions.UnpaidCountExpr.Compile();
        Assert.Equal(1, func(this.billingPeriod));
    }

    [Fact]
    public void InvoiceTotalIsCalculatedCorrectly()
    {
        var invoice = InvoiceWithLineItems(null, 5M, 10M, 7.5M);

        var func = MemberBillingExpressions.InvoiceTotalExpr.Compile();
        Assert.Equal(22.5M, func(invoice));
    }

    private DateTime FromDays(int days) => Now.AddDays(days);

    private MemberInvoice InvoiceWithLineItems(int? paidOn, params decimal[] lineItemDollars)
    {
        return new MemberInvoice
        {
            Reference = Guid.NewGuid().ToString(),
            Paid = paidOn.HasValue ? FromDays(paidOn.Value) : null,
            LineItems = lineItemDollars
                .Select((dollars, idx) => new MemberInvoiceLineItem
                {
                    Description = Guid.NewGuid().ToString(),
                    AmountCents = (int)(dollars * 100),
                    Ordinal = idx,
                })
                .ToList(),
        };
    }
}
