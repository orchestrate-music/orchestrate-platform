using Microsoft.EntityFrameworkCore;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling;

public class SendInvoiceCustomSaveTests : FullIntegrationTestHarness
{
    public SendInvoiceCustomSaveTests()
    {
        SendInvoice = new SendInvoiceCustomSave(GetService<IMemberInvoiceSender>());
    }

    private SendInvoiceCustomSave SendInvoice { get; }
    private MockEmailService MockEmailService => GetServiceAs<IEmailService, MockEmailService>();

    [Fact]
    public async Task SendingInvoiceGeneratesCorrectly()
    {
        var bobMarley = await Context.Member
            .Include(e => e.Details)
            .SingleAsync(e => e.Details!.Email == OrchestrateSeed.BobMarleyEmail);
        var invoice = await Context.MemberInvoice
            .OrderByDescending(e => e.MemberBillingPeriod!.Due)
            .FirstAsync(e => e.MemberId == bobMarley.Id);

        await SendInvoice.ModifyEntity(Context, invoice, CancellationToken.None);

        var sentEmail = MockEmailService.LastEmail;
        Assert.NotNull(sentEmail);
        Assert.Equal("Bob Marley", sentEmail.To?.Name);
        Assert.Equal(OrchestrateSeed.BobMarleyEmail, sentEmail.To?.Address);
        Assert.Single(sentEmail.Attachments);
    }
}
