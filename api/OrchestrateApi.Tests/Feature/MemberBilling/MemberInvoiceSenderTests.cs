using System.Text;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Email;
using OrchestrateApi.Feature.MemberBilling;

namespace OrchestrateApi.Tests.Feature.MemberBilling
{
    public class MemberInvoiceSenderTests
    {
        private readonly MockEmailService mockEmailService;
        private readonly MemberInvoiceSender sender;
        private readonly Tenant tenant;
        private readonly MemberBillingConfig billingConfig;
        private readonly MemberInvoice invoice;

        public MemberInvoiceSenderTests()
        {
            this.mockEmailService = new MockEmailService();
            this.sender = new MemberInvoiceSender(
                this.mockEmailService,
                new MockInvoiceRenderingService());
            this.tenant = new Tenant
            {
                TimeZone = "Australia/Perth",
            };
            this.billingConfig = new MemberBillingConfig
            {
                InvoicePrefix = string.Empty,
                BankBsb = string.Empty,
                BankAccountNo = string.Empty,
                EarlyBirdDiscountDollars = 10,
                NotificationContactName = "Test Person",
                NotificationContactEmail = "test@email.com",
            };
            this.invoice = new MemberInvoice
            {
                Reference = "INV-00001",
                MemberBillingPeriod = new MemberBillingPeriod { Name = "Test Billing Period", Due = DateTime.UtcNow },
                Member = new Member
                {
                    FirstName = "FirstName",
                    LastName = "LastName",
                    Details = new MemberDetails { Email = "member@email.com" }
                },
            };
        }

        private EmailMessage LastEmail => this.mockEmailService.LastEmail ?? throw new ArgumentNullException();

        [Theory]
        [InlineData(EmailResultStatus.Success, MemberInvoiceSendStatus.Pending)]
        [InlineData(EmailResultStatus.Error, MemberInvoiceSendStatus.Failed)]
        public async Task EmailResultMapsToInvoiceSendResult(EmailResultStatus emailStatus, MemberInvoiceSendStatus sendStatus)
        {
            this.mockEmailService.NextResultStatus = emailStatus;
            this.mockEmailService.NextResultMessage = $"Result is: {emailStatus}";

            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });

            Assert.Equal(sendStatus, invoice.SendStatus);
            Assert.Equal(this.mockEmailService.NextResultMessage, invoice.SendStatusMessage);
        }

        [Fact]
        public async Task NullsOutSendStatusUpdatedField()
        {
            this.invoice.SendStatusUpdated = DateTime.UtcNow;
            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });
            Assert.Null(this.invoice.SendStatusUpdated);
        }

        [Fact]
        public async Task GeneratesReceiptEmail()
        {
            this.invoice.Paid = DateTime.UtcNow;

            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });

            Assert.Equal("Test Billing Period receipt", LastEmail.Subject);
            Assert.Equal("FirstName LastName", LastEmail.To?.Name);
            Assert.Equal("member@email.com", LastEmail.To?.Address);
            Assert.Contains("FirstName", LastEmail.HtmlContent);
            Assert.Contains("receipt", LastEmail.HtmlContent);
            Assert.Equal(this.tenant, LastEmail.SendContext?.Tenant);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(1)]
        public async Task GeneratesInitialInvoiceEmail(int? earlyBirdDueIn)
        {
            this.invoice.MemberBillingPeriod!.Due = DateTime.UtcNow.AddDays(2);
            if (earlyBirdDueIn.HasValue)
            {
                this.invoice.MemberBillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(earlyBirdDueIn.Value);
            }

            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });

            Assert.Equal(this.billingConfig.NotificationContactName, LastEmail.ReplyTo?.Name);
            Assert.Equal(this.billingConfig.NotificationContactEmail, LastEmail.ReplyTo?.Address);
            Assert.Contains("Test Billing Period invoice", LastEmail.Subject);
            Assert.Equal("FirstName LastName", LastEmail.To?.Name);
            Assert.Equal("member@email.com", LastEmail.To?.Address);
            Assert.Contains("FirstName", LastEmail.HtmlContent);
            Assert.Contains("now been generated", LastEmail.HtmlContent);
            Assert.Equal(this.tenant, LastEmail.SendContext?.Tenant);
            AssertHasInvoiceAttachment();

            if (earlyBirdDueIn.HasValue)
            {
                Assert.Contains("early bird discount", LastEmail.HtmlContent);
            }
            else
            {
                Assert.DoesNotContain("early bird discount", LastEmail.HtmlContent);
            }
        }

        [Fact]
        public async Task GeneratesEarlyBirdEndedInvoiceEmail()
        {
            this.invoice.MemberBillingPeriod!.Due = DateTime.UtcNow.AddDays(2);
            this.invoice.MemberBillingPeriod.EarlyBirdDue = DateTime.UtcNow.AddDays(-1);

            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });

            Assert.Equal(this.billingConfig.NotificationContactName, LastEmail.ReplyTo?.Name);
            Assert.Equal(this.billingConfig.NotificationContactEmail, LastEmail.ReplyTo?.Address);
            Assert.Contains("Test Billing Period invoice", LastEmail.Subject);
            Assert.Equal("FirstName LastName", LastEmail.To?.Name);
            Assert.Equal("member@email.com", LastEmail.To?.Address);
            Assert.Contains("FirstName", LastEmail.HtmlContent);
            Assert.Contains(
                "past the early bird period",
                LastEmail.HtmlContent!.Replace("\n", "").Replace("    ", " "));
            Assert.Equal(this.tenant, LastEmail.SendContext?.Tenant);
            AssertHasInvoiceAttachment();
        }

        [Fact]
        public async Task GeneratesOverdueInvoiceEmail()
        {
            this.invoice.MemberBillingPeriod!.Due = DateTime.UtcNow.AddDays(-1);

            await this.sender.SendAsync(this.tenant, this.billingConfig, new[] { this.invoice });

            Assert.Equal(this.billingConfig.NotificationContactName, LastEmail.ReplyTo?.Name);
            Assert.Equal(this.billingConfig.NotificationContactEmail, LastEmail.ReplyTo?.Address);
            Assert.Contains("Test Billing Period invoice", LastEmail.Subject);
            Assert.Equal("FirstName LastName", LastEmail.To?.Name);
            Assert.Equal("member@email.com", LastEmail.To?.Address);
            Assert.Contains("FirstName", LastEmail.HtmlContent);
            Assert.Contains("now overdue", LastEmail.HtmlContent);
            Assert.Equal(this.tenant, LastEmail.SendContext?.Tenant);
            AssertHasInvoiceAttachment();
        }

        private void AssertHasInvoiceAttachment()
        {
            var pdf = Assert.Single(LastEmail.Attachments);
            Assert.Equal($"{this.invoice.Reference}.pdf", pdf.Name);
            Assert.Equal("application/pdf", pdf.MimeType);
            Assert.Equal("Very real pdf", Encoding.UTF8.GetString(Convert.FromBase64String(pdf.Base64Content)));
        }

        private sealed class MockInvoiceRenderingService : IMemberInvoiceRenderingService
        {
            public byte[] RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice)
            {
                return Encoding.UTF8.GetBytes("Very real pdf");
            }

            public void RenderAsPdf(Tenant tenant, MemberBillingConfig billingConfig, MemberInvoice memberInvoice, Stream stream)
            {
                // Noop
            }
        }
    }
}
