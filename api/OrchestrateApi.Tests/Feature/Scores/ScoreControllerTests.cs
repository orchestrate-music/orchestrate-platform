using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.Scores;

namespace OrchestrateApi.Tests.Feature.Scores;

public class ScoreControllerTests : AutoRollbackTestHarness
{
    private readonly ScoreController controller;

    public ScoreControllerTests()
    {
        this.controller = new ScoreController(Context);
    }

    [Fact]
    public async Task SingleConcertAndPerformanceIsPopulatedCorrectly()
    {
        var score = new Score { Title = "title" };
        var concert = await AddConcertAndPerformances(score, "abc", UtcDateTime(2023, 2, 18), "ensemble");

        var concertDtos = await this.controller.GetScoreConcerts(score.Id);

        var concertDto = Assert.Single(concertDtos);
        Assert.Equal(concert.Id, concertDto.ConcertId);
        Assert.Equal("abc", concertDto.Occasion);
        Assert.Equal(UtcDateTime(2023, 2, 18), concertDto.Date);
        var performanceDto = Assert.Single(concertDto.Performances);
        Assert.Equal("ensemble", performanceDto.EnsembleName);
    }

    [Fact]
    public async Task MultipleConcertsAndPerformancesArePopulatedCorrectly()
    {
        var score = new Score { Title = "title" };
        var concert1 = await AddConcertAndPerformances(score, "abc", UtcDateTime(2023, 2, 18), "ensemble");
        var concert2 = await AddConcertAndPerformances(score, "def", UtcDateTime(2023, 2, 19), "ensemble", "ensemble2");

        var concertDtos = await this.controller.GetScoreConcerts(score.Id);

        Assert.Equal(2, concertDtos.Count);
        Assert.True(concertDtos.Zip(concertDtos.Skip(1)).All(a => a.First.Date > a.Second.Date));

        var concertDto1 = Assert.Single(concertDtos, e => e.Occasion == "abc");
        Assert.Equal(UtcDateTime(2023, 2, 18), concertDto1.Date);
        var performanceDto1 = Assert.Single(concertDto1.Performances);
        Assert.Equal("ensemble", performanceDto1.EnsembleName);

        var concertDto2 = Assert.Single(concertDtos, e => e.Occasion == "def");
        Assert.Equal(UtcDateTime(2023, 2, 19), concertDto2.Date);
        Assert.Equal(2, concertDto2.Performances.Count());
        Assert.Single(concertDto2.Performances, e => e.EnsembleName == "ensemble");
        Assert.Single(concertDto2.Performances, e => e.EnsembleName == "ensemble2");
    }

    private async Task<Concert> AddConcertAndPerformances(
        Score score, string concertOccasion, DateTime concertDate, params string[] ensembleNames)
    {
        var concert = new Concert { Occasion = concertOccasion, Date = concertDate };
        foreach (var name in ensembleNames)
        {
            var ensemble = await Context.Ensemble.SingleOrDefaultAsync(e => e.Name == name)
                ?? new Ensemble { Name = name };
            Context.Add(new Performance
            {
                Concert = concert,
                Ensemble = ensemble,
                PerformanceScores =
                {
                    new PerformanceScore {Score = score},
                },
            });

            // Ensure ensembles have been created in the DB for the above check
            await Context.SaveChangesAsync();
        }

        return concert;
    }

    private static DateTime UtcDateTime(int year, int month, int day)
        => new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
}
