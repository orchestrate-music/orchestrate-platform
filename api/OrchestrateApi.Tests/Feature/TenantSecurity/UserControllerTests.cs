using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Feature.TenantSecurity;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests.Feature.TenantSecurity
{
    public class UserControllerTests : FullIntegrationTestHarness
    {
        private readonly ITenantService tenantService;
        private readonly UserManager<OrchestrateUser> userManager;
        private readonly UserController userController;

        public UserControllerTests()
        {
            this.tenantService = GetService<ITenantService>();
            this.userManager = GetService<UserManager<OrchestrateUser>>();
            this.userController = new(
                this.userManager,
                GetService<RoleManager<OrchestrateRole>>(),
                GetService<UserService>(),
                GetService<OrchestrateContext>());
        }

        [Fact]
        public async Task ScopesByActiveTenant()
        {
            var bobUser = await this.userManager.FindByEmailAsync(OrchestrateSeed.BobMarleyEmail);
            Assert.NotNull(bobUser);

            var otherTenantEntry = Context.Add(new Tenant
            {
                Name = "Test",
                Abbreviation = "test",
                TimeZone = "Australia/Perth",
            });
            Member otherMember;
            await Context.SaveChangesAsync();

            using (var tenantOverride = this.tenantService.ForceOverrideTenantId(otherTenantEntry.Entity.Id))
            {
                otherMember = new Member
                {
                    FirstName = "__FIRST__",
                    LastName = "__LAST__",
                    DateJoined = DateTime.UtcNow.AddDays(-1),
                    Details = new MemberDetails
                    {
                        Address = "abc",
                        Email = bobUser.Email,
                        PhoneNo = "1234",
                        User = bobUser,
                    },
                };
                var ensemble = new Ensemble { Name = "__ENSEMBLE__" };
                var ensembleRole = new EnsembleRole
                {
                    Ensemble = ensemble,
                    Role = await Context.Roles.SingleAsync(e => e.Name == StandardRole.Viewer),
                };
                var membership = new EnsembleMembership
                {
                    Member = otherMember,
                    Ensemble = ensemble,
                    DateJoined = DateTime.UtcNow.AddDays(-1),
                };

                await this.userManager.AddToRoleAsync(bobUser, StandardRole.Administrator);
            }

            var bobMember = await Context.Member.SingleAsync(e => e.Details!.UserId == bobUser.Id);

            var userRoles = await this.userController.GetUsers();
            Assert.DoesNotContain(userRoles, e => e.MemberId == otherMember.Id);
            Assert.DoesNotContain(userRoles, e => e.Name.Contains("__FIRST__") || e.Name.Contains("__LAST__"));
            var bobRoles = userRoles.Single(e => e.UserId == bobUser.Id);
            Assert.Equal(bobUser.UserName, bobRoles.Username);
            Assert.Equal(bobMember.Id, bobRoles.MemberId);
        }

        [Fact]
        public async Task MembersWithNoActiveRolesAreStillReturned()
        {
            var memberEntry = Context.Add(new Member
            {
                FirstName = "a",
                LastName = "b",
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Details = new MemberDetails
                {
                    Address = "abc",
                    Email = "a@email.com",
                    PhoneNo = "1234",
                    User = new OrchestrateUser
                    {
                        Email = "a@email.com",
                        UserName = "a@email.com",
                    },
                },
            });
            var user = memberEntry.Entity.Details!.User!;
            var result = await this.userManager.CreateAsync(user);
            Assert.True(result.Succeeded);
            await Context.SaveChangesAsync();

            var userDtos = await this.userController.GetUsers();
            var addedUserDto = userDtos.Single(e => e.UserId == user.Id);
            Assert.Equal("a@email.com", addedUserDto.Username);
            Assert.Equal("a b", addedUserDto.Name);
            Assert.Equal(memberEntry.Entity.Id, addedUserDto.MemberId);
            Assert.Equal(UserController.UserStatus.NoAccess, addedUserDto.Status);
            Assert.Empty(addedUserDto.Roles);
            Assert.Empty(addedUserDto.EnsembleRoles);
        }

        [Fact]
        public async Task MembersWithOnlyDirectRolesPopulateCorrectly()
        {
            var memberEntry = Context.Add(new Member
            {
                FirstName = "a",
                LastName = "b",
                DateJoined = DateTime.UtcNow.AddDays(-1),
                Details = new MemberDetails
                {
                    Address = "abc",
                    Email = "a@email.com",
                    PhoneNo = "1234",
                    User = new OrchestrateUser
                    {
                        Email = "a@email.com",
                        UserName = "a@email.com",
                    },
                },
            });
            var user = memberEntry.Entity.Details!.User!;
            await this.userManager.CreateAsync(user);
            await this.userManager.AddToRoleAsync(user, StandardRole.Administrator);
            await Context.SaveChangesAsync();

            var userDtos = await this.userController.GetUsers();
            var addedUserDto = userDtos.Single(e => e.UserId == user.Id);
            Assert.Single(addedUserDto.Roles);
            Assert.Single(addedUserDto.Roles, e => e == StandardRole.Administrator);
            Assert.Empty(addedUserDto.EnsembleRoles);
        }

        [Fact]
        public async Task EnsembleRolesArePopulatedCorrectly()
        {
            var bobUser = await this.userManager.FindByEmailAsync(OrchestrateSeed.BobMarleyEmail);
            Assert.NotNull(bobUser);
            await this.userManager.AddToRoleAsync(bobUser, StandardRole.Administrator);

            var bobMember = await Context.Member.SingleAsync(e => e.Details!.UserId == bobUser.Id);
            var ensembleRoleEntry = Context.Add(new EnsembleRole
            {
                Ensemble = await Context.EnsembleMembership.Where(e => e.MemberId == bobMember.Id).Select(e => e.Ensemble).FirstAsync(),
                Role = await Context.Roles.SingleAsync(e => e.Name == StandardRole.Viewer),
            });
            await Context.SaveChangesAsync();

            var userRoles = await this.userController.GetUsers();
            Assert.Single(userRoles, e => e.MemberId == bobMember.Id);

            var bobRoles = userRoles.Single(e => e.MemberId == bobMember.Id);
            Assert.Single(bobRoles.Roles, e => e == StandardRole.Administrator);
            Assert.Single(bobRoles.EnsembleRoles);
            Assert.Equal(ensembleRoleEntry.Entity.EnsembleId, bobRoles.EnsembleRoles.First().EnsembleId);
            Assert.Equal(ensembleRoleEntry.Entity.Ensemble!.Name, bobRoles.EnsembleRoles.First().EnsembleName);
            Assert.Single(bobRoles.EnsembleRoles.First().Roles, e => e == StandardRole.Viewer);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                this.userManager.Dispose();
            }
        }
    }
}
