using Microsoft.Extensions.FileProviders;

namespace OrchestrateApi.Tests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1065")]
    public class MockHostEnvironment : IHostEnvironment
    {
        public const string UnitTestEnvironmentName = "UnitTest";

        public string EnvironmentName { get; set; } = UnitTestEnvironmentName;
        public string ApplicationName { get; set; } = typeof(MockHostEnvironment).Assembly.FullName!;
        public string ContentRootPath
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
        public IFileProvider ContentRootFileProvider
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
    }
}
