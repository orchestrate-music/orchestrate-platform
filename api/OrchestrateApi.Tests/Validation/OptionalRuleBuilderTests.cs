using System.Diagnostics.CodeAnalysis;
using FluentValidation;
using OrchestrateApi.Common;
using OrchestrateApi.Validation;
using OrchestrateApi.Validation.Validators;

namespace OrchestrateApi.Tests.Validation;

public class OptionalValidatorTests
{
    [Fact]
    public void NotEmptyIsValidatedCorrectly()
    {
        var v = new DtoValidator<string?>();
        v.RuleFor(e => e.Prop).Optional().NotEmpty();

        Assert.True(v.Validate(new Dto<string?>()).IsValid);
        Assert.True(v.Validate(new Dto<string?> { Prop = "abc" }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = "" }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = "  " }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = null }).IsValid);
    }

    [Fact]
    public void EmailAddressIsValidatedCorrectly()
    {
        var v = new DtoValidator<string?>();
        v.RuleFor(e => e.Prop).Optional().EmailAddress();

        Assert.True(v.Validate(new Dto<string?>()).IsValid);
        Assert.True(v.Validate(new Dto<string?> { Prop = "a@b.com" }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = "a" }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = "" }).IsValid);
        Assert.False(v.Validate(new Dto<string?> { Prop = "  " }).IsValid);
        Assert.True(v.Validate(new Dto<string?> { Prop = null }).IsValid);
    }

    [SuppressMessage("Call async methods when in an async method", "CA1849")]
    [Fact]
    public async Task WholeOptionalValidatorIsValidatedCorrectly()
    {
        var dtoValidator = new DtoValidator<int>();
        dtoValidator.RuleFor(e => e.Prop).Must(i => !i.HasValue || i.Value > 0);
        var v = new OptionalValidator<Dto<int>>(dtoValidator);

        Assert.True(v.Validate(new Optional<Dto<int>>()).IsValid);
        Assert.True(v.Validate(new Optional<Dto<int>>(new Dto<int>())).IsValid);
        Assert.True(v.Validate(new Optional<Dto<int>>(new Dto<int> { Prop = 1 })).IsValid);
        Assert.False(v.Validate(new Optional<Dto<int>>(new Dto<int> { Prop = -1 })).IsValid);

        Assert.True((await v.ValidateAsync(new Optional<Dto<int>>())).IsValid);
        Assert.True((await v.ValidateAsync(new Optional<Dto<int>>(new Dto<int>()))).IsValid);
        Assert.True((await v.ValidateAsync(new Optional<Dto<int>>(new Dto<int> { Prop = 1 }))).IsValid);
        Assert.False((await v.ValidateAsync(new Optional<Dto<int>>(new Dto<int> { Prop = -1 }))).IsValid);
    }

    [Fact]
    public async Task OptionalRuleBuilderValidatesCorrectly()
    {
        var v = new DtoValidator<int>();
        v.RuleFor(e => e.Prop).Optional().MustAsync((e, ct) => Task.FromResult(e > 2));
        v.RuleSet("Sync", () =>
        {
            v.RuleFor(e => e.Prop).Optional().Must(e => e > 1);
        });

        Assert.True((await v.ValidateAsync(new Dto<int> { Prop = 3 })).IsValid);
        Assert.False((await v.ValidateAsync(new Dto<int> { Prop = 2 })).IsValid);

        Assert.True(v.Validate(new Dto<int> { Prop = 2 }, options => options.IncludeRuleSets("Sync")).IsValid);
        Assert.False(v.Validate(new Dto<int> { Prop = 1 }, options => options.IncludeRuleSets("Sync")).IsValid);
    }

    [Fact]
    public void OptionalValidatorValidatesCorrectly()
    {
        var dV = new DtoValidator<int>();
        dV.RuleFor(e => e.Prop).Optional().Must(e => e > 2);
        var wV = new DtoWrapperValidator<int>();
        wV.RuleFor(e => e.Dto).SetOptionalValidator(dV);

        Assert.True(wV.Validate(new DtoWrapper<int> { Dto = new Dto<int> { Prop = 3 } }).IsValid);
        Assert.False(wV.Validate(new DtoWrapper<int> { Dto = new Dto<int> { Prop = 1 } }).IsValid);

        dV = new DtoValidator<int>();
        dV.RuleFor(e => e.Prop).Optional().Must(e => e > 2);
        wV = new DtoWrapperValidator<int>();
        wV.RuleFor(e => e.Dto).Optional().SetValidator(dV);

        Assert.True(wV.Validate(new DtoWrapper<int> { Dto = new Dto<int> { Prop = 3 } }).IsValid);
        Assert.False(wV.Validate(new DtoWrapper<int> { Dto = new Dto<int> { Prop = 1 } }).IsValid);
    }

    private sealed class DtoWrapper<T>
    {
        public Optional<Dto<T>> Dto { get; set; }
    }

    private sealed class DtoWrapperValidator<T> : AbstractValidator<DtoWrapper<T>>
    {
    }

    private sealed class Dto<T>
    {
        public Optional<T> Prop { get; set; }
    }

    private sealed class DtoValidator<T> : AbstractValidator<Dto<T>>
    {
    }
}
