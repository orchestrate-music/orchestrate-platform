using Microsoft.AspNetCore.Identity;
using OrchestrateApi.DAL;
using OrchestrateApi.DAL.Model;
using OrchestrateApi.Security;

namespace OrchestrateApi.Tests
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("", "CA1305")]
    public partial class OrchestrateSeed
    {
        public static readonly Guid TenantId = Guid.NewGuid();
        public const string TimeZone = "Australia/Perth";

        private readonly IDictionary<string, Guid> roleIds = new Dictionary<string, Guid>();

        public OrchestrateSeed(OrchestrateContext context)
        {
            Context = context;
        }

        private OrchestrateContext Context { get; }

        private Member BobMarley { get; set; } = null!;
        private Member GlennStevens { get; set; } = null!;
        private Member IggyPop { get; set; } = null!;

        public void Seed()
        {
            AddReferenceData();

            AddEntity(new Tenant
            {
                Id = TenantId,
                Name = GroupName,
                Abbreviation = GroupAbbr,
                TimeZone = TimeZone,
                Website = Website,
                DateRegistered = DateTime.UtcNow,
                DateFounded = ParseAsUtcDateTime("2000-01-01"),
                Plan =
                {
                    Type = PlanType.Demo,
                }
            });

            var clarinet = AddEntity(new Asset
            {
                Description = "Bb Clarinet",
                Quantity = 1,
                SerialNo = "XYZ",
                DateAcquired = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                ValuePaid = 1000,
                InsuranceValue = 1000,
                CurrentLocation = "Scout Hall",
                Notes = "",
            });
            var trumpet = AddEntity(new Asset
            {
                Description = "Bb Trumpet",
                Quantity = 1,
                SerialNo = "123ABC",
                DateAcquired = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                ValuePaid = 2000,
                InsuranceValue = 1500,
                CurrentLocation = "On Loan",
                Notes = ""
            });
            AddEntity(new Asset
            {
                Description = "Bass Drum",
                Quantity = 1,
                SerialNo = "HIJK",
                DateAcquired = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                ValuePaid = 1500,
                DateDiscarded = ParseAsUtcDateTime("2015-11-19 01:16:48"),
                InsuranceValue = 1000,
                CurrentLocation = "School",
                Notes = "Broken Wheel"
            });

            BobMarley = AddEntity(new Member
            {
                FirstName = "Bob",
                LastName = "Marley",
                DateJoined = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                Details = new MemberDetails
                {
                    PhoneNo = "+61400000000",
                    Email = BobMarleyEmail,
                    Address = "1 Home Street",
                    FeeClass = FeeClass.Concession,
                    Notes = "Great Reggae",
                    User = AddUser(BobMarleyEmail, "orchestrate"),
                },
            });
            AddUserRole(BobMarley, StandardRole.Administrator);
            AddEntity(new MemberInstrument { Member = BobMarley, InstrumentName = "Clarinet" });
            AddEntity(new MemberInstrument { Member = BobMarley, InstrumentName = "Flute" });
            AddEntity(new AssetLoan
            {
                Member = BobMarley,
                Asset = clarinet,
                DateBorrowed = ParseAsUtcDateTime("2016-11-14 05:43:48"),
                DateReturned = ParseAsUtcDateTime("2016-11-15 05:43:48"),
            });
            AddEntity(new AssetLoan
            {
                Member = BobMarley,
                Asset = trumpet,
                DateBorrowed = ParseAsUtcDateTime("2016-11-14 05:43:48"),
            });

            GlennStevens = AddEntity(new Member
            {
                FirstName = "Glenn",
                LastName = "Stevens",
                DateJoined = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                Details = new MemberDetails
                {
                    PhoneNo = "+61411111111",
                    Email = GlennStevensEmail,
                    Address = "5 Iggy Street",
                    FeeClass = FeeClass.Full,
                    Notes = "Don't know who he is",
                    User = AddUser(GlennStevensEmail, "orchestrate"),
                },
            });
            AddUserRole(GlennStevens, StandardRole.Viewer);
            AddEntity(new MemberInstrument { Member = GlennStevens, InstrumentName = "Trumpet" });
            AddEntity(new AssetLoan
            {
                Member = GlennStevens,
                Asset = clarinet,
                DateBorrowed = ParseAsUtcDateTime("2016-10-14 05:43:48"),
                DateReturned = ParseAsUtcDateTime("2016-10-15 05:43:48"),
            });

            IggyPop = AddEntity(new Member
            {
                FirstName = "Iggy",
                LastName = "Pop",
                DateJoined = ParseAsUtcDateTime("2015-11-17 01:16:48"),
                DateLeft = ParseAsUtcDateTime("2015-11-19 01:16:48"),
                Details = new MemberDetails
                {
                    PhoneNo = "+61433333333",
                    Email = IggyPopEmail,
                    Address = "2 Infinity Way",
                    FeeClass = FeeClass.Special,
                    Notes = "Somehow still alive",
                    User = AddUser(IggyPopEmail, "orchestrate"),
                },
            });
            AddUserRole(IggyPop, StandardRole.Editor);

            var bohemianRhapsody = AddEntity(new Score
            {
                Title = "Bohemian Rhapsody",
                Composer = "Queen",
                InLibrary = true,
                Genre = "Rock",
                Grade = "3",
                Duration = 582,
                DatePurchased = ParseAsUtcDateTime("2016-09-19 07:04:52"),
                ValuePaid = 100.90M,
                Location = "Scout Hall",
                Notes = "Great Song",
            });
            var powerOfLove = AddEntity(new Score
            {
                Title = "The Power of Love",
                Composer = "Huey Lewis and the News",
                InLibrary = true,
                Genre = "Rock",
                Grade = "4",
                Duration = 256,
                DatePurchased = ParseAsUtcDateTime("2016-09-19 07:04:52"),
                ValuePaid = 200.90M,
                Location = "Leeming High School",
                Notes = "From a great movie",
            });
            var nothingElseMatters = AddEntity(new Score
            {
                Title = "Nothing Else Matters",
                InLibrary = true,
                Composer = "Metallica",
                Genre = "Rock",
                Grade = "2.5",
                Duration = 212,
                DatePurchased = ParseAsUtcDateTime("2016-09-19 07:04:52"),
                ValuePaid = 50.90M,
                Location = "Scout Hall",
                Notes = "Also Great Song",
            });

            var concertBand = AddEntity(new Ensemble
            {
                Name = ConcertBandName,
                MemberBillingPeriodFeeDollars = 90,
            });
            AddEntity(new EnsembleMembership
            {
                Member = BobMarley,
                Ensemble = concertBand,
                DateJoined = ParseAsUtcDateTime("2016-11-14 05:43:48")
            });
            AddEntity(new EnsembleMembership
            {
                Member = GlennStevens,
                Ensemble = concertBand,
                DateJoined = ParseAsUtcDateTime("2016-11-14 05:43:48"),
                DateLeft = ParseAsUtcDateTime("2016-11-21 05:43:48")
            });
            var bigBand = AddEntity(new Ensemble
            {
                Name = "Big Band",
                MemberBillingPeriodFeeDollars = 50,
            });
            AddEntity(new EnsembleMembership
            {
                Member = BobMarley,
                Ensemble = bigBand,
                DateJoined = ParseAsUtcDateTime("2016-11-14 05:43:48")
            });
            var saxEnsemble = AddEntity(new Ensemble { Name = "Saxophone Ensemble" });
            AddEntity(new EnsembleMembership
            {
                Member = GlennStevens,
                Ensemble = saxEnsemble,
                DateJoined = ParseAsUtcDateTime("2016-11-14 05:43:48")
            });

            var states = AddEntity(new Concert
            {
                Occasion = "State Championships",
                Location = "Christchurch Grammer School",
                Date = ParseAsUtcDateTime("2016-10-08 05:27:29"),
                Notes = "We won!",
            });
            var concertBandStates = AddEntity(new Performance { Concert = states, Ensemble = concertBand });
            AddEntity(new PerformanceMember { Performance = concertBandStates, Member = BobMarley });
            AddEntity(new PerformanceMember { Performance = concertBandStates, Member = GlennStevens });
            AddEntity(new PerformanceScore { Performance = concertBandStates, Score = bohemianRhapsody });
            AddEntity(new PerformanceScore { Performance = concertBandStates, Score = powerOfLove });
            var saxStates = AddEntity(new Performance { Concert = states, Ensemble = saxEnsemble });
            AddEntity(new PerformanceMember { Performance = saxStates, Member = IggyPop });
            AddEntity(new PerformanceScore { Performance = saxStates, Score = nothingElseMatters });

            var halloween = AddEntity(new Concert
            {
                Occasion = "Halloween Concert",
                Location = "Cockburn Youth Centre",
                Date = ParseAsUtcDateTime("2016-10-30 05:27:29"),
                Notes = "Spooky",
            });
            AddEntity(new Performance { Concert = halloween, Ensemble = concertBand });

            var christmas = AddEntity(new Concert
            {
                Occasion = "Christmas Gig",
                Location = "Christchurch Grammer School",
                Date = ParseAsUtcDateTime("2016-12-13 05:27:29"),
                Notes = "Ho ho ho",
            });
            var concertBandChristmas = AddEntity(new Performance { Concert = christmas, Ensemble = concertBand });
            AddEntity(new PerformanceMember { Performance = concertBandChristmas, Member = BobMarley });
            AddEntity(new PerformanceScore { Performance = concertBandChristmas, Score = powerOfLove });

            CreateSeededMemberBillingData(BobMarley, GlennStevens);
        }

        private void CreateSeededMemberBillingData(Member bobMarley, Member glennStevens)
        {
            AddEntity(new MemberBillingConfig
            {
                IsEnabled = true,
                AssociationPeriodFeeDollars = 40,
                ConcessionRate = 0.5M,
                EarlyBirdDiscountDollars = 10,
                EarlyBirdDiscountDaysValid = 30,
                NotificationContactName = "Test Person",
                NotificationContactEmail = "test@email.com",
                BankName = "Music Bank",
                BankAccountName = "Musical Community Bands Inc",
                BankAccountNo = "1234567",
                BankBsb = "001-123",
                InvoicePrefix = "INV",
                NextInvoiceNumber = 9,
            });
            AddEntity(new MemberBillingPeriodConfig
            {
                Name = "Semester 1",
                AutoGenerateMonth = 3,
                EndOfPeriodMonth = 6,
            });
            AddEntity(new MemberBillingPeriodConfig
            {
                Name = "Semester 2",
                AutoGenerateMonth = 9,
                EndOfPeriodMonth = 12,
            });

            var sem1BillingPeriod = AddEntity(new MemberBillingPeriod
            {
                Name = "2020 - Semester 1",
                State = MemberBillingPeriodState.Closed,
                Generated = ParseAsUtcDateTime("2020-03-01"),
                Due = ParseAsUtcDateTime("2020-06-30 15:59:59"),
                Sent = ParseAsUtcDateTime("2020-03-10"),
                EarlyBirdDue = ParseAsUtcDateTime("2020-04-10 15:59:59"),
                Closed = ParseAsUtcDateTime("2020-07-04 03:00:00"),
            });
            var bobSem1Invoice = AddEntity(new MemberInvoice
            {
                Member = bobMarley,
                MemberBillingPeriod = sem1BillingPeriod,
                IsConcession = true,
                Reference = "INV-000001",
                Sent = ParseAsUtcDateTime("2020-03-10"),
                SendStatus = MemberInvoiceSendStatus.Delivered,
                SendStatusUpdated = ParseAsUtcDateTime("2020-03-10 05:42:00"),
                SendStatusMessage = "Delivered to recipient successfully",
                Paid = ParseAsUtcDateTime("2020-04-09"),
                Notes = "First invoice!",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem1Invoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem1Invoice,
                Description = "Big Band Fee (Concession)",
                AmountCents = 3500,
                Ordinal = 1,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem1Invoice,
                Description = "Early Bird Discount",
                AmountCents = -1000,
                Ordinal = 2,
            });
            var glennSem1Invoice = AddEntity(new MemberInvoice
            {
                Member = glennStevens,
                MemberBillingPeriod = sem1BillingPeriod,
                Reference = "INV-000002",
                Sent = ParseAsUtcDateTime("2020-03-10"),
                SendStatus = MemberInvoiceSendStatus.Opened,
                SendStatusUpdated = ParseAsUtcDateTime("2020-03-10 05:47:00"),
                SendStatusMessage = "Recipient has opened invoice",
                Paid = ParseAsUtcDateTime("2020-05-09"),
                Notes = "Overdue invoice",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennSem1Invoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennSem1Invoice,
                Description = "Concert Band",
                AmountCents = 9000,
                Ordinal = 1,
            });

            var sem2BillingPeriod = AddEntity(new MemberBillingPeriod
            {
                Name = "2020 - Semester 2",
                State = MemberBillingPeriodState.Closed,
                Generated = ParseAsUtcDateTime("2020-09-01"),
                Due = ParseAsUtcDateTime("2020-12-31 15:59:59"),
                Sent = ParseAsUtcDateTime("2020-09-10"),
                EarlyBirdDue = ParseAsUtcDateTime("2020-10-10 15:59:59"),
                Closed = ParseAsUtcDateTime("2021-01-14 11:23:12"),
            });
            var bobSem2Invoice = AddEntity(new MemberInvoice
            {
                Member = bobMarley,
                MemberBillingPeriod = sem2BillingPeriod,
                IsConcession = true,
                Reference = "INV-000003",
                Sent = ParseAsUtcDateTime("2020-09-10"),
                SendStatus = MemberInvoiceSendStatus.Delivered,
                SendStatusUpdated = ParseAsUtcDateTime("2020-09-10 06:42:00"),
                SendStatusMessage = "Delivered to recipient successfully",
                Paid = ParseAsUtcDateTime("2020-10-09"),
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem2Invoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem2Invoice,
                Description = "Concert Band Fee (Concession)",
                AmountCents = 9000,
                Ordinal = 1,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobSem2Invoice,
                Description = "Early Bird Discount",
                AmountCents = -1000,
                Ordinal = 2,
            });
            var glennSem2Invoice = AddEntity(new MemberInvoice
            {
                Member = glennStevens,
                MemberBillingPeriod = sem2BillingPeriod,
                Reference = "INV-000004",
                Sent = ParseAsUtcDateTime("2020-09-20"),
                SendStatus = MemberInvoiceSendStatus.Delivered,
                SendStatusUpdated = ParseAsUtcDateTime("2020-09-10 07:23:00"),
                SendStatusMessage = "Delivered to recipient successfully",
                Paid = ParseAsUtcDateTime("2020-10-09"),
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennSem2Invoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennSem2Invoice,
                Description = "Concert Band Fee",
                AmountCents = 9000,
                Ordinal = 1,
            });

            var openBillingPeriod = AddEntity(new MemberBillingPeriod
            {
                Name = "2021 - Semester 1",
                State = MemberBillingPeriodState.Open,
                Generated = ParseAsUtcDateTime("2021-02-18"),
                Sent = ParseAsUtcDateTime("2021-02-21"),
                EarlyBirdDue = ParseAsUtcDateTime("2021-03-21 15:59:59"),
                Due = ParseAsUtcDateTime("2021-06-30 15:59:59"),
            });
            var bobOpenInvoice = AddEntity(new MemberInvoice
            {
                Member = bobMarley,
                MemberBillingPeriod = openBillingPeriod,
                IsConcession = true,
                Reference = "INV-000005",
                Sent = ParseAsUtcDateTime("2021-02-21"),
                SendStatus = MemberInvoiceSendStatus.Failed,
                SendStatusUpdated = ParseAsUtcDateTime("2021-02-21 05:42:00"),
                SendStatusMessage = "Email bounced (marked as spam)",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobOpenInvoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobOpenInvoice,
                Description = "Concert Band (Concession)",
                AmountCents = 4500,
                Ordinal = 1,
            });
            var glennOpenInvoice = AddEntity(new MemberInvoice
            {
                Member = glennStevens,
                MemberBillingPeriod = openBillingPeriod,
                Reference = "INV-000006",
                Sent = ParseAsUtcDateTime("2021-02-21"),
                SendStatus = MemberInvoiceSendStatus.Pending,
                SendStatusUpdated = ParseAsUtcDateTime("2021-02-21 05:42:00"),
                SendStatusMessage = "Delivery pending",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennOpenInvoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennOpenInvoice,
                Description = "Concert Band Fee",
                AmountCents = 9000,
                Ordinal = 1,
            });

            var draftBillingPeriod = AddEntity(new MemberBillingPeriod
            {
                Name = "2021 - Semester 2",
                State = MemberBillingPeriodState.Draft,
                Generated = ParseAsUtcDateTime("2021-09-01"),
                Due = ParseAsUtcDateTime("2021-12-31 15:59:59"),
            });
            var bobDraftInvoice = AddEntity(new MemberInvoice
            {
                Member = bobMarley,
                MemberBillingPeriod = draftBillingPeriod,
                Reference = "INV-000007",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobDraftInvoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = bobDraftInvoice,
                Description = "Concert Band Fee (Concession)",
                AmountCents = 4500,
                Ordinal = 1,
            });
            var glennDraftInvoice = AddEntity(new MemberInvoice
            {
                Member = glennStevens,
                MemberBillingPeriod = draftBillingPeriod,
                Reference = "INV-000008",
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennDraftInvoice,
                Description = "Association Fee",
                AmountCents = 4000,
                Ordinal = 0,
            });
            AddEntity(new MemberInvoiceLineItem
            {
                MemberInvoice = glennDraftInvoice,
                Description = "Concert Band Fee",
                AmountCents = 9000,
                Ordinal = 1,
            });
        }

        private void AddReferenceData()
        {
            foreach (var roleName in StandardRole.AllRoleNames)
            {
                var entry = Context.Add(new OrchestrateRole
                {
                    Id = Guid.NewGuid(),
                    Name = roleName,
                    NormalizedName = roleName.ToUpperInvariant(),
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                });
                this.roleIds[roleName] = entry.Entity.Id;
            }

            Context.Add(new PlanMetadata { Type = PlanType.Free, MemberLimit = 10, });
            var paidPlans = new[] { PlanType.Small, PlanType.Medium, PlanType.Large }
                .Select((p, i) => new PaidPlanMetadata
                {
                    Type = p,
                    MemberLimit = (i + 2) * 10,
                    MonthlyFeeCents = (i + 1) * 1000,
                    StripePriceId = $"price_{p}",
                    StripeProductId = $"prod_{p}",
                });
            foreach (var plan in paidPlans)
            {
                Context.Add(plan);
            }
        }

        private OrchestrateUser AddUser(string email, string password)
        {
            var passwordHasher = new PasswordHasher<OrchestrateUser>();
            var user = new OrchestrateUser
            {
                Id = Guid.NewGuid(),
                UserName = email,
                NormalizedUserName = email.ToUpperInvariant(),
                Email = email,
                NormalizedEmail = email.ToUpperInvariant(),
                ConcurrencyStamp = Guid.NewGuid().ToString(),
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            user.PasswordHash = passwordHasher.HashPassword(user, password);
            Context.Add(user);
            return user;
        }

        private void AddUserRole(Member member, string roleName)
        {
            AddEntity(new OrchestrateUserRole
            {
                // Entities haven't been saved yet, so get the temporary key
                UserId = Context.Entry(member!.Details!.User!).Property(u => u.Id).CurrentValue,
                RoleId = this.roleIds[roleName],
            });
        }

        private T AddEntity<T>(T entity) where T : notnull
        {
            Context.Add(entity);
            return entity;
        }

        // Shouldn't need this after #73
        private static DateTime ParseAsUtcDateTime(string datetime)
        {
            return DateTime.Parse($"{datetime}Z").ToUniversalTime();
        }
    }
}
