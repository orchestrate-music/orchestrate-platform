variables:
    NUGET_PACKAGES_DIR: .nuget

stages:
    - generate-metadata
    - test
    - package
    - deploy

.api:base:
    image: mcr.microsoft.com/dotnet/sdk:9.0
    cache:
        key:
            files:
                - api/OrchestrateApi/OrchestrateApi.csproj
                - api/OrchestrateApi.Tests/OrchestrateApi.Tests.csproj
        paths:
            - $NUGET_PACKAGES_DIR
    before_script:
        - dotnet restore api/orchestrate-api.sln --packages $NUGET_PACKAGES_DIR

.api:with-database:
    extends: .api:base
    services:
        - postgres:16
    variables:
        POSTGRES_DB: orchestrate-test
        POSTGRES_USER: test-user
        POSTGRES_PASSWORD: ""
        POSTGRES_HOST_AUTH_METHOD: trust
        DATABASE_URL: postgresql://test-user@postgres/orchestrate-test

api:generate-metadata:
    extends: .api:with-database
    stage: generate-metadata
    interruptible: true
    script:
        - dotnet run --project api/OrchestrateApi/OrchestrateApi.csproj --no-restore --verbosity quiet -- generate-metadata > web-client/src/app/dal/breeze-metadata.json
    artifacts:
        paths:
            # Can't use a variable here :(
            - web-client/src/app/dal/breeze-metadata.json

api:format-and-lint:
    extends: .api:base
    stage: test
    interruptible: true
    needs: []
    script:
        - dotnet format --no-restore --verify-no-changes --verbosity detailed api/orchestrate-api.sln

api:test:
    extends: .api:with-database
    stage: test
    interruptible: true
    needs: []
    script:
        - dotnet test ./api/OrchestrateApi.Tests/OrchestrateApi.Tests.csproj --no-restore --collect "XPlat Code Coverage" --settings ./api/OrchestrateApi.Tests/coverlet.runsettings
        - mv api/OrchestrateApi.Tests/TestResults/**/coverage.cobertura.xml api/OrchestrateApi.Tests/TestResults/
        - api/print-cobertura-coverage.sh api/OrchestrateApi.Tests/TestResults/coverage.cobertura.xml
    coverage: /TOTAL_COVERAGE=(\d+.\d+)/
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: api/OrchestrateApi.Tests/TestResults/coverage.cobertura.xml

.api:deploy:base:
    image: ubuntu
    stage: deploy
    variables:
        FLY_API_HOSTNAME: https://api.machines.dev
    before_script:
        - apt-get update -qq && apt-get install -y curl jq
        - curl -L https://fly.io/install.sh | sh
    script:
        - ~/.fly/bin/flyctl deploy api/ --app $FLY_APP
        - |
            machines=$(curl -sS -X GET -H "Authorization: Bearer ${FLY_API_TOKEN}" -H "Content-Type: application/json" "${FLY_API_HOSTNAME}/v1/apps/${FLY_APP}/machines")
            periodicMachine=$(echo $machines | jq 'map(select(.config.metadata.role == "periodic-job-runner")) | first')
            periodicMachineId=$(echo $periodicMachine | jq --raw-output '.id')
            appMachine=$(echo $machines | jq 'map(select(.config.env.FLY_PROCESS_GROUP == "app")) | first')
            appMachineImage=$(echo $appMachine | jq --raw-output '.config.image')
        - ~/.fly/bin/flyctl machine update $periodicMachineId --image=$appMachineImage --app=$FLY_APP --yes

api:deploy:staging:
    extends: .api:deploy:base
    environment: staging-api
    only:
        - develop

api:deploy:production:
    extends: .api:deploy:base
    environment: production-api
    only:
        - master

.web-client:base:
    image: node:22
    cache:
        key:
            files:
                - web-client/package-lock.json
        paths:
            - web-client/node_modules/
    before_script:
        - cd web-client
        - npm install

web-client:lint:
    extends: .web-client:base
    stage: test
    interruptible: true
    needs: []
    script:
        - npx ng lint

web-client:format:
    extends: .web-client:base
    stage: test
    interruptible: true
    needs: []
    script:
        - npx prettier --check .

# web-client:test:
#     extends: .web-client:base
#     stage: test
#     interruptible: true
#     needs: ["api:generate-metadata"]
#     variables:
#         CHROME_BIN: "/usr/bin/chromium"
#     script:
#         - apt-get update && apt-get install -y chromium
#         - src/scripts/generate-app-config.sh src/app-config.json
#         - npx ng test --watch=false --code-coverage --include src/**/*.spec.ts --browsers ChromeHeadlessNoSandbox

web-client:package:
    extends: .web-client:base
    stage: package
    interruptible: true
    needs: ["api:generate-metadata"]
    script:
        - npx ng build --configuration production --source-map
    artifacts:
        paths:
            - web-client/dist

.web-client:deploy:base:
    extends: .web-client:base
    stage: deploy
    script:
        - src/scripts/generate-app-config.sh dist/app-config.json
        - npx netlify deploy --prod

web-client:deploy:staging:
    extends: .web-client:deploy:base
    environment: staging-web
    variables:
        ENVIRONMENT_NAME: Staging
    only:
        - develop

web-client:deploy:production:
    extends: .web-client:deploy:base
    environment: production-web
    variables:
        ENVIRONMENT_NAME: Production
    only:
        - master
