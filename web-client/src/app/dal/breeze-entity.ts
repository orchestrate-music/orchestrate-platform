import { Entity, EntityAspect, EntityType } from "breeze-client";

export class BreezeEntity implements Entity {
    entityAspect!: EntityAspect;
    entityType!: EntityType;
}
