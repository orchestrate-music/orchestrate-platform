import { SaveError, ServerError } from "breeze-client/src/entity-manager";

export function isBreezeServerError(error: any): error is ServerError {
    return typeof error?.status === "number";
}

export function isBreezeSaveError(error: any): error is SaveError {
    return Array.isArray(error.entityErrors);
}
