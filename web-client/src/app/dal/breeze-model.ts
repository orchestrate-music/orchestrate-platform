import { BreezeEntity } from "app/dal/breeze-entity";
import { Validator } from "breeze-client";

export interface BreezeModel<T extends BreezeEntity> {
    typeName: string;
    type: new () => T;
    additionalValidators?: Partial<{ [K in keyof T]: Validator[] }>;
}
