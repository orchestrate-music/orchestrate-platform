import { OrchestrateBlobMetadata } from "./orchestrate-blob-metadata";

export enum EntityAttachmentSensitivity {
    Secured = "Secured",
    Sensitive = "Sensitive",
}

export interface EntityAttachmentAccessData {
    sensitivity: EntityAttachmentSensitivity;
}

export interface EntityAttachementLinkData {
    entityId: number;
}

export class EntityAttachmentMetadata
    implements OrchestrateBlobMetadata<EntityAttachmentAccessData>
{
    id!: string;
    name!: string;
    mimeType!: string;
    modified!: Date;
    sizeBytes!: number;
    accessStrategy!: { name: string; accessData: EntityAttachmentAccessData };

    public constructor(blob: OrchestrateBlobMetadata<EntityAttachmentAccessData>) {
        Object.assign(this, blob);
    }

    public get sensitivity() {
        return this.accessStrategy.accessData.sensitivity;
    }

    public set sensitivity(value: EntityAttachmentSensitivity) {
        this.accessStrategy.accessData.sensitivity = value;
    }
}
