import { Pipe, PipeTransform } from "@angular/core";
import { formatBytes } from "app/common/utils";

@Pipe({
    name: "appFormatBytes",
    standalone: false,
})
export class FormatBytesPipe implements PipeTransform {
    public transform(value: number, decimals?: number): any {
        if (typeof value !== "number") {
            return null;
        }

        return formatBytes(value, decimals);
    }
}
