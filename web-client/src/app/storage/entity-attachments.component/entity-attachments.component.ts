import { Component, Input, OnInit } from "@angular/core";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { getErrorMessage } from "app/common/error";
import { deleteArrayElement } from "app/common/utils";
import { BehaviorSubject, map, of, switchMap, tap } from "rxjs";
import {
    EntityAttachmentMetadata,
    EntityAttachmentSensitivity,
} from "../entity-attachment-metadata";
import { EntityAttachmentService } from "../entity-attachment.service";
import { UploadStatus } from "../storage.service";

interface UploadingFile {
    file: File;
    progress?: number;
    error?: string;
}

function sensitiveTooltip(part?: string) {
    const inserted = part ? ` ${part}` : "";
    return `File can only be viewed by members with permission to edit${inserted}, click to change`;
}

function securedTooltip(part?: string) {
    const inserted = part ? ` ${part}` : "";
    return `File can be viewed by members with permission to read${inserted}, click to change`;
}

@Component({
    selector: "app-storage-entity-attachments",
    templateUrl: "./entity-attachments.component.html",
    styleUrls: ["./entity-attachments.component.scss"],
    standalone: false,
})
export class EntityAttachmentsComponent implements OnInit {
    @Input() public canEdit: boolean | null = false;
    @Input() public attachmentService!: EntityAttachmentService;
    @Input() public entityAreaText?: string;

    public attachments?: EntityAttachmentMetadata[] | undefined;
    public uploadingFiles: UploadingFile[] = [];

    private uploadsInProgress$ = new BehaviorSubject<UploadingFile[]>([]);

    public sensitivity: Record<EntityAttachmentSensitivity, { icon: string; tooltip: string }> = {
        [EntityAttachmentSensitivity.Secured]: {
            icon: "lock_open",
            tooltip: securedTooltip(),
        },
        [EntityAttachmentSensitivity.Sensitive]: {
            icon: "lock",
            tooltip: sensitiveTooltip(),
        },
    };

    public constructor(private dialogService: DialogService) {}

    public ngOnInit(): void {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (!this.attachmentService) {
            throw new Error("AttachmentService must be defined");
        }

        this.attachmentService.getMetadata().subscribe((attachments) => {
            this.attachments = attachments.map((a) => new EntityAttachmentMetadata(a));
        });

        this.sensitivity[EntityAttachmentSensitivity.Secured].tooltip = securedTooltip(
            this.entityAreaText,
        );
        this.sensitivity[EntityAttachmentSensitivity.Sensitive].tooltip = sensitiveTooltip(
            this.entityAreaText,
        );
    }

    public get uploadInProgress$() {
        return this.uploadsInProgress$.pipe(
            map((files) => files.filter((f) => !f.error).length > 0),
        );
    }

    public download = (blob?: EntityAttachmentMetadata) => {
        if (!blob) {
            return Promise.resolve();
        }

        return this.attachmentService.download(blob);
    };

    public removeAttachment = (attachment: EntityAttachmentMetadata | undefined) => {
        if (!attachment) {
            return of(undefined);
        }

        return this.dialogService
            .openConfirmation(`Are you sure you would like to delete "${attachment.name}"?`)
            .pipe(
                switchMap(() => this.attachmentService.delete(attachment)),
                tap(() => {
                    if (this.attachments) {
                        deleteArrayElement(this.attachments, attachment);
                    }
                }),
            );
    };

    public queueFileUpload(files: File[]) {
        for (const file of files) {
            const upload: UploadingFile = { file };
            this.uploadingFiles.push(upload);
            this.uploadsInProgress$.next(this.uploadingFiles);

            this.attachmentService.upload(file).subscribe({
                next: (event) => {
                    upload.progress = event.progress;

                    if (event.status === UploadStatus.Done) {
                        this.attachments?.push(new EntityAttachmentMetadata(event.result));
                        deleteArrayElement(this.uploadingFiles, upload);
                        this.uploadsInProgress$.next(this.uploadingFiles);
                    }
                },
                error: (error) => {
                    upload.error = getErrorMessage(
                        error,
                        "Unknown error uploading. Please try again.",
                    );
                },
            });
        }
    }

    public toggleSensitivity = (attachment?: EntityAttachmentMetadata) => {
        if (!attachment) {
            return of(undefined);
        }

        attachment.sensitivity =
            attachment.sensitivity === EntityAttachmentSensitivity.Secured
                ? EntityAttachmentSensitivity.Sensitive
                : EntityAttachmentSensitivity.Secured;
        return this.attachmentService.setSensitivity(attachment, attachment.sensitivity);
    };

    public retryUpload(upload: UploadingFile) {
        deleteArrayElement(this.uploadingFiles, upload);
        this.queueFileUpload([upload.file]);
    }

    public removeUpload(upload: UploadingFile) {
        deleteArrayElement(this.uploadingFiles, upload);
    }
}
