import { Component, Input, OnChanges } from "@angular/core";

const defaultIcon = "attach_file";

@Component({
    selector: "app-file-icon",
    template: ` <mat-icon>{{ icon }}</mat-icon> `,
    standalone: false,
})
export class FileIconComponent implements OnChanges {
    @Input() public mimeType?: string;

    public icon = defaultIcon;

    public ngOnChanges(): void {
        if (!this.mimeType) {
            this.icon = defaultIcon;
            return;
        }

        if (this.mimeType.startsWith("image/")) {
            this.icon = "insert_photo";
        } else if (this.mimeType.startsWith("audio/")) {
            this.icon = "audiotrack";
        } else if (this.mimeType.startsWith("video/")) {
            this.icon = "movie";
        } else if (this.mimeType.startsWith("text/")) {
            this.icon = "text_snippet";
        } else if (this.mimeType === "application/pdf") {
            this.icon = "picture_as_pdf";
        } else {
            this.icon = defaultIcon;
        }
    }
}
