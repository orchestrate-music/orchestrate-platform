import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import { MatNativeDateModule } from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { AggregateFormControlNameErrorsDirective } from "./aggregate-form-control-name-errors.directive";
import {
    BreezeFormControlErrorDirective,
    BreezeFormControlNameErrorDirective,
} from "./breeze-form-error.directive";
import { SelectMonthComponent } from "./select-month/select-month.component";
import { TagBoxComponent } from "./tag-box/tag-box.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        MatSelectModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatIconModule,
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatCheckboxModule,
        MatSelectModule,
        BreezeFormControlErrorDirective,
        BreezeFormControlNameErrorDirective,
        AggregateFormControlNameErrorsDirective,
        SelectMonthComponent,
        TagBoxComponent,
    ],
    declarations: [
        SelectMonthComponent,
        TagBoxComponent,
        BreezeFormControlErrorDirective,
        BreezeFormControlNameErrorDirective,
        AggregateFormControlNameErrorsDirective,
    ],
})
export class AppFormModule {}
