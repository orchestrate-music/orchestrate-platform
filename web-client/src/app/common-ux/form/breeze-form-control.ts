import { FormControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";
import { BreezeEntity } from "app/dal/breeze-entity";
import { fromBreezeEvent } from "app/dal/breeze-rxjs-utilities";
import { Observable } from "rxjs";
import { startWith, takeUntil } from "rxjs/operators";

type BreezeEntityConcreteProperties<T extends BreezeEntity> = Exclude<
    keyof T,
    "entityAspect" | "entityType"
>;

function entityValidatorAdapter<T extends BreezeEntity>(
    entity: T,
    field: BreezeEntityConcreteProperties<T>,
): ValidatorFn {
    return (_control) => {
        const validationErrors = entity.entityAspect.getValidationErrors(field as string);
        if (validationErrors.length === 0) {
            return null;
        }

        return validationErrors.reduce<ValidationErrors>((errors, nextError, index) => {
            const validationError = nextError.validator?.name ?? `breeze${index}`;
            errors[validationError] = nextError.errorMessage;
            return errors;
        }, {});
    };
}

export class BreezeFormControl<
    T extends BreezeEntity,
    K extends BreezeEntityConcreteProperties<T>,
> extends FormControl {
    public constructor(
        private entity: T,
        private field: K,
        destroy$: Observable<any>,
    ) {
        super(entity[field], entityValidatorAdapter<T>(entity, field));

        fromBreezeEvent(entity.entityAspect.validationErrorsChanged)
            .pipe(takeUntil(destroy$))
            .subscribe((data) => {
                const changedValidationErrors = data.added.concat(data.removed);
                if (changedValidationErrors.some((e) => e.isServerError)) {
                    this.updateValueAndValidity();
                }
            });
    }

    public get value$(): Observable<T[K]> {
        return this.valueChanges.pipe(startWith(this.value));
    }

    setValue(value: any, options?: any): void {
        this.entity[this.field] = value;
        super.setValue(value, options);
    }

    patchValue(value: any, options?: any): void {
        this.entity[this.field] = value;
        super.patchValue(value, options);
    }

    reset(value?: any, options?: any): void {
        const originalValues: Record<string, any> = this.entity.entityAspect.originalValues;
        if (Object.prototype.hasOwnProperty.call(originalValues, this.field)) {
            const originalValue = originalValues[this.field as string];
            this.entity[this.field] = originalValue;
            // eslint-disable-next-line @typescript-eslint/no-dynamic-delete
            delete originalValues[this.field as string];

            if (Object.getOwnPropertyNames(originalValues).length === 0) {
                this.entity.entityAspect.setUnchanged();
            }
        }

        super.reset(value, options);
    }
}

type BreezeFormGroupControls<
    T extends BreezeEntity,
    P extends readonly BreezeEntityConcreteProperties<T>[],
> = {
    [K in P[number]]: BreezeFormControl<T, K>;
};

function buildBreezeFormGroupControls<
    TEntity extends BreezeEntity,
    TProps extends readonly BreezeEntityConcreteProperties<TEntity>[],
>(
    entity: TEntity,
    props: TProps,
    destroy$: Observable<any>,
): BreezeFormGroupControls<TEntity, TProps> {
    return props.reduce(
        (group, prop) => {
            group[prop] = new BreezeFormControl(entity, prop, destroy$);
            return group;
        },
        {} as BreezeFormGroupControls<TEntity, TProps>,
    );
}

export class BreezeFormGroup<
    TEntity extends BreezeEntity,
    TProps extends readonly BreezeEntityConcreteProperties<TEntity>[],
> extends FormGroup<BreezeFormGroupControls<TEntity, TProps>> {
    public constructor(
        public readonly entity: TEntity,
        props: TProps,
        destroy$: Observable<any>,
    ) {
        // TODO Add validator for whole entity as provided by breeze
        super(buildBreezeFormGroupControls(entity, props, destroy$));
    }
}
