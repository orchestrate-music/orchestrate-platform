import { Component, EventEmitter, Input, Output } from "@angular/core";

const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];

@Component({
    selector: "app-select-month",
    template: `
        <mat-form-field subscriptSizing="dynamic">
            <mat-label *ngIf="label">{{ label }}</mat-label>
            <mat-select
                [ngModel]="month"
                (ngModelChange)="monthChange.emit($event)"
                [required]="required"
                [disabled]="disabled"
            >
                <mat-option
                    *ngFor="let month of months; let index = index"
                    [value]="index + 1"
                    [disabled]="(!!min && index + 1 < min) || (!!max && index + 1 > max)"
                >
                    {{ month }}
                </mat-option>
            </mat-select>
            <mat-hint *ngIf="hint">{{ hint }}</mat-hint>
        </mat-form-field>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
    standalone: false,
})
export class SelectMonthComponent {
    @Input() public month?: number;
    @Output() public monthChange = new EventEmitter<number>();

    @Input() public min?: number;
    @Input() public max?: number;
    @Input() label?: string;
    @Input() hint?: string;
    @Input() required = false;
    @Input() disabled = false;

    public months = months;
}
