import { CommonModule } from "@angular/common";
import { Component, HostBinding, Input, NgModule } from "@angular/core";

@Component({
    selector: "app-proportion-bar",
    template: ` <ng-content select="app-proportion"></ng-content> `,
    styles: [
        `
            :host {
                display: flex;
                border-radius: 4px;
                overflow: hidden; /* So border radius applies to children */
            }
        `,
    ],
    standalone: false,
})
export class ProportionBarComponent {}

@Component({
    selector: "app-proportion",
    template: ``,
    styles: [
        `
            :host {
                flex-basis: auto;
            }
        `,
    ],
    standalone: false,
})
export class ProportionComponent {
    @Input()
    @HostBinding("style.flex-grow")
    @HostBinding("style.flex-shrink")
    public proportion = 1;
}

@NgModule({
    imports: [
        CommonModule,
    ],
    exports: [
        ProportionBarComponent,
        ProportionComponent,
    ],
    declarations: [
        ProportionBarComponent,
        ProportionComponent,
    ],
})
export class AppProportionBarModule {}
