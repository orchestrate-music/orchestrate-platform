import { Component, Input } from "@angular/core";
import { BehaviorSubject, combineLatest, map, Observable } from "rxjs";
import { ResponsiveService } from "../responsive.service";

@Component({
    selector: "app-icon-text-pair",
    template: `
        <mat-icon
            [matTooltip]="text"
            [matTooltipDisabled]="!(showMobileTooltip && (isCollapsed$ | async))"
        >
            {{ icon }}
        </mat-icon>
        <span *ngIf="(isCollapsed$ | async) === false">
            {{ text }}
        </span>
    `,
    styles: [
        `
            :host {
                display: flex;
                align-items: center;
            }

            :host-context(app-badge) mat-icon {
                width: 1em;
                height: 1em;
                font-size: 1.2em;
            }

            mat-icon + span {
                margin-left: 0.25rem;
            }
        `,
    ],
    standalone: false,
})
export class IconTextPairComponent {
    @Input() public icon = "";
    @Input() public text = "";
    @Input() public showMobileTooltip = true;
    @Input() public set isCollapsible(value: boolean) {
        this.isCollapsible$.next(value);
    }

    public isCollapsed$: Observable<boolean>;
    private isCollapsible$ = new BehaviorSubject<boolean>(true);

    public constructor(responsiveService: ResponsiveService) {
        this.isCollapsed$ = combineLatest([
            this.isCollapsible$,
            responsiveService.isMobile$,
        ]).pipe(map(([isCollapsible, isMobile]) => isCollapsible && isMobile));
    }
}
