import { formatCurrency } from "@angular/common";
import { ColDef, ValueFormatterParams } from "ag-grid-community";
import { GridCurrencyCellEditorComponent } from "./grid-currency-cell-editor";
import { columnExportColDef } from "./grid-export-options";

export function currencyColDef<T>(): ColDef<T> {
    // TODO Get current currency some how instead of hard-coding
    return {
        type: "rightAligned",
        valueFormatter: (params: ValueFormatterParams) =>
            params.value ? formatCurrency(params.value, navigator.language, "$") : "",
        cellEditor: GridCurrencyCellEditorComponent,
        ...columnExportColDef({
            preprocessCellValue: (params) => formatCurrency(params.value, navigator.language, "$"),
        }),
    };
}
