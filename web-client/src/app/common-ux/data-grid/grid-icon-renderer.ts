import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";

export abstract class GridIconRenderer<T> implements ICellRendererComp {
    private gui: HTMLSpanElement;

    public constructor() {
        this.gui = document.createElement("span");
        this.gui.classList.add("material-icons");
        this.gui.style.verticalAlign = "bottom";
        this.gui.style.lineHeight = "inherit";
        this.gui.style.opacity = "0.6";
        this.gui.style.cursor = "help";
    }

    public init(params: ICellRendererParams): void {
        this.updateData(params.value);
    }

    public refresh(params: ICellRendererParams): boolean {
        this.updateData(params.value);
        return true;
    }

    public getGui() {
        return this.gui;
    }

    private updateData(value: T) {
        this.gui.innerText = this.icon(value);
        this.gui.title = this.tooltip(value);
    }

    protected abstract icon(value: T): string;
    protected abstract tooltip(value: T): string;
}
