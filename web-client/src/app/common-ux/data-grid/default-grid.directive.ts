import { Directive, HostBinding, OnDestroy, Optional } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { ColumnState } from "ag-grid-community";
import { Subscription } from "rxjs";
import { delay, switchMap } from "rxjs/operators";
import { ResponsiveService } from "../responsive.service";
import { DataGridComponent } from "./data-grid.component";

@Directive({
    selector: "ag-grid-angular",
    standalone: false,
})
export class DefaultGridDirective<T> implements OnDestroy {
    @HostBinding("class.ag-theme-material") public readonly isMaterialTheme = true;

    private mobileHandlingSubscription: Subscription;
    private originalPinStates: ColumnState[] = [];

    public constructor(
        agGrid: AgGridAngular,
        responsiveService: ResponsiveService,
        @Optional() dataGrid?: DataGridComponent<T>,
    ) {
        agGrid.accentedSort = true;
        agGrid.animateRows = true;
        agGrid.suppressMovableColumns = true;
        agGrid.enableCellTextSelection = true;

        if (dataGrid) {
            agGrid.isExternalFilterPresent = dataGrid.isExternalFilterPresent;
            agGrid.doesExternalFilterPass = dataGrid.filterRow;
        }

        this.mobileHandlingSubscription = agGrid.gridReady
            .pipe(
                // Need delay so api is defined below
                delay(0),
                switchMap(() => responsiveService.isMobile$),
            )
            .subscribe((isMobile) => {
                if (isMobile) {
                    this.originalPinStates = agGrid.api.getColumnState().map((c) => ({
                        colId: c.colId,
                        pinned: c.pinned,
                    }));
                    agGrid.api.applyColumnState({
                        defaultState: {
                            pinned: null,
                        },
                    });
                } else {
                    agGrid.api.applyColumnState({
                        state: this.originalPinStates,
                    });
                    this.originalPinStates = [];
                }
            });
    }

    public ngOnDestroy() {
        this.mobileHandlingSubscription.unsubscribe();
    }
}
