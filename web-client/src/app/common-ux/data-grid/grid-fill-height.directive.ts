import { Directive, ElementRef, HostBinding, Input, OnDestroy, OnInit } from "@angular/core";
import { Subscription, delay, distinctUntilChanged, fromEvent, map, startWith } from "rxjs";

@Directive({
    standalone: true,
    selector: "ag-grid-angular[appGridFillHeight]",
})
export class GridFillHeightDirective implements OnInit, OnDestroy {
    @HostBinding("style.height") public gridHeight?: string;
    @Input() public additionalBottomOffset = 0;
    @Input() @HostBinding("style.min-height") public minHeight?: string;

    private subscription?: Subscription;

    public constructor(private elementRef: ElementRef<HTMLElement>) {}

    public ngOnInit() {
        this.subscription = fromEvent(window, "resize")
            .pipe(
                startWith({}),
                delay(0), // Ensure ag-grid has time to initialise
                map(() => {
                    const containerElement =
                        this.elementRef.nativeElement.closest(".grid-container");
                    return containerElement ?? this.elementRef.nativeElement;
                }),
                map((element) => {
                    const distanceFromTop = element.getBoundingClientRect().top;
                    // Give some space at the bottom of the screen
                    const otherHeight = distanceFromTop + 16 + this.additionalBottomOffset;
                    return otherHeight;
                }),
                distinctUntilChanged(),
            )
            .subscribe((otherHeight) => {
                this.gridHeight = `calc(100vh - ${otherHeight}px)`;
            });
    }

    public ngOnDestroy(): void {
        this.subscription?.unsubscribe();
    }
}
