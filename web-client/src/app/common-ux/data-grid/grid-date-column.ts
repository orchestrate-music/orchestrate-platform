import { formatDate } from "@angular/common";
import { ColDef } from "ag-grid-community";

export function dateColDef<T>(): ColDef<T> {
    return {
        type: "rightAligned",
        filter: "agDateColumnFilter",
        valueFormatter: (params) =>
            params.value ? formatDate(params.value, "dd/MM/yyyy", navigator.language) : "",
    };
}
