import {
    ColDef,
    ProcessCellForExportParams,
    ProcessHeaderForExportParams,
} from "ag-grid-community";

export interface GridExportOptions<T> {
    skipColumn?: boolean;
    processHeaderCallback?: (params: ProcessHeaderForExportParams<T>) => string;
    preprocessCellValue?: (params: ProcessCellForExportParams<T>) => string;
}

export function columnExportColDef<T>(options: GridExportOptions<T>): ColDef<T> {
    return { context: options } as ColDef<T>;
}

export function getColumnExportOptions<T>(column: ColDef<T>) {
    return column.context as GridExportOptions<T> | undefined;
}
