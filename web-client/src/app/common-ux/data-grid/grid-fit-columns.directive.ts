import { Directive, OnDestroy } from "@angular/core";
import { AgGridAngular } from "ag-grid-angular";
import { combineLatest, interval, Subscription } from "rxjs";

@Directive({
    selector: "ag-grid-angular[appGridFitColumns]",
    standalone: false,
})
export class GridFitColumnsDirective implements OnDestroy {
    private subscription: Subscription;

    public constructor(agGrid: AgGridAngular) {
        this.subscription = combineLatest([
            agGrid.gridReady,
            interval(500),
        ]).subscribe(() => {
            agGrid.api.sizeColumnsToFit();
        });
    }

    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
