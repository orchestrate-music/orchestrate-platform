import { animate, AnimationEvent, state, style, transition, trigger } from "@angular/animations";
import { Overlay, OverlayRef } from "@angular/cdk/overlay";
import { TemplatePortal } from "@angular/cdk/portal";
import {
    AfterViewInit,
    Component,
    ElementRef,
    Input,
    OnDestroy,
    TemplateRef,
    ViewChild,
    ViewContainerRef,
} from "@angular/core";
import { BehaviorSubject, combineLatest, map, Observable } from "rxjs";
import { ResponsiveService } from "../responsive.service";

@Component({
    selector: "app-mobile-collapsible-content",
    templateUrl: "./mobile-collapsible-content.component.html",
    styleUrls: ["./mobile-collapsible-content.component.scss"],
    animations: [
        // Copy of mat-menu: https://github.com/angular/components/blob/main/src/material/menu/menu-animations.ts
        trigger("transformMenu", [
            state(
                "void",
                style({
                    opacity: 0,
                    transform: "scale(0.8)",
                }),
            ),
            transition(
                "void => enter",
                animate(
                    "120ms cubic-bezier(0, 0, 0.2, 1)",
                    style({
                        opacity: 1,
                        transform: "scale(1)",
                    }),
                ),
            ),
            transition("* => void", animate("100ms 25ms linear", style({ opacity: 0 }))),
        ]),
    ],
    standalone: false,
})
export class MobileCollapsibleContentComponent implements AfterViewInit, OnDestroy {
    @Input() collapsedColour?: "primary" | "accent" | "warn";
    @Input() collapsedIcon = "more_vert";
    @Input() collapsedTooltip = "Show content";
    @Input() set allowMobileCollapse(value: boolean) {
        this.allowMobileCollapse$.next(value);
    }

    @ViewChild("overlayTrigger", { read: ElementRef })
    public overlayTrigger?: ElementRef<HTMLButtonElement>;
    @ViewChild("overlayTemplate") public overlayTemplate!: TemplateRef<any>;

    public isCollapsed$: Observable<boolean>;
    public allowMobileCollapse$ = new BehaviorSubject(true);
    public animationState: "void" | "enter" = "enter";

    private overlayRef?: OverlayRef;

    public constructor(
        private overlay: Overlay,
        private viewContainerRef: ViewContainerRef,
        responsiveService: ResponsiveService,
    ) {
        this.isCollapsed$ = combineLatest([
            this.allowMobileCollapse$,
            responsiveService.isMobile$,
        ]).pipe(map(([allowed, isMobile]) => allowed && isMobile));
    }

    public ngAfterViewInit(): void {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (!this.overlayTemplate) {
            throw new Error("expected");
        }
    }

    public ngOnDestroy(): void {
        this.overlayRef?.dispose();
    }

    public openOverlay() {
        if (!this.overlayTrigger) {
            return;
        }

        const position = this.overlay
            .position()
            .flexibleConnectedTo(this.overlayTrigger)
            .withPositions([
                {
                    originX: "end",
                    originY: "bottom",
                    overlayX: "end",
                    overlayY: "top",
                },
            ]);
        this.overlayRef = this.overlay.create({
            positionStrategy: position,
            scrollStrategy: this.overlay.scrollStrategies.block(),
            hasBackdrop: true,
            backdropClass: "cdk-overlay-transparent-backdrop",
        });

        const contentPortal = new TemplatePortal(this.overlayTemplate, this.viewContainerRef);
        this.overlayRef.attach(contentPortal);
        this.animationState = "enter";

        this.overlayRef.backdropClick().subscribe(() => {
            this.closeOverlay();
        });
    }

    public closeOverlay() {
        this.animationState = "void";
    }

    public animationStart(event: AnimationEvent) {
        if (event.fromState !== "void" && event.toState === "void") {
            this.overlayRef?.detachBackdrop();
        }
    }

    public animationDone(event: AnimationEvent) {
        if (event.fromState !== "void" && event.toState === "void") {
            this.overlayRef?.dispose();
        }
    }
}
