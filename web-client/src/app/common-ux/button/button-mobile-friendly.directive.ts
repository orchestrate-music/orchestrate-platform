import {
    AfterViewInit,
    ContentChild,
    Directive,
    ElementRef,
    Input,
    OnDestroy,
    Optional,
} from "@angular/core";
import { MatAnchor, MatButton } from "@angular/material/button";
import { BehaviorSubject, combineLatest, Subscription } from "rxjs";
import { IconTextPairComponent } from "../icon-text-pair/icon-text-pair.component";
import { ResponsiveService } from "../responsive.service";

const matButtonAttributes = [
    "mat-button",
    "mat-raised-button",
    "mat-icon-button",
    "mat-fab",
    "mat-mini-fab",
    "mat-stroked-button",
    "mat-flat-button",
];

@Directive({
    selector: `button[mat-button][mobileFriendly], button[mat-raised-button][mobileFriendly], button[mat-icon-button][mobileFriendly],
               button[mat-fab][mobileFriendly], button[mat-mini-fab][mobileFriendly], button[mat-stroked-button][mobileFriendly],
               button[mat-flat-button][mobileFriendly],
               a[mat-button][mobileFriendly], a[mat-raised-button][mobileFriendly], a[mat-icon-button][mobileFriendly],
               a[mat-fab][mobileFriendly], a[mat-mini-fab][mobileFriendly], a[mat-stroked-button][mobileFriendly],
               a[mat-flat-button][mobileFriendly]`,
    standalone: false,
})
export class MatButtonMobileFriendlyDirective implements AfterViewInit, OnDestroy {
    // eslint-disable-next-line @angular-eslint/no-input-rename
    @Input("mobileFriendly") public set isEnabled(value: boolean | "") {
        if (value === "") {
            value = true;
        }

        this.isEnabled$.next(value);
    }
    @ContentChild(IconTextPairComponent) public iconTextPairComponent!: IconTextPairComponent;

    private isEnabled$ = new BehaviorSubject<boolean>(true);
    private subscription: Subscription;

    public constructor(
        @Optional() matButton: MatButton | null,
        @Optional() matAnchor: MatAnchor | null,
        elementRef: ElementRef<HTMLElement>,
        responsiveService: ResponsiveService,
    ) {
        this.subscription = combineLatest([
            this.isEnabled$,
            responsiveService.isMobile$,
        ]).subscribe(([isEnabled, isMobile]) => {
            const button = matButton ?? matAnchor;
            if (!button) {
                return;
            }

            const buttonType = matButtonAttributes.find((a) =>
                elementRef.nativeElement.hasAttribute(a),
            );
            if (!buttonType) {
                return;
            }

            // We're relying on the fact that only the class is changed from each button type
            // https://github.com/angular/components/blob/main/src/material/button/button.ts#L105
            // Dynamic type requested here: https://github.com/angular/components/issues/15367
            const buttonClasses = elementRef.nativeElement.classList;
            if (isEnabled && isMobile) {
                buttonClasses.remove(buttonType.replace("mat-", "mat-mdc-"), "mdc-button");
                buttonClasses.add("mat-mdc-icon-button", "mdc-icon-button", "is-mobile");
            } else {
                buttonClasses.remove("mat-mdc-icon-button", "mdc-icon-button", "is-mobile");
                buttonClasses.add(buttonType.replace("mat-", "mat-mdc-"), "mdc-button");
            }
        });
    }

    public ngAfterViewInit(): void {
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        if (!this.iconTextPairComponent) {
            throw new Error("Expected button to have IconTextPair");
        }
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
