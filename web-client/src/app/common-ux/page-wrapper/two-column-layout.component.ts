import { Component, Optional } from "@angular/core";
import { PageWrapperComponent } from "./page-wrapper.component";

@Component({
    selector: "app-two-column-layout",
    template: `
        <div class="two-column-container">
            <div class="column column-left">
                <ng-content select="[left-column]"></ng-content>
            </div>
            <div class="column column-right">
                <ng-content select="[right-column]"></ng-content>
            </div>
        </div>
    `,
    styles: [
        `
            .two-column-container {
                display: flex;
                flex-direction: column;
            }

            .column {
                padding: 1rem;
            }

            /* TODO remove this hard coded media query */
            @media (min-width: 992px) {
                .two-column-container {
                    height: 100%;
                    flex-direction: row;
                }

                .column {
                    height: 100%;
                    overflow: auto;
                }

                .column-left {
                    width: 41.66%;
                }

                .column-right {
                    width: 58.34%;
                }
            }

            @media (min-width: 1200px) {
                .column-left {
                    width: 33.33%;
                }

                .column-right {
                    width: 66.67%;
                }
            }
        `,
    ],
    standalone: false,
})
export class TwoColumnLayoutComponent {
    public constructor(@Optional() pageWrapperComponent?: PageWrapperComponent) {
        pageWrapperComponent?.removeDefaultContainer();
    }
}
