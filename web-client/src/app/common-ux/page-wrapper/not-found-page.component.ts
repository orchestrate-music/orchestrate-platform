import { Component } from "@angular/core";

@Component({
    selector: "app-not-found-page",
    template: `
        <app-page-wrapper [hideDefaultToolbar]="true">
            <div class="not-found-container">
                <mat-card>
                    <mat-card-header>
                        <mat-card-title>Not found</mat-card-title>
                    </mat-card-header>
                    <img
                        mat-card-image
                        src="assets/not_found.png"
                        alt="broken vinyl record"
                    />
                    <ng-content></ng-content>
                </mat-card>
            </div>
        </app-page-wrapper>
    `,
    styles: [
        `
            .not-found-container {
                display: flex;
                justify-content: space-around;
            }

            mat-card {
                max-width: 600px;
            }
        `,
    ],
    standalone: false,
})
export class NotFoundPageComponent {}
