import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseDialog } from "../dialog/base-dialog";

@Component({
    template: `
        <h2 mat-dialog-title>Error</h2>
        <mat-dialog-content>
            <app-save-error [saveError]="error"></app-save-error>
        </mat-dialog-content>
        <mat-dialog-actions>
            <button
                mat-button
                (click)="resolve()"
            >
                Close
            </button>
        </mat-dialog-actions>
    `,
    standalone: false,
})
export class SaveErrorDialogComponent extends BaseDialog<Error, void> {
    public readonly dialogName = "SaveError";

    public constructor(
        dialogRef: MatDialogRef<SaveErrorDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public error: Error,
    ) {
        super(dialogRef);
    }
}
