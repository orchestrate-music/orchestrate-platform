import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatIconModule } from "@angular/material/icon";
import { AppButtonModule } from "../button/button.module";
import { SaveCancelButtonsComponent } from "./save-cancel-buttons.component";
import { SaveCancelFormButtonsComponent } from "./save-cancel-form-buttons.component";
import { SaveErrorDialogComponent } from "./save-error-dialog.component";
import { SaveErrorComponent } from "./save-error.component";

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatDialogModule,
        AppButtonModule,
        SaveErrorComponent,
    ],
    exports: [
        SaveCancelButtonsComponent,
        SaveCancelFormButtonsComponent,
        SaveErrorComponent,
    ],
    declarations: [
        SaveCancelButtonsComponent,
        SaveCancelFormButtonsComponent,
        SaveErrorDialogComponent,
    ],
})
export class AppSaveCancelButtonsModule {}
