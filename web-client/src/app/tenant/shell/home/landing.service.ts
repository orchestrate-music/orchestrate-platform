import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { FeeClass } from "app/dal/model/member";
import { MemberBillingPeriodState } from "app/dal/model/member-billing-period";
import { Observable, switchMap } from "rxjs";

export interface LandingSummaryDto {
    personal?: {
        memberId: number;
        details: {
            name: string;
            email?: string;
            address?: string;
            phoneNo?: string;
            feeClass: FeeClass;
            instruments: string[];
        };
        currentMembership: {
            ensembleId: number;
            ensembleName: string;
            dateJoined: Date;
        }[];
        totalPerformanceCount: number;
        performanceHistory: PerformanceHistoryDto[];
        lastInvoice?: InvoiceSummaryDto;
        unpaidInvoices: InvoiceSummaryDto[];
        loanedAssets: {
            assetDescription: string;
            serialNo?: string;
            dateBorrowed: Date;
        }[];
        mailingLists: {
            id: number;
            name: string;
            address: string;
        }[];
    };
    tenant: {
        details: {
            name: string;
            website?: string;
            dateFounded?: Date;
        };
        assets?: {
            count: number;
            onLoanCount: number;
        };
        musicLibrary?: {
            count: number;
            neverPlayedCount: number;
            performedInLastYearCount: number;
        };
        contacts?: {
            currentCount: number;
        };
        ensembleSummaries: {
            id: number;
            name: string;
            memberCount: number;
            firstConcertDate?: Date;
            concertCount: number;
        }[];
        membershipSnapshot?: {
            joinedCount: number;
            stayedCount: number;
            leftCount: number;
            joinedAndLeftCount: number;
        };
        lastConcert?: {
            id: number;
            occasion: string;
            location?: string;
            date: Date;
            coverPhotoBlobId?: string;
            performances: {
                id: number;
                ensembleName: string;
                memberCount: number;
                pieceCount: number;
            }[];
        };
        latestBillingPeriods: {
            id: number;
            name: string;
            due: Date;
            state: MemberBillingPeriodState;
            dollarsReceived: number;
            invoiceCount: number;
            paidEarly: number;
            paidOnTime: number;
            paidLate: number;
            unpaid: number;
        }[];
        mailingListUsage: {
            id: number;
            name: string;
            usageCount: number;
        }[];
    };
}

export interface PerformanceHistoryDto {
    year: number;
    ensembleName: string;
    performanceCount: number;
}

export interface InvoiceSummaryDto {
    reference: string;
    billingPeriodName: string;
    billingPeriodState: MemberBillingPeriodState;
    total: number;
    earlyBirdDue?: Date;
    due: Date;
    paid?: Date;
}

@Injectable()
export class LandingService {
    public constructor(
        private httpClient: HttpClient,
        private appConfig: AppConfig,
    ) {}

    public getLandingSummary(): Observable<LandingSummaryDto> {
        return this.appConfig
            .serverEndpoint(`landing/summary`)
            .pipe(switchMap((url) => this.httpClient.get<LandingSummaryDto>(url)));
    }
}
