import { inject } from "@angular/core";
import { CanActivateChildFn } from "@angular/router";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { isOnPrimaryRouteTree } from "app/common/routing-utils";
import { BreezeService } from "app/dal/breeze.service";
import { of, switchMap } from "rxjs";
import { UnsavedChangesDialogComponent } from "./unsaved-changes-dialog/unsaved-changes-dialog.component";

export const unsavedChangesGuard: CanActivateChildFn = (route, state) => {
    const breezeService = inject(BreezeService);
    const dialogService = inject(DialogService);

    if (!isOnPrimaryRouteTree(route)) {
        return true;
    }

    return breezeService.getChanges().pipe(
        switchMap((changes) => {
            if (changes.length === 0) {
                return of(true);
            }

            return dialogService.open(UnsavedChangesDialogComponent, changes);
        }),
    );
};
