import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { BreezeEntity } from "app/dal/breeze-entity";
import { BreezeService } from "app/dal/breeze.service";
import { tap } from "rxjs";

@Component({
    templateUrl: "./unsaved-changes-dialog.component.html",
    standalone: false,
})
export class UnsavedChangesDialogComponent extends BaseDialog<BreezeEntity[], boolean> {
    public readonly dialogName = "UnsavedChanges";

    public constructor(
        dialogRef: MatDialogRef<UnsavedChangesDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public changes: BreezeEntity[],
        private breezeService: BreezeService,
    ) {
        super(dialogRef);
    }

    public saveChanges = () => {
        return this.breezeService.saveChanges(...this.changes).pipe(tap(() => this.resolve(true)));
    };

    public cancelChanges = () => {
        return this.breezeService
            .cancelChanges(...this.changes)
            .pipe(tap(() => this.resolve(true)));
    };

    public stayHere() {
        this.resolve(false);
    }
}
