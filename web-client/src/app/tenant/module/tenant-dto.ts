export interface TenantDto {
    tenantId: string;
    name: string;
    abbreviation: string;
    logo?: string;
}
