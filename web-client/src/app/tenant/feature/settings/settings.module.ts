import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatTabsModule } from "@angular/material/tabs";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppRenderComponentModule } from "app/common-ux/render-component/render-component.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { GeneralSettingsComponent } from "./general-settings/general-settings.component";
import { MemberBillingSettingsComponent } from "./member-billing-settings/member-billing-settings.component";
import { SettingsPageComponent } from "./settings-page/settings-page.component";
import { AppConfigRoutingModule } from "./settings-routing.module";
import { SubscriptionSettingsComponent } from "./subscription-settings/subscription-settings.component";

@NgModule({
    imports: [
        CommonModule,

        MatTabsModule,
        MatCardModule,
        MatDividerModule,
        MatTooltipModule,
        MatExpansionModule,
        MatButtonToggleModule,

        AppConfigRoutingModule,
        AppPageWrapperModule,
        AppFormModule,
        AppSaveCancelButtonsModule,
        AppRenderComponentModule,
        AppLoadingModule,
        AppButtonModule,
    ],
    declarations: [
        SettingsPageComponent,
        GeneralSettingsComponent,
        SubscriptionSettingsComponent,
        MemberBillingSettingsComponent,
    ],
})
export class AppSettingsModule {}
