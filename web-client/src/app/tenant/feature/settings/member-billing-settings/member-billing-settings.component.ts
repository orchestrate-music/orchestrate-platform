import { Component, OnDestroy, OnInit } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { cacheLatest } from "app/common/rxjs-utilities";
import { padInteger } from "app/common/utils";
import { Ensemble } from "app/dal/model/ensemble";
import { MemberBillingConfig } from "app/dal/model/member-billing-config";
import { MemberBillingPeriodConfig } from "app/dal/model/member-billing-period-config";
import { HelpablePage, HelpBrokerService, HelpDataHooks } from "app/help/help-broker.service";
import { BehaviorSubject, EMPTY, Observable, of } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { SettingsService } from "../setting.service";
import { SettingsComponent } from "../settings-component.interface";
import { memberBillingSettingsPageId } from "../settings-consts";

@Component({
    selector: "app-member-billing-settings",
    templateUrl: "./member-billing-settings.component.html",
    styleUrls: ["./member-billing-settings.component.scss"],
    standalone: false,
})
export class MemberBillingSettingsComponent
    implements SettingsComponent, HelpablePage, OnInit, OnDestroy
{
    public pageId = memberBillingSettingsPageId;
    public memberBillingConfig$: Observable<MemberBillingConfig | undefined>;
    public billingPeriodConfigs$: Observable<MemberBillingPeriodConfig[]>;
    public billedEnsembles$: Observable<Ensemble[]>;
    public freeEnsembles$: Observable<Ensemble[]>;
    public hasUnclosedBillingPeriods$: Observable<boolean>;
    public dataHooks?: HelpDataHooks;

    public exampleBillingPeriodName$: Observable<string>;
    public refreshData$ = new BehaviorSubject<void>(undefined);
    private createdBillingConfig?: MemberBillingConfig;
    private modifiedBillingPeriodConfigs = new Set<MemberBillingPeriodConfig>();

    public constructor(
        private settingsService: SettingsService,
        private helpBroker: HelpBrokerService,
    ) {
        this.memberBillingConfig$ = this.refreshData$.pipe(
            switchMap(() =>
                this.createdBillingConfig?.entityAspect.entityState.isAdded()
                    ? of(this.createdBillingConfig)
                    : settingsService.getMemberBillingConfig(),
            ),
            cacheLatest(),
        );
        this.billingPeriodConfigs$ = this.refreshData$.pipe(
            switchMap(() => this.getBillingPeriodConfigs()),
            cacheLatest(),
        );

        const ensembles$ = this.refreshData$.pipe(
            switchMap(() => settingsService.getEnsembles()),
            map((ensembles) => ensembles.sort((a, b) => a.name.localeCompare(b.name))),
            cacheLatest(),
        );
        this.billedEnsembles$ = ensembles$.pipe(
            map((ensembles) => ensembles.filter((e) => e.memberBillingPeriodFeeDollars > 0)),
        );
        this.freeEnsembles$ = ensembles$.pipe(
            map((ensembles) => ensembles.filter((e) => e.memberBillingPeriodFeeDollars === 0)),
        );

        this.hasUnclosedBillingPeriods$ = this.refreshData$.pipe(
            switchMap(() => settingsService.hasUnclosedBillingPeriods()),
            cacheLatest(),
        );

        this.exampleBillingPeriodName$ = this.billingPeriodConfigs$.pipe(
            map((configs) => {
                return configs[0]
                    ? `${new Date().getFullYear()} - ${configs[0].name}`
                    : `${new Date().getFullYear()} - Semester 1`;
            }),
        );

        this.dataHooks = {
            addDataPermission: FeaturePermission.MemberBillingWrite,
            hasNoData$: this.memberBillingConfig$.pipe(map((config) => !config)),
            addData: () => this.createBillingConfig(),
        };
    }

    public ngOnInit() {
        this.helpBroker.addHelpablePage(this);
    }

    public ngOnDestroy() {
        this.helpBroker.removeHelpablePage(this);
    }

    private getBillingPeriodConfigs() {
        return this.settingsService.getMemberBillingPeriodConfigs().pipe(
            map((configs) => {
                const addedConfigs = Array.from(this.modifiedBillingPeriodConfigs).filter((e) =>
                    e.entityAspect.entityState.isAdded(),
                );
                configs.push(...addedConfigs);
                return configs.sort((a, b) => a.endOfPeriodMonth - b.endOfPeriodMonth);
            }),
        );
    }

    public nextInvoiceReference(billingConfig: MemberBillingConfig) {
        return `${billingConfig.invoicePrefix ?? ""}-${padInteger(
            billingConfig.nextInvoiceNumber,
            6,
        )}`;
    }

    public getAutoGenerateMinMonth(billingConfigs: MemberBillingPeriodConfig[], index: number) {
        if (index <= 0 || index >= billingConfigs.length) {
            return 1;
        } else {
            return billingConfigs[index - 1]!.endOfPeriodMonth + 1;
        }
    }

    public getEndOfPeriodMaxMonth(billingConfigs: MemberBillingPeriodConfig[], index: number) {
        if (index < 0 || index === billingConfigs.length - 1) {
            return 12;
        } else {
            return billingConfigs[index + 1]!.autoGenerateMonth - 1;
        }
    }

    public updateBillingPeriods(configs: MemberBillingPeriodConfig[], yearlyBillFrequency: number) {
        this.createOrDeleteBillingPeriodsAsNecessary(configs, yearlyBillFrequency)
            .pipe(
                tap((modifiedConfigs) => {
                    modifiedConfigs.forEach((c) => this.modifiedBillingPeriodConfigs.add(c));
                }),
                switchMap(() => this.getBillingPeriodConfigs()),
            )
            .subscribe((currentConfigs) => {
                if (currentConfigs.length !== yearlyBillFrequency) {
                    throw new Error(
                        "Expected count of billing periods to be equal to what was just set",
                    );
                }

                const periodLengthMonths = Math.floor(12 / yearlyBillFrequency);
                for (let i = 1; i <= currentConfigs.length; i++) {
                    const idx = i - 1;
                    currentConfigs[idx]!.name = `Billing Period ${i}`;
                    // Generate halfway through the current period
                    currentConfigs[idx]!.autoGenerateMonth =
                        periodLengthMonths * i - Math.ceil(periodLengthMonths / 2);
                    currentConfigs[idx]!.endOfPeriodMonth = periodLengthMonths * i;
                }

                this.refreshData();
            });
    }

    private createOrDeleteBillingPeriodsAsNecessary(
        configs: MemberBillingPeriodConfig[],
        yearlyBillFrequency: number,
    ) {
        if (configs.length < yearlyBillFrequency) {
            const numToCreate = yearlyBillFrequency - configs.length;
            return this.settingsService.createBillingPeriodConfigs(numToCreate).pipe();
        } else if (configs.length > yearlyBillFrequency) {
            const numToDelete = configs.length - yearlyBillFrequency;
            const configsToDelete = configs.slice(-numToDelete);
            configsToDelete.forEach((c) => {
                c.entityAspect.setDeleted();
            });
            return of(configsToDelete);
        } else {
            return EMPTY;
        }
    }

    public createBillingConfig() {
        this.settingsService.createMemberBillingConfig().subscribe((config) => {
            this.createdBillingConfig = config;
            this.updateBillingPeriods([], 2);
        });
    }

    public refreshData() {
        this.refreshData$.next();
    }
}
