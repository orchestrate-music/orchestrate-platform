import { Injectable } from "@angular/core";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import { Concert } from "app/dal/model/concert";
import {
    Performance,
    PerformanceBreezeModel,
    PerformanceMemberBreezeModel,
    PerformanceScoreBreezeModel,
} from "app/dal/model/performance";
import { forkJoin, of } from "rxjs";
import { map, switchMap } from "rxjs/operators";

@Injectable()
export class PerformanceService extends BreezeEntityService<Performance> {
    protected readonly breezeModel = PerformanceBreezeModel;

    public create(concert: Concert) {
        return this.breezeService.create(PerformanceBreezeModel, {
            concert,
        });
    }

    public getByIdWithMembersAndScores(performanceId: number) {
        const query = this.breezeService
            .createQuery(PerformanceBreezeModel)
            .where("id", "==", performanceId)
            .expand("performanceMembers.member, performanceScores.score");
        return this.breezeService.executeQuery<Performance>(query).pipe(map((e) => e[0]));
    }

    public getByConcertIdWithEnsemble(concertId: number) {
        const query = this.breezeService
            .createQuery(PerformanceBreezeModel)
            .where("concertId", "==", concertId)
            .expand("ensemble");
        return this.breezeService.executeQuery<Performance>(query);
    }

    public createPerformance(
        initialValues: Partial<Performance>,
        memberIds: number[],
        scoreIds: number[],
    ) {
        return this.breezeService.create(PerformanceBreezeModel, initialValues).pipe(
            switchMap((performance) => {
                return forkJoin([
                    of(performance),
                    ...memberIds.map((m) =>
                        this.breezeService.create(PerformanceMemberBreezeModel, {
                            performance,
                            memberId: m,
                        }),
                    ),
                    ...scoreIds.map((s) =>
                        this.breezeService.create(PerformanceScoreBreezeModel, {
                            performance,
                            scoreId: s,
                        }),
                    ),
                ]);
            }),
        );
    }

    public createPerformanceMember(performance: Performance, memberId: number) {
        return this.breezeService.create(PerformanceMemberBreezeModel, { performance, memberId });
    }

    public createPerformanceScore(performance: Performance, scoreId: number) {
        return this.breezeService.create(PerformanceScoreBreezeModel, { performance, scoreId });
    }
}
