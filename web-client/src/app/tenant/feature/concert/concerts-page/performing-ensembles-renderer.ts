import { ActivatedRoute, Router } from "@angular/router";
import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";
import { Concert } from "app/dal/model/concert";
import { fromEvent, Subscription, tap, switchMap } from "rxjs";
import { isLinkRendererContext } from "./link-renderer-context";

export class PerformingEnsemblesRenderer implements ICellRendererComp {
    private gui = document.createElement("div");
    private router!: Router;
    private route!: ActivatedRoute;
    private subscriptions: Subscription[] = [];

    public init(params: ICellRendererParams) {
        if (!isLinkRendererContext(params.context)) {
            throw new Error("Invalid context passed to PerformingEnsemblesRenderer");
        }

        this.router = params.context.router;
        this.route = params.context.route;
        this.setUrl(params.data);
    }

    public refresh(params: ICellRendererParams) {
        this.setUrl(params.data);
        return true;
    }

    private setUrl(concert: Concert) {
        this.clearSubscriptions();
        this.gui.innerHTML = "";

        for (const performance of concert.performance) {
            const anchor = document.createElement("a");
            anchor.innerText = performance.ensemble?.name ?? "<<Not Loaded>>";
            const urlTree = this.router.createUrlTree(
                [concert.id, { focussedPerformance: performance.id }],
                { relativeTo: this.route },
            );
            anchor.href = this.router.serializeUrl(urlTree);

            this.subscriptions.push(
                fromEvent(anchor, "click")
                    .pipe(
                        // Need to do this so that we don't get a refresh on the link click
                        // (navigation has to be done by Angular Router)
                        tap((e) => e.preventDefault()),
                        switchMap(async () => {
                            const url = new URL(anchor.href);
                            await this.router.navigateByUrl(url.pathname);
                        }),
                    )
                    .subscribe(),
            );

            this.gui.append(anchor, document.createElement("br"));
        }
    }

    public getGui() {
        return this.gui;
    }

    public destroy() {
        this.clearSubscriptions();
    }

    private clearSubscriptions() {
        for (const sub of this.subscriptions) {
            sub.unsubscribe();
        }

        this.subscriptions = [];
    }
}
