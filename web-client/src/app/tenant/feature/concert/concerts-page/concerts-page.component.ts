import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ColDef, ValueGetterParams } from "ag-grid-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { cacheLatest } from "app/common/rxjs-utilities";
import { Concert } from "app/dal/model/concert";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { Observable, map, switchMap } from "rxjs";
import { concertsPageId } from "../concert-consts";
import { ConcertService } from "../concert.service";
import { CreateConcertDialogComponent } from "../create-concert-dialog/create-concert-dialog.component";
import { ConcertLinkRenderer } from "./concert-link-renderer";
import { PerformingEnsemblesRenderer } from "./performing-ensembles-renderer";

@Component({
    templateUrl: "./concerts-page.component.html",
    standalone: false,
})
export class ConcertsPageComponent implements HelpablePage {
    public readonly pageId = concertsPageId;
    public concerts$: Observable<Concert[]>;
    public columns = this.getColumns();

    public canEdit$: Observable<boolean>;
    public dataHooks: HelpDataHooks;

    public constructor(
        private concertService: ConcertService,
        authorisationService: TenantAuthorisationService,
        public route: ActivatedRoute,
        public router: Router,
        private dialogService: DialogService,
    ) {
        this.concerts$ = concertService.getAllWithEnsembles().pipe(cacheLatest());
        this.canEdit$ = authorisationService.hasPermission(FeaturePermission.ConcertWrite);
        this.dataHooks = {
            addDataPermission: FeaturePermission.ConcertWrite,
            hasNoData$: this.concerts$.pipe(map((concerts) => concerts.length === 0)),
            addData: () => this.createConcert().subscribe(),
        };
    }

    public createConcert = () => {
        return this.concertService.create().pipe(
            switchMap((concert) =>
                this.dialogService.open(CreateConcertDialogComponent, concert, {
                    maxWidth: 450,
                }),
            ),
            switchMap((concert) =>
                this.router.navigate([concert.id, "edit"], {
                    relativeTo: this.route,
                }),
            ),
        );
    };

    private getColumns(): ColDef[] {
        return [
            {
                headerName: "Occasion",
                field: "occasion",
                cellRenderer: ConcertLinkRenderer,
                width: 250,
                pinned: "left",
                ...wrapTextColDef(),
            },
            {
                field: "date",
                width: 120,
                sortable: true,
                sort: "desc",
                ...dateColDef(),
            },
            {
                field: "location",
                width: 220,
                ...wrapTextColDef(),
            },
            {
                headerName: "Performing Ensembles",
                valueGetter: (params: ValueGetterParams<Concert>) =>
                    params.data?.performance.map((p) => p.ensemble?.name ?? "").join(" ") ?? [],
                cellRenderer: PerformingEnsemblesRenderer,
                width: 200,
                ...wrapTextColDef(),
            },
            {
                field: "notes",
                flex: 1,
                minWidth: 400,
                ...wrapTextColDef(),
            },
        ];
    }
}
