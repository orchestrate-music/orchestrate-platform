import { ActivatedRoute, Router } from "@angular/router";

interface ILinkRendererContext {
    router: Router;
    route: ActivatedRoute;
}

export function isLinkRendererContext(obj: unknown): obj is ILinkRendererContext {
    return (
        typeof obj === "object" &&
        obj !== null &&
        (obj as any).router instanceof Router &&
        (obj as any).route instanceof ActivatedRoute
    );
}
