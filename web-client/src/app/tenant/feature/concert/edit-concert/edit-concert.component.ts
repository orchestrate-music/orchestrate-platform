import { Component, Input, OnChanges, OnDestroy } from "@angular/core";
import { BreezeFormGroup } from "app/common-ux/form/breeze-form-control";
import { Concert } from "app/dal/model/concert";
import { Subject } from "rxjs";

@Component({
    selector: "app-edit-concert",
    templateUrl: "./edit-concert.component.html",
    styleUrls: ["./edit-concert.component.scss"],
    standalone: false,
})
export class EditConcertComponent implements OnChanges, OnDestroy {
    @Input() public concert?: Concert;
    public form?: BreezeFormGroup<Concert, ["occasion", "location", "date", "notes"]>;
    public destroy$ = new Subject<void>();

    public ngOnChanges(): void {
        if (!this.concert) {
            throw new Error("Concert must be passed into this component");
        }

        this.form = new BreezeFormGroup(
            this.concert,
            [
                "occasion",
                "location",
                "date",
                "notes",
            ],
            this.destroy$,
        );
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }
}
