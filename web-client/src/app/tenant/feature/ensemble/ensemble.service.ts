import { Injectable } from "@angular/core";
import { BreezeEntityService } from "app/dal/breeze-domain.service";
import {
    Ensemble,
    EnsembleBreezeModel,
    EnsembleMembershipBreezeModel,
    EnsembleRole,
    EnsembleRoleBreezeModel,
    EnsembleStatus,
} from "app/dal/model/ensemble";
import { EnsembleMembership } from "app/dal/model/ensemble-membership";
import { FeeClass } from "app/dal/model/member";
import {
    Performance,
    PerformanceBreezeModel,
    PerformanceMember,
    PerformanceMemberBreezeModel,
    PerformanceScore,
    PerformanceScoreBreezeModel,
} from "app/dal/model/performance";
import { Role, RoleBreezeModel } from "app/dal/model/role";
import { Predicate } from "breeze-client";
import { map, switchMap } from "rxjs/operators";

export interface EnsembleWithMetadata {
    ensemble: Ensemble;
    memberCount: number;
    performanceCount: number;
}

export interface EnsembleStats {
    id: number;
    name: string;
    currentMemberCount: number;
    performanceCount: number;
    membership: Record<FeeClass, number>;
}

export interface EnsembleSummary {
    currentMemberCount: number;
    pastMemberCount: number;
    performanceCount: number;
    firstConcertDate?: Date;
    lastConcertDate?: Date;
    membership: { feeClass: FeeClass; count: number }[];
    yearlyStats: YearEnsembleStats[];
}

export interface YearEnsembleStats {
    year: number;
    performanceCount: number;
    memberJoinedCount: number;
    memberStayedCount: number;
    memberLeftCount: number;
    memberJoinedAndLeftCount: number;
}

export type MemberPerformanceCounts = Record<number, number>;

@Injectable({
    providedIn: "root",
})
export class EnsembleService extends BreezeEntityService<Ensemble> {
    protected readonly breezeModel = EnsembleBreezeModel;

    public create() {
        return this.breezeService.create(EnsembleBreezeModel, {
            status: EnsembleStatus.Active,
            memberBillingPeriodFeeDollars: 0,
        });
    }

    public getCurrent() {
        return this.breezeService.getByPredicate(
            EnsembleBreezeModel,
            new Predicate("status", "==", EnsembleStatus.Active),
        );
    }

    public getCurrentMembershipWithInstruments(ensemble: Ensemble) {
        const query = this.breezeService
            .createQuery(EnsembleMembershipBreezeModel)
            .where(new Predicate("ensembleId", "==", ensemble.id))
            .where(
                new Predicate("dateLeft", "==", null).or(
                    new Predicate("dateLeft", ">", new Date()),
                ),
            )
            .expand("member.memberInstrument");
        return this.breezeService.executeQuery<EnsembleMembership>(query);
    }

    public getPastMembershipWithInstruments(ensemble: Ensemble) {
        const query = this.breezeService
            .createQuery(EnsembleMembershipBreezeModel)
            .where(new Predicate("ensembleId", "==", ensemble.id))
            .where(new Predicate("dateLeft", "<=", new Date()))
            .expand("member.memberInstrument");
        return this.breezeService.executeQuery<EnsembleMembership>(query);
    }

    public getEnsemblePerformances(ensemble: Ensemble) {
        const query = this.breezeService
            .createQuery(PerformanceBreezeModel)
            .where(new Predicate("ensembleId", "==", ensemble.id))
            .orderByDesc("concert.date")
            .expand("concert");
        return this.breezeService.executeQuery<Performance>(query);
    }

    public getPerformanceMembers(performance: Performance) {
        const query = this.breezeService
            .createQuery(PerformanceMemberBreezeModel)
            .where(new Predicate("performanceId", "==", performance.id))
            .expand("member");
        return this.breezeService
            .executeQuery<PerformanceMember>(query)
            .pipe(map((performanceMembers) => performanceMembers.map((e) => e.member!)));
    }

    public getPerformanceScores(performance: Performance) {
        const query = this.breezeService
            .createQuery(PerformanceScoreBreezeModel)
            .where(new Predicate("performanceId", "==", performance.id))
            .expand("score");
        return this.breezeService
            .executeQuery<PerformanceScore>(query)
            .pipe(map((performanceScores) => performanceScores.map((e) => e.score!)));
    }

    public getAllRoles() {
        return this.breezeService.getAll(RoleBreezeModel);
    }

    public getEnsembleRolesWithRole(ensemble: Ensemble) {
        const query = this.breezeService
            .createQuery(EnsembleRoleBreezeModel)
            .where(new Predicate("ensembleId", "==", ensemble.id))
            .expand("role");
        return this.breezeService.executeQuery<EnsembleRole>(query);
    }

    public addEnsembleRole(ensemble: Ensemble, role: Role) {
        return this.breezeService.create(EnsembleRoleBreezeModel, {
            ensemble,
            role,
        });
    }

    public getStats(status: EnsembleStatus) {
        return this.apiEndpoint$.pipe(
            switchMap((endpoint) =>
                this.http.get<EnsembleStats[]>(`${endpoint}/ensemble/stats`, {
                    params: { status },
                }),
            ),
        );
    }

    public getSummary(ensemble: Ensemble) {
        return this.apiEndpoint$.pipe(
            switchMap((endpoint) =>
                this.http.get<EnsembleSummary>(`${endpoint}/ensemble/${ensemble.id}/summary`),
            ),
        );
    }

    public getMemberPerformanceCounts(ensemble: Ensemble) {
        return this.apiEndpoint$.pipe(
            switchMap((endpoint) =>
                this.http.get<MemberPerformanceCounts>(
                    `${endpoint}/ensemble/${ensemble.id}/member-performance-counts`,
                ),
            ),
        );
    }
}
