import { formatDate } from "@angular/common";
import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import {
    AgBarSeriesOptions,
    AgCartesianChartOptions,
    AgPolarChartOptions,
} from "ag-charts-community";
import { ColDef, ICellRendererParams, ValueGetterParams } from "ag-grid-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { HslColour } from "app/common-ux/colours";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { pastelPalette } from "app/common-ux/palette";
import { numberRouteParamFunc } from "app/common/routing-utils";
import { cacheLatest, filterForDefinedValue, propagateSwitchMap } from "app/common/rxjs-utilities";
import { formatMonthYearDate, groupBy, isArrayWithData } from "app/common/utils";
import { Ensemble, EnsembleStatus } from "app/dal/model/ensemble";
import { EnsembleMembership, EnsembleMembershipType } from "app/dal/model/ensemble-membership";
import { Member } from "app/dal/model/member";
import { Performance } from "app/dal/model/performance";
import {
    BehaviorSubject,
    Observable,
    combineLatest,
    debounceTime,
    distinctUntilChanged,
    map,
    startWith,
    switchMap,
    tap,
} from "rxjs";
import { membershipCategories } from "../common-membership-ux";
import { DisbandEnsembleDialogComponent } from "../disband-ensemble-dialog/disband-ensemble-dialog.component";
import { EditEnsembleDialogComponent } from "../edit-ensemble-dialog/edit-ensemble-dialog.component";
import { EnsembleService, EnsembleSummary, YearEnsembleStats } from "../ensemble.service";
import { EnsembleMembershipTypeIconRenderer } from "./membership-type-icon-renderer";

export enum EnsembleDataView {
    Summary = "summary",
    CurrentMembers = "current-members",
    Concerts = "concerts",
    PastMembers = "past-members",
}

interface CurrentMembershipData {
    type: EnsembleMembershipType;
    dateJoined: Date;
    member: Member;
    performanceCount: number;
}

interface PastMembershipData {
    member: Member;
    membership: EnsembleMembership[];
    performanceCount: number;
}

const chips: Chip<EnsembleDataView>[] = [
    {
        icon: "dashboard",
        text: "Summary",
        value: EnsembleDataView.Summary,
    },
    {
        icon: "event",
        text: "Concerts",
        value: EnsembleDataView.Concerts,
    },
    {
        icon: "face",
        text: "Current Members",
        value: EnsembleDataView.CurrentMembers,
    },
    {
        icon: "inventory",
        text: "Past Members",
        value: EnsembleDataView.PastMembers,
    },
];

type MembershipDates = [Date, Date | null];

@Component({
    templateUrl: "./ensemble-page.component.html",
    styleUrls: ["./ensemble-page.component.scss"],
    standalone: false,
})
export class EnsemblePageComponent {
    public static readonly viewParam = "view";

    public readonly EnsembleStatus = EnsembleStatus;
    public readonly EnsembleDataView = EnsembleDataView;
    public readonly FeaturePermission = FeaturePermission;

    public ensemble$: Observable<Ensemble | undefined>;
    public ensembleSummary$: Observable<EnsembleSummary>;
    public currentMembershipColumns = this.getCurrentMembershipColumns();
    public pastMembershipColumns = this.getPastMembershipColumns();
    public memberBreakdownChartOptions$: Observable<AgPolarChartOptions>;

    public view$: Observable<EnsembleDataView>;
    public filterValue$ = new BehaviorSubject<string | undefined>(undefined);
    public yearlyStatsChartOptions$: Observable<AgCartesianChartOptions>;
    public performances$: Observable<Performance[]>;
    public currentMembershipData$: Observable<CurrentMembershipData[]>;
    public pastMembershipData$: Observable<PastMembershipData[]>;

    public chips$: Observable<Chip<EnsembleDataView>[]>;
    private refreshChips$ = new BehaviorSubject<void>(undefined);

    public constructor(
        private route: ActivatedRoute,
        private router: Router,
        ensembleService: EnsembleService,
        private dialogService: DialogService,
    ) {
        this.ensemble$ = route.paramMap.pipe(
            map(numberRouteParamFunc("ensembleId")),
            distinctUntilChanged(),
            propagateSwitchMap((ensembleId) => ensembleService.getById(ensembleId)),
            cacheLatest(),
        );
        this.ensembleSummary$ = this.ensemble$.pipe(
            filterForDefinedValue(),
            switchMap((ensemble) => ensembleService.getSummary(ensemble)),
            cacheLatest(),
        );

        this.memberBreakdownChartOptions$ = this.ensembleSummary$.pipe(
            map((summary) => ({
                theme: orchestrateAgChartTheme,
                data: summary.membership,
                series: [
                    {
                        type: "pie",
                        angleKey: "count",
                        calloutLabelKey: "feeClass",
                        fills: membershipCategories.map((c) => c.fill.toRgbColour().toCssString()),
                        strokes: membershipCategories.map((c) =>
                            c.stroke.toRgbColour().toCssString(),
                        ),
                    },
                ],
                legend: {
                    position: "bottom",
                },
            })),
        );

        this.view$ = route.paramMap.pipe(
            startWith(route.snapshot.paramMap),
            map((params) => params.get(EnsemblePageComponent.viewParam) as EnsembleDataView),
            tap((view) => {
                if (!Object.values(EnsembleDataView).find((e) => e === view)) {
                    void this.updateView(EnsembleDataView.Summary);
                }
            }),
        );
        this.yearlyStatsChartOptions$ = this.ensembleSummary$.pipe(
            map((summary) => summary.yearlyStats),
            map((data) => this.buildChartOptions(data)),
        );

        const filterValue$ = this.filterValue$.pipe(debounceTime(200));
        const performances$ = this.ensemble$.pipe(
            filterForDefinedValue(),
            switchMap((ensemble) => ensembleService.getEnsemblePerformances(ensemble)),
        );
        this.performances$ = combineLatest([performances$, filterValue$]).pipe(
            map(([performances, filterValue]) => {
                if (!filterValue) {
                    return performances;
                }

                return performances.filter((i) => {
                    const date = formatDate(i.concert!.date, "MMM dd, yyyy", navigator.language);
                    const text = `${i.concert?.occasion ?? ""}\t${date}\t${
                        i.concert?.location ?? ""
                    }`;
                    return text.toLowerCase().includes(filterValue);
                });
            }),
        );

        const memberPerformanceCounts$ = this.ensemble$.pipe(
            filterForDefinedValue(),
            switchMap((ensemble) => ensembleService.getMemberPerformanceCounts(ensemble)),
            cacheLatest(),
        );
        const currentMembership$ = this.ensemble$.pipe(
            filterForDefinedValue(),
            switchMap((ensemble) => ensembleService.getCurrentMembershipWithInstruments(ensemble)),
        );
        this.currentMembershipData$ = combineLatest([
            currentMembership$,
            memberPerformanceCounts$,
        ]).pipe(
            map(([membership, performanceCounts]) =>
                membership.map((m) => ({
                    type: m.type,
                    dateJoined: m.dateJoined,
                    member: m.member!,
                    performanceCount: performanceCounts[m.memberId] ?? 0,
                })),
            ),
        );
        const pastMembership$ = this.ensemble$.pipe(
            filterForDefinedValue(),
            switchMap((ensemble) => ensembleService.getPastMembershipWithInstruments(ensemble)),
            map((membership) => groupBy(membership, (m) => m.member!)),
        );
        this.pastMembershipData$ = combineLatest([pastMembership$, memberPerformanceCounts$]).pipe(
            map(([groups, performanceCounts]) =>
                groups.map(([member, membership]) => ({
                    member,
                    membership,
                    performanceCount: performanceCounts[member.id] ?? 0,
                })),
            ),
        );

        this.chips$ = combineLatest([this.ensemble$, this.refreshChips$]).pipe(
            map(([ensemble, _]) =>
                chips.filter((c) => {
                    return (
                        (ensemble?.isActive ?? false) || c.value !== EnsembleDataView.CurrentMembers
                    );
                }),
            ),
        );
    }

    public async updateView(view?: EnsembleDataView | null) {
        if (!view) {
            view = EnsembleDataView.Summary;
        }

        await this.router.navigate([{ [EnsemblePageComponent.viewParam]: view }], {
            relativeTo: this.route,
            replaceUrl: true,
        });
    }

    public editEnsemble(ensemble: Ensemble) {
        this.dialogService.open(EditEnsembleDialogComponent, ensemble).subscribe();
    }

    public disbandEnsemble(ensemble: Ensemble) {
        this.dialogService.open(DisbandEnsembleDialogComponent, ensemble).subscribe(() => {
            this.refreshChips$.next();
        });
    }

    public applyFilter(filterText: string) {
        this.filterValue$.next(filterText.toLowerCase());
    }

    public hasChartData(chartOptions?: AgCartesianChartOptions | AgPolarChartOptions) {
        return isArrayWithData(chartOptions?.data);
    }

    private buildChartOptions(data: YearEnsembleStats[]): AgCartesianChartOptions {
        const yearField: keyof YearEnsembleStats = "year";
        const performanceCountField: keyof YearEnsembleStats = "performanceCount";

        // Seems to be a bug in ag-grid that number year causes rendering issues
        const adjustedData = data.map((d) => ({
            ...d,
            [yearField]: String(d[yearField]),
        }));

        const categories: [keyof YearEnsembleStats, string, HslColour][] = [
            ["memberJoinedCount", "Joined", pastelPalette.lightGreen],
            ["memberStayedCount", "Stayed", pastelPalette.lightBlue],
            ["memberLeftCount", "Left", pastelPalette.lightOrange],
            ["memberJoinedAndLeftCount", "Joined & left in same year", pastelPalette.lightRed],
        ];

        return {
            theme: orchestrateAgChartTheme,
            data: adjustedData,
            series: [
                ...categories.map<AgBarSeriesOptions>(([field, label, colour]) => ({
                    type: "bar",
                    stacked: true,
                    xKey: yearField,
                    yKey: field,
                    yName: label,
                    fill: colour.toRgbColour().toCssString(),
                    stroke: colour.addLightness(-10).toRgbColour().toCssString(),
                })),
                {
                    type: "line",
                    xKey: yearField,
                    yKey: performanceCountField,
                    yName: "Performances",
                    stroke: pastelPalette.darkPurple.addLightness(-10).toRgbColour().toCssString(),
                    marker: {
                        size: 12,
                        fill: pastelPalette.darkPurple.toRgbColour().toCssString(),
                        stroke: pastelPalette.darkPurple
                            .addLightness(-10)
                            .toRgbColour()
                            .toCssString(),
                    },
                },
            ],
            axes: [
                {
                    type: "category",
                    position: "bottom",
                    label: {
                        rotation: -90,
                    },
                },
                {
                    type: "number",
                    position: "left",
                    title: {
                        text: "Number of members",
                        enabled: true,
                    },
                    keys: categories.map(([field]) => field),
                    min: 0,
                },
                {
                    type: "number",
                    position: "right",
                    title: {
                        text: "Number of performances",
                        enabled: true,
                    },
                    keys: [performanceCountField],
                    tick: {
                        width: 0,
                    },
                    min: 0,
                },
            ],
            legend: {
                position: "bottom",
            },
        };
    }

    private getCurrentMembershipColumns(): ColDef[] {
        return [
            {
                cellRenderer: EnsembleMembershipTypeIconRenderer,
                headerName: "",
                field: "type",
                width: 48,
                cellStyle: { padding: "0", "padding-left": "1rem" },
            },
            {
                headerName: "Name",
                field: "member.fullName",
                sortable: true,
                sort: "asc",
                width: 150,
                headerClass: "ag-no-left-header-padding",
                cellStyle: { "padding-left": "0" },
            },
            {
                field: "dateJoined",
                sortable: true,
                width: 120,
                ...dateColDef(),
            },
            {
                headerName: "Performance count",
                field: "performanceCount",
                sortable: true,
                width: 150,
            },
            {
                headerName: "Instruments",
                field: "member.instrumentList",
                minWidth: 280,
                flex: 1,
                filter: true,
            },
        ];
    }

    private getPastMembershipColumns(): ColDef[] {
        return [
            {
                headerName: "Name",
                field: "member.fullName",
                sortable: true,
                sort: "asc",
                width: 150,
            },
            {
                headerName: "Dates active",
                valueGetter: (params: ValueGetterParams<PastMembershipData>) => {
                    if (!params.data) {
                        return [];
                    }

                    const dates: MembershipDates[] = params.data.membership
                        .sort((a, b) => a.dateJoined.getTime() - b.dateJoined.getTime())
                        .map((m) => [m.dateJoined, m.dateLeft]);
                    return dates;
                },
                cellRenderer: (params: ICellRendererParams) => {
                    const activePeriods: MembershipDates[] = params.value;
                    return activePeriods
                        .map(
                            (d) =>
                                `${formatMonthYearDate(d[0])} - ${formatMonthYearDate(
                                    d[1] ?? new Date(),
                                )}`,
                        )
                        .join("<br/>");
                },
                comparator: (a: MembershipDates[], b: MembershipDates[]) => {
                    if (a.length === 0 && b.length === 0) {
                        return 0;
                    } else if (a.length === 0 && b.length > 0) {
                        return -1;
                    } else if (a.length > 0 && b.length === 0) {
                        return 1;
                    } else {
                        return a[0]![0].getTime() - b[0]![0].getTime();
                    }
                },
                sortable: true,
                width: 180,
                ...wrapTextColDef(),
            },
            {
                headerName: "Performance count",
                field: "performanceCount",
                sortable: true,
                width: 150,
            },
            {
                headerName: "Instruments",
                field: "member.instrumentList",
                minWidth: 280,
                flex: 1,
                filter: true,
            },
        ];
    }
}
