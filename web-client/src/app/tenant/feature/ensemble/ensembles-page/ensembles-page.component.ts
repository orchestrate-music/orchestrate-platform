import { Component } from "@angular/core";
import { AgBarSeriesOptions, AgCartesianChartOptions } from "ag-charts-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import {
    cacheLatest,
    filterForDefinedValue,
    undefinedWhileLoading,
} from "app/common/rxjs-utilities";
import { isArrayWithData } from "app/common/utils";
import { EnsembleStatus } from "app/dal/model/ensemble";
import { FeeClass } from "app/dal/model/member";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { BehaviorSubject, Observable, combineLatest, filter, map, switchMap } from "rxjs";
import { membershipCategories } from "../common-membership-ux";
import { EditEnsembleDialogComponent } from "../edit-ensemble-dialog/edit-ensemble-dialog.component";
import { ensemblesPageId } from "../ensemble-consts";
import { EnsembleDataView } from "../ensemble-page/ensemble-page.component";
import { EnsembleService, EnsembleStats } from "../ensemble.service";

@Component({
    templateUrl: "./ensembles-page.component.html",
    styleUrls: ["./ensembles-page.component.scss"],
    standalone: false,
})
export class EnsemblesPageComponent implements HelpablePage {
    public readonly pageId = ensemblesPageId;
    public readonly EnsembleStatus = EnsembleStatus;
    public readonly EnsembleDataView = EnsembleDataView;

    public ensembleStats$: Observable<EnsembleStats[] | undefined>;
    public ensembleSummaryChartOptions$: Observable<AgCartesianChartOptions>;

    public selectedStatus$ = new BehaviorSubject(EnsembleStatus.Active);
    public ensembleChips: Chip<EnsembleStatus>[] = [
        {
            icon: "group",
            text: "Active",
            value: EnsembleStatus.Active,
        },
        {
            icon: "directions_walk",
            text: "Disbanded",
            value: EnsembleStatus.Disbanded,
        },
    ];

    public readonly dataHooks: HelpDataHooks;

    public constructor(
        private ensembleService: EnsembleService,
        private dialogService: DialogService,
    ) {
        this.ensembleStats$ = this.selectedStatus$.pipe(
            undefinedWhileLoading((status) => ensembleService.getStats(status)),
            cacheLatest(),
        );
        this.ensembleSummaryChartOptions$ = combineLatest([
            this.selectedStatus$,
            this.ensembleStats$,
        ]).pipe(
            filter(([status, stats]) => status === EnsembleStatus.Active && !!stats),
            map(([_, stats]) => this.buildChartOptions(stats!)),
        );
        this.dataHooks = {
            hasNoData$: this.ensembleStats$.pipe(
                filterForDefinedValue(),
                map((stats) => stats.length === 0),
            ),
            addDataPermission: FeaturePermission.EnsembleWrite,
            addData: () => this.addEnsemble(),
        };
    }

    public addEnsemble() {
        this.ensembleService
            .create()
            .pipe(
                switchMap((ensemble) =>
                    this.dialogService.open(EditEnsembleDialogComponent, ensemble),
                ),
            )
            .subscribe(() => {
                this.selectedStatus$.next(this.selectedStatus$.value);
            });
    }

    public hasChartData(chartOptions?: AgCartesianChartOptions) {
        return isArrayWithData(chartOptions?.data);
    }

    public updateStatus(status?: EnsembleStatus) {
        if (typeof status !== "undefined") {
            this.selectedStatus$.next(status);
        }
    }

    private buildChartOptions(data: EnsembleStats[]): AgCartesianChartOptions {
        const defaults: Record<FeeClass, number> = {
            [FeeClass.Full]: 0,
            [FeeClass.Concession]: 0,
            [FeeClass.Special]: 0,
        };

        const chartData = data.map((d) => ({
            name: d.name,
            ...defaults,
            ...d.membership,
        }));

        return {
            theme: orchestrateAgChartTheme,
            data: chartData,
            series: membershipCategories.map<AgBarSeriesOptions>((c) => ({
                type: "bar",
                stacked: true,
                xKey: "name",
                yKey: c.feeClass,
                yName: c.label,
                fill: c.fill.toRgbColour().toCssString(),
                stroke: c.stroke.toRgbColour().toCssString(),
            })),
            axes: [
                {
                    type: "category",
                    position: "bottom",
                    label: {
                        rotation: -45,
                    },
                },
                {
                    type: "number",
                    position: "left",
                    title: {
                        text: "Number of active members",
                        enabled: true,
                    },
                    keys: membershipCategories.map((e) => e.feeClass),
                    min: 0,
                    max: this.calculateAxisMax(
                        data.map((d) => Object.values(d.membership).reduce((p, c) => p + c, 0)),
                        10,
                    ),
                },
            ],
            legend: {
                position: "bottom",
            },
            // To give us some space to display the rotated ensemble names
            padding: {
                left: 60,
            },
        };
    }

    private calculateAxisMax(values: number[], multiple: number) {
        const max = Math.max(...values);
        const factor = Math.ceil(max / multiple);
        return Math.max(1, factor) * multiple;
    }
}
