import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { EnsemblePageComponent } from "./ensemble-page/ensemble-page.component";
import { EnsemblesPageComponent } from "./ensembles-page/ensembles-page.component";

const ensembleRoutes: AppRoutes = [
    {
        path: "",
        component: EnsemblesPageComponent,
        data: {
            title: "Ensembles",
            icon: "groups",
            restrictedToPermission: FeaturePermission.EnsembleRead,
            help: [helpPages.ensembles],
        },
    },
    {
        path: ":ensembleId",
        component: EnsemblePageComponent,
        data: {
            icon: "group",
            restrictedToPermission: FeaturePermission.EnsembleRead,
            help: [helpPages.ensembles],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(ensembleRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppEnsembleRoutingModule {}
