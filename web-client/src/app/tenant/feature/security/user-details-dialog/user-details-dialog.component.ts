import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { RoleName } from "app/auth/role.enum";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { filterForDefinedValue } from "app/common/rxjs-utilities";
import { TenantService } from "app/tenant/module/tenant.service";
import { lastValueFrom, map, Observable } from "rxjs";
import { TenantSecurityService, TenantUserDto, UserStatus } from "../tenant-security.service";

@Component({
    templateUrl: "./user-details-dialog.component.html",
    styleUrls: ["./user-details-dialog.component.scss"],
    standalone: false,
})
export class UserDetailsDialogComponent extends BaseDialog<TenantUserDto> {
    public readonly dialogName = "UserDetails";

    public tenantAbbr$: Observable<string>;
    public UserStatus = UserStatus;

    public constructor(
        dialogRef: MatDialogRef<UserDetailsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public user: TenantUserDto,
        private tenantSecurityService: TenantSecurityService,
        tenantService: TenantService,
        private dialogService: DialogService,
    ) {
        super(dialogRef);
        this.tenantAbbr$ = tenantService.currentTenant$.pipe(
            filterForDefinedValue(),
            map((t) => t.abbreviation),
        );
    }

    public async deleteRole(role: RoleName) {
        try {
            await lastValueFrom(
                this.tenantSecurityService.removeUserFromRole(this.user.userId, role),
            );
            this.dialogService.success("Role removed successfully");
        } catch (e) {
            this.dialogService.danger("Failed to remove role from user");
        }
    }

    public async addRole(role: RoleName) {
        try {
            await lastValueFrom(this.tenantSecurityService.addUserToRole(this.user.userId, role));
            this.dialogService.success("Role added successfully");
        } catch (e) {
            this.dialogService.danger("Failed to add role to user");
        }
    }
}
