import { Component, Input } from "@angular/core";
import { MemberBillingPeriod, MemberBillingPeriodState } from "app/dal/model/member-billing-period";

@Component({
    selector: "app-billing-period-card",
    templateUrl: "./billing-period-card.component.html",
    styleUrls: ["./billing-period-card.component.scss"],
    standalone: false,
})
export class BillingPeriodCardComponent {
    @Input() public billingPeriod?: MemberBillingPeriod;

    public now = new Date();

    public get isInDraftState() {
        return this.billingPeriod?.state === MemberBillingPeriodState.Draft;
    }

    public get isinOpenState() {
        return this.billingPeriod?.state === MemberBillingPeriodState.Open;
    }

    public get isClosed() {
        return this.billingPeriod?.state === MemberBillingPeriodState.Closed;
    }
}
