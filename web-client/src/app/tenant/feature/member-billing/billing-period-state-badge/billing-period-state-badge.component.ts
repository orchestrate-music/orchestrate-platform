import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { MemberBillingPeriodState } from "app/dal/model/member-billing-period";

const stateIcon: Record<MemberBillingPeriodState, string> = {
    [MemberBillingPeriodState.Created]: "schedule",
    [MemberBillingPeriodState.Draft]: "edit_note",
    [MemberBillingPeriodState.Open]: "play_arrow",
    [MemberBillingPeriodState.Closed]: "stop",
};

const stateText: Record<MemberBillingPeriodState, string> = {
    [MemberBillingPeriodState.Created]: "Created",
    [MemberBillingPeriodState.Draft]: "In Review",
    [MemberBillingPeriodState.Open]: "Open",
    [MemberBillingPeriodState.Closed]: "Closed",
};

const stateClass: Record<MemberBillingPeriodState, string> = {
    [MemberBillingPeriodState.Created]: "draft",
    [MemberBillingPeriodState.Draft]: "draft",
    [MemberBillingPeriodState.Open]: "open",
    [MemberBillingPeriodState.Closed]: "",
};

export interface BillingPeriodShape {
    earlyBirdDue?: Date | null;
    due: Date;
    state: MemberBillingPeriodState;
}

@Component({
    selector: "app-billing-period-state-badge",
    template: `
        <app-badge [ngClass]="stateClass">
            <app-icon-text-pair
                [icon]="stateIcon"
                [text]="stateText"
            ></app-icon-text-pair>
        </app-badge>
    `,
    styles: [
        `
            :host {
                font-size: 14px;
                line-height: 20px;
                font-weight: 500;
            }

            .draft {
                background-color: #e0e0e0;
            }

            .early-bird {
                background-color: #b1d8b7;
            }

            .open {
                background-color: #68bbe3;
            }

            .overdue {
                background-color: hsl(29, 86%, 65%);
            }
        `,
    ],
    imports: [
        CommonModule,
        AppBadgeModule,
    ],
})
export class BillingPeriodStateBadgeComponent {
    @Input() public billingPeriod?: BillingPeriodShape;

    public now = new Date();

    public get stateIcon() {
        if (!this.billingPeriod) {
            return "";
        }

        return stateIcon[this.billingPeriod.state];
    }

    public get stateText() {
        if (!this.billingPeriod) {
            return "";
        }

        if (this.billingPeriod.state === MemberBillingPeriodState.Open) {
            if (this.billingPeriod.due < this.now) {
                return "Past Due";
            } else if (
                this.billingPeriod.earlyBirdDue &&
                this.billingPeriod.earlyBirdDue > this.now
            ) {
                return "Early Bird";
            }
        }

        return stateText[this.billingPeriod.state];
    }

    public get stateClass() {
        if (!this.billingPeriod) {
            return "";
        }

        if (this.billingPeriod.state === MemberBillingPeriodState.Open) {
            if (this.billingPeriod.due < this.now) {
                return "overdue";
            } else if (
                this.billingPeriod.earlyBirdDue &&
                this.billingPeriod.earlyBirdDue > this.now
            ) {
                return "early-bird";
            }
        }

        return stateClass[this.billingPeriod.state];
    }
}
