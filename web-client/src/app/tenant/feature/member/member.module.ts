import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatChipsModule } from "@angular/material/chips";
import { MatDividerModule } from "@angular/material/divider";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatIconModule } from "@angular/material/icon";
import { MatMenuModule } from "@angular/material/menu";
import { MatTooltipModule } from "@angular/material/tooltip";
import { AppChipSelectionModule } from "app/common-ux/chip-selection/chip-selection.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { AllMembersPageComponent } from "./all-members-page/all-members-page.component";
import { EditMemberDialogComponent } from "./edit-member-dialog/edit-member-dialog.component";
import { EditMemberEnsemblesDialogComponent } from "./edit-member-ensembles-dialog/edit-member-ensembles-dialog.component";
import { AppMemberRoutingModule } from "./member-routing.module";
import { MemberTableComponent } from "./member-table/member-table.component";
import { AppMemberWidgetModule } from "./member-widget.module";
import { MembershipTypeIconComponent } from "./membership-type-icon/membership-type-icon.component";
import { PerformanceSummaryComponent } from "./performance-summary/performance-summary.component";
import { SelectMembershipTypeComponent } from "./select-membership-type/select-membership-type.component";
import { SummaryPageComponent } from "./summary-page/summary-page.component";

@NgModule({
    imports: [
        CommonModule,

        MatButtonModule,
        MatIconModule,
        MatDividerModule,
        MatCheckboxModule,
        MatChipsModule,
        MatTooltipModule,
        MatAutocompleteModule,
        MatExpansionModule,
        MatMenuModule,
        MatCardModule,

        AppDataGridModule,
        AppMemberWidgetModule,
        AppMemberRoutingModule,
        AppPageWrapperModule,
        AppDialogModule,
        AppFormModule,
        AppSaveCancelButtonsModule,
        AppChipSelectionModule,
    ],
    declarations: [
        MemberTableComponent,
        AllMembersPageComponent,
        EditMemberEnsemblesDialogComponent,
        PerformanceSummaryComponent,
        SummaryPageComponent,
        EditMemberDialogComponent,
        MembershipTypeIconComponent,
        SelectMembershipTypeComponent,
    ],
})
export class AppMemberModule {}
