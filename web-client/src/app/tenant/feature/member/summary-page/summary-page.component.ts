import { Component, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ColDef } from "ag-grid-community";
import { cacheLatest } from "app/common/rxjs-utilities";
import { BehaviorSubject, Observable, Subscription, combineLatest } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { MemberService, MemberSummary } from "../member.service";

export const selectedMemberParamName = "selected-member";

@Component({
    selector: "app-summary",
    templateUrl: "./summary-page.component.html",
    styleUrl: "./summary-page.component.scss",
    standalone: false,
})
export class SummaryPageComponent implements OnDestroy {
    public getCurrentMembersOnly$ = new BehaviorSubject<boolean>(true);
    public members$: Observable<MemberSummary[]>;
    public columns = this.getColumns();
    public selectedMember?: MemberSummary;

    private subscription: Subscription;

    constructor(
        memberService: MemberService,
        private route: ActivatedRoute,
        private router: Router,
    ) {
        this.members$ = this.getCurrentMembersOnly$.pipe(
            switchMap((currOnly) => {
                return currOnly
                    ? memberService.getCurrentMemberSummary()
                    : memberService.getMemberSummary();
            }),
            cacheLatest(),
        );
        this.subscription = combineLatest([
            this.members$,
            route.paramMap.pipe(
                map((p) => p.get(selectedMemberParamName)),
                map((memberId) => (memberId ? Number(memberId) : undefined)),
            ),
        ]).subscribe(([members, memberId]) => {
            this.selectedMember = members.find((m) => m.id === memberId);
        });
    }

    public ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public async setSelectedMemberId(member?: MemberSummary) {
        const commands = member ? [{ [selectedMemberParamName]: member.id }] : [{}];
        await this.router.navigate(commands, { relativeTo: this.route, replaceUrl: true });
    }

    private getColumns(): ColDef<MemberSummary>[] {
        return [
            {
                headerName: "Name",
                sortable: true,
                resizable: true,
                flex: 1,
                valueGetter: (params) => {
                    if (!params.data) {
                        return "";
                    }

                    return `${params.data.firstName} ${params.data.lastName}`;
                },
            },
            {
                field: "performanceCount",
                headerName: "Count",
                type: "numericColumn",
                sort: "desc",
                sortable: true,
                resizable: true,
                width: 110,
            },
        ];
    }
}
