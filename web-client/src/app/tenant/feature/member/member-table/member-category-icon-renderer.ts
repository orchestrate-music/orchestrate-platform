import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";
import { MemberCategory, MemberCategoryMetadata } from "app/dal/model/member";

export class MemberCategoryIconRenderer extends GridIconRenderer<MemberCategory> {
    protected icon(value: MemberCategory) {
        return MemberCategoryMetadata[value].icon;
    }

    protected tooltip(value: MemberCategory) {
        return MemberCategoryMetadata[value].singularText;
    }
}
