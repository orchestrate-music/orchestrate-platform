import { Component, EventEmitter, Input, Output } from "@angular/core";
import {
    AllEnsembleMembershipTypeMetadata,
    EnsembleMembershipType,
    EnsembleMembershipTypeMetadata,
} from "app/dal/model/ensemble-membership";

@Component({
    selector: "app-select-membership-type",
    template: `
        <mat-form-field
            *ngIf="metadata"
            subscriptSizing="dynamic"
        >
            <mat-label>Player type</mat-label>
            <mat-select
                [ngModel]="membershipType"
                (ngModelChange)="membershipTypeChange.emit($event)"
            >
                <mat-option
                    *ngFor="let m of AllMembershipTypeMetadata"
                    [value]="m.type"
                >
                    {{ m.label }}
                </mat-option>
            </mat-select>
            <mat-hint *ngIf="hintText">{{ hintText }}</mat-hint>
        </mat-form-field>
    `,
    styles: [
        `
            mat-form-field {
                width: 100%;
            }
        `,
    ],
    standalone: false,
})
export class SelectMembershipTypeComponent {
    @Input() public membershipType?: EnsembleMembershipType;
    @Output() public membershipTypeChange = new EventEmitter<EnsembleMembershipType>();

    public AllMembershipTypeMetadata = AllEnsembleMembershipTypeMetadata;

    public get metadata() {
        if (typeof this.membershipType === "undefined") {
            return undefined;
        }

        return EnsembleMembershipTypeMetadata[this.membershipType];
    }

    public get hintText() {
        if (!this.metadata) {
            return undefined;
        }

        return this.metadata.feesAreCharged
            ? "Ensemble fees will be charged"
            : "Ensemble fees will not be charged";
    }
}
