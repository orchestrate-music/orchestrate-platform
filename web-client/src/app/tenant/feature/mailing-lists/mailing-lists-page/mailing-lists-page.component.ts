import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { MatCardModule } from "@angular/material/card";
import { MatDividerModule } from "@angular/material/divider";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";
import { AgChartsModule } from "ag-charts-angular";
import { AgBarSeriesOptions, AgCartesianChartOptions, time } from "ag-charts-community";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { orchestrateAgChartTheme } from "app/common-ux/chart-theme";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { cacheLatest } from "app/common/rxjs-utilities";
import { HelpDataHooks, HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { Observable, combineLatest, map, switchMap } from "rxjs";
import { CreateMailingListDialogComponent } from "../create-mailing-list-dialog/create-mailing-list-dialog.component";
import { mailingListsPageId } from "../mailing-lists-consts";
import {
    MailingListsPageService,
    MailingListsSummaryDto,
    UsageDatum,
} from "./mailing-lists-page.service";

@Component({
    selector: "app-mailing-lists-page",
    templateUrl: "./mailing-lists-page.component.html",
    styleUrls: ["./mailing-lists-page.component.scss"],
    imports: [
        CommonModule,
        RouterModule,
        MatCardModule,
        MatDividerModule,
        MatTooltipModule,
        AgChartsModule,
        AppPageWrapperModule,
        AppLoadingModule,
        AppButtonModule,
        AppBadgeModule,
    ],
    providers: [
        MailingListsPageService,
    ],
})
export class MailingListsPageComponent implements HelpablePage {
    public readonly pageId = mailingListsPageId;
    public canEdit$: Observable<boolean>;
    public summary$: Observable<MailingListsSummaryDto>;
    public subtitleText$: Observable<string | undefined>;
    public usageChartOptions$: Observable<AgCartesianChartOptions>;
    public dataHooks: HelpDataHooks;

    public constructor(
        pageService: MailingListsPageService,
        private dialogService: DialogService,
        authService: TenantAuthorisationService,
        private router: Router,
        private route: ActivatedRoute,
    ) {
        this.canEdit$ = authService.hasPermission(FeaturePermission.MailingListWrite);
        this.summary$ = pageService.getSummary().pipe(cacheLatest());
        this.usageChartOptions$ = this.summary$.pipe(
            map((summary) => this.buildChartOptions(summary.usageHistory)),
        );
        this.dataHooks = {
            hasNoData$: this.summary$.pipe(map((s) => s.mailingLists.length === 0)),
            addDataPermission: FeaturePermission.MailingListWrite,
            addData: () => this.createMailingList(),
        };

        this.subtitleText$ = combineLatest([this.canEdit$, this.summary$]).pipe(
            map(([canEdit, summary]) => {
                if (!canEdit) {
                    return undefined;
                }

                const availableCount = summary.maxMailingListCount - summary.mailingLists.length;
                if (availableCount > 0) {
                    return (
                        `You can create ${availableCount} more mailing list${availableCount > 1 ? "s" : ""},` +
                        ` up to a total of ${summary.maxMailingListCount}.`
                    );
                } else {
                    return (
                        `You can have reached the maximum number of Mailing Lists allowed.` +
                        ` To create more, please delete an existing one.`
                    );
                }
            }),
        );
    }

    public createMailingList() {
        this.dialogService
            .open(CreateMailingListDialogComponent, null)
            .pipe(
                switchMap((mailingList) =>
                    this.router.navigate([mailingList.id, "edit"], { relativeTo: this.route }),
                ),
            )
            .subscribe();
    }

    public hasChartData(options: AgCartesianChartOptions) {
        return Array.isArray(options.series) && options.series.length > 0;
    }

    private buildChartOptions(usage: UsageDatum[]): AgCartesianChartOptions {
        interface ChartDatum {
            date: number;
            [mailingListId: string]: number;
        }

        const mailingLists = new Map(usage.map((u) => [u.mailingListId, u.mailingListName]));
        const dates = Array.from(new Set(usage.map((u) => u.date.getTime()))).sort();
        const data = dates.map((d) => {
            const datum: ChartDatum = { date: d };
            for (const id of mailingLists.keys()) {
                const u = usage.find((u) => u.date.getTime() === d && u.mailingListId === id);
                datum[String(id)] = u?.sendCount ?? 0;
            }
            return datum;
        });

        return {
            theme: orchestrateAgChartTheme,
            data,
            series: Array.from(mailingLists.entries()).map<AgBarSeriesOptions>(([id, name]) => ({
                type: "bar",
                stacked: true,
                xKey: "date",
                yKey: String(id),
                yName: name,
            })),
            axes: [
                {
                    type: "number",
                    position: "left",
                    min: 0,
                    interval: {
                        step: Math.ceil(Math.max(...usage.map((d) => d.sendCount)) / 4),
                    },
                    label: {
                        format: "d",
                    },
                },
                {
                    type: "time",
                    nice: false,
                    position: "bottom",
                    max: new Date().getTime(),
                    interval: {
                        step: time.day.every(7, { snapTo: "end" }),
                    },
                    label: {
                        autoRotate: true,
                        formatter(params) {
                            return new Date(params.value).toLocaleDateString();
                        },
                    },
                },
            ],
            legend: {
                position: "bottom",
            },
            padding: {
                left: 36,
                right: 32,
            },
        };
    }
}
