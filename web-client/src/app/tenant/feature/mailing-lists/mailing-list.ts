export interface MailingList {
    id: number;
    name: string;
    slug: string;
    allowedSenders: AllowedSenders;
    replyTo: ReplyStrategy;
    footer?: string;
}

export enum AllowedSenders {
    GlobalSendOnly,
    RecipientsOnly,
    Anyone,
}

export enum ReplyStrategy {
    MailingList,
    Sender,
}

export interface MailingListAddressTemplateParameters {
    tenantSlug: string;
    domain: string;
}
