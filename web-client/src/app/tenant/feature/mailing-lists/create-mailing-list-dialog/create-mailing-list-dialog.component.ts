import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { Observable, tap } from "rxjs";
import { EditMailingListComponent } from "../edit-mailing-list/edit-mailing-list.component";
import { MailingList } from "../mailing-list";
import {
    CreateMailingListDialogDataDto,
    CreateMailingListDialogService,
    CreateMailingListDto,
} from "./create-mailing-list-dialog.service";

@Component({
    selector: "app-create-mailing-list",
    templateUrl: "./create-mailing-list-dialog.component.html",
    styleUrls: ["./create-mailing-list-dialog.component.scss"],
    imports: [
        CommonModule,
        AppDialogModule,
        AppFormModule,
        AppSaveCancelButtonsModule,
        AppLoadingModule,

        EditMailingListComponent,
    ],
    providers: [
        CreateMailingListDialogService,
    ],
})
export class CreateMailingListDialogComponent extends BaseDialog<never, MailingList> {
    public dialogName = "CreateMailingList";
    public dialogData$: Observable<CreateMailingListDialogDataDto>;
    public formGroup = EditMailingListComponent.buildFormGroup();
    public error?: Error;

    public constructor(
        dialogRef: MatDialogRef<CreateMailingListDialogComponent>,
        private dialogService: CreateMailingListDialogService,
    ) {
        super(dialogRef);
        this.dialogData$ = dialogService.getData();
    }

    public save = (form: typeof this.formGroup) => {
        return this.dialogService
            .create(form.value as CreateMailingListDto)
            .pipe(tap((mailingList) => this.resolve(mailingList)));
    };
}
