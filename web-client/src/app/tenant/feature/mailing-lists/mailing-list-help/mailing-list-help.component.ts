import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { HelpBrokerService } from "app/help/help-broker.service";
import { AppHelpScaffoldModule } from "app/help/scaffold/help-scaffold.module";
import { mailingListsPageId } from "../mailing-lists-consts";

@Component({
    templateUrl: "./mailing-list-help.component.html",
    imports: [
        CommonModule,
        AppHelpScaffoldModule,
    ],
})
export class MailingListHelpComponent {
    public mailingListHooks$ = this.helpBroker.helpablePageDataHooks(mailingListsPageId);

    public constructor(private helpBroker: HelpBrokerService) {}
}
