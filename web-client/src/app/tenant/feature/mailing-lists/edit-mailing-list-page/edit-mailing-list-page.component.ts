import { CommonModule } from "@angular/common";
import { Component } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatChipsModule } from "@angular/material/chips";
import { MatMenuModule } from "@angular/material/menu";
import { MatTooltipModule } from "@angular/material/tooltip";
import { ActivatedRoute, Router } from "@angular/router";
import { AgGridModule } from "ag-grid-angular";
import { ColDef } from "ag-grid-community";
import { AppBadgeModule } from "app/common-ux/badge/badge.module";
import { GridEmailLinkRenderer } from "app/common-ux/data-grid/grid-email-link-renderer";
import { GridFillHeightDirective } from "app/common-ux/data-grid/grid-fill-height.directive";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { modifiedValues } from "app/common-ux/form/form-utilities";
import { AppFormModule } from "app/common-ux/form/form.module";
import { TagBoxComponent } from "app/common-ux/form/tag-box/tag-box.component";
import { AppLoadingModule } from "app/common-ux/loading/loading.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { SaveErrorDialogComponent } from "app/common-ux/save-cancel-buttons/save-error-dialog.component";
import { numberRouteParamFunc } from "app/common/routing-utils";
import {
    cacheLatest,
    filterForDefinedValue,
    propagateMap,
    propagateSwitchMap,
    undefinedWhileLoading,
} from "app/common/rxjs-utilities";
import { areSameArray, formatLongDate } from "app/common/utils";
import {
    Observable,
    debounceTime,
    distinctUntilChanged,
    first,
    map,
    merge,
    startWith,
    switchMap,
} from "rxjs";
import { EditMailingListComponent } from "../edit-mailing-list/edit-mailing-list.component";
import { MailingList } from "../mailing-list";
import {
    ContactDto,
    EditMailingListPageService,
    EnsembleDto,
    MailingListPageData,
    MemberDto,
    Recipient,
    SuppressionDto,
} from "./edit-mailing-list-page.service";
import { RecipientSendStatusIconRenderer } from "./recipient-send-status-icon-renderer";
import { RecipientSourceIconRenderer } from "./recipient-source-icon-renderer";

@Component({
    selector: "app-edit-mailing-list-page",
    templateUrl: "./edit-mailing-list-page.component.html",
    styleUrls: ["./edit-mailing-list-page.component.scss"],
    imports: [
        CommonModule,
        MatCardModule,
        MatChipsModule,
        MatButtonModule,
        MatMenuModule,
        MatTooltipModule,
        AgGridModule,
        AppPageWrapperModule,
        AppLoadingModule,
        AppBadgeModule,
        AppSaveCancelButtonsModule,
        AppFormModule,

        EditMailingListComponent,
        GridFillHeightDirective,
    ],
    providers: [
        EditMailingListPageService,
    ],
})
export class EditMailingListPageComponent {
    public pageData$: Observable<MailingListPageData | undefined>;

    public recipients$: Observable<Recipient[] | undefined>;
    public recipientCounts$: Observable<{ all: number; deliverable: number } | undefined>;
    public gridColumns = this.getColumns();

    protected formGroup$: Observable<ReturnType<typeof this.buildFormGroup>>;

    public constructor(
        private pageService: EditMailingListPageService,
        private router: Router,
        private route: ActivatedRoute,
        private dialogService: DialogService,
    ) {
        this.pageData$ = route.paramMap.pipe(
            map(numberRouteParamFunc("mailingListId")),
            propagateSwitchMap((id) => pageService.getPageData(id)),
            cacheLatest(),
        );
        this.formGroup$ = this.pageData$.pipe(
            filterForDefinedValue(),
            map((pageData) => this.buildFormGroup(pageData)),
            cacheLatest(),
        );
        this.recipients$ = this.formGroup$.pipe(
            switchMap((fg) =>
                merge(
                    fg.controls.ensembles.valueChanges,
                    fg.controls.members.valueChanges,
                    fg.controls.contacts.valueChanges,
                    fg.controls.suppressions.valueChanges,
                ).pipe(
                    debounceTime(100),
                    startWith(undefined),
                    map(
                        () =>
                            [
                                fg.controls.ensembles.value,
                                fg.controls.members.value,
                                fg.controls.contacts.value,
                                fg.controls.suppressions.value,
                            ] as const,
                    ),
                ),
            ),
            distinctUntilChanged(
                (previous, current) =>
                    areSameArray(previous[0], current[0]) &&
                    areSameArray(previous[1], current[1]) &&
                    areSameArray(previous[2], current[2]) &&
                    areSameArray(previous[3], current[3]),
            ),
            undefinedWhileLoading(([ensembles, members, contacts, suppressions]) =>
                pageService.buildRecipientList({
                    ensembleIds: ensembles.map((e) => e.id),
                    memberIds: members.map((e) => e.id),
                    contactIds: contacts.map((e) => e.id),
                    suppressions: suppressions.map((e) => e.email),
                }),
            ),
            cacheLatest(),
        );
        this.recipientCounts$ = this.recipients$.pipe(
            propagateMap((rs) => ({
                all: rs.length,
                deliverable: rs.filter((r) => !r.dropReason).length,
            })),
        );
    }

    private buildFormGroup(pageData: MailingListPageData) {
        const filterById = <T extends { id: number }>(data: T[], ids: number[]) =>
            data.filter((d) => ids.includes(d.id));
        return new FormGroup({
            mailingList: EditMailingListComponent.buildFormGroup(pageData.mailingList),
            ensembles: TagBoxComponent.buildFormControl(
                filterById(pageData.referenceData.ensembles, pageData.recipients.ensembleIds),
            ),
            members: TagBoxComponent.buildFormControl(pageData.recipients.members),
            contacts: TagBoxComponent.buildFormControl(pageData.recipients.contacts),
            suppressions: new FormControl(pageData.suppressions, { nonNullable: true }),
        });
    }

    protected isEqual = <T extends { id: number }>(a: T, b: T) => a.id === b.id;
    protected formatEnsemble = (dto: EnsembleDto) =>
        `${dto.name} (${dto.numRecipients} recipients)`;
    protected formatRecipient = (dto: MemberDto | ContactDto) => dto.name;

    protected suppressionTooltip(suppression: SuppressionDto) {
        return `${formatLongDate(suppression.addedAt)}: ${suppression.reason}`;
    }
    protected removeSuppression(control: FormControl<SuppressionDto[]>, value: SuppressionDto) {
        control.setValue(control.value.filter((v) => v !== value));
    }

    protected saveMailingList = (formGroup: ReturnType<typeof this.buildFormGroup>) => {
        return this.pageData$.pipe(
            first(),
            map((pd) => pd?.mailingList),
            propagateSwitchMap((mailingList) => {
                return this.pageService.update(mailingList.id, {
                    mailingList: modifiedValues(formGroup.controls.mailingList),
                    ensembleIds: formGroup.controls.ensembles.value.map((v) => v.id),
                    memberIds: formGroup.controls.members.value.map((v) => v.id),
                    contactIds: formGroup.controls.contacts.value.map((v) => v.id),
                    removedSuppressions: formGroup.controls.suppressions.defaultValue
                        .filter((v) => !formGroup.controls.suppressions.value.includes(v))
                        .map((v) => v.email),
                });
            }),
        );
    };

    protected deleteMailingList(mailingList: MailingList) {
        return this.dialogService
            .openConfirmation(
                `Are you sure you want to delete the ${mailingList.name} mailing list?` +
                    " All send history will be removed and any emails sent to its address" +
                    " will be rejected.",
                () => this.pageService.delete(mailingList.id),
            )
            .pipe(switchMap(() => this.goToMailingListsPage()))
            .subscribe();
    }

    protected goToMailingListsPage() {
        return this.router.navigate(["../.."], {
            relativeTo: this.route,
        });
    }

    protected showErrorDialog(error?: Error) {
        if (!error) {
            return;
        }

        this.dialogService.open(SaveErrorDialogComponent, error).subscribe();
    }

    private getColumns(): ColDef<Recipient>[] {
        return [
            {
                headerName: "",
                width: 48,
                cellStyle: { padding: 0, "padding-left": "1rem" },
                cellRenderer: RecipientSendStatusIconRenderer,
                field: "dropReason",
            },
            {
                field: "name",
                width: 200,
                wrapText: true,
                sort: "asc",
            },
            {
                headerName: "Source",
                width: 120,
                cellRenderer: RecipientSourceIconRenderer,
            },
            {
                field: "email",
                cellRenderer: GridEmailLinkRenderer,
                width: 280,
                ...wrapTextColDef(),
            },
            {
                headerName: "Notes",
                field: "dropReason",
                minWidth: 180,
                flex: 1,
                ...wrapTextColDef(),
            },
        ];
    }
}
