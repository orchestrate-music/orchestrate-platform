import {
    GridMultiIconRenderer,
    IconDetails,
} from "app/common-ux/data-grid/grid-multi-icon-renderer";
import { Recipient, RecipientSource } from "./edit-mailing-list-page.service";

export class RecipientSourceIconRenderer extends GridMultiIconRenderer<Recipient> {
    protected getIcons(data: Recipient): IconDetails[] {
        const icons: IconDetails[] = [];

        if (data.ensemble !== RecipientSource.NotApplicable) {
            icons.push({
                icon: "groups",
                tooltip: "Is a member of a selected ensemble",
            });
        }
        if (data.member !== RecipientSource.NotApplicable) {
            icons.push({
                icon: "face",
                tooltip: "Is a selected member",
            });
        }
        if (data.contact !== RecipientSource.NotApplicable) {
            icons.push({
                icon: "contacts",
                tooltip: "Is a selected contact",
            });
        }

        return icons;
    }
}
