import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { switchMap } from "rxjs";
import { MailingList, MailingListAddressTemplateParameters } from "../mailing-list";

export interface MailingListPageData {
    mailingList: MailingList;
    recipients: {
        ensembleIds: number[];
        members: MemberDto[];
        contacts: ContactDto[];
    };
    suppressions: SuppressionDto[];
    referenceData: {
        addressParameters: MailingListAddressTemplateParameters;
        ensembles: EnsembleDto[];
        members: MemberDto[];
        contacts: ContactDto[];
    };
}

export interface EnsembleDto {
    id: number;
    name: string;
    numRecipients: number;
}

export interface MemberDto {
    id: number;
    name: string;
    email?: string;
}

export interface ContactDto {
    id: number;
    name: string;
    email?: string;
}

export interface SuppressionDto {
    email: string;
    addedAt: Date;
    reason: string;
    isRemovable: boolean;
}

export interface RecipientListBuilderDto {
    ensembleIds: number[];
    memberIds: number[];
    contactIds: number[];
    suppressions: string[];
}

export enum RecipientSource {
    NotApplicable,
    Inactive,
    Active,
}

export interface Recipient {
    name: string;
    email?: string;
    isSuppressed: boolean;
    ensemble: RecipientSource;
    member: RecipientSource;
    contact: RecipientSource;
    dropReason?: string;
}

export interface UpdateMailingListAndRecipientsDto {
    mailingList: UpdateMailingListDto;
    ensembleIds: number[];
    memberIds: number[];
    contactIds: number[];
    removedSuppressions: string[];
}
export type UpdateMailingListDto = Partial<Omit<MailingList, "id">>;

@Injectable()
export class EditMailingListPageService {
    public constructor(
        private appConfig: AppConfig,
        private httpClient: HttpClient,
    ) {}

    public getPageData(id: number) {
        return this.appConfig
            .serverEndpoint(`edit-mailing-list-page/data/${id}`)
            .pipe(switchMap((url) => this.httpClient.get<MailingListPageData>(url)));
    }

    public buildRecipientList(sourcesDto: RecipientListBuilderDto) {
        return this.appConfig
            .serverEndpoint(`edit-mailing-list-page/build-recipient-list`)
            .pipe(switchMap((url) => this.httpClient.post<Recipient[]>(url, sourcesDto)));
    }

    public update(id: number, data: UpdateMailingListAndRecipientsDto) {
        return this.appConfig
            .serverEndpoint(`edit-mailing-list-page/${id}`)
            .pipe(switchMap((url) => this.httpClient.patch<MailingList>(url, data)));
    }

    public delete(id: number) {
        return this.appConfig
            .serverEndpoint(`edit-mailing-list-page/${id}`)
            .pipe(switchMap((url) => this.httpClient.delete<void>(url)));
    }
}
