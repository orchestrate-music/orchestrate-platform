import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ColDef } from "ag-grid-community";
import { DataGridComponent } from "app/common-ux/data-grid/data-grid.component";
import { currencyColDef } from "app/common-ux/data-grid/grid-currency-column";
import { dateColDef } from "app/common-ux/data-grid/grid-date-column";
import { ColumnAction, buildEditColumn } from "app/common-ux/data-grid/grid-edit-column";
import { wrapTextColDef } from "app/common-ux/data-grid/grid-wrap-column";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { cacheLatest } from "app/common/rxjs-utilities";
import { AssetLoan } from "app/dal/model/asset-loan";
import { buildGridAttachmentActions } from "app/storage/attachment-grid-utils";
import { EditAssetDialogComponent } from "app/tenant/feature/asset/edit-asset-dialog/edit-asset-dialog.component";
import { Observable, ReplaySubject, combineLatest } from "rxjs";
import { map, switchMap, tap } from "rxjs/operators";
import { AssetAttachmentsDialogComponent } from "../asset-attachments-dialog/asset-attachments-dialog.component";
import { AssetLoanRendererComponent } from "../asset-loan-renderer/asset-loan-renderer.component";
import { AssetMetadataDto, AssetService } from "../asset.service";

export interface IAssetTableContext {
    currentLoans: Record<number, AssetLoan | undefined>;
}

@Component({
    selector: "app-asset-table",
    templateUrl: "./asset-table.component.html",
    styleUrls: ["./asset-table.component.scss"],
    standalone: false,
})
export class AssetTableComponent {
    @Input() public allowEditing: boolean | null = false;
    public context: IAssetTableContext = {
        currentLoans: {},
    };

    @Output() public assetCount = new EventEmitter<number>();

    @ViewChild(DataGridComponent)
    public set dataGrid(v: DataGridComponent<AssetMetadataDto>) {
        this.dataGrid$.next(v);
    }
    private dataGrid$ = new ReplaySubject<DataGridComponent<AssetMetadataDto>>(1);

    public assets$: Observable<AssetMetadataDto[]>;
    public columns$: Observable<ColDef<AssetMetadataDto>[]>;

    constructor(
        private dialogService: DialogService,
        private assetService: AssetService,
    ) {
        this.assets$ = combineLatest([
            this.assetService.getAllWithMetadata(),
            this.assetService.getCurrentLoans(),
        ]).pipe(
            map(([assets, currentLoans]) => {
                for (const loan of currentLoans) {
                    this.context.currentLoans[loan.assetId] = loan;
                }

                return assets;
            }),
            tap((assets) => this.assetCount.emit(assets.length)),
            cacheLatest(),
        );
        const additionalActions$ = buildGridAttachmentActions({
            source: this.assets$,
            getAttachmentCount: (assetWithMetadata) => assetWithMetadata.attachmentCount,
            setAttachmentCount: (assetWithMetadata, count) =>
                (assetWithMetadata.attachmentCount = count),
            allowEditing: () => !!this.allowEditing,
            openAttachmentDialog: (assetWithMetadata) =>
                this.dialogService.open(AssetAttachmentsDialogComponent, assetWithMetadata.asset, {
                    autoFocus: false,
                }),
        });
        this.columns$ = combineLatest([this.dataGrid$, additionalActions$]).pipe(
            map(([dataGrid, additionalActions]) => this.getColumns(dataGrid, additionalActions)),
        );
    }

    public addAsset = () =>
        this.assetService.create().pipe(
            switchMap((asset) => this.dialogService.open(EditAssetDialogComponent, asset)),
            map((asset): AssetMetadataDto => ({ asset, attachmentCount: 0 })),
        );

    public editAsset = (dto: AssetMetadataDto) =>
        this.dialogService.open(EditAssetDialogComponent, dto.asset).pipe(map(() => dto));

    public deleteAsset = (dto: AssetMetadataDto) => this.assetService.deleteAndSave(dto.asset);

    private getColumns(
        dataGrid: DataGridComponent<AssetMetadataDto>,
        additionalActions: ColumnAction<AssetMetadataDto>[],
    ): ColDef[] {
        return [
            {
                headerName: "Description",
                field: "asset.description",
                pinned: "left",
                sort: "asc",
                width: 200,
                ...wrapTextColDef(),
            },
            {
                headerName: "Quantity",
                field: "asset.quantity",
                type: "numericColumn",
            },
            {
                headerName: "Serial No.",
                field: "asset.serialNo",
            },
            {
                headerName: "Date Acquired",
                field: "asset.dateAcquired",
                ...dateColDef(),
            },
            {
                headerName: "Value Paid",
                field: "asset.valuePaid",
                ...currencyColDef(),
            },
            {
                headerName: "Date Discarded",
                field: "asset.dateDiscarded",
                ...dateColDef(),
            },
            {
                headerName: "Value Discarded",
                field: "asset.valueDiscarded",
                ...currencyColDef(),
            },
            {
                headerName: "Insurance Value",
                field: "asset.insuranceValue",
                ...currencyColDef(),
            },
            {
                headerName: "Location",
                field: "asset.currentLocation",
            },
            {
                headerName: "Loaned To",
                valueGetter: (params) => {
                    if (!params.data) {
                        return "";
                    }

                    const loan = this.context.currentLoans[params.data.asset.id];
                    const member = loan?.member;
                    return member ? `${member.firstName} ${member.lastName}` : "";
                },
                cellRenderer: AssetLoanRendererComponent,
            },
            {
                headerName: "Notes",
                field: "asset.notes",
                width: 400,
                ...wrapTextColDef(),
            },
            buildEditColumn<any>(dataGrid, additionalActions),
        ];
    }
}
