import { TextFieldModule } from "@angular/cdk/text-field";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MatNativeDateModule } from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { AppAuthModule } from "app/auth/auth.module";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppDataGridModule } from "app/common-ux/data-grid/data-grid.module";
import { AppDialogModule } from "app/common-ux/dialog/dialog.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AppSaveCancelButtonsModule } from "app/common-ux/save-cancel-buttons/save-cancel-buttons.module";
import { AppStorageModule } from "app/storage/storage.module";
import { EditAssetDialogComponent } from "app/tenant/feature/asset/edit-asset-dialog/edit-asset-dialog.component";
import { AppSelectMemberModule } from "app/tenant/feature/member/select-member/select-member.module";
import { AllAssetsComponent } from "./all-assets/all-assets.component";
import { AssetAttachmentsDialogComponent } from "./asset-attachments-dialog/asset-attachments-dialog.component";
import { AssetLoanRendererComponent } from "./asset-loan-renderer/asset-loan-renderer.component";
import { AppAssetRoutingModule } from "./asset-routing.module";
import { AssetTableComponent } from "./asset-table/asset-table.component";
import { LoanHistoryComponent } from "./loan-history/loan-history.component";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,

        TextFieldModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,

        AppAuthModule,
        AppButtonModule,
        AppAssetRoutingModule,
        AppDataGridModule,
        AppDialogModule,
        AppPageWrapperModule,
        AppSelectMemberModule,
        AppSaveCancelButtonsModule,
        AppStorageModule,
    ],
    declarations: [
        AssetTableComponent,
        AllAssetsComponent,
        LoanHistoryComponent,
        EditAssetDialogComponent,
        AssetAttachmentsDialogComponent,
        AssetLoanRendererComponent,
    ],
})
export class AppAssetModule {}
