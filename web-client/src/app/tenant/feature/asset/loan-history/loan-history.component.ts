import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { DialogService } from "app/common-ux/dialog/dialog.service";
import { deleteArrayElement } from "app/common/utils";
import { Asset } from "app/dal/model/asset";
import { AssetLoan } from "app/dal/model/asset-loan";
import { Member } from "app/dal/model/member";
import { AssetService } from "app/tenant/feature/asset/asset.service";
import { lastValueFrom } from "rxjs";

@Component({
    selector: "app-loan-history",
    templateUrl: "./loan-history.component.html",
    styleUrls: ["./loan-history.component.scss"],
    standalone: false,
})
export class LoanHistoryComponent implements OnInit {
    public currentLoan?: AssetLoan;
    public selectedMember?: Member;
    public loanHistory: AssetLoan[] = [];
    public asset?: Asset;

    constructor(
        private route: ActivatedRoute,
        private assetService: AssetService,
        private dialogService: DialogService,
    ) {}

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    async ngOnInit() {
        const assetId = this.route.snapshot.params.assetId;
        if (!assetId) {
            return;
        }

        [this.asset, this.currentLoan, this.loanHistory] = await Promise.all([
            lastValueFrom(this.assetService.getById(assetId)),
            this.assetService.getCurrentLoanByAssetId(assetId),
            this.assetService.getLoanHistoryByAssetId(assetId),
        ]);

        // Don't include current loan
        this.loanHistory = this.loanHistory.filter((h) => !!h.dateReturned);
    }

    public async returnAssetOnClick() {
        const loan = await this.assetService.returnLoanByAssetId(this.asset!.id);

        if (loan) {
            this.loanHistory.unshift(loan);
        }

        this.currentLoan = undefined;
    }

    public async loanAssetToMember() {
        const assetLoan = await this.assetService.loanAssetToMember(
            this.asset!.id,
            this.selectedMember!.id,
        );
        this.currentLoan = assetLoan;
        this.selectedMember = undefined;
    }

    public async deleteLoan(loan: AssetLoan) {
        const confirmText =
            "Are you sure you want to delete loan of the " +
            this.asset!.toString() +
            ` to ${loan.member?.fullName ?? ""}?`;

        await lastValueFrom(this.dialogService.openConfirmation(confirmText));

        try {
            await this.assetService.deleteLoan(loan.id);
            deleteArrayElement(this.loanHistory, loan);
            this.dialogService.success("Loan deleted");
        } catch {
            this.dialogService.danger("Failed to delete loan");
        }
    }

    public async updateLoan<T extends keyof AssetLoan>(
        loan: AssetLoan,
        fieldUpdated: T,
        value: AssetLoan[T],
    ) {
        loan[fieldUpdated] = value;

        const updateObject: Partial<AssetLoan> = {};
        updateObject[fieldUpdated] = loan[fieldUpdated];

        try {
            await this.assetService.updateLoan(loan.id, updateObject);
            this.dialogService.success("Loan Updated");
        } catch {
            this.dialogService.danger("Failed to update loan");
        }
    }
}
