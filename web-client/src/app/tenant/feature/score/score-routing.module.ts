import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { AppRoutes } from "app/common/app-routing-types";
import { helpPages } from "app/help/route/help-list";
import { AllScoresComponent } from "./all-scores/all-scores.component";

export const scoreRoutes: AppRoutes = [
    {
        path: "",
        component: AllScoresComponent,
        data: {
            title: "Music Library",
            icon: "library_music",
            restrictedToPermission: FeaturePermission.ScoreRead,
            help: [helpPages.musicLibrary],
        },
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(scoreRoutes),
    ],
    exports: [
        RouterModule,
    ],
})
export class AppScoreRoutingModule {}
