import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AppAuthModule } from "app/auth/auth.module";
import { AppChipSelectionModule } from "app/common-ux/chip-selection/chip-selection.module";
import { AppCommonUxModule } from "app/common-ux/common-ux.module";
import { AppPageWrapperModule } from "app/common-ux/page-wrapper/page-wrapper.module";
import { AllScoresComponent } from "./all-scores/all-scores.component";
import { AppScoreRoutingModule } from "./score-routing.module";
import { AppScoreWidgetModule } from "./score-widget.module";

@NgModule({
    imports: [
        CommonModule,
        AppAuthModule,
        AppCommonUxModule,
        AppScoreRoutingModule,
        AppPageWrapperModule,
        AppScoreWidgetModule,
        AppChipSelectionModule,
    ],
    declarations: [
        AllScoresComponent,
    ],
})
export class AppScoreModule {}
