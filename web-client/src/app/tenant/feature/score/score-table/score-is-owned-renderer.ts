import { ICellRendererComp, ICellRendererParams } from "ag-grid-community";
import { ScoreMetadataDto } from "../score.service";

export class ScoreIsOwnedRenderer implements ICellRendererComp {
    private gui = document.createElement("div");
    private checkbox: HTMLInputElement;

    public constructor() {
        this.checkbox = document.createElement("input");
        this.checkbox.type = "checkbox";
        // Setting readonly doesn't work, just cancel any changes instead
        this.checkbox.onclick = () => false;
    }

    public init(params: ICellRendererParams) {
        const row: ScoreMetadataDto = params.data;

        if (row.score.inLibrary) {
            this.checkbox.checked = !!row.score.isOwned;
            this.gui.append(this.checkbox);
        }
    }

    public refresh(params: ICellRendererParams) {
        this.gui.innerHTML = "";
        this.init(params);
        return true;
    }

    public getGui(): HTMLElement {
        return this.gui;
    }
}
