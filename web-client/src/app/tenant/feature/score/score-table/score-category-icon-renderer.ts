import { GridIconRenderer } from "app/common-ux/data-grid/grid-icon-renderer";
import { ScoreCategory, ScoreCategoryMetadata } from "app/dal/model/score";

export class ScoreCategoryIconRenderer extends GridIconRenderer<ScoreCategory> {
    protected icon(value: ScoreCategory): string {
        return ScoreCategoryMetadata[value].icon;
    }

    protected tooltip(value: ScoreCategory): string {
        return ScoreCategoryMetadata[value].singularText;
    }
}
