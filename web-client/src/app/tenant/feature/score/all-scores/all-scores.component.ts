import { Component } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { Chip } from "app/common-ux/chip-selection/chip-selection.component";
import { addRowRef } from "app/common-ux/data-grid/data-grid-consts";
import { findElementByRef } from "app/common/utils";
import { AllScoreCategoryMetadata, Score, ScoreCategory } from "app/dal/model/score";
import { HelpablePage } from "app/help/help-broker.service";
import { TenantAuthorisationService } from "app/tenant/module/tenant-authorisation.service";
import { ReplaySubject } from "rxjs";
import { allScoresPageId } from "../score-consts";

@Component({
    selector: "app-all-scores",
    templateUrl: "./all-scores.component.html",
    styles: [
        `
            app-chip-selection {
                display: flex;
                justify-content: center;
                padding: 0.5rem 0;
            }

            app-score-table {
                display: block;
                margin: 1rem;
                margin-top: 0;
            }
        `,
    ],
    standalone: false,
})
export class AllScoresComponent implements HelpablePage {
    public readonly pageId = allScoresPageId;
    public hasEditPermissions$ = this.authorisationService.hasPermission(
        FeaturePermission.ScoreWrite,
    );
    public chips: Chip<ScoreCategory>[];
    public selectedCategories: ScoreCategory[];
    public dataHooks = {
        hasNoData$: new ReplaySubject<boolean>(1),
        addDataPermission: FeaturePermission.ScoreWrite,
        addData: () => findElementByRef(addRowRef)?.click(),
    };

    constructor(private authorisationService: TenantAuthorisationService) {
        this.chips = AllScoreCategoryMetadata.map((cm) => ({
            text: cm.label,
            icon: cm.icon,
            tooltip: cm.pluralText,
            value: cm.category,
        }));
        this.selectedCategories = [ScoreCategory.MusicLibrary];
    }

    public updateSelectedIndex(score: Score) {
        const categoryAlreadySelected = !!this.selectedCategories.find((c) => c === score.category);
        if (!categoryAlreadySelected) {
            this.selectedCategories = [...this.selectedCategories, score.category];
        }
    }
}
