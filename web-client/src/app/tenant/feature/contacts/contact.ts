export enum ContactCategory {
    Current = "Current",
    Archived = "Archived",
}

export interface Contact {
    id: number;
    name: string;
    email?: string;
    phoneNo?: string;
    affiliation?: string;
    notes?: string;
    archivedOn?: Date;
}

export function contactCategory(contact: Contact) {
    return contact.archivedOn ? ContactCategory.Archived : ContactCategory.Current;
}

export const ContactCategoryMetadata: Record<
    ContactCategory,
    { category: ContactCategory; icon: string; label: string }
> = {
    [ContactCategory.Current]: {
        category: ContactCategory.Current,
        icon: "contacts",
        label: "Current",
    },
    [ContactCategory.Archived]: {
        category: ContactCategory.Archived,
        icon: "inventory",
        label: "Archived",
    },
};
