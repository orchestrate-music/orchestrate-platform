import { Component, Inject } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { BaseDialog } from "app/common-ux/dialog/base-dialog";
import { modifiedValues } from "app/common-ux/form/form-utilities";
import { shallowMergeObjects } from "app/common/utils";
import { map, tap } from "rxjs";
import { Contact } from "../contact";
import { ContactService, CreateContactDto, UpdateContactDto } from "../contact.service";

@Component({
    selector: "app-edit-contact",
    templateUrl: "./edit-contact-dialog.component.html",
    styleUrls: ["./edit-contact-dialog.component.scss"],
    standalone: false,
})
export class EditContactDialogComponent extends BaseDialog<Partial<Contact> | undefined, Contact> {
    public dialogName = "EditContact";

    public contactForm: ReturnType<typeof this.buildForm>;
    public contact: Partial<Contact>;
    public archivedOn?: Date;

    public constructor(
        dialogRef: MatDialogRef<EditContactDialogComponent>,
        @Inject(MAT_DIALOG_DATA) contact: Contact | undefined,
        private contactService: ContactService,
    ) {
        super(dialogRef);
        this.contact = contact ?? {};
        this.archivedOn = contact?.archivedOn;
        this.contactForm = this.buildForm(this.contact);
    }

    public buildForm(contact: Partial<Contact>) {
        return new FormGroup({
            name: new FormControl(contact.name, {
                nonNullable: true,
                validators: [Validators.required],
            }),
            email: new FormControl(contact.email, {
                validators: [Validators.email],
            }),
            affiliation: new FormControl(contact.affiliation),
            phoneNo: new FormControl(contact.phoneNo),
            notes: new FormControl(contact.notes),
        });
    }

    public saveContact = () => {
        if (typeof this.contact.id === "number") {
            const modifications = modifiedValues(this.contactForm);
            return this.contactService
                .update(this.contact.id, modifications as UpdateContactDto)
                .pipe(
                    map(
                        (updatedContact) =>
                            shallowMergeObjects(this.contact, updatedContact) as Contact,
                    ),
                    tap((contact) => this.resolve(contact)),
                );
        } else {
            return this.contactService
                .create(this.contactForm.value as CreateContactDto)
                .pipe(tap((contact) => this.resolve(contact)));
        }
    };
}
