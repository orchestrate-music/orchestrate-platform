import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatStepperModule } from "@angular/material/stepper";
import { RouterModule, Routes } from "@angular/router";
import { AppButtonModule } from "app/common-ux/button/button.module";
import { AppFormModule } from "app/common-ux/form/form.module";
import { RegisterPageComponent } from "./register-page/register-page.component";
import { HcaptchaComponent } from "./hcaptcha/hcaptcha.component";

const routes: Routes = [
    {
        path: "",
        component: RegisterPageComponent,
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),

        MatStepperModule,

        AppFormModule,
        AppButtonModule,
    ],
    declarations: [
        RegisterPageComponent,
        HcaptchaComponent,
    ],
})
export class AppRegisterModule {}
