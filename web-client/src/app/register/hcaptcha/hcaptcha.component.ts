import { AfterViewInit, Component, ElementRef, EventEmitter, NgZone, Output } from "@angular/core";
import { AppConfig } from "app/common/app-config";
import { firstValueFrom } from "rxjs";

@Component({
    selector: "app-hcaptcha",
    template: "",
    standalone: false,
})
export class HcaptchaComponent implements AfterViewInit {
    @Output() public responseToken = new EventEmitter<string | undefined>();

    private static scriptLoaded = false;

    public constructor(
        private elementRef: ElementRef<HTMLElement>,
        private appConfig: AppConfig,
        private ngZone: NgZone,
    ) {}

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    public async ngAfterViewInit() {
        const sitekey = await firstValueFrom(this.appConfig.hcaptchaSiteKey$);
        if (!sitekey) {
            return;
        }

        await this.loadScript();
        hcaptcha.render(this.elementRef.nativeElement, {
            sitekey,
            callback: (response) => {
                this.ngZone.run(() => this.responseToken.emit(response));
            },
            "expired-callback": () => {
                this.ngZone.run(() => this.responseToken.emit());
            },
        });
    }

    private loadScript() {
        if (HcaptchaComponent.scriptLoaded) {
            return Promise.resolve();
        }

        return new Promise<void>((resolve, reject) => {
            const script = document.createElement("script");
            script.src = "https://js.hcaptcha.com/1/api.js?render=explicit";
            script.onload = () => {
                HcaptchaComponent.scriptLoaded = true;
                resolve();
            };
            script.onerror = reject;
            document.head.append(script);
        });
    }
}
