import { Route } from "@angular/router";
import { FeaturePermission } from "app/auth/feature-permission";
import { HelpPage } from "app/help/route/help-page";

export type AppRoutes = AppRoute[];

export interface AppRoute extends Route {
    data?: AppRouteData;
}

export interface AppRouteData {
    title?: string;
    icon?: string;
    /**
     * If provided, access to the route is only granted if the user has
     * at least one of the permissions.
     */
    restrictedToPermission?: FeaturePermission | FeaturePermission[];
    isShellRoute?: boolean;
    help?: HelpPage[];
}
