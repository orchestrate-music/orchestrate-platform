import { Injectable } from "@angular/core";
import { FeaturePermission } from "app/auth/feature-permission";
import { BehaviorSubject, map, Observable, Subscription } from "rxjs";

export interface HelpablePage {
    readonly pageId: string;
    readonly dataHooks?: HelpDataHooks;
}

export interface HelpDataHooks {
    hasNoData$: Observable<boolean>;
    addDataPermission: FeaturePermission;
    addData(): void | Subscription | unknown[];
}

function isHelpablePage(p: unknown): p is HelpablePage {
    return typeof p === "object" && p !== null && typeof (p as any).pageId === "string";
}

@Injectable()
export class HelpBrokerService {
    private _currentHelpEnabledPage$ = new BehaviorSubject<HelpablePage[]>([]);

    public helpablePageDataHooks(pageId: string) {
        return this._currentHelpEnabledPage$.pipe(
            map((pages) => pages.find((p) => p.pageId === pageId)),
            map((p) => p?.dataHooks),
        );
    }

    public onRouteActivate(componentInstance: unknown) {
        if (isHelpablePage(componentInstance)) {
            this.addHelpablePage(componentInstance);
        }
    }

    public addHelpablePage(helpablePage: HelpablePage) {
        const pages = [...this._currentHelpEnabledPage$.value, helpablePage];
        this._currentHelpEnabledPage$.next(pages);
    }

    public onRouteDeactivate(componentInstance: unknown) {
        if (isHelpablePage(componentInstance)) {
            this.removeHelpablePage(componentInstance);
        }
    }

    public removeHelpablePage(helpablePage: HelpablePage) {
        const pages = this._currentHelpEnabledPage$.value.filter((p) => p !== helpablePage);
        this._currentHelpEnabledPage$.next(pages);
    }
}
